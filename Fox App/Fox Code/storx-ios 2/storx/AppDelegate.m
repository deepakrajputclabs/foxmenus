//
//  AppDelegate.m
//  Storx
//
//  Created by clicklabs on 12/2/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "HomeViewController.h"
#import "ScheduledLocationManager.h"
//#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"])
    {
        
    }
    else if ([identifier isEqualToString:@"answerAction"]){
        
    }
}

#endif

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GMSServices provideAPIKey:SMGoogle_APIKey];
    //edited
    //[Crashlytics startWithAPIKey:@"9a344ba4644c7661f8ee12e1e5573f21396bbaf0"];
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:isCurrentLocationNow];
     [[ScheduledLocationManager getSharedInstance] allocatLocation];

    if ([[[UIDevice currentDevice] systemVersion]floatValue] >= 8.0) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge |UIUserNotificationTypeSound  |UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    return YES;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString * tokenAsString = [[[deviceToken description]
                                 stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                                stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"tokenAsString: %@",tokenAsString);
    [[NSUserDefaults standardUserDefaults] setObject:tokenAsString forKey:DeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"error=>%@",error.description);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo //fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
   // [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSLog(@"%@",userInfo);
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"flag"]]  forKey:@"Flag"];
    NSString *pushNotificationType = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"flag"]];
    
    [[[UIAlertView alloc] initWithTitle:@"" message:[userInfo valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    if([pushNotificationType isEqualToString:@"15"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"listOrderRefreshNotification" object:nil userInfo:userInfo];
    }
    else if([pushNotificationType isEqualToString:@"40"]) { // Rate driver
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"type"]] forKey:@"Type"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"order_id"]] forKey:@"OrderId"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderTrackNotification" object:nil userInfo:userInfo];
    }
    else if([pushNotificationType isEqualToString:@"41"]) { // Your Order id here
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"order_id"]] forKey:@"OrderId"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderTrackNotification" object:nil userInfo:userInfo];
    }
    else if([pushNotificationType isEqualToString:@"19"]) { // Your Order id here
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[userInfo valueForKey:@"order_id"]] forKey:@"OrderId"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderTrackNotification" object:nil userInfo:userInfo];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    //[self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "clicklabs.Storx" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Storx" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Storx.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


@end
