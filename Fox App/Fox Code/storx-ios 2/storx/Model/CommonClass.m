//
//  CommonClass.m
// Storx
//
//  Created by clicklabs on 11/3/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "CommonClass.h"

static CommonClass *sharedInstance = nil;

@implementation CommonClass

+(CommonClass*)getSharedInstance
{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
    }
    return sharedInstance;
}

#pragma mark - Fade In/Out

- (void)startFade :(UIView *)view {
    view.hidden = NO;
    [view setAlpha:0.f];
    
    [UIView animateWithDuration:0.5f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        [view setAlpha:1.f];
    } completion:^(BOOL finished) {
    }];
}

- (void)EndFade :(UIView *)view {
    
    [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        //[backGroundprofile setAlpha:1.f];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [view setAlpha:0.f];
        } completion:nil];
    }];
}

#pragma mark - Activity indicator function

-(void)ShowActivityIndicatorWithTitle:(NSString *)Title {
    
    [SVProgressHUD showWithStatus:Title maskType:SVProgressHUDMaskTypeGradient];

}

-(void)HideActivityIndicator {
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
    });
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - Save image/video

- (BOOL)saveImage: (UIImage *)aImage ImageName:(NSString *)imageName FolderName:(NSString *)foldername {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:foldername];
    NSError   *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    NSString *PathToFile = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImageJPEGRepresentation(aImage, 0.5) writeToFile:PathToFile atomically:YES];
    return YES;
}

#pragma marks - Save Viedo

- (BOOL)saveViedo: (NSURL *)urlViedo ImageName:(NSString *)ViedoName FolderName:(NSString *)foldername {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:foldername];
    NSError   *error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    NSString *PathToFile = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",ViedoName]];
    NSData *videoData    = [NSData dataWithContentsOfURL:urlViedo];
    [videoData writeToFile:PathToFile atomically:NO];
    return YES;
}

#pragma mark - Fetch image

- (NSString *)getImageFRom:(NSString *)foldername imageName:(NSString *)imageName {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:foldername];
    NSString *PathToFile = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    return PathToFile;
}

#pragma mark - Remove Image

- (BOOL)removeImageFRom:(NSString *)foldername {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:foldername];
    NSError *error;
    BOOL success =[[NSFileManager defaultManager] removeItemAtPath:dataPath error:&error];
    return success;
}

#pragma mark - Get Date

-(NSString *)getdate :(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *theTime = [dateFormatter stringFromDate:date];
    return theTime;
}

-(NSString *)getdateLocal :(NSString *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat:@"dd MMM, yyyy hh:mm a"]; //@"YYYY-MM-dd HH:mm:ss"
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *time = [dateFormatter stringFromDate:dateFromString];
    
    return time;
}

-(NSMutableArray *)getdateAsArrayFromDate :(NSArray *)arry {
    NSMutableArray *time = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < arry.count; i++)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"HH:mm:s"]; //@"yyyy-MM-dd HH:mm:s"
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        NSDate *theTimeday = [dateFormatter dateFromString:[arry objectAtIndex:i]];
        
        [dateFormatter setDateFormat:@"hh:mm a"]; //@"MM/dd/yyyy hh:mm a"
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        if([dateFormatter stringFromDate:theTimeday] != nil)
            [time addObject:[dateFormatter stringFromDate:theTimeday]];
        else
            [time addObject:@"0"];

    }
    return time;
}

#pragma mark : Render ImageView

- (UIImage *)imageOfView:(UIView *)view {
    // This if-else clause used to check whether the device support retina display or not so that
    // we can render image for both retina and non retina devices.
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    }
    else
    {
        UIGraphicsBeginImageContext(view.bounds.size);
    }
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

#pragma mark - Blur

- (UIImage*)blur:(UIImage*)theImage {
    // ***********If you need re-orienting (e.g. trying to blur a photo taken from the device camera front facing camera in portrait mode)
    // theImage = [self reOrientIfNeeded:theImage];
    
    // create our blurred image
    
    @autoreleasepool {
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
        
        // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
        
        CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
        [filter setValue:inputImage forKey:kCIInputImageKey];
        [filter setValue:[NSNumber numberWithFloat:5.0f] forKey:@"inputRadius"];
        CIImage *result = [filter valueForKey:kCIOutputImageKey];
        
        // CIGaussianBlur has a tendency to shrink the image a little,
        // this ensures it matches up exactly to the bounds of our original image
        CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
        
        UIImage *returnImage = [UIImage imageWithCGImage:cgImage];//create a UIImage for this function to "return" so that ARC can manage the memory of the blur... ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
        
        CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
        
        inputImage = nil;
        context = nil;
        filter = nil;
        cgImage = nil;
        
        return returnImage;
    }
}

#pragma mark - Alert message

- (void)alertMsg : (NSString *)stringMsg title:(NSString *)title {
    alert = nil;
    alert = [[UIAlertView alloc] initWithTitle:title message:stringMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    alert = nil;
}

#pragma mark - Rotate image

-(UIImage *)imagerotate:(UIImage *)aImage {
    int kMaxResolution = 640; // Or whatever
    CGImageRef imgRef = aImage.CGImage;
    CGFloat widthR = CGImageGetWidth(imgRef);
    CGFloat heightR = CGImageGetHeight(imgRef);
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, widthR, heightR);
    if (widthR > kMaxResolution || heightR > kMaxResolution) {
        CGFloat ratio = widthR/heightR;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    CGFloat scaleRatio = bounds.size.width / widthR;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = aImage.imageOrientation;
    switch(orient) {
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
    }
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -heightR, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -heightR);
    }
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, widthR, heightR), imgRef);
    aImage = UIGraphicsGetImageFromCurrentImageContext();
    
    return aImage;
}

#pragma mark - Get Label Size

-(CGSize)getLabelSize:(NSString *)labelText width:(float)width height:(float)height fontSize:(NSInteger)fontSize
{
    CGSize constraintSize;
    constraintSize.height = height;
    constraintSize.width = width;
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:FontSemiBold size:fontSize], NSFontAttributeName,
                                          nil];
    CGRect frame = [labelText boundingRectWithSize:constraintSize
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:attributesDictionary
                                            context:nil];
    return frame.size;
}

- (CLLocationCoordinate2D)currentDealLocation
{
    CLLocationCoordinate2D coordinateCurrent;
    if([CLLocationManager locationServicesEnabled])
    {
        CLLocationManager *locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [locationManager startUpdatingLocation];
        NSLog(@"locationManager: %f",locationManager.location.coordinate.latitude);
        NSLog(@"locationManager: %f",locationManager.location.coordinate.longitude);
        
        coordinateCurrent.latitude = locationManager.location.coordinate.latitude;
        coordinateCurrent.longitude = locationManager.location.coordinate.longitude;
    }
    else
    {
//        [[CommonClass getSharedInstance] alertMsg:@"To enable, please go to Settings and turn on Location Service for this app." title:@"Location Service Disable"];
    }
    return coordinateCurrent;
}

//currentSupSize = currentSupviewDeviceSize
- (CGFloat)targetSize:(CGFloat)targetSuperviewSize
        targetSubSize:(CGFloat)targetSubviewSize
    currentSupSize:(CGFloat)currentSizeOfSuperview {
    
    return currentSizeOfSuperview/(targetSuperviewSize/targetSubviewSize);
    
}

- (NSString *)encodeString:(NSString *)string {
    
    NSString *encodedString;
    NSData *data1 = [string dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *goodValue = [[NSString alloc] initWithData:data1 encoding:NSUTF8StringEncoding];
    encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( NULL,(CFStringRef)goodValue, NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8 ));
    return encodedString;
}

#pragma mark - UTC to Local Date Only Conversion

- (NSMutableArray *)UTCDateToLocalDateArray:(NSArray *)utcDateTimeArray
{
    NSTimeZone *dSystemTimeZone = [NSTimeZone systemTimeZone];
    NSDate *now = [[NSDate alloc] init];
    
    NSInteger destinationGMTOffset = [dSystemTimeZone secondsFromGMTForDate: now];
    NSMutableArray *objArray = [[NSMutableArray alloc]init];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];

    for (int i = 0; i < [utcDateTimeArray count]; i++) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];  // specify the date-time format that is coming from serve
        NSDate * dateFromUtc = [dateFormatter dateFromString:[utcDateTimeArray objectAtIndex:i]];
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:GregorianCalendar];

        [offsetComponents setSecond:destinationGMTOffset];
        NSDate *localDate = [gregorian dateByAddingComponents: offsetComponents
                                                       toDate: dateFromUtc options:0];
        
        [dateFormatter setDateFormat:@"MMM dd, yyyy | hh:mm a"];  //Specify the format that is required.
        NSString *time = [dateFormatter stringFromDate:localDate];
        [objArray addObject:time];
        dateFormatter = nil;
    }
    return objArray;
}


- (NSString *)convertUtcDateToLocalDateOnly:(NSString *)utcDateTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];  // specify the date-time format that is coming from server
    
    NSTimeZone *dSystemTimeZone = [NSTimeZone systemTimeZone];
    NSDate *now = [[NSDate alloc] init];
    
    NSInteger destinationGMTOffset = [dSystemTimeZone secondsFromGMTForDate: now];
    
    NSDate * dateFromUtc = [dateFormatter dateFromString: utcDateTime];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:GregorianCalendar];
    
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setSecond:destinationGMTOffset];
    
    NSDate *localDate = [gregorian dateByAddingComponents: offsetComponents
                                                   toDate: dateFromUtc options:0];
    
    [dateFormatter setDateFormat:@"MMM dd, yyyy | hh:mm a"];  //Specify the format that is required.
    NSString *time = [dateFormatter stringFromDate:localDate];
    return time;
}

#pragma mark - Time Convert

- (NSArray *)convertTimeArray: (NSArray *)utcDateTimeArray {

    NSTimeZone *dSystemTimeZone = [NSTimeZone systemTimeZone];
    NSDate *now = [[NSDate alloc] init];
    
    NSInteger destinationGMTOffset = [dSystemTimeZone secondsFromGMTForDate: now];
    NSMutableArray *objArray = [[NSMutableArray alloc]init];

    for (int i = 0; i < [utcDateTimeArray count]; i++) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"HH:mm:s"];  // specify the date-time format that is coming from server
        NSDate * dateFromUtc = [dateFormatter dateFromString: [utcDateTimeArray objectAtIndex:i]];
        NSCalendar *gregorian = [[NSCalendar alloc]
                                 initWithCalendarIdentifier:GregorianCalendar];
        
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        [offsetComponents setSecond:destinationGMTOffset];
        
        NSDate *localDate = [gregorian dateByAddingComponents: offsetComponents
                                                       toDate: dateFromUtc options:0];
        
        [dateFormatter setDateFormat:@"hh:mm a"];  //hh:mm aSpecify the format that is required.
        NSString *time = [dateFormatter stringFromDate:localDate];
        [objArray addObject:time];
        dateFormatter = nil;
    }
    return objArray;
}

@end
