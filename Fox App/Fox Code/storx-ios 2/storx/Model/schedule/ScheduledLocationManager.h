//
//  ScheduledLocationManager.h
//  Jugnoo
//
//  Created by Samar Singla on 01/07/14.
//  Copyright (c) 2014 Click Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol ScheduledLocationManagerDelegate <NSObject>

-(void)scheduledLocationManageDidFailWithError:(NSError*)error;
-(void)scheduledLocationManageDidUpdateLocations:(CLLocation*)currentLocations;


@end
@interface ScheduledLocationManager : NSObject<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}
@property(retain,strong) id<ScheduledLocationManagerDelegate> delegate;

- (void)allocatLocation;
-(void)getUserLocationWithInterval:(int)interval;
-(BOOL)isLocationServiceAvailable;
-(void)removeTimerForLocationUpdates;
+ (ScheduledLocationManager *)getSharedInstance;
@end