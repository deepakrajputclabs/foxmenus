//
//  CommonClass.h
// Storx
//
//  Created by clicklabs on 11/3/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Header.h"
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

//#import <AudioUnit/AudioUnit.h>
#import <AVFoundation/AVAudioPlayer.h>

@interface CommonClass : NSObject <CLLocationManagerDelegate>
{
    BOOL isReturn;
    UIImage *imageRotated;
    //AVAudioPlayer *objAudioPlayer;
    UIImageView *backGround;
    UIAlertView *alert;
    UIImageView *bGPopupImageView;
}

+(CommonClass*)getSharedInstance;

- (NSArray *)convertTimeArray: (NSArray *)utcDateTimeArray;
- (NSMutableArray *)UTCDateToLocalDateArray:(NSArray *)utcDateTimeArray;
- (NSString *)convertUtcDateToLocalDateOnly:(NSString *)utcDateTime;

- (NSString *)encodeString:(NSString *)string;
- (void)startFade :(UIView *)view;
- (void)EndFade :(UIView *)view;

- (UIImage *)imageFromColor:(UIColor *)color;

- (BOOL)saveImage: (UIImage *)aImage ImageName:(NSString *)imageName FolderName:(NSString *)foldername;
- (BOOL)saveViedo: (NSURL *)urlViedo ImageName:(NSString *)ViedoName FolderName:(NSString *)foldername;

- (NSString *)getImageFRom:(NSString *)foldername imageName:(NSString *)imageName;
- (BOOL)removeImageFRom:(NSString *)foldername;

- (NSString *)getdate :(NSDate *)date;
- (NSMutableArray *)getdateAsArrayFromDate :(NSArray *)string;
- (NSString *)getdateLocal :(NSString *)date;
- (UIImage *)imagerotate :(UIImage *)aImage;

- (void)ShowActivityIndicatorWithTitle:(NSString *)Title;
- (void)HideActivityIndicator;
- (UIImage*) blur:(UIImage*)theImage;
- (void)alertMsg : (NSString *)stringMsg title:(NSString *)title;
- (UIImage *)imageOfView:(UIView *)view;

#pragma mark - GetLabelSize

- (CGSize)getLabelSize:(NSString *)labelText width:(float)width height:(float)height fontSize:(NSInteger)fontSize;
- (CLLocationCoordinate2D)currentDealLocation;
- (CGFloat)targetSize:(CGFloat)targetSuperviewSize
        targetSubSize:(CGFloat)targetSubviewSize
    currentSupSize:(CGFloat)currentSizeOfSuperview;
@end