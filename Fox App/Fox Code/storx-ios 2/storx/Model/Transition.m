//
//  Transition.m
//  bistro-ios-consumer
//
//  Created by Rakesh's Mac on 27/08/13.
//  Copyright (c) 2013 Rakesh's Mac. All rights reserved.
//

#import "Transition.h"

@implementation Transition
/*------------------------- Move from One View to another View animation --------------------------------------*/

+ (Transition *)getSharedInstance
{
    static Transition *getSharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getSharedInstance = [[Transition alloc] init];
    });
    return getSharedInstance;
}

-(void)imageView:(UIView *)uiv_slide withDuration:(double)d_duration toX:(CGFloat)xValue andY:(CGFloat)yValue
{
    //Make an animation to slide the view off the screen
    [UIView animateWithDuration:d_duration
                     animations:^{
         [uiv_slide setFrame:CGRectMake(xValue, yValue, uiv_slide.frame.size.width, uiv_slide.frame.size.height)];
     } completion:^(BOOL finished){
     
     }];
}

-(void)popupAlertViewAnimationStart:(UIView *)view sizeOfScreen:(CGRect)screenBounds
{
    view.center=CGPointMake(screenBounds.size.width/2,screenBounds.size.height/2);
    view.transform = CGAffineTransformScale(view.transform, 0.1,0.1);
    view.alpha=0.0;
    [UIView animateWithDuration:0.4
                     animations:^{
                         view.center=CGPointMake(screenBounds.size.width/2,screenBounds.size.height/2);
                         view.transform = CGAffineTransformMakeRotation(0);
                         view.transform = CGAffineTransformScale(view.transform, 1,1);
                         view.alpha=1.0;
                     }];
}

@end
