//
//  Transition.h
//  bistro-ios-consumer
//
//  Created by Rakesh's Mac on 27/08/13.
//  Copyright (c) 2013 Rakesh's Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
    
@interface Transition : NSObject

-(void) imageView:(UIView *)uiv_slide withDuration:(double)d_duration toX:(CGFloat)xValue andY:(CGFloat)yValue;
-(void)popupAlertViewAnimationStart:(UIView *)view sizeOfScreen:(CGRect)screenBounds;
+ (Transition *)getSharedInstance;
@end
