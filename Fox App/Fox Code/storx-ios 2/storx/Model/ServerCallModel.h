//
//  ServerCallModel.h
// Storx
//
//  Created by clicklabs on 11/3/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Header.h"
#import "ASIFormDataRequest.h"
#import "UIImageView+AFNetworking.h"

@protocol ServerReturnProtocol <NSObject>
@optional
- (void)doneButtonDelegate;
- (void)invalidAccessToken;
- (void)dataReturn:(NSDictionary *)json;
@end

@interface ServerCallModel : NSObject <NSURLSessionDelegate>
{
    NSDictionary *jsonResponse;
}

@property (nonatomic, assign) id <ServerReturnProtocol> serverReturnDelegate;

+ (ServerCallModel*) getSharedInstance;


- (NSDictionary *)serverGetJSON:(NSString *)url
                           data:(NSData *)data
                           Type:(NSString *)type
               loadingindicator:(BOOL)isLoadingYes;
- (void)updateLocationDriver :(NSString *)url :(NSDictionary *)params;
- (void)signUpCall :(NSString *)url
                name:(NSString *)name
           patientId:(NSString *)patientId
       physicianName:(NSString *)physicianName
             contact:(NSString *)contact
        emailAddress:(NSString *)emailAddress
            password:(NSString *)password
     confirmPassword:(NSString *)confirmPassword
              image :(NSString *)imagePath
              deals:(NSString *)deals;

- (void)getDispensary :(NSDictionary *)params;
@end
