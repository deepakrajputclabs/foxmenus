//
//  ServerCallModel.m
// Storx
//
//  Created by clicklabs on 11/3/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ServerCallModel.h"


@implementation ServerCallModel
@synthesize serverReturnDelegate = _serverReturnDelegate;

+(ServerCallModel*)getSharedInstance
{
    static dispatch_once_t p = 0;
    __strong static id getSharedInstance = nil;
    dispatch_once(&p, ^{
        getSharedInstance = [[self alloc] init];
    });
    return getSharedInstance;
}

- (NSDictionary *)serverGetJSON:(NSString *)url data:(NSData *)data Type:(NSString *)type loadingindicator:(BOOL)isLoadingYes
{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus == NotReachable) {
        [[CommonClass getSharedInstance] alertMsg:ErrorInternetConnection title:@"Error"];
        return jsonResponse;
    }
    else {
        
        if(isLoadingYes)
            [self showActivityIndicatorWithTitle];
        
        NSURL *submiturl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]];
        NSLog(@"%@%@",ServerUrl,url);
        NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:submiturl];
        [submitrequest setHTTPMethod:type];
        [submitrequest setHTTPBody:data];
        NSURLResponse *response = NULL;
        NSError *requestError = NULL;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:submitrequest returningResponse:&response error:&requestError];
        
        if (responseData) {
            jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&requestError];
            [self hideActivityIndicator];
            return jsonResponse;
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"Error!" message:ErrorServerConnection delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            jsonResponse = nil;
        }
    }
    
    [self hideActivityIndicator];
    return jsonResponse;
}

#pragma mark : Get Order List - Home Screen

- (void)updateLocationDriver :(NSString *)url :(NSDictionary *)params {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@%@",ServerUrl,@"update_driver_location"] parameters:params success:^(AFHTTPRequestOperation *operation, id json) {
        NSLog(@"json: %@",json);

        if(json)
        {
            if([NSString stringWithFormat:@"%@",[json valueForKey:@"status"]] == 0) {
                
            }
            else if([[NSString stringWithFormat:@"%@",[json valueForKey:@"status"]] intValue] == 1){
                
            }
            else if([[NSString stringWithFormat:@"%@",[json valueForKey:@"status"]] intValue] == 2){
                
            }
        }
        [self hideActivityIndicator];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSLog(@"json: %@",error.description);
        [self hideActivityIndicator];
    }];
}

- (void)getDispensary :(NSDictionary *)params {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@get_business",ServerUrl] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    if(responseObject)
    {
            if([[responseObject objectForKey:@"status"] intValue] == 0) {
                [_serverReturnDelegate dataReturn:responseObject];
            }
            else if([[responseObject objectForKey:@"status"] intValue] == 1){
                [[[UIAlertView alloc] initWithTitle:@"" message:[responseObject valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else if([[responseObject objectForKey:@"status"] intValue] == 2){
                [_serverReturnDelegate invalidAccessToken];
            }
        }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error.description);
               [[[UIAlertView alloc]initWithTitle:@"" message:ErrorServerConnection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
          }];
}

#pragma mark- loading indicator bar
#pragma mark-

- (void)showActivityIndicatorWithTitle
{
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.001]];
}

- (void)hideActivityIndicator
{
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [SVProgressHUD dismiss];
    });
}

#pragma mark Server Call 

- (void)signUpCall:(NSString *)url
              name:(NSString *)name
         patientId:(NSString *)patientId
     physicianName:(NSString *)physicianName
           contact:(NSString *)contact
      emailAddress:(NSString *)emailAddress
          password:(NSString *)password
   confirmPassword:(NSString *)confirmPassword
            image :(NSString *)imagePath
            deals :(NSString *)deals{
    
    NSLog(@"imagePath: %@",imagePath);
    NSLog(@"%@",name);
    NSLog(@"%@",emailAddress);
    NSLog(@"%@",contact);
    NSLog(@"%@",patientId);
    NSLog(@"%@",physicianName);
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@",ServerUrl,url]);

    ASIFormDataRequest *requestObj = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,url]]];
    [requestObj setDelegate:self];
    //requestObj.shouldAttemptPersistentConnection   = NO;
    [requestObj setRequestMethod:@"POST"];
    [requestObj setPostValue:name                                        forKey:@"user_name"];
    [requestObj setPostValue:contact                                     forKey:@"phone_no"];
    [requestObj setPostValue:emailAddress                                forKey:@"email"];
    [requestObj setPostValue:password                                    forKey:@"password"];
    [requestObj setPostValue:@"1"                                        forKey:@"device_type"];
    [requestObj setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:DeviceToken]                                     forKey:@"device_token"];
    [requestObj setPostValue:AppVersion                                  forKey:@"app_version"];
    [requestObj setPostValue:patientId                                   forKey:@"patient_id"];
    [requestObj setFile:imagePath                                        forKey:@"user_image"];
    [requestObj setPostValue:physicianName                               forKey:@"physician_name"];
    [requestObj setPostValue:deals                                       forKey:@"deals"];
    [requestObj startSynchronous];
}

#pragma mark : ASSIHTTPrequest

- (void)requestWentWrong:(ASIHTTPRequest *)request {
    [[CommonClass getSharedInstance] HideActivityIndicator];
}

-(void)requestStarted:(ASIHTTPRequest *)request{
    NSLog(@"ASIHTTP start request");
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    NSLog(@"ASI failed %@",request.error.localizedDescription);
    
    [[CommonClass getSharedInstance] HideActivityIndicator];
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    NSString *jsonString = [request responseString];
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    NSLog(@"Sign Up: %@",json);
    
    if(json) {
        
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            [[NSUserDefaults standardUserDefaults] setValue:[json objectForKey:UserData] forKey:UserData];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"access_token"] forKey:AccessToken];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"current_user_status"] forKey:DriverMode];
            
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"email"] forKey:UserEmail];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"patient_id"] forKey:UserPatientId];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"phone_no"] forKey:UserPhone];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"physician_name"] forKey:UserPhysicianName];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"user_image"] forKey:UserImage];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"user_name"] forKey:UserName];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self hideActivityIndicaterr];
            [_serverReturnDelegate doneButtonDelegate];
        }
        
        else if ([[json objectForKey:@"status"] intValue] == 1) {
            [self hideActivityIndicaterr];
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Email is already registered." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        
        else if ([[json objectForKey:@"status"] intValue] == 2) {
        }
    }
}

- (void)hideActivityIndicaterr
{
    [SVProgressHUD dismiss];
}



@end
