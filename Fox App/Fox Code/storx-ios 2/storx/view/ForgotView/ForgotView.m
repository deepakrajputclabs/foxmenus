//
//  ForgotView.m
// Storx
//
//  Created by clicklabs on 11/12/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ForgotView.h"

@implementation ForgotView
{
    UITextField *usernameTextfield;
    CGRect screenBounds;
    UIImageView *bkImageView;
    UITextField *registerTextfield;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
        [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
        [[NSNotificationCenter defaultCenter] removeObserver:@"ProfilePicChange"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        bkImageView.image = [UIImage imageNamed:@"bg.png"];
        bkImageView.userInteractionEnabled = YES;
        [self addSubview:bkImageView];
        
        UIButton *backButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
        [backButton addTarget:self action:@selector(backToLoginView) forControlEvents:UIControlEventTouchUpInside];
        [bkImageView addSubview:backButton];
        
        UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
        storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
        [bkImageView addSubview:storxImageView];

        int textFieldY = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:378/3 currentSupSize:SCREEN_HEIGHT];
        int imageViewY  = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:410/3 currentSupSize:SCREEN_HEIGHT];
        UIImageView *imageView;
        
        imageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:110/3 currentSupSize:SCREEN_WIDTH], imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:55/3 currentSupSize:SCREEN_HEIGHT])];
        imageView.image = [UIImage imageNamed:@"email_icon.png"];
        [bkImageView addSubview:imageView];
        
        registerTextfield = [[UITextField alloc] initWithFrame: CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:270/3 currentSupSize:SCREEN_WIDTH], textFieldY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:870/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        registerTextfield.tag =  10;
        registerTextfield.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
        registerTextfield.placeholder = @"Email";
        registerTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        registerTextfield.backgroundColor = [UIColor clearColor];
        registerTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
        registerTextfield.keyboardType = UIKeyboardTypeDefault;
        registerTextfield.returnKeyType = UIReturnKeyDone;
        registerTextfield.clearButtonMode = UITextFieldViewModeWhileEditing;
        registerTextfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        registerTextfield.delegate = self;
        registerTextfield.autocapitalizationType = UITextAutocapitalizationTypeWords;
        registerTextfield.textColor = [UIColor whiteColor];
        [bkImageView addSubview:registerTextfield];
        
        UIView *lineView =    [[UIView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:72/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:498/3+12/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1093/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
        lineView.backgroundColor = ColorGrayBk;
        [self addSubview:lineView];

        UIButton *forgotButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 900/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:630/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:900/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
        [bkImageView addSubview:forgotButton];
        [forgotButton setBackgroundColor:ColorGreenButton];
        forgotButton.layer.masksToBounds =YES;
        forgotButton.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:75/3 currentSupSize:SCREEN_HEIGHT];
        [forgotButton setTitle:@"Request Password Reset" forState:UIControlStateNormal];
        forgotButton.titleLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
        [forgotButton addTarget:self action:@selector(requestButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *instructionLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 1050/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:840/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:1050/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:600/3 currentSupSize:SCREEN_HEIGHT])];
        instructionLabel.text = @"Please provide your registered Email id. We will send you instructions to reset your password";
        instructionLabel.textColor = ColorFFFFFF;
        instructionLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
        instructionLabel.numberOfLines = 3;
        instructionLabel.textAlignment = NSTextAlignmentCenter;
        [bkImageView addSubview:instructionLabel];
        instructionLabel.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)requestButtonAction {
   
    if(![usernameTextfield.text isEqualToString:@""])
    {
        NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:[NSString stringWithFormat:@"forgotPasswordFromEmail"]
                                                                           data:[[NSString stringWithFormat:@"email=%@",                                                                                  [[CommonClass getSharedInstance] encodeString:registerTextfield.text]] dataUsingEncoding:NSUTF8StringEncoding]
                                                                           Type:@"POST"
                                                               loadingindicator:YES];
        NSLog(@"login details: %@",json);
        if([[json objectForKey:@"status"]integerValue]==0)
        {
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@""        message:[json objectForKey:@"log"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert1 show];
            [self backToLoginView];
        }
        else {
            
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Error!"        message:[json objectForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert1 show];
            
        }
    }
    else{
        
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Error!"        message:@"Please fill mandatory fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert1 show];
    }
}

- (void)backToLoginView {

    [self.forgotScreenDelegate backButtonAction];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if((self.frame.origin.y==-135)||(self.frame.origin.y==-100))
    {
        [UIView animateWithDuration:.22 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^
         {
             self.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
         }
                         completion:^(BOOL finished)
         {
         }];
    }
    
    [self endEditing:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if((self.frame.origin.y==-135)||(self.frame.origin.y==-100))
    {
        [UIView animateWithDuration:.22 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^
         {
             self.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
         }
                         completion:^(BOOL finished)
         {
         }];
    }
    
    [self endEditing:YES];
    
    
    return YES;
}




- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if(screenBounds.size.height==480)
    {
        if(self.frame.origin.y!=-135)
        {
            [UIView animateWithDuration:.22 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^
             {
                 self.frame=CGRectMake(self.frame.origin.x, self.frame.origin.y-135, self.frame.size.width, self.frame.size.height);
             }
                             completion:^(BOOL finished)
             {
             }];
        }
    }
    else if(screenBounds.size.height==568)
    {
        if(self.frame.origin.y!=-100)
        {
            [UIView animateWithDuration:.22 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^
             {
                 self.frame=CGRectMake(self.frame.origin.x, self.frame.origin.y-100, self.frame.size.width, self.frame.size.height);
             }
                             completion:^(BOOL finished)
             {
             }];
        }

        
    }
    
}

@end
