//
//  ForgotView.h
// Storx
//
//  Created by clicklabs on 11/12/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "CommonClass.h"

@protocol forgotViewDelegate;


@interface ForgotView : UIView<UITextFieldDelegate>

@property (nonatomic, assign) id<forgotViewDelegate> forgotScreenDelegate;
@end
@protocol forgotViewDelegate <NSObject>
- (void)backButtonAction;
@end
