//
//  GoogleMapView.m
//  Jugnoo
//
//  Created by Samar Singla on 02/07/14.
//  Copyright (c) 2014 Click Labs. All rights reserved.
//

#import "GoogleMapView.h"

@implementation GoogleMapView
static GoogleMapView *shared=NULL;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(GoogleMapView*)shareCommonGoogleMap
{
    if (nil != shared)  {
        return shared;
    }
    static dispatch_once_t pred;        // Lock
    dispatch_once(&pred, ^{             // This code is called at most once per app
        shared = [[GoogleMapView alloc] init];
    });
    
    return shared;
}

-(GMSMapView*)allocateGoogleMapForCommonUseInMultipleClasses:(GMSCameraPosition*)cameraPosition withScreenSize:(CGRect)screenBounds
{
    //GMSMapView *googleMapView=[GMSMapView mapWithFrame:CGRectMake(0, 0, screenBounds.size.width, screenBounds.size.height) camera:cameraPosition];
        googleMapScreen=[[GMSMapView alloc] initWithFrame:CGRectMake(screenBounds.origin.x, screenBounds.origin.y, screenBounds.size.width, screenBounds.size.height)];
    [googleMapScreen setFrame:CGRectMake(screenBounds.origin.x, screenBounds.origin.y, screenBounds.size.width, screenBounds.size.height)];
    //[googleMapScreen setCamera:cameraPosition];
    [googleMapScreen setMapType:kGMSTypeNormal];
    //[googleMapView setDelegate:self];
    [googleMapScreen setMyLocationEnabled:YES];
    googleMapScreen.myLocationEnabled = TRUE;
    [googleMapScreen setUserInteractionEnabled:YES];
    return googleMapScreen;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
