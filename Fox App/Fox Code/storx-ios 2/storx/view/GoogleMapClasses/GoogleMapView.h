//
//  GoogleMapView.h
//  Jugnoo
//
//  Created by Samar Singla on 02/07/14.
//  Copyright (c) 2014 Click Labs. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface GoogleMapView : GMSMapView
{
    GMSMapView *googleMapScreen;
}
+(GoogleMapView*)shareCommonGoogleMap;
-(GMSMapView*)allocateGoogleMapForCommonUseInMultipleClasses:(GMSCameraPosition*)cameraPosition withScreenSize:(CGRect)screenBounds;
@end
