//
//  DetailView.m
//  Storx
//
//  Created by clicklabs on 12/23/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "DetailView.h"
#import "SKSTableViewCell.h"


@implementation DetailView
{
    UILabel *headerLabel;
    NSArray *qtyArray;
    NSArray *priceArray;

    UIView *pickerBgView;
    UIView *requestBgView;

    UIPickerView *pickerViewToSelect;
    UIView *ViewForValuePicker;
    UILabel *totalLabel;
    
    SKSTableView *detailTableView;
    NSString *dispensaryIdString;
    NSString *priceValue;
    NSString *qtyValue;
    NSString *qtyType;
    NSString *isVeg;
    NSString *one;
    //int tagValue;
    
    UIButton *crossButton;
    UIButton *selectButton;
    
    int tagValueForButton; // Check Button
    UIImageView *noMedicineBk;
}

#pragma mark - Dispensary View

- (void)dispensaryView :(NSString *)dispensaryId
         dispensaryName:(NSString *)dispensaryName
              ordertype:(NSString *)orderType {
    
    dispensaryIdString = dispensaryId;
    
    self.orderType              = [NSString stringWithFormat:@"%@",orderType];
    self.itemIdArray            = [[NSMutableArray alloc] init];
    self.itemImageArray         = [[NSMutableArray alloc] init];
    self.itemPriceArray         = [[NSMutableArray alloc] init];
    self.itemQtyArray           = [[NSMutableArray alloc] init];
    self.itemNameArray          = [[NSMutableArray alloc] init];
    
    self.itemDefaultPriceArray      = [[NSMutableArray alloc] init];
    self.itemDefaultQtyTypeArray    = [[NSMutableArray alloc] init];
    self.itemDefaultQtyValueArray   = [[NSMutableArray alloc] init];
    self.itemDescriptionArray       = [[NSMutableArray alloc] init];

    self.itemDiscountArray      = [[NSMutableArray alloc] init];
    self.rowsCountMutArray      = [[NSMutableArray alloc] init];
    self.expandMutArray         = [[NSMutableArray alloc] init];
    self.checkMutArray          = [[NSMutableArray alloc] init];

    self.requestIdArray         = [[NSMutableArray alloc] init];
    self.requestImageArray      = [[NSMutableArray alloc] init];
    self.requestNameArray       = [[NSMutableArray alloc] init];
    self.requestPriceArray      = [[NSMutableArray alloc] init];
    self.requestQtyValueArray   = [[NSMutableArray alloc] init];
    self.requestQtyTypeArray    = [[NSMutableArray alloc] init];

    self.bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    self.bkImageView.image = [UIImage imageNamed:@"bg.png"];
    self.bkImageView.userInteractionEnabled = YES;
    [self addSubview:self.bkImageView];
    
    UIButton *backButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backMethodClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.bkImageView addSubview:backButton];
    
    headerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:100/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:183/3 currentSupSize:SCREEN_HEIGHT])];
    headerLabel.textAlignment   = NSTextAlignmentCenter;
    headerLabel.textColor       = [UIColor whiteColor];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.text        = dispensaryName;
    headerLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
    [self.bkImageView addSubview:headerLabel];

    UIButton *filterBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1050/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:183/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:183/3 currentSupSize:SCREEN_HEIGHT])];
    [filterBtn setBackgroundImage:[UIImage imageNamed:@"info_iconDetail.png"] forState:UIControlStateNormal];
    [filterBtn setBackgroundImage:[UIImage imageNamed:@"info_iconPressedDetail.png"] forState:UIControlStateHighlighted];
    filterBtn.backgroundColor = [UIColor clearColor];
    [filterBtn addTarget:self action:@selector(infoMethodClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.bkImageView addSubview:filterBtn];
}

- (void)dispensaryReturn :(NSDictionary *)json  {
    NSLog(@"Detail Dispensary info: %@",json);
    
    json1 = json;
    self.dispensaryInfo     = [[json objectForKey:@"dispensery_details"] objectAtIndex:0];
    
    if([[json valueForKey:@"is_empty"] integerValue] ==  1) {
        [self noLocationSelectedView];
    }
    else {
        self.productArray       = [[json objectForKey:@"menu"] valueForKey:@"products"];
        self.menuNameArray      = [[json objectForKey:@"menu"] valueForKey:@"weed_name"];

        for (int i = 0; i < self.productArray.count; i++) {

            [self.rowsCountMutArray addObject:[NSString stringWithFormat:@"%d",[[self.productArray objectAtIndex:i] count]]];
            [self.itemIdArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"item_id"]];
            [self.itemNameArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"weed_menu_item"]];
            [self.itemImageArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"item_image"]];
            [self.itemQtyArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"item_quantity"]];
            [self.itemPriceArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"item_price"]];
            [self.itemDescriptionArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"description"]];
            [self.itemDefaultPriceArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"item_default_price"]];
            [self.itemDefaultQtyTypeArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"item_default_quantity"]];
            [self.itemDefaultQtyValueArray addObject:[[self.productArray objectAtIndex:i] valueForKey:@"default"]];
        }

        detailTableView = [[SKSTableView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT] - [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT])];
        detailTableView.tag = 2228;
        detailTableView.bounces = FALSE;
        detailTableView.SKSTableViewDelegate = self;
        [detailTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        detailTableView.backgroundColor = [UIColor clearColor];
        [self.bkImageView addSubview:detailTableView];
    }
}

#pragma mark : No Order View

- (void)noLocationSelectedView {
    
    [noMedicineBk removeFromSuperview];
    noMedicineBk = nil;
    
    noMedicineBk =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:488/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT])];
    noMedicineBk.image = [UIImage imageNamed:@"BaseBack.png"];
    [self addSubview:noMedicineBk];
    
    UILabel *txtLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:620/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT])];
    txtLabel.textAlignment = NSTextAlignmentCenter;
    txtLabel.textColor =  ColorFFFFFF;
    txtLabel.backgroundColor = [UIColor clearColor];
    txtLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    txtLabel.text = @"No Menus Available!";
    [noMedicineBk addSubview:txtLabel];
    
    [[CommonClass getSharedInstance] startFade:noMedicineBk];
}

#pragma mark - tableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.menuNameArray.count != 0)
        return 1;
    else
        return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [self.menuNameArray count];
}

- (void)showSelectedRow{
    
    for (int i = 0; i<[self.productArray count]; i++) {
        BOOL isContainingVal = FALSE;
        for (int j = 0; j<[[self.productArray objectAtIndex:i] count]; j++) {
            if ([self.checkMutArray containsObject:[NSString stringWithFormat:@"%d",[[[[self.productArray objectAtIndex:i] objectAtIndex:j]valueForKey:@"item_id" ] integerValue]+2000]]) {
                isContainingVal = YES;
                break;
            }
            else{
                isContainingVal = NO;
            }
        }
        
        if (isContainingVal) {
            [(UIButton *)[self viewWithTag:8000+i] setBackgroundImage:[UIImage imageNamed:@"EllipseSel.png"] forState:UIControlStateNormal];
        }
        else {
            [(UIButton *)[self viewWithTag:8000+i] setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        }
    }
}

- (BOOL)tableView:(SKSTableView *)tableView shouldExpandSubRowsOfCellAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self.rowsCountMutArray objectAtIndex:indexPath.row] integerValue];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:171/3 currentSupSize:SCREEN_HEIGHT] ;
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:454/3 currentSupSize:SCREEN_HEIGHT];
}

- (UITableViewCell *)tableView:(SKSTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SKSTableViewCell";
    
    SKSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIImageView *cellBkImageView;
    if (!cell)
        cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    else
    {
        for(UIImageView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
        [cellBkImageView removeFromSuperview];
        cellBkImageView = nil;
        
        UIImageView *lineSepratorView = (UIImageView *)[cell.contentView viewWithTag:indexPath.row + 100];
        [lineSepratorView removeFromSuperview];
        lineSepratorView = nil;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];

    cellBkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:141/3 currentSupSize:SCREEN_HEIGHT])];
    cellBkImageView.userInteractionEnabled = YES;
    cellBkImageView.tag = indexPath.row + 100;
    cellBkImageView.backgroundColor  =   [UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:0.7];
   //[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.3];
    [cell.contentView addSubview:cellBkImageView];
    [self.expandMutArray addObject:[NSString stringWithFormat:@"%d",indexPath.row + 100]];

    UIButton *checkButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:80/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(141/3 - 36/3)/2 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:36/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:36/3 currentSupSize:SCREEN_HEIGHT])];
    checkButton.tag = indexPath.row + 8000;
    [cellBkImageView addSubview:checkButton];
    
    UILabel *cellLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:150/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH]  - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:270/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:141/3 currentSupSize:SCREEN_HEIGHT])];
    cellLabel.textAlignment   = NSTextAlignmentLeft;
    cellLabel.textColor       = [UIColor whiteColor];
    cellLabel.backgroundColor = [UIColor clearColor];
    cellLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    cellLabel.text = [self.menuNameArray objectAtIndex:indexPath.row];
    [cellBkImageView addSubview:cellLabel];
    
    cell.expandable = YES;

    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    UIImageView *cellSubBkImageView;
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else {
        
        for(UIImageView *subview in [cell.contentView subviews])
            [subview removeFromSuperview];
        
        UIImageView *lineSepratorView = (UIImageView *)[cell.contentView viewWithTag:indexPath.row + 100];
        [lineSepratorView removeFromSuperview];
        lineSepratorView = nil;
    }
    cellSubBkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:454/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubBkImageView.userInteractionEnabled = YES;
    cellSubBkImageView.tag = indexPath.row + 1000;
    cellSubBkImageView.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:cellSubBkImageView];
    
    UIButton *checkButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:10/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:100/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
    checkButton.tag = [[[self.itemIdArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1] integerValue] + 2000;
    checkButton.backgroundColor = [UIColor clearColor];
    //[checkButton setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
  
  if ([isVeg isEqualToString:one]) {
    [checkButton setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
  } else {
    [checkButton setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
  }
  [checkButton addTarget:self action:@selector(checkMethodClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cellSubBkImageView addSubview:checkButton];
    
    if(self.checkMutArray.count >= 1) {

        if ([self.checkMutArray containsObject:[NSString stringWithFormat:@"%d",[[[self.itemIdArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1] integerValue] + 2000]]) {
            
          if ([isVeg isEqualToString:one]) {
            [checkButton setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
          } else {
            [checkButton setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
          }
        }
        else {
          if ([isVeg isEqualToString:one]) {
            [checkButton setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
          } else {
            [checkButton setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
          }
        }
    }
    
    UILabel *cellSubNameLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:140/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH]  - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:380/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubNameLabel.textAlignment   = NSTextAlignmentLeft;
    cellSubNameLabel.textColor       = [UIColor whiteColor];
    cellSubNameLabel.backgroundColor = [UIColor clearColor];
    cellSubNameLabel.font = [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:50/3 currentSupSize:SCREEN_HEIGHT]];
    cellSubNameLabel.text = [[self.itemNameArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1];
    [cellSubBkImageView addSubview:cellSubNameLabel];
   
    UILabel *cellSubDesLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:140/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1050/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubDesLabel.textAlignment   = NSTextAlignmentLeft;
    cellSubDesLabel.textColor       = [UIColor whiteColor];
    cellSubDesLabel.backgroundColor = [UIColor clearColor];
    cellSubDesLabel.font = [UIFont fontWithName:FontLight size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    //cellSubDesLabel.text = [[self.itemDescriptionArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1];
  isVeg = [[self.itemDescriptionArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1];
  one = @"1";
 // NSLog(isVeg);
  
  if ([isVeg isEqualToString:one]) {
    cellSubDesLabel.text = @"Non Veg";
  } else {
    cellSubDesLabel.text = @"Veg";
  }
  
    [cellSubBkImageView addSubview:cellSubDesLabel];
  
    UILabel *cellSubPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:150/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:260/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:300/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubPriceLabel.textAlignment   = NSTextAlignmentLeft;
    cellSubPriceLabel.textColor       = [UIColor whiteColor];
    cellSubPriceLabel.backgroundColor = [UIColor clearColor];
    cellSubPriceLabel.font = [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
    cellSubPriceLabel.text = [NSString stringWithFormat:@"INR %@",[[self.itemDefaultPriceArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1]];//INR
    cellSubPriceLabel.tag = [[[self.itemIdArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1] integerValue] + 200;
    [cellSubBkImageView addSubview:cellSubPriceLabel];
    
    // Selection Box
    UIImageView *cellSubSelectImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:729/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:454/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:128/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubSelectImageView.userInteractionEnabled = YES;
    cellSubSelectImageView.tag = [[[self.itemIdArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1] integerValue];
    cellSubSelectImageView.image = [UIImage imageNamed:@"GmBoxUnselected.png"];
    [cellSubBkImageView addSubview:cellSubSelectImageView];
    
     UILabel *cellSubDefQtyLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:10/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:300/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:128/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubDefQtyLabel.textAlignment   = NSTextAlignmentCenter;
    cellSubDefQtyLabel.textColor       = [UIColor whiteColor];
    cellSubDefQtyLabel.backgroundColor = [UIColor clearColor];
    cellSubDefQtyLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    cellSubDefQtyLabel.tag = [[[self.itemIdArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1] integerValue] + 3000;
    cellSubDefQtyLabel.text = [NSString stringWithFormat:@"%@ %@",[[self.itemDefaultQtyValueArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1],[[self.itemDefaultQtyTypeArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1]];
    [cellSubSelectImageView addSubview:cellSubDefQtyLabel];

    UIButton *selectPickerButton     =   [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:454/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:128/3 currentSupSize:SCREEN_HEIGHT])];
    [selectPickerButton addTarget:self action:@selector(selectPickerMethodClicked:) forControlEvents:UIControlEventTouchUpInside];
    selectPickerButton.tag = [[[self.itemIdArray objectAtIndex:indexPath.row] objectAtIndex:indexPath.subRow-1] integerValue];
    [cellSubSelectImageView addSubview:selectPickerButton];

    if([[self.itemDefaultPriceArray objectAtIndex:indexPath.row] count] != indexPath.subRow) {
        UIImageView *cellLineImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:430/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
        cellLineImageView.backgroundColor = ColorGrayBk;
        [cellSubBkImageView addSubview:cellLineImageView];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%@",self.expandMutArray);
    
    UIImageView *bkImageView =    (UIImageView *)[self viewWithTag:indexPath.row + 100];

    if([self.expandMutArray containsObject:[NSString stringWithFormat:@"%d",indexPath.row + 100]]) {
        [self.expandMutArray removeObject:[NSString stringWithFormat:@"%d",indexPath.row + 100]];
        bkImageView.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.3];
    }
    else {
         [self.expandMutArray addObject:[NSString stringWithFormat:@"%d",indexPath.row + 100]];
         bkImageView.backgroundColor = [UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:0.7];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Check Method
// Method call on check box clicked

- (void)checkMethodClicked:(UIButton *)sender {
    tagValueForButton = sender.tag-2000;
    //UIButton *check = (UIButton *)[self viewWithTag:sender.tag];
    //[check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];

    [self addAndRemoveValues:sender.tag];
}

- (NSString *)rowsIAndJForIndex :(int)getTag{
    int rowi = 0;
    int rowj = 0;
    
    for (int i = 0; i < self.itemIdArray.count; i++) {
        for (int j = 0; j < [[self.itemIdArray objectAtIndex:i] count]; j++) {
            if([[NSString stringWithFormat:@"%@",[[self.itemIdArray objectAtIndex:i] objectAtIndex:j]] isEqualToString:[NSString stringWithFormat:@"%d",getTag-2000]]) {
                rowi = i;
                rowj = j;
            }
        }
    }
    NSString *rows = [NSString stringWithFormat:@"%d#%d",rowi,rowj];
    return rows;
}

- (void) addAndRemoveValues :(int)tagForCheckButton{
    
    NSArray *temp = [[self rowsIAndJForIndex:tagForCheckButton] componentsSeparatedByString:@"#"];
    int rowi = [[temp objectAtIndex:0] intValue];
    int rowj = [[temp objectAtIndex:1] intValue];

    if([[[self.itemDefaultQtyValueArray objectAtIndex:rowi] objectAtIndex:rowj] isEqualToString:@"0"]) {
        [self selectPickerMethodClicked:nil];
    }
    else {
        UIButton *check = (UIButton *)[self viewWithTag:tagForCheckButton];

        if(self.checkMutArray.count >= 1) {
            
            if ([self.checkMutArray containsObject:[NSString stringWithFormat:@"%d",tagForCheckButton]]) {
                [self.checkMutArray removeObject:[NSString stringWithFormat:@"%d",tagForCheckButton]];
                //[check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
              if ([isVeg isEqualToString:one]) {
                [check setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
              } else {
                [check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
              }
              [self romveObjectToArray:tagValueForButton];

                if(self.checkMutArray.count < 1) {
                    [self removeRequest];
                }
            }
            else {
                //[check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
              if ([isVeg isEqualToString:one]) {
                [check setBackgroundImage:[UIImage imageNamed:@"redSelected.png"] forState:UIControlStateNormal];
              } else {
                [check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
              }
                [self romveObjectToArray:tagValueForButton];
                [self addObjectToArray:rowi :rowj];
                
                [self.checkMutArray addObject:[NSString stringWithFormat:@"%d",tagForCheckButton]];
                [self requestMedicineView];
            }
        }
        else {
          
            //[check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
          if ([isVeg isEqualToString:one]) {
            [check setBackgroundImage:[UIImage imageNamed:@"redSelected.png"] forState:UIControlStateNormal];
          } else {
            [check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
          }
          [self addObjectToArray:rowi :rowj];
            
            [self.checkMutArray addObject:[NSString stringWithFormat:@"%d",tagForCheckButton]];
            [self requestMedicineView];
        }
    }
    
    [self showSelectedRow];
}

#pragma mark : Remove Object

- (void)romveObjectToArray:(int)tagAtIndexTobeRemoved {
  
    for (int p = 0; p < self.requestIdArray.count; p++) {
        if([[self.requestIdArray objectAtIndex:p] intValue] == tagAtIndexTobeRemoved) {
            
            [self.requestIdArray removeObjectAtIndex:p];
            [self.requestNameArray removeObjectAtIndex:p];
            [self.requestImageArray removeObjectAtIndex:p];
            [self.requestPriceArray removeObjectAtIndex:p];
            [self.requestQtyValueArray removeObjectAtIndex:p];
            [self.requestQtyTypeArray removeObjectAtIndex:p];
        }
    }
    //NSLog(@"self.requestImageArray: %@",self.requestImageArray);
    //NSLog(@"self.requestImageArray: %@",self.requestIdArray);
    //NSLog(@"self.requestImageArray: %@",self.requestNameArray);
}

#pragma mark : Add Object

- (void)addObjectToArray:(int)i :(int)j {
    [self.requestIdArray addObject:[[self.itemIdArray objectAtIndex:i] objectAtIndex:j]];
    [self.requestNameArray addObject:[[self.itemNameArray objectAtIndex:i] objectAtIndex:j]];
    [self.requestImageArray addObject:[[self.itemImageArray objectAtIndex:i] objectAtIndex:j]];
    [self.requestPriceArray addObject:[[self.itemDefaultPriceArray objectAtIndex:i] objectAtIndex:j]];
    [self.requestQtyValueArray addObject:[[self.itemDefaultQtyValueArray objectAtIndex:i] objectAtIndex:j]];
    [self.requestQtyTypeArray addObject:[[self.itemDefaultQtyTypeArray objectAtIndex:i] objectAtIndex:j]];
    // NSLog(@"self.requestImageArray: %@",self.requestImageArray);
    //NSLog(@"self.requestImageArray: %@",self.requestIdArray);
    //NSLog(@"self.requestImageArray: %@",self.requestNameArray);
}

#pragma mark - Request Medicine

- (void)removeRequest{
    detailTableView.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT] - [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT]);
    
    for(UIView *subview in [requestBgView subviews]) {
        [subview removeFromSuperview];
    }
    [requestBgView removeFromSuperview];
    requestBgView = nil;
}

- (void)requestMedicineView {
    [self removeRequest];
    
    [requestBgView removeFromSuperview];
    requestBgView = nil;
    requestBgView = [[UIView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1745/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:470/3 currentSupSize:SCREEN_HEIGHT])];
    requestBgView.backgroundColor = [UIColor blackColor];
    [self.bkImageView addSubview:requestBgView];
    
    UIButton *requestMedicineButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:257/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [requestMedicineButton setExclusiveTouch:TRUE];
    [requestBgView addSubview:requestMedicineButton];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"FormMapView"]) {
        
        if([self.orderType isEqualToString:@"0"]) { //Delivery
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:PickUpOrderType];

            [requestMedicineButton addTarget:self action:@selector(requestDeliveryClicked) forControlEvents:UIControlEventTouchUpInside];
            [requestMedicineButton setBackgroundImage:[UIImage imageNamed:@"Delivery.png"] forState:UIControlStateNormal];
        }
        else if([self.orderType isEqualToString:@"1"]) { //Pickup
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:PickUpOrderType];
            [requestMedicineButton setBackgroundImage:[UIImage imageNamed:@"pick up.png"] forState:UIControlStateNormal];
            [requestMedicineButton addTarget:self action:@selector(requestPickUpClicked) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else {
        if([self.orderType isEqualToString:@"0"]) { //Delivery
            [requestMedicineButton addTarget:self action:@selector(requestDeliveryClicked) forControlEvents:UIControlEventTouchUpInside];
            [requestMedicineButton setBackgroundImage:[UIImage imageNamed:@"Delivery.png"] forState:UIControlStateNormal];
        }
        else if([self.orderType isEqualToString:@"1"]) { //Pickup
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:PickUpOrderType];
            [requestMedicineButton setBackgroundImage:[UIImage imageNamed:@"pick up.png"] forState:UIControlStateNormal];
            [requestMedicineButton addTarget:self action:@selector(requestPickUpClicked) forControlEvents:UIControlEventTouchUpInside];
        }
        else {
            //Both
            UIButton *requestPickUpButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:40/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:257/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:560/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
            [requestPickUpButton setExclusiveTouch:TRUE];
            [requestPickUpButton setBackgroundImage:[UIImage imageNamed:@"delivery smallDetail.png"] forState:UIControlStateNormal];
             [requestPickUpButton setBackgroundImage:[UIImage imageNamed:@"delivery small onclickDetail.png"] forState:UIControlStateHighlighted];
            [requestPickUpButton addTarget:self action:@selector(requestDeliveryClicked) forControlEvents:UIControlEventTouchUpInside];
            [requestBgView addSubview:requestPickUpButton];
            
            requestMedicineButton.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:640/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:257/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:560/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT]);
            [requestMedicineButton addTarget:self action:@selector(requestPickUpClicked) forControlEvents:UIControlEventTouchUpInside];
            [requestMedicineButton setBackgroundImage:[UIImage imageNamed:@"pick up smallDetail.png"] forState:UIControlStateNormal];
            [requestMedicineButton setBackgroundImage:[UIImage imageNamed:@"pick up small onclickDetail.png"] forState:UIControlStateHighlighted];
        }
    }
    
    UILabel *totalOrderLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:40/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    totalOrderLabel.textAlignment   = NSTextAlignmentCenter;
    totalOrderLabel.textColor       = [UIColor whiteColor];
    totalOrderLabel.backgroundColor = [UIColor clearColor];
    totalOrderLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    float priceTotal = 0.0;
    for (int i = 0; i < self.requestPriceArray.count; i++) {
        if(i == 0)
            priceTotal = [[self.requestPriceArray objectAtIndex:i] floatValue];
        else
            priceTotal =  priceTotal + [[self.requestPriceArray objectAtIndex:i] floatValue];
    }
    totalOrderLabel.text = [NSString stringWithFormat:@"Total Order: %lu",(unsigned long)[self.checkMutArray count]];
    [requestBgView addSubview:totalOrderLabel];
    
    UILabel *totalDonationLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    totalDonationLabel.textAlignment   = NSTextAlignmentCenter;
    totalDonationLabel.textColor       = [UIColor whiteColor];
    totalDonationLabel.backgroundColor = [UIColor clearColor];
    totalDonationLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    totalDonationLabel.text = [NSString stringWithFormat:@"Total Amount: INR %.2f",priceTotal];
    [requestBgView addSubview:totalDonationLabel];
    
    detailTableView.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT] - [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT] - [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:470/3 currentSupSize:SCREEN_HEIGHT]);
}

- (void)requestPickUpClicked {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:PickUpOrderType]; // PickUp
    [self.detailDelegate requestMethodId:self.requestIdArray
                            nameMutArray:self.requestNameArray
                           priceMutArray:self.requestPriceArray
                         qtyTypeMutArray:self.requestQtyTypeArray
                        qtyValueMutArray:self.requestQtyValueArray
                               imaeArray:self.requestImageArray
                         dispensaryArray:self.dispensaryInfo];
}

- (void)requestDeliveryClicked {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:PickUpOrderType]; // Delivery
    [self.detailDelegate requestMethodId:self.requestIdArray
                            nameMutArray:self.requestNameArray
                           priceMutArray:self.requestPriceArray
                         qtyTypeMutArray:self.requestQtyTypeArray
                        qtyValueMutArray:self.requestQtyValueArray
                               imaeArray:self.requestImageArray
                         dispensaryArray:self.dispensaryInfo];
}


#pragma mark - Picker Method

- (void)selectPickerMethodClicked: (UIButton *)sender {
    qtyValue = @"0.00";
    if ([sender isKindOfClass:[UIButton self]]) {
        tagValueForButton = sender.tag;
    }
    UIButton *check = (UIButton *)[self viewWithTag:2000+tagValueForButton];
    //[check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
  if ([isVeg isEqualToString:one]) {
    [check setBackgroundImage:[UIImage imageNamed:@"redSelected.png"] forState:UIControlStateNormal];
  } else {
    [check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
  }
    qtyArray    = [[NSArray alloc] init];
    priceArray  = [[NSArray alloc] init];

    NSString *stringQty;
    NSString *stringPrice;
    NSString *stringDefaultPrice;

    int row = 0;
    for (int i = 0; i < self.itemIdArray.count; i++) {
        for (int j = 0; j < [[self.itemIdArray objectAtIndex:i] count]; j++) {
            if([[NSString stringWithFormat:@"%@",[[self.itemIdArray objectAtIndex:i] objectAtIndex:j]] isEqualToString:[NSString stringWithFormat:@"%d",tagValueForButton]]){
                stringQty          = [[self.itemQtyArray objectAtIndex:i] objectAtIndex:j];
                stringPrice        = [[self.itemPriceArray objectAtIndex:i] objectAtIndex:j];
                stringDefaultPrice = [[self.itemDefaultPriceArray objectAtIndex:i] objectAtIndex:j];
                              row  = j;
                break;
            }
        }
    }

    qtyArray   = [stringQty componentsSeparatedByString:@","];
    priceArray = [stringPrice componentsSeparatedByString:@","];

    UIImageView *cellSubSelectImageView = (UIImageView *)[self viewWithTag:tagValueForButton];
    cellSubSelectImageView.image = [UIImage imageNamed:@"GmBoxSelected.png"];

    [pickerBgView removeFromSuperview];
    pickerBgView = nil;
    pickerBgView = [[UIView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    pickerBgView.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
    [self.bkImageView addSubview:pickerBgView];
    
    [ViewForValuePicker removeFromSuperview];
    ViewForValuePicker = nil;
    ViewForValuePicker = [[UIView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1250/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:960/3 currentSupSize:SCREEN_HEIGHT])];
    ViewForValuePicker.backgroundColor = [UIColor colorWithRed:28/255.0f green:28/255.0f blue:28/255.0f alpha:1.0];
    [self addSubview:ViewForValuePicker];
    
    [crossButton removeFromSuperview];
    crossButton = nil;
    crossButton     =   [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1250/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:390/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [crossButton.titleLabel setFont:[UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
    [crossButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [crossButton setTitle:[NSString stringWithFormat:@"Done"] forState:UIControlStateNormal];
    crossButton.backgroundColor = [UIColor clearColor];
    [crossButton addTarget:self action:@selector(crossPickerMethodClicked) forControlEvents:UIControlEventTouchDown];
    [self addSubview:crossButton];
    
    [selectButton removeFromSuperview];
    selectButton = nil;
    selectButton     =   [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:900/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1250/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:340/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [selectButton.titleLabel setFont:[UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
    [selectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selectButton setTitle:[NSString stringWithFormat:@"Cancel"] forState:UIControlStateNormal];
    selectButton.backgroundColor = [UIColor clearColor];
    [selectButton addTarget:self action:@selector(selectMethodClicked) forControlEvents:UIControlEventTouchDown];
    [self addSubview:selectButton];

    /*UILabel *cellSubDefQtyLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:161/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:930/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:126/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubDefQtyLabel.textAlignment   = NSTextAlignmentCenter;
    cellSubDefQtyLabel.textColor       = [UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:1.0];
    cellSubDefQtyLabel.backgroundColor = [UIColor clearColor];
    cellSubDefQtyLabel.font = [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    cellSubDefQtyLabel.text = @"Select Your Required Quantity";
    [ViewForValuePicker addSubview:cellSubDefQtyLabel]; */
    
    [totalLabel removeFromSuperview];
    totalLabel = nil;
    totalLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1500/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:330/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:660/3 currentSupSize:SCREEN_HEIGHT])];
    totalLabel.textAlignment   = NSTextAlignmentCenter;
    totalLabel.textColor       = [UIColor whiteColor];
    totalLabel.backgroundColor = [UIColor clearColor];
    totalLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
    totalLabel.text = [NSString stringWithFormat:@"INR %@",stringDefaultPrice];//INR
    [self addSubview:totalLabel];
    
    [pickerViewToSelect removeFromSuperview];
    pickerViewToSelect = nil;
    pickerViewToSelect = [[UIPickerView alloc] init];
    pickerViewToSelect.backgroundColor = [UIColor clearColor];
    [pickerViewToSelect setDataSource: self];
    [pickerViewToSelect setDelegate: self];
    [pickerViewToSelect setFrame: CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:110 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1480/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 130) currentSupSize:SCREEN_WIDTH],216.0)];
    pickerViewToSelect.showsSelectionIndicator = YES;
    [pickerViewToSelect selectRow:0 inComponent:0 animated:YES];
    [pickerViewToSelect selectRow:0 inComponent:1 animated:YES];
    //[pickerViewToSelect selectRow:row inComponent:2 animated:YES];
    [self addSubview:pickerViewToSelect];
    [self bringSubviewToFront:pickerViewToSelect];
}

#pragma mark : Cross Picker Method

- (void)crossPickerMethodClicked {
   
    UILabel *name = (UILabel*)[self viewWithTag:tagValueForButton + 3000]; // Selection box
    if([name.text isEqualToString:@"0 gm"] ) {
        UIButton *check = (UIButton *)[self viewWithTag:2000+tagValueForButton];
        //[check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
      if ([isVeg isEqualToString:one]) {
        [check setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
      } else {
        [check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
      }
    }
    else {
        UIButton *check = (UIButton *)[self viewWithTag:2000+tagValueForButton];
        //[check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
      if ([isVeg isEqualToString:one]) {
        [check setBackgroundImage:[UIImage imageNamed:@"redSelected.png"] forState:UIControlStateNormal];
      } else {
        [check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
      }
    }
    [self removePicker];
}

#pragma mark : Select Picker Method

- (void)selectMethodClicked {
    //NSLog(@"priceValue: %@ qtyValue: %@ qty:%@ tagValue:%d",priceValue ,qtyValue, qtyType, tagValueForButton);
    
    if(priceValue.length < 1 || [priceValue isEqualToString:@""]) {
        // Do Nothing
        UIButton *check = (UIButton *)[self viewWithTag:2000+tagValueForButton];
        //[check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
      if ([isVeg isEqualToString:one]) {
        [check setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
      } else {
        [check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
      }
    }
    else {
        if(![qtyValue isEqualToString:@"0.00"]) {
            UILabel *name = (UILabel*)[self viewWithTag:tagValueForButton + 3000]; // Selection box
            [name setText:[NSString stringWithFormat:@"%@ %@",qtyValue,qtyType]];
            name = (UILabel*)[self viewWithTag:tagValueForButton + 200]; // Total Label Price
            [name setText:[NSString stringWithFormat:@"INR %@",priceValue]];
            
            for (int i = 0; i < self.itemIdArray.count; i++) {
                NSMutableArray *objArray1 = [[self.itemDefaultPriceArray objectAtIndex:i] mutableCopy];
                NSMutableArray *objArray2 = [[self.itemDefaultQtyTypeArray objectAtIndex:i] mutableCopy];
                NSMutableArray *objArray3 = [[self.itemDefaultQtyValueArray objectAtIndex:i] mutableCopy];
                for (int j = 0; j < [[self.itemIdArray objectAtIndex:i] count]; j++) {
                    
                    if([[NSString stringWithFormat:@"%@",[[self.itemIdArray objectAtIndex:i] objectAtIndex:j]] isEqualToString:[NSString stringWithFormat:@"%d",tagValueForButton]]) {
                        
                        [objArray1 replaceObjectAtIndex:j withObject:priceValue];
                        [objArray2 replaceObjectAtIndex:j withObject:qtyType];
                        [objArray3 replaceObjectAtIndex:j withObject:qtyValue];
                    }
                }
                [self.itemDefaultPriceArray replaceObjectAtIndex:i withObject:objArray1];
                [self.itemDefaultQtyTypeArray replaceObjectAtIndex:i withObject:objArray2];
                [self.itemDefaultQtyValueArray replaceObjectAtIndex:i withObject:objArray3];
            }
            [self selectFromPicker:tagValueForButton+2000];
        }
        else {
            UIButton *check = (UIButton *)[self viewWithTag:2000+tagValueForButton];
            //[check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
          if ([isVeg isEqualToString:one]) {
            [check setBackgroundImage:[UIImage imageNamed:@"redChecked.png"] forState:UIControlStateNormal];
          } else {
            [check setBackgroundImage:[UIImage imageNamed:@"UncheckDetail.png"] forState:UIControlStateNormal];
          }
        }
    }
    [self removePicker];
}

- (void)selectFromPicker:(int)tagForCheckButton {
    NSArray *temp = [[self rowsIAndJForIndex:tagForCheckButton] componentsSeparatedByString:@"#"];
    int rowi = [[temp objectAtIndex:0] intValue];
    int rowj = [[temp objectAtIndex:1] intValue];
    
        UIButton *check = (UIButton *)[self viewWithTag:tagForCheckButton];
        if(self.checkMutArray.count >= 1) {
            if ([self.checkMutArray containsObject:[NSString stringWithFormat:@"%d",tagForCheckButton]]) {
                [self romveObjectToArray:tagValueForButton];
                [self addObjectToArray:rowi :rowj];
                
                if(self.checkMutArray.count < 1) {
                    [self removeRequest];
                }
            }
            else {
              //  [check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
              if ([isVeg isEqualToString:one]) {
                [check setBackgroundImage:[UIImage imageNamed:@"redSelected.png"] forState:UIControlStateNormal];
              } else {
                [check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
              }
                [self addObjectToArray:rowi :rowj];

                [self.checkMutArray addObject:[NSString stringWithFormat:@"%d",tagForCheckButton]];
                [self requestMedicineView];
            }
        }
        else {
            //[check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
          if ([isVeg isEqualToString:one]) {
            [check setBackgroundImage:[UIImage imageNamed:@"redSelected.png"] forState:UIControlStateNormal];
          } else {
            [check setBackgroundImage:[UIImage imageNamed:@"checkDetail.png"] forState:UIControlStateNormal];
          }
            [self addObjectToArray:rowi :rowj];
            
            [self.checkMutArray addObject:[NSString stringWithFormat:@"%d",tagForCheckButton]];
            [self requestMedicineView];
        }
    [self showSelectedRow];
}

#pragma mark : Remove Picker Method

- (void)removePicker {
    [selectButton removeFromSuperview];
    selectButton = nil;
    [crossButton removeFromSuperview];
    crossButton = nil;
    [totalLabel removeFromSuperview];
    totalLabel = nil;
    for(UIView *subview in [pickerViewToSelect subviews]) {
        [subview removeFromSuperview];
    }
    [pickerViewToSelect removeFromSuperview];
    pickerViewToSelect = nil;
    
    for(UIView *subview in [ViewForValuePicker subviews]) {
        [subview removeFromSuperview];
    }
    [ViewForValuePicker removeFromSuperview];
    ViewForValuePicker = nil;
    
    for(UIView *subview in [pickerBgView subviews]) {
        [subview removeFromSuperview];
    }
    [pickerBgView removeFromSuperview];
    pickerBgView = nil;
}

#pragma mark -
#pragma mark UIPicker Delegate & DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(component == 1)
        return qtyArray.count;
    else
        return 9;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:  (NSInteger)component {
    return [NSString stringWithFormat:@"%ld",(long)row+1];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    NSAttributedString *attString;
    
    if (component == 0) {
        title = [NSString stringWithFormat:@"%ld",(long)row+1];
        attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    }
    else if (component == 1) {
        if(row <= qtyArray.count - 1) {
            title = [NSString stringWithFormat:@"%@",[qtyArray objectAtIndex:row]];
            attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        }
    }
    return attString;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"row: %ld component:  %ld", (long)row, (long)component);

    NSInteger choose0 = [pickerView selectedRowInComponent:0];
    NSInteger choose1 = [pickerView selectedRowInComponent:1];
    //NSInteger choose2 = [pickerView selectedRowInComponent:2];
    
    NSString *totalValue = [NSString stringWithFormat:@"%d.00",choose0+1];
    float totalPrice =  [totalValue floatValue] * [[priceArray objectAtIndex:choose1] floatValue];
    totalLabel.text = [NSString stringWithFormat:@"INR %.2f",totalPrice] ;
    priceValue      = [NSString stringWithFormat:@"%.2f",totalPrice];
    qtyType         = [qtyArray objectAtIndex:choose1] ;
    qtyValue        = [NSString stringWithFormat:@"%@",totalValue];
}

#pragma mark - Back Method

- (void)backMethodClicked {
    [self.detailDelegate backMethod];
}

#pragma mark - Info Method

- (void)infoMethodClicked {
    [self.detailDelegate infoMethod:self.dispensaryInfo];
}

@end
