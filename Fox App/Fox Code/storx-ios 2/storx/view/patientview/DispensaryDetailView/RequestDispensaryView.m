//
//  RequestDispensaryView.m
//  Storx
//
//  Created by clicklabs on 12/27/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "RequestDispensaryView.h"

@implementation RequestDispensaryView
{
    float priceTotal;
    UIView *pickerBgView;
    UIView *ViewForValuePicker;
    UIImageView *endBkImageView;

    UIPickerView *pickerViewToSelect;
    UIDatePicker *datePicker;
    
    NSArray *pickerArray;
    
    UILabel *weeklyLabel;
    UILabel *endsOnLabel;
    UILabel *timeLabel;
    
    NSString *endsOnDate;

    NSInteger rowPickerSelected;
    UIButton *crossButton;
    UIButton *selectButton;
    int tabelHeight;
    int selectIndexForFrequencyPicker;
    NSDictionary *dictonaryInfoToPass;
}

- (void)requestInfoView:(NSString *)dispensaryId
         dispensaryName:(NSString *)dispensaryName
        requestMethodId:(NSMutableArray *)requestIdMutArray
    requestNameMutArray:(NSMutableArray *)requestNameMutArray
   requestPriceMutArray:(NSMutableArray *)requestPriceMutArray
 requestQtyTypeMutArray:(NSMutableArray *)requestQtyTypeMutArray
requestQtyValueMutArray:(NSMutableArray *)requestQtyValueMutArray
       requestImaeArray:(NSMutableArray *)requestImaeArray
         dispensaryInfo:(NSDictionary *)dispensaryInfo
{
    selectIndexForFrequencyPicker = 0;
    dictonaryInfoToPass = dispensaryInfo;
    endsOnDate = @"";
    self.dispensaryIdTemp       = dispensaryId;
    self.requestIdArray         = requestIdMutArray;
    self.requestImageArray      = requestImaeArray;
    self.requestNameArray       = requestNameMutArray;
    self.requestPriceArray      = requestPriceMutArray;
    self.requestQtyValueArray   = requestQtyValueMutArray;
    self.requestQtyTypeArray    = requestQtyTypeMutArray;
    
    self.bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    self.bkImageView.image = [UIImage imageNamed:@"bg.png"];
    self.bkImageView.userInteractionEnabled = YES;
    [self addSubview:self.bkImageView];
    
    UIButton *backButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backMethodClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.bkImageView addSubview:backButton];
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:100/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:183/3 currentSupSize:SCREEN_HEIGHT])];
    headerLabel.textAlignment   = NSTextAlignmentCenter;
    headerLabel.textColor       = [UIColor whiteColor];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
    headerLabel.text = dispensaryName;
    [self.bkImageView addSubview:headerLabel];
    
    UIButton *infoBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1050/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [infoBtn setBackgroundImage:[UIImage imageNamed:@"info_iconDetail.png"] forState:UIControlStateNormal];
    [infoBtn setBackgroundImage:[UIImage imageNamed:@"info_iconPressedDetail.png"] forState:UIControlStateHighlighted];
    infoBtn.backgroundColor = [UIColor clearColor];
    [infoBtn addTarget:self action:@selector(infoMethodClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.bkImageView addSubview:infoBtn];
    
    UILabel *reqHeaderLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:241/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:98/3 currentSupSize:SCREEN_HEIGHT])];
    reqHeaderLabel.backgroundColor = ColorBlackBK;
    reqHeaderLabel.textColor = [UIColor whiteColor];
    reqHeaderLabel.text = @"    Requested Item Details";
    reqHeaderLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    [self.bkImageView addSubview:reqHeaderLabel];
   
    pickerArray = [[NSArray alloc] initWithObjects:@"Once",@"Daily",@"Weekly",@"Monthly", nil];

    tabelHeight = (self.requestIdArray.count + 1) * [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT];

    self.listTableView = [[UITableView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:344/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],tabelHeight) style:UITableViewStylePlain];
    self.listTableView.tag = 101;
    self.listTableView.dataSource=self;
    self.listTableView.delegate=self;
    self.listTableView.layer.borderWidth = 5;
    self.listTableView.layer.borderColor = [UIColor clearColor].CGColor;
    [self.listTableView setBackgroundView:nil];
    [self.listTableView setBackgroundColor:[UIColor clearColor]];
    [self.listTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.bkImageView addSubview:self.listTableView];
    
    tabelHeight = tabelHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:344/3 currentSupSize:SCREEN_HEIGHT];
    
    [self scheduleView];
}

- (void)scheduleView {
    UILabel *medicineLabel;
    // Medicine PickUp/ Delivery Time
    if([[NSUserDefaults standardUserDefaults] boolForKey:PickUpOrderType]) { // PickUp
        UIImageView *pickUpBkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], tabelHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
        pickUpBkImageView.backgroundColor = ColorBlackBK;
        pickUpBkImageView.userInteractionEnabled = YES;
        [self.bkImageView addSubview:pickUpBkImageView];
        
        medicineLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:580/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
        medicineLabel.backgroundColor = [UIColor clearColor];
        medicineLabel.textColor = [UIColor whiteColor];
        medicineLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
        [pickUpBkImageView addSubview:medicineLabel];
        
        tabelHeight = tabelHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT];
        
        UIImageView *timeImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], tabelHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
        timeImageView.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.1];
        timeImageView.userInteractionEnabled = YES;
        [self.bkImageView addSubview:timeImageView];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd MMM, yyyy hh:mm a"];
        
        NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:(15*60)];
        NSString *theDate = [dateFormat stringFromDate:newDate];
        dateFormat = nil;
        
        timeLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:90/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:750/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor whiteColor];
        timeLabel.textAlignment = NSTextAlignmentLeft;
        timeLabel.text = theDate;
        timeLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
        [timeImageView addSubview:timeLabel];
        
        UIButton *timeEditButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:877/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(200/3 - 120/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:300/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        timeEditButton.tag = 30002;
        [timeEditButton setBackgroundImage:[UIImage imageNamed:@"edit button.png"] forState:UIControlStateNormal];
        [timeEditButton setBackgroundImage:[UIImage imageNamed:@"edit buttonPressed.png"] forState:UIControlStateHighlighted];
        [timeEditButton addTarget:self action:@selector(selectPickerMethodClicked:) forControlEvents:UIControlEventTouchUpInside];
        [timeImageView addSubview:timeEditButton];
        
        tabelHeight = tabelHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT];
    }
   
    // Frequency Of Delivery View
    UIImageView *scheduleBkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], tabelHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
    scheduleBkImageView.backgroundColor = ColorBlackBK;
    scheduleBkImageView.userInteractionEnabled = YES;
    [self.bkImageView addSubview:scheduleBkImageView];
    
    UIImageView *ScheduleImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(160/3 - 40/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:84/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:40/3 currentSupSize:SCREEN_HEIGHT])];
    [scheduleBkImageView addSubview:ScheduleImageView];
    
    UILabel *reqHeaderLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:140/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:580/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
    reqHeaderLabel.backgroundColor = [UIColor clearColor];
    reqHeaderLabel.textColor = [UIColor whiteColor];
    reqHeaderLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    [scheduleBkImageView addSubview:reqHeaderLabel];

    UIButton *pickerButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:750/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(160/3 - 128/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:454/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:128/3 currentSupSize:SCREEN_HEIGHT])];
    [pickerButton setBackgroundImage:[UIImage imageNamed:@"GmBoxUnselected.png"] forState:UIControlStateNormal];
    [pickerButton setBackgroundImage:[UIImage imageNamed:@"GmBoxSelected.png"] forState:UIControlStateHighlighted];
    pickerButton.tag = 300101;
    [pickerButton addTarget:self action:@selector(selectPickerMethodClicked:) forControlEvents:UIControlEventTouchUpInside];
    [scheduleBkImageView addSubview:pickerButton];
    
    weeklyLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:330/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:128/3 currentSupSize:SCREEN_HEIGHT])];
    weeklyLabel.backgroundColor = [UIColor clearColor];
    weeklyLabel.textColor = [UIColor whiteColor];
    weeklyLabel.text = @"Once";
    weeklyLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    [pickerButton addSubview:weeklyLabel];
    
    UIButton *confirmButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 650/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1980/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:650/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [confirmButton setBackgroundImage:[UIImage imageNamed:@"confirmButton.png"] forState:UIControlStateNormal];
    [confirmButton setBackgroundImage:[UIImage imageNamed:@"confirmButtonPressed.png"] forState:UIControlStateHighlighted];
    [confirmButton addTarget:self action:@selector(confirmMethodCall) forControlEvents:UIControlEventTouchUpInside];
    [self.bkImageView addSubview:confirmButton];
    
    tabelHeight = tabelHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT];

    if([[NSUserDefaults standardUserDefaults] boolForKey:PickUpOrderType]) { // PickUp
        reqHeaderLabel.text = @"Frequency of Pick Up:";
        medicineLabel.text  = @"    Order Pick Up Time";
        ScheduleImageView.image = [UIImage imageNamed:@"pickup iconConfirm.png"];
    }
    else {
        reqHeaderLabel.text = @"Frequency of Delivery:";
        ScheduleImageView.image = [UIImage imageNamed:@"DeliverIcon.png"];
    }
}

- (void)endsView :(NSString *)string {
    
    for(UIImageView *subview in [endBkImageView subviews]) {
        [subview removeFromSuperview];
    }
    [endBkImageView removeFromSuperview];
    endBkImageView = nil;
    
    if(![string isEqualToString:@"Once"]) {
        // End Date View
        [endBkImageView removeFromSuperview];
        endBkImageView = nil;
        endBkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], tabelHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
        endBkImageView.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.1];;
        endBkImageView.userInteractionEnabled = YES;
        [self.bkImageView addSubview:endBkImageView];
        
        endsOnLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:550/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
        endsOnLabel.backgroundColor = [UIColor clearColor];
        endsOnLabel.textColor = [UIColor whiteColor];
        endsOnLabel.textAlignment = NSTextAlignmentRight;
        endsOnLabel.text = @"Ends on:   Select a date";
        endsOnLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
        [endBkImageView addSubview:endsOnLabel];
        
        UIImageView *calenderImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:640/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(160/3 - 50/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:44/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:50/3 currentSupSize:SCREEN_HEIGHT])];
        calenderImageView.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.1];
        calenderImageView.backgroundColor = [UIColor clearColor];
        calenderImageView.image = [UIImage imageNamed:@"CalendarIcon.png"];
        [endBkImageView addSubview:calenderImageView];
        
        UIButton *selectEndDateButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:750/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
        selectEndDateButton.backgroundColor = [UIColor clearColor];
        selectEndDateButton.tag = 300201;
        [selectEndDateButton addTarget:self action:@selector(selectPickerMethodClicked:) forControlEvents:UIControlEventTouchUpInside];
        [endBkImageView addSubview:selectEndDateButton];
    }
}

#pragma mark : TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.requestIdArray count] + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"cell";
    UITableViewCell *cell;
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else {
        
        [[cell.contentView viewWithTag:100] removeFromSuperview];
    }
    cell.backgroundColor =  [UIColor clearColor];
    
    UIImageView *cellBkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
    cellBkImageView.userInteractionEnabled = YES;
    cellBkImageView.tag = indexPath.row + 100;
    cellBkImageView.backgroundColor  = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.1];
    [cell.contentView addSubview:cellBkImageView];
    
    if(indexPath.row == self.requestIdArray.count) {
        
        UILabel *priceLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
        priceLabel.backgroundColor = [UIColor clearColor];
        priceLabel.textColor = [UIColor whiteColor];
        priceLabel.textAlignment = NSTextAlignmentRight;
        
        for (int i = 0; i < self.requestPriceArray.count; i++) {
            if(i == 0)
                priceTotal = [[self.requestPriceArray objectAtIndex:i] floatValue];
            else
                priceTotal =  priceTotal + [[self.requestPriceArray objectAtIndex:i] floatValue];
        }
        NSString *stringPrice = [NSString stringWithFormat:@"INR %.2f",priceTotal];
        
        priceLabel.text = [NSString stringWithFormat:@"Total Amount: INR %.2f",priceTotal];
        priceLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [cellBkImageView addSubview:priceLabel];
        
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:priceLabel.text];
        [title addAttribute:NSFontAttributeName value:[UIFont fontWithName:FontSemiBold  size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]] range:NSMakeRange(@"Total Order: ".length, stringPrice.length)];
        priceLabel.attributedText = title;
    }
    else {

        UIImageView *picImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(200/3 - 150/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:150/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
        picImageView.layer.cornerRadius = picImageView.frame.size.height/2;
        picImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        picImageView.layer.borderWidth = 2;
        picImageView.layer.masksToBounds = YES;
        [picImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.requestImageArray objectAtIndex:indexPath.row]]] placeholderImage:[UIImage imageNamed:DispensaryDefaultImage]];
        picImageView.contentMode = UIViewContentModeScaleAspectFill;
        [cellBkImageView addSubview:picImageView];
        
        UILabel *nameLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:240/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:500/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
        nameLabel.backgroundColor = [UIColor clearColor];
        nameLabel.textColor = [UIColor whiteColor];
        nameLabel.text = [self.requestNameArray objectAtIndex:indexPath.row];
        nameLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [cellBkImageView addSubview:nameLabel];
        
        UILabel *qtyLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:750/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:280/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
        qtyLabel.textAlignment = NSTextAlignmentCenter;
        qtyLabel.backgroundColor = [UIColor clearColor];
        qtyLabel.textColor = [UIColor whiteColor];
        qtyLabel.text = [NSString stringWithFormat:@"%@ %@",[self.requestQtyValueArray objectAtIndex:indexPath.row],[self.requestQtyTypeArray objectAtIndex:indexPath.row]];
        qtyLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [cellBkImageView addSubview:qtyLabel];
        
        UILabel *priceLabel =    [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1040/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:190/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
        priceLabel.backgroundColor = [UIColor clearColor];
        priceLabel.textColor = [UIColor whiteColor];
        priceLabel.textAlignment = NSTextAlignmentCenter;
        priceLabel.text = [NSString stringWithFormat:@"INR %@",[self.requestPriceArray objectAtIndex:indexPath.row]];
        priceLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [cellBkImageView addSubview:priceLabel];
    }
    
    return cell;
}

#pragma mark - Picker

- (void)selectPickerMethodClicked:(UIButton *)sender {
    
    [pickerBgView removeFromSuperview];
    pickerBgView = nil;
    pickerBgView = [[UIView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], 0, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    pickerBgView.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
    [self.bkImageView addSubview:pickerBgView];
    
    [pickerBgView removeFromSuperview];
    pickerBgView = nil;
    ViewForValuePicker = [[UIView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1300/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:320 currentSupSize:SCREEN_HEIGHT])];
    ViewForValuePicker.backgroundColor = [UIColor colorWithRed:28/255.0f green:28/255.0f blue:28/255.0f alpha:1.0];
    [self addSubview:ViewForValuePicker];
    
    [crossButton removeFromSuperview];
    crossButton = nil;
    crossButton     =   [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1300/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:350/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
    [crossButton.titleLabel setFont:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
    [crossButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [crossButton setTitle:[NSString stringWithFormat:@"Cancel"] forState:UIControlStateNormal];
    crossButton.backgroundColor = [UIColor clearColor];
    [crossButton addTarget:self action:@selector(crossPickerMethodClicked) forControlEvents:UIControlEventTouchDown];
    [self addSubview:crossButton];
    
    [selectButton removeFromSuperview];
    selectButton = nil;
    selectButton     =   [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:900/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1300/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:380/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT])];
    [selectButton.titleLabel setFont:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
    [selectButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [selectButton setTitle:[NSString stringWithFormat:@"Done"] forState:UIControlStateNormal];
    selectButton.backgroundColor = [UIColor clearColor];
    [self addSubview:selectButton];
    
   /* UILabel *cellSubDefQtyLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:161/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:930/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:130/3 currentSupSize:SCREEN_HEIGHT])];
    cellSubDefQtyLabel.textAlignment   = NSTextAlignmentCenter;
    cellSubDefQtyLabel.textColor       = [UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:1.0];
    cellSubDefQtyLabel.backgroundColor = [UIColor clearColor];
    cellSubDefQtyLabel.font = [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:50/3 currentSupSize:SCREEN_HEIGHT]];
    cellSubDefQtyLabel.text = @"Schedule";
    [ViewForValuePicker addSubview:cellSubDefQtyLabel]; */

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM, yyyy hh:mm a"];
    
    if(sender.tag == 300101) {
        // Select Week Month
        rowPickerSelected = 0;
        [selectButton addTarget:self action:@selector(selectMethodClicked) forControlEvents:UIControlEventTouchDown];

        pickerViewToSelect = [[UIPickerView alloc] init];
        pickerViewToSelect.backgroundColor = [UIColor clearColor];
        [pickerViewToSelect setDataSource: self];
        [pickerViewToSelect setDelegate: self];
        [pickerViewToSelect setFrame: CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1500/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], 216.0)];
        pickerViewToSelect.showsSelectionIndicator = YES;
        [pickerViewToSelect selectRow:selectIndexForFrequencyPicker inComponent:0 animated:YES];
        [self addSubview:pickerViewToSelect];
    }
    else if(sender.tag == 30002) {
        [selectButton addTarget:self action:@selector(selectDateAndTime) forControlEvents:UIControlEventTouchDown];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1500/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], 216.0)];
        datePicker.backgroundColor = [UIColor clearColor];
        datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:(15*60)];
        datePicker.minimumDate = newDate;
        [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
        [self addSubview:datePicker];
    }
    else {
        [selectButton addTarget:self action:@selector(selectDate) forControlEvents:UIControlEventTouchDown];

        datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1500/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], 216.0)];
        datePicker.backgroundColor=[UIColor clearColor];
        datePicker.datePickerMode = UIDatePickerModeDate;
        NSTimeInterval secondsPerDay = 24 * 60 * 60;
        NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:secondsPerDay];
        [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
        datePicker.minimumDate = newDate;
        datePicker.selected = YES;
        [self addSubview:datePicker];
        
        SEL selector = NSSelectorFromString(@"setHighlightsToday:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
        
        BOOL no = NO;
        
        [invocation setSelector:selector];
        [invocation setArgument:&no atIndex:2];
        [invocation invokeWithTarget:datePicker];
    }
}

#pragma mark : Select Picker Method

- (void)selectMethodClicked {
    weeklyLabel.text = [pickerArray objectAtIndex:rowPickerSelected];
    [self endsView:weeklyLabel.text];
    selectIndexForFrequencyPicker = (int)rowPickerSelected;
    [selectButton setBackgroundImage:[UIImage imageNamed:@"GmBoxSelected.png"] forState:UIControlStateNormal];

    [self crossPickerMethodClicked];
}

#pragma mark : Cross Picker Method

- (void)crossPickerMethodClicked {
    [selectButton removeFromSuperview];
    selectButton = nil;
    [crossButton removeFromSuperview];
    crossButton = nil;

    for(UIView *subview in [pickerViewToSelect subviews]) {
        [subview removeFromSuperview];
    }
    [pickerViewToSelect removeFromSuperview];
    pickerViewToSelect = nil;
    
    for(UIView *subview in [ViewForValuePicker subviews]) {
        [subview removeFromSuperview];
    }
    [ViewForValuePicker removeFromSuperview];
    ViewForValuePicker = nil;
    
    for(UIView *subview in [pickerBgView subviews]) {
        [subview removeFromSuperview];
    }
    [pickerBgView removeFromSuperview];
    pickerBgView = nil;
    
    for(UIView *subview in [datePicker subviews]) {
        [subview removeFromSuperview];
    }
    [datePicker removeFromSuperview];
    datePicker = nil;
}

#pragma mark -
#pragma mark UIPicker Delegate & DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [pickerArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:  (NSInteger)component {
    return [NSString stringWithFormat:@"%@",[pickerArray objectAtIndex:row]];
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[pickerArray objectAtIndex:row]] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    //NSLog(@"row: %ld component:  %ld", (long)row, (long)component);
    rowPickerSelected = row;
}

#pragma mark : Date Picker

- (void)selectDateAndTime {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM, yyyy hh:mm a"];
    NSString *theDate = [dateFormat stringFromDate:datePicker.date];
    timeLabel.text = [NSString stringWithFormat:@"%@",theDate];
    dateFormat = nil;
    [self crossPickerMethodClicked];
}

- (void)selectDate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM, yyyy"];
    NSString *theDate = [dateFormat stringFromDate:datePicker.date];
    endsOnLabel.text = [NSString stringWithFormat:@"Ends on:  %@",theDate];

    //To send for server
    NSDateFormatter *dateFormatTemp = [[NSDateFormatter alloc] init];
    [dateFormatTemp setDateFormat:@"dd MMM, yyyy hh:mm a"];
    NSString *theDateTemp = [dateFormatTemp stringFromDate:datePicker.date];
    endsOnDate = theDateTemp;

    dateFormat = nil;
    [self crossPickerMethodClicked];
}

#pragma mark - Confirm Method

- (void)confirmMethodCall {

    [self showActivityIndicaterr];

    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:[[[NSUserDefaults standardUserDefaults] valueForKey:@"latToSendAPICall"] floatValue] longitude:[[[NSUserDefaults standardUserDefaults] valueForKey:@"longToSendAPICall"] floatValue]];
    
    [ceo reverseGeocodeLocation:loc completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         //NSLog(@"placemark %@",placemark);
         
         NSString *address = [NSString stringWithFormat:@"%@ %@",placemark.subLocality,placemark.locality];
         
         if(address.length >= 1) {
             if([weeklyLabel.text isEqualToString:@"Once"]) {
                 [self callAPI:address lat:[[NSUserDefaults standardUserDefaults] valueForKey:@"latToSendAPICall"] longCord:[[NSUserDefaults standardUserDefaults] valueForKey:@"longToSendAPICall"]];
             }
             else {
                 if ([endsOnLabel.text isEqualToString:@"Ends on:   Select a date"]) {
                     [self hideActivityIndicaterr];
                     [[[UIAlertView alloc]initWithTitle:@"" message:@"Please select end date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                 }
                 else {
                     [self callAPI:address lat:[[NSUserDefaults standardUserDefaults] valueForKey:@"latToSendAPICall"] longCord:[[NSUserDefaults standardUserDefaults] valueForKey:@"longToSendAPICall"]];
                 }
             }
         }
         else {
             [self hideActivityIndicaterr];
             [[[UIAlertView alloc]initWithTitle:@"" message:ErrorInternetConnection delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
         }
    }];
}

- (void)callAPI:(NSString *)address
            lat:(NSString *)stringLat
       longCord:(NSString *)longCord {
    NSMutableArray *stringQty = [[NSMutableArray alloc]init];
    
    NSString *itemId        = [self.requestIdArray componentsJoinedByString:@","];
    NSString *itemPrice     = [self.requestPriceArray componentsJoinedByString:@","];
    
    for (int i=0; i < [self.requestQtyTypeArray count]; i++) {
        [stringQty   addObject: [NSString stringWithFormat:@"%@ %@",[self.requestQtyValueArray objectAtIndex:i],[self.requestQtyTypeArray objectAtIndex:i]]];
    }
    
    NSString *qtyTye        = [stringQty componentsJoinedByString:@","];
    NSString *discount      = [self.requestPriceArray componentsJoinedByString:@","];
    NSString *endTime;
    
    if(endsOnDate.length >= 1)
        endTime       = [[CommonClass getSharedInstance] getdateLocal:endsOnDate];

    NSString *orderType;
    NSString *recurringType;
    NSString *pickUpTime;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:PickUpOrderType]) {
        orderType  = @"1"; // Pickup
        pickUpTime = [[CommonClass getSharedInstance] getdateLocal:timeLabel.text];
    }
    else {
        orderType  = @"0"; // Dilevery
        pickUpTime = @"";
    }
    
    for (int i = 0; i < [pickerArray count]; i++) {
        if([weeklyLabel.text isEqualToString:[pickerArray objectAtIndex:i]]) {
            recurringType = [NSString stringWithFormat:@"%d",i];
            break;
        }
    }

    //NSLog(@"address:%@",address);
    [self.requestDelegate confirmOrderMethod:self.dispensaryIdTemp
                                      itemId:itemId
                                         qty:qtyTye
                                   orderType:orderType
                                  pickupTime:pickUpTime
                                   itemPrice:itemPrice
                                       price:[NSString stringWithFormat:@"%.2f",priceTotal]
                                    discount:discount
                               recurringType:recurringType
                                     endDate:endTime
                                     address:address
                                     latCord:stringLat
                                    longCord:longCord];
    [self hideActivityIndicaterr];
}

#pragma mark - Back Method

- (void)backMethodClicked {
    [self.requestDelegate backMethod];
}

#pragma mark - Info Method

- (void)infoMethodClicked {
    [self.requestDelegate infoMethod:dictonaryInfoToPass];
}

#pragma mark - loading indicator bar

- (void)showActivityIndicaterr {
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}

- (void)hideActivityIndicaterr {
    [SVProgressHUD dismiss];
}

@end
