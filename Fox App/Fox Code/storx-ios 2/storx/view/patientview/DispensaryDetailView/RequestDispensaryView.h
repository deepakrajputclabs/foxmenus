//
//  RequestDispensaryView.h
//  Storx
//
//  Created by clicklabs on 12/27/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@protocol requestProtocol <NSObject>
@optional
- (void)confirmOrderMethod:(NSString *)dispId
                    itemId:(NSString *)itemId
                       qty:(NSString *)qnty
                 orderType:(NSString *)orderType
                pickupTime:(NSString *)pickUpTime
                 itemPrice:(NSString *)itemPrice
                     price:(NSString *)price
                  discount:(NSString *)discount
             recurringType:(NSString *)recurringType
                   endDate:(NSString *)endDate
                   address:(NSString *)address
                   latCord:(NSString *)latCord
                  longCord:(NSString *)longCord;
- (void)backMethod;
- (void)infoMethod:(NSDictionary *)dispensaryINfo;
- (void)requestMethodId:(NSMutableArray *)idMutArray
           nameMutArray:(NSMutableArray *)nameMutArray
          priceMutArray:(NSMutableArray *)priceMutArray
        qtyTypeMutArray:(NSMutableArray *)qtyTypeMutArray
       qtyValueMutArray:(NSMutableArray *)qtyValueMutArray
              imaeArray:(NSMutableArray *)imageArray
        dispensaryArray:(NSDictionary *)dispensaryArray;
@end


@interface RequestDispensaryView : UIView <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

- (void)requestInfoView:(NSString *)dispensaryId
         dispensaryName:(NSString *)dispensaryName
        requestMethodId:(NSMutableArray *)requestIdMutArray
    requestNameMutArray:(NSMutableArray *)requestNameMutArray
   requestPriceMutArray:(NSMutableArray *)requestPriceMutArray
 requestQtyTypeMutArray:(NSMutableArray *)requestQtyTypeMutArray
requestQtyValueMutArray:(NSMutableArray *)requestQtyValueMutArray
       requestImaeArray:(NSMutableArray *)requestImaeArray
         dispensaryInfo:(NSDictionary *)dispensaryInfo;

@property (weak) id <requestProtocol> requestDelegate;
@property(strong, nonatomic) NSString *dispensaryIdTemp;
@property(strong, nonatomic) NSArray *requestIdArray;
@property(strong, nonatomic) NSArray *requestImageArray;
@property(strong, nonatomic) NSArray *requestPriceArray;
@property(strong, nonatomic) NSArray *requestQtyTypeArray;
@property(strong, nonatomic) NSArray *requestQtyValueArray;
@property(strong, nonatomic) NSArray *requestNameArray;
@property(strong, nonatomic) UIImageView *bkImageView;
@property(strong, nonatomic) UITableView *listTableView;

@end
