//
//  DispensaryView.m
//  Storx
//
//  Created by clicklabs on 12/30/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "DispensaryView.h"

@implementation DispensaryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dispensaryInfoAPICall :(NSDictionary *)dicJson {
    
    picImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:540/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:188/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:360/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:360/3 currentSupSize:SCREEN_HEIGHT])];
    picImageView.layer.cornerRadius = picImageView.frame.size.height/2;
    picImageView.contentMode = UIViewContentModeScaleToFill;
    picImageView.layer.borderWidth = 1.0;
    picImageView.layer.borderColor = ColorGreenButton.CGColor;
    picImageView.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:364/6 currentSupSize:SCREEN_HEIGHT];
    picImageView.layer.masksToBounds = YES;
    picImageView.userInteractionEnabled = NO;
    [self addSubview:picImageView];
    
    nameLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:650/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
    nameLabel.textAlignment   = NSTextAlignmentCenter;
    nameLabel.textColor       = [UIColor whiteColor];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.font = [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    [self addSubview:nameLabel];
    
    boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:850/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:400/3 currentSupSize:SCREEN_HEIGHT])];
    boxImage.backgroundColor = [UIColor clearColor];
    boxImage.userInteractionEnabled = YES;
    [self addSubview:boxImage];
    [self showInfo:dicJson];
}

- (void)showInfo:(NSDictionary *)jsonInfo {
    //NSLog(@"Dispensery Info: %@",[jsonInfo valueForKey:@"dispensery_image"]);

    [picImageView setImageWithURL:[NSURL URLWithString:[jsonInfo valueForKey:@"dispensery_image"]] placeholderImage:[UIImage imageNamed:DispensaryDefaultImage]];
    phoneNo = [NSString stringWithFormat:@"%@",[jsonInfo valueForKey:@"phone_no"]];
    nameLabel.text          = [jsonInfo valueForKey:@"dispensery_name"];
    NSArray *dayArray       = [[jsonInfo objectForKey:@"timings"] valueForKey:@"day"];
    NSArray *startTimeArray   = [[CommonClass getSharedInstance] convertTimeArray:[[jsonInfo objectForKey:@"timings"] valueForKey:@"start_time"]];
    NSArray *endTimeArray = [[CommonClass getSharedInstance] convertTimeArray:[[jsonInfo objectForKey:@"timings"] valueForKey:@"end_time"]];

    float labelY = 0;
    
    UIImageView *underlineImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], labelY , [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
    underlineImage.backgroundColor = ColorGrayBk;
    [boxImage addSubview:underlineImage];
    
    NSArray *iconArray = [[NSArray alloc]initWithObjects:@"LocationIcon.png",@"contactIcon.png",@"SchedIcon.png", nil];
    
    for(int i = 0; i < 3 ; i++) {
        underlineImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], labelY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:199/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
        underlineImage.backgroundColor = ColorGrayBk;
        [boxImage addSubview:underlineImage];
        
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], labelY +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
        iconImage.image = [UIImage imageNamed:[iconArray objectAtIndex:i]];
        [boxImage addSubview:iconImage];
        
        UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], labelY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:850/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:199/3 currentSupSize:SCREEN_HEIGHT])];
        [infoLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
        infoLabel.textColor = ColorFFFFFF;
        infoLabel.backgroundColor = [UIColor clearColor];
        infoLabel.textAlignment = NSTextAlignmentLeft;
        [boxImage addSubview:infoLabel];
        
        if(i == 0) {
            infoLabel.text =  [jsonInfo valueForKey:@"address"];
            infoLabel.numberOfLines = 2;
            iconImage.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], labelY +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:55/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT]);
            labelY = labelY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:199/3 currentSupSize:SCREEN_HEIGHT];
        }
        else if(i == 1) {
            infoLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH],labelY , [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:700/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:199/3 currentSupSize:SCREEN_HEIGHT]);
            infoLabel.text =  [jsonInfo valueForKey:@"phone_no"];

            UIButton *selectButton     =   [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:920/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:121/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
            [selectButton setBackgroundImage:[UIImage imageNamed:@"call action.png"] forState:UIControlStateNormal];
            [selectButton setBackgroundImage:[UIImage imageNamed:@"call actionPressed.png"] forState:UIControlStateHighlighted];
            selectButton.backgroundColor = [UIColor clearColor];
            [selectButton addTarget:self action:@selector(callMethodClicked) forControlEvents:UIControlEventTouchUpInside];
            [boxImage addSubview:selectButton];
            labelY = labelY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(38*[dayArray count]) currentSupSize:SCREEN_HEIGHT];
        }
        else {
            int labelRepeatY = 0;
            labelRepeatY = labelRepeatY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:199/3 currentSupSize:SCREEN_HEIGHT] +  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:199/3 currentSupSize:SCREEN_HEIGHT];
            
            iconImage.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], labelRepeatY +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]);
            iconImage.image = [UIImage imageNamed:[iconArray objectAtIndex:i]];
            
            int lablYForTime = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1280/3 currentSupSize:SCREEN_HEIGHT]    ;
            for (int i = 0; i < [dayArray count]; i++) {
                UILabel *dayLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:370/3 currentSupSize:SCREEN_WIDTH], lablYForTime, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:150/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
                
                [dayLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];
                dayLabel.textColor = ColorFFFFFF;
                dayLabel.backgroundColor = [UIColor clearColor];
                dayLabel.textAlignment = NSTextAlignmentLeft;
                dayLabel.text = [NSString stringWithFormat:@"%@",[[dayArray objectAtIndex:i] substringToIndex:3]];
                [self addSubview:dayLabel];
                
                UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:530/3 currentSupSize:SCREEN_WIDTH], lablYForTime, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:650/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
                [infoLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];
                infoLabel.textColor = ColorFFFFFF;
                infoLabel.backgroundColor = [UIColor clearColor];
                infoLabel.textAlignment = NSTextAlignmentLeft;
                infoLabel.text = [NSString stringWithFormat:@"%@ - %@",[startTimeArray objectAtIndex:i],[endTimeArray objectAtIndex:i]];

                [self addSubview:infoLabel];
                
                lablYForTime = lablYForTime + 70/3;
            }
        }
    }
}

- (void)callMethodClicked {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNo]]){
        
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",phoneNo] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
        
    }else{
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your device doesn't support call facility!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
    }
}

@end
