//
//  DispensaryView.h
//  Storx
//
//  Created by clicklabs on 12/30/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@interface DispensaryView : UIView {
    UIImageView *boxImage;
    UIImageView *picImageView;
    UILabel *nameLabel;
    NSString *phoneNo;
}

- (void)dispensaryInfoAPICall :(NSDictionary *)dict;

@end
