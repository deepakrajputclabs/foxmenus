//
//  DetailView.h
//  Storx
//
//  Created by clicklabs on 12/23/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "SKSTableView.h"

@protocol detailProtocol <NSObject>
- (void)backMethod;
- (void)infoMethod:(NSDictionary *)dict;
- (void)requestMethodId:(NSMutableArray *)idMutArray
           nameMutArray:(NSMutableArray *)nameMutArray
          priceMutArray:(NSMutableArray *)priceMutArray
        qtyTypeMutArray:(NSMutableArray *)qtyTypeMutArray
       qtyValueMutArray:(NSMutableArray *)qtyValueMutArray
              imaeArray:(NSMutableArray *)imageArray
        dispensaryArray:(NSDictionary *)dispensaryArray;
@end

@interface DetailView : UIView <SKSTableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSDictionary *json1;
}
- (void)dispensaryView :(NSString *)dispensaryId
         dispensaryName:(NSString *)dispensaryName
              ordertype:(NSString *)orderType;
- (void)dispensaryReturn :(NSDictionary *)json;

@property (weak) id <detailProtocol> detailDelegate;
@property(strong, nonatomic) UIImageView *bkImageView;
@property(strong, nonatomic) NSArray *productArray;
@property(strong, nonatomic) NSArray *menuNameArray;
@property(strong, nonatomic) NSString *orderType;


@property(strong, nonatomic) NSDictionary *dispensaryInfo;

@property(strong, nonatomic) NSMutableArray *itemIdArray;
@property(strong, nonatomic) NSMutableArray *itemImageArray;
@property(strong, nonatomic) NSMutableArray *itemPriceArray;
@property(strong, nonatomic) NSMutableArray *itemQtyArray;
@property(strong, nonatomic) NSMutableArray *itemNameArray;
@property(strong, nonatomic) NSMutableArray *itemDescriptionArray;
@property(strong, nonatomic) NSMutableArray *itemDefaultPriceArray;
@property(strong, nonatomic) NSMutableArray *itemDefaultQtyTypeArray;
@property(strong, nonatomic) NSMutableArray *itemDefaultQtyValueArray; //Default
@property(strong, nonatomic) NSMutableArray *itemDiscountArray;

@property(strong, nonatomic) NSMutableArray *checkMutArray;
@property(strong, nonatomic) NSMutableArray *expandMutArray;
@property(strong, nonatomic) NSMutableArray *rowsCountMutArray;

// Final Values Sent To RequestController
@property(strong, nonatomic) NSMutableArray *requestIdArray;
@property(strong, nonatomic) NSMutableArray *requestImageArray;
@property(strong, nonatomic) NSMutableArray *requestPriceArray;
@property(strong, nonatomic) NSMutableArray *requestQtyTypeArray;
@property(strong, nonatomic) NSMutableArray *requestQtyValueArray;
@property(strong, nonatomic) NSMutableArray *requestNameArray;
@end
