//
//  HomeScreenView.h
// Storx
//
//  Created by ClickLabs26 on 11/9/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GoogleMapView.h"
#import "ASIHTTPRequestDelegate.h"
#import "ASIFormDataRequest.h"
#import "FilterView.h"

#define kSourceSansProRegular @"SourceSansPro"
#define kSourceSansProSemibold @"SourceSansPro"

@protocol homeScreenViewProtocol <NSObject>
@optional
- (void)detailDispensary :(NSString *)dispensaryId
           dispensaryName:(NSString *)dispensaryName
                orderType:(NSString *)orderType;

- (void)filterMethod;
- (void)menuButtonAction;
- (void)searchLocationSelected:(float)latitudeValue
                     longValue:(float)longitudeValue startDistance:(NSString *)startDis endDistance:(NSString *)endDis;
@end

@interface HomeScreenView : UIView<GMSMapViewDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ASIHTTPRequestDelegate,UIGestureRecognizerDelegate>
{
    ASIFormDataRequest *request;
    UITableView *dropDowntableView;
    UIImageView *pinImageView;
}
@property(nonatomic,retain)CLLocation *getCurrentLocation;
@property (weak) id <homeScreenViewProtocol> homeScreenViewDelegate;
@property(nonatomic)BOOL isMapCreated;

// Map properties
@property(strong, nonatomic) NSArray *addressArray;
@property(strong, nonatomic) NSArray *dispensaryNameArray;
@property(strong, nonatomic) NSArray *dispensaryImageArray;
@property(strong, nonatomic) NSArray *distanceArray;
@property(strong, nonatomic) NSArray *idArray;
@property(strong, nonatomic) NSArray *phoneNoArray;
@property(strong, nonatomic) NSArray *latArray;
@property(strong, nonatomic) NSArray *longArray;
@property(strong, nonatomic) NSArray *ratingArray;
@property(strong, nonatomic) NSArray *typeArray;
@property(strong, nonatomic) UIImageView *noDispensaryBk;
//Search
@property(strong, nonatomic) NSMutableArray *placeIdArray;
@property(strong, nonatomic) NSMutableArray *descArray;
@property(strong, nonatomic) NSArray *startTimeArray;
@property(strong, nonatomic) NSArray *endTimeArray;

@property(strong, nonatomic) NSString *curLatString;
@property(strong, nonatomic) NSString *curLongString;

@property(strong, nonatomic) UIImageView *bkImageView;
@property(strong, nonatomic) UITableView *listTableView;

- (void)dataRecieved:(NSDictionary *)json;
- (void)addingMarkerToParticularPlace :(CLLocation *)location;
- (void)clearMarker;
- (void)removeView;
-(void)removeMap;

@end
