//
//  HomeScreenView.m
// Storx
//
//  Created by ClickLabs26 on 11/9/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.

#import "HomeScreenView.h"
#import "Transition.h"


@implementation HomeScreenView
{
    UIButton *deliveryBtn;
    UIButton *pickUpBtn;
    UIButton *mapCheckInBtn;
    UIButton *currentButton;
    
    float latitude;
    float longitude;
    
    float lastScale;
    float lastZoomLevel;
    
    GMSMapView *mapView_;
    GMSMarker *marker;
    
    NSMutableArray *customerInfoMutArray;
    UILabel *textToggleLabel;
    NSString *todayDayAsString;
    
    UIView *topHeaderView;
    UIView *searchView;
    UITextField *searchTextField;
    
    NSString *locationString;
    NSString *todayAsString;
    
    int whiteSpaceCount2;
    int getTypeValue;
    int getIndex;

    int getOrderId;
    UIActivityIndicatorView *activityIndicatorView;
}

@synthesize homeScreenViewDelegate;
@synthesize getCurrentLocation;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (mapView_) {
            [mapView_ clear];
            [mapView_ removeFromSuperview];
        }
        
        self.bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        self.bkImageView.image = [UIImage imageNamed:@"bg.png"];
        self.bkImageView.userInteractionEnabled = YES;
        [self addSubview:self.bkImageView];

        [self createViewScreen];
    }
    return self;
}

#pragma mark - HeaderView

- (void)createViewScreen{
    
    UIButton *menuBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
    [menuBtn setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
    menuBtn.backgroundColor = [UIColor clearColor];
    menuBtn.exclusiveTouch = YES;
    [menuBtn addTarget:self action:@selector(menuButtonMethod) forControlEvents:UIControlEventTouchUpInside];
    [self.bkImageView addSubview:menuBtn];
    
    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
    storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
    [self.bkImageView addSubview:storxImageView];
    
    UIButton *filterBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1109/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:83/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:120/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    [filterBtn setImage:[UIImage imageNamed:@"filter.png"] forState:UIControlStateNormal];
    [filterBtn setImageEdgeInsets:UIEdgeInsetsMake([[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH])];
    filterBtn.backgroundColor = [UIColor clearColor];
    [filterBtn addTarget:self action:@selector(filterMethodCall) forControlEvents:UIControlEventTouchUpInside];
    [self.bkImageView addSubview:filterBtn];
    
    // Search View
    topHeaderView = [[UIView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:25/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:265/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:862/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    topHeaderView.layer.cornerRadius = topHeaderView.frame.size.height/2;
    topHeaderView.backgroundColor = ColorGrayBk;
    [self.bkImageView addSubview:topHeaderView];
    
    UIImageView *searchIconImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:35/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:38/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:45/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:45/3 currentSupSize:SCREEN_HEIGHT])];
    searchIconImageView.image = [UIImage imageNamed:@"Search.png"];
    [topHeaderView addSubview:searchIconImageView];
    
    searchTextField = [[UITextField alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:102/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:862/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:102/3 currentSupSize:SCREEN_WIDTH] , [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    searchTextField.font = [UIFont fontWithName:FontLight size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
    searchTextField.textColor = [UIColor blackColor];
    searchTextField.backgroundColor = [UIColor clearColor];
    searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    searchTextField.keyboardType = UIKeyboardTypeDefault;
    searchTextField.returnKeyType = UIReturnKeySearch;
    searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    searchTextField.delegate = self;
    searchTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    searchTextField.placeholder = @"Search a location";
    [topHeaderView addSubview:searchTextField];
    
    // Toggle View
    UIView *toggleView = [[UIView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:920/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:265/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:292/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    toggleView.layer.cornerRadius = topHeaderView.frame.size.height/2;
    toggleView.backgroundColor = ColorGrayBk;
    [self.bkImageView addSubview:toggleView];
    
    textToggleLabel = [[UILabel alloc] init];
    mapCheckInBtn   = [[UIButton alloc] init];
 
    [self createMapView];
    
    searchView = [[UIView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:416/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    searchView.backgroundColor = ColorGrayBk;
    [self addSubview:searchView];
    searchView.hidden = YES;
    
    dropDowntableView = [[UITableView alloc] initWithFrame:CGRectMake(0, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT],  [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT] -  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:416/3 currentSupSize:SCREEN_HEIGHT]) style:UITableViewStylePlain];//569
    dropDowntableView.tag = 109;
    dropDowntableView.backgroundColor = [UIColor clearColor];
    dropDowntableView.delegate = self;
    dropDowntableView.dataSource = self;
    dropDowntableView.hidden = TRUE;
    [dropDowntableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [searchView addSubview:dropDowntableView];
    
    [self.listTableView removeFromSuperview];
    self.listTableView = nil;
    self.listTableView = [[UITableView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:416/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1800/3 currentSupSize:SCREEN_HEIGHT]) style:UITableViewStylePlain];
    self.listTableView.tag = 101;
    self.listTableView.dataSource=self;
    self.listTableView.delegate=self;
    self.listTableView.layer.borderWidth = 5;
    self.listTableView.layer.borderColor = [UIColor clearColor].CGColor;
    [self.listTableView setBackgroundView:nil];
    [self.listTableView setBackgroundColor:[UIColor clearColor]];
    [self.listTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self addSubview:self.listTableView];
    
    deliveryBtn.hidden = YES;
    pickUpBtn.hidden = YES;

    if([[NSUserDefaults standardUserDefaults] boolForKey:ToggleForList]) {
        mapView_.hidden = YES;
        currentButton.hidden = YES;
        self.listTableView.hidden = NO;
        pinImageView.hidden  = YES;
        
        textToggleLabel.text = [NSString stringWithFormat:@"List"];
        
        mapCheckInBtn.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:920/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:263/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:132/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:133/3 currentSupSize:SCREEN_HEIGHT]);
        
        textToggleLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(260/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(292/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT]);
    }
    else {
        mapView_.hidden = NO;
        self.listTableView.hidden = YES;
        pinImageView.hidden  = NO;

        textToggleLabel.text = [NSString stringWithFormat:@"Map"];
        
        mapCheckInBtn.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1084/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:263/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:132/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:133/3 currentSupSize:SCREEN_HEIGHT]);
        
        textToggleLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(292/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT]);
    }
  
    
    textToggleLabel.textAlignment = NSTextAlignmentCenter;
    textToggleLabel.textColor =  [UIColor blackColor];
    textToggleLabel.backgroundColor = [UIColor clearColor];
    textToggleLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
    [toggleView addSubview:textToggleLabel];
    
    [mapCheckInBtn setBackgroundImage:[UIImage imageNamed:@"knob.png"] forState:UIControlStateNormal];
    mapCheckInBtn.backgroundColor = [UIColor clearColor];
     [mapCheckInBtn addTarget:self action:@selector(toggleMethod) forControlEvents:UIControlEventTouchUpInside];
    mapCheckInBtn.userInteractionEnabled = YES;
    [self addSubview:mapCheckInBtn];
    
    UIButton *tapToggleButton = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:292/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    tapToggleButton.backgroundColor = [UIColor clearColor];
    tapToggleButton.exclusiveTouch = YES;
    tapToggleButton.userInteractionEnabled = YES;
    [tapToggleButton addTarget:self action:@selector(toggleMethod) forControlEvents:UIControlEventTouchUpInside];
    [toggleView addSubview:tapToggleButton];
}

#pragma mark - MapView

- (void)createMapView {

    self.isMapCreated = NO;
    GMSCameraPosition *camera;
    NSLog(@"getCurrentLocation %f,%f",getCurrentLocation.coordinate.latitude,getCurrentLocation.coordinate.longitude);
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:isCurrentLocationNow]) {
        camera = [GMSCameraPosition cameraWithLatitude:getCurrentLocation.coordinate.latitude //currentCoordinate.latitude
                                             longitude:getCurrentLocation.coordinate.longitude //currentCoordinate.longitude
                                                  zoom:12];
        
    }
    else {
        float searchLatitude  = [[[NSUserDefaults standardUserDefaults]objectForKey:searchedLatitude]floatValue];
        float searchLongitude = [[[NSUserDefaults standardUserDefaults]objectForKey:searchedLongitude]floatValue];
        camera = [GMSCameraPosition cameraWithLatitude:searchLatitude
                                             longitude:searchLongitude
                                                  zoom:12];
    }
    
    mapView_ = [[GoogleMapView shareCommonGoogleMap] allocateGoogleMapForCommonUseInMultipleClasses:camera withScreenSize:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:416/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1792/3 currentSupSize:SCREEN_HEIGHT])];
    mapView_.myLocationEnabled = YES;
    mapView_.settings.zoomGestures = NO;
    mapView_.settings.scrollGestures = YES;
    mapView_.settings.myLocationButton = NO;
    mapView_.delegate = self;
    [self addSubview: mapView_];
   
    mapView_.camera = camera;
    lastZoomLevel = 12.0;
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget: self action:@selector(didPan:)];
    mapView_.gestureRecognizers = @[panRecognizer];
    
    UITapGestureRecognizer * _doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapGestureRecognized:)];
    _doubleTapGestureRecognizer.numberOfTouchesRequired = 1;
    _doubleTapGestureRecognizer.numberOfTapsRequired = 2;
    [mapView_ addGestureRecognizer:_doubleTapGestureRecognizer];
    
    _doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapZoomOutRecognized:)];
    _doubleTapGestureRecognizer.numberOfTouchesRequired = 2;
    _doubleTapGestureRecognizer.numberOfTapsRequired = 2;
    [mapView_ addGestureRecognizer:_doubleTapGestureRecognizer];
    
    UIPinchGestureRecognizer * recognizer2 = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handleTickle:)];
    recognizer2.delegate = self;
    [mapView_ addGestureRecognizer:recognizer2];
  
    currentButton = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1080/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:486/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:102/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:102/3 currentSupSize:SCREEN_HEIGHT])];
    currentButton.backgroundColor = [UIColor clearColor];
    currentButton.alpha = 1;
    [currentButton setExclusiveTouch:TRUE];
    [currentButton setBackgroundImage:[UIImage imageNamed:@"my location.png"] forState:UIControlStateNormal];
    [currentButton addTarget:self action:@selector(gpsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:currentButton];
 
//    UIImageView *pinImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 264/3)/2 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(mapView_.frame.size.height - 264/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:264/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:264/3 currentSupSize:SCREEN_HEIGHT])];
    
    [pinImageView removeFromSuperview];
    pinImageView = nil;
    pinImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(self.frame.size.width/2-264/2)/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:((mapView_.frame.size.height)/2-264/2)/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:264/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:264/3 currentSupSize:SCREEN_HEIGHT])];
    pinImageView.image = [UIImage imageNamed:@"pin.png"];
    pinImageView.center = mapView_.center;
    pinImageView.frame=CGRectMake(pinImageView.frame.origin.x, pinImageView.frame.origin.y-15, pinImageView.frame.size.width, pinImageView.frame.size.height);
    pinImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:pinImageView];
    
    [activityIndicatorView removeFromSuperview];
    activityIndicatorView=nil;
    activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.backgroundColor=[UIColor clearColor];
    [activityIndicatorView setFrame:CGRectMake(pinImageView.center.x-5 , pinImageView.center.y-22, 10, 10)];
    [mapView_ addSubview:activityIndicatorView];
    
    self.isMapCreated = YES;
}

#pragma mark - Gestures

- (void)doubleTapZoomOutRecognized :(UIGestureRecognizer*)gestureRecognizer {
    
    if(lastZoomLevel<21.0&&lastZoomLevel>2.0) {
        
        lastZoomLevel = lastZoomLevel-1;
        [mapView_ animateToZoom:lastZoomLevel];
    }
    // lastScale = [gestureRecognizer scale];  // Sto
}

- (void)didPan:(UIPinchGestureRecognizer *)recognizer {
   
}

- (void)handleTickle:(UIPinchGestureRecognizer *)recognizer {
    //lastScale=0;
    if([(UIPinchGestureRecognizer*)recognizer state] == UIGestureRecognizerStateEnded) {
        lastScale = 0;
        mapView_.settings.scrollGestures=YES;
        return;
    }
    else{
        mapView_.settings.scrollGestures=NO;
    }
    
    CGFloat pinchscale = [(UIPinchGestureRecognizer*)recognizer scale];
    lastScale = pinchscale-1-lastScale;
    //NSLog(@"pinchscale %f",pinchscale);
    // NSLog(@"lastScale %f",lastScale);
    if(lastScale+lastZoomLevel<=21.0&&lastScale+lastZoomLevel>=2.0) {
        lastZoomLevel = lastScale/4+lastZoomLevel;
        [mapView_ animateToZoom:lastZoomLevel];
    }
    //NSLog(@"lastZoomLevel %f",lastZoomLevel);
}

- (void)doubleTapGestureRecognized:(UIGestureRecognizer*)gestureRecognizer {
    if(lastZoomLevel<21.0&&lastZoomLevel>=2.0) {
        lastZoomLevel = 1+lastZoomLevel;
        [mapView_ animateToZoom:lastZoomLevel];
    }
    // lastScale = [gestureRecognizer scale];  // Sto
}

#pragma mark - Remove Map View 

-(void) removeMap {
    if (mapView_) {
        [mapView_ clear];
        [mapView_ removeFromSuperview];
    }
}


#pragma mark : Toggle Action

- (void)toggleMethod {
    
    [self.noDispensaryBk removeFromSuperview];
    self.noDispensaryBk = nil;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:ToggleForList]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ToggleForList];
        
      textToggleLabel.text  = @"Map";

        mapView_.hidden           = NO;
        pickUpBtn.hidden          = YES;
        deliveryBtn.hidden        = YES;
        currentButton.hidden      = NO;
        self.listTableView.hidden = YES;
        pinImageView.hidden       = NO;

        mapCheckInBtn.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1084/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:263/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:132/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:133/3 currentSupSize:SCREEN_HEIGHT]);
        
        textToggleLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(292/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT]);
    }
    else {
        textToggleLabel.text  = @"List";

        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ToggleForList];
        mapView_.hidden           = YES;
        currentButton.hidden      = YES;
        deliveryBtn.hidden        = YES;
        pickUpBtn.hidden          = YES;
        pinImageView.hidden       = YES;

        self.listTableView.hidden = NO;
        [self.listTableView reloadData];

        if(self.idArray.count < 1) {
            [self.noDispensaryBk removeFromSuperview];
            self.noDispensaryBk = nil;
            [self noOrderView];
        }
        
        mapCheckInBtn.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:920/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:263/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:132/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:133/3 currentSupSize:SCREEN_HEIGHT]);
        
        textToggleLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(260/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(292/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT]);
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark : No Order View

- (void)noOrderView {
    [self.noDispensaryBk removeFromSuperview];
    self.noDispensaryBk = nil;
    
     if(self.idArray.count < 1) {
         self.noDispensaryBk =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:488/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT])];
         self.noDispensaryBk.image = [UIImage imageNamed:@"BaseBack.png"];
         [self addSubview:self.noDispensaryBk];
         
         UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:620/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT])];
         headerLabel.textAlignment = NSTextAlignmentCenter;
         headerLabel.textColor =  ColorFFFFFF;
         headerLabel.backgroundColor = [UIColor clearColor];
         headerLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
         headerLabel.text = @"No Restaurants Available!";
         [self.noDispensaryBk addSubview:headerLabel];
         
         [[CommonClass getSharedInstance] startFade:self.noDispensaryBk];
     }
}

#pragma mark : TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag == 109) // dropDowntableView
        return self.placeIdArray.count;
    else
       return self.idArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   if(tableView.tag == 109)
        return [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT];
    else
        return [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:435/3 currentSupSize:SCREEN_HEIGHT];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"cell";
    UITableViewCell *cell;
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:MyIdentifier];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else {
        for(UIImageView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
        [[cell.contentView viewWithTag:100] removeFromSuperview];
    }
    cell.backgroundColor =  [UIColor clearColor];
    
    UIImageView *baseCell = [[UIImageView alloc] init];
    baseCell.userInteractionEnabled = YES;
    baseCell.backgroundColor = ColorBlackBK;
    [cell.contentView addSubview:baseCell];

    if(tableView.tag == 109) {
        baseCell.backgroundColor = [UIColor clearColor];

        baseCell.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT]);
        baseCell.tag = indexPath.row + 9000;

        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
        headerLabel.textAlignment = NSTextAlignmentLeft;
        headerLabel.tag = 100;
        headerLabel.textColor =  [UIColor blackColor];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
        headerLabel.text = [NSString stringWithFormat:@"%@",[self.descArray objectAtIndex:indexPath.row]];
        [baseCell addSubview:headerLabel];
    }
    else {
        baseCell.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:375/3 currentSupSize:SCREEN_HEIGHT]);
        baseCell.tag = indexPath.row + 9000;

        UIImageView *picImageView = [[UIImageView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:253/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:253/3 currentSupSize:SCREEN_HEIGHT])];
        picImageView.userInteractionEnabled = YES;
        picImageView.tag = 100;
        picImageView.backgroundColor = [UIColor whiteColor];
        picImageView.layer.cornerRadius = picImageView.frame.size.height/2;
        picImageView.layer.masksToBounds = YES;
        picImageView.contentMode = UIViewContentModeScaleToFill;
        [picImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.dispensaryImageArray objectAtIndex:indexPath.row]]] placeholderImage:[UIImage imageNamed:DispensaryDefaultImage]];
        [baseCell addSubview:picImageView];
        
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:359/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:379/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT])];
        headerLabel.textAlignment = NSTextAlignmentLeft;
        headerLabel.textColor =  [UIColor whiteColor];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.font = [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        headerLabel.text = [NSString stringWithFormat:@"%@",[self.dispensaryNameArray objectAtIndex:indexPath.row]];
        [baseCell addSubview:headerLabel];
        //Get Label Size
        CGSize labelWidth = [[CommonClass getSharedInstance] getLabelSize:headerLabel.text
                                                                    width:headerLabel.frame.size.width
                                                                   height:headerLabel.frame.size.height
                                                                 fontSize:54/3];
        headerLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:379/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:labelWidth.width currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:66/3 currentSupSize:SCREEN_HEIGHT]);
        
        //Star ImageView
       /* UIImageView *starImageView = [[UIImageView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:379/3 + labelWidth.width + 20 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:47/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:45/3 currentSupSize:SCREEN_HEIGHT])];
        starImageView.tag = 100;
        starImageView.image = [UIImage imageNamed:@"fullStar.png"];
        [baseCell addSubview:starImageView];
        
        // Rate Label
       UILabel *rateLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(379/3 + labelWidth.width + 20 + [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:47/3 currentSupSize:SCREEN_WIDTH]) currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:100/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
        rateLabel.textAlignment = NSTextAlignmentCenter;
        rateLabel.textColor =  [UIColor whiteColor];
        rateLabel.backgroundColor = [UIColor clearColor];
        rateLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
        rateLabel.text = [NSString stringWithFormat:@"%@",[self.ratingArray objectAtIndex:indexPath.row]];
        [baseCell addSubview:rateLabel];*/
        
        // Icon ImageView
        UIImageView *iconImageView1 = [[UIImageView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:367/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:145/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:34/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:49/3 currentSupSize:SCREEN_HEIGHT])];
        iconImageView1.tag = 100;
        iconImageView1.image = [UIImage imageNamed:@"location.png"];
        [baseCell addSubview:iconImageView1];
        
        UIImageView *iconImageView2 =   [[UIImageView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:367/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:215/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:33/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:33/3 currentSupSize:SCREEN_HEIGHT])];
        iconImageView2.image = [UIImage imageNamed:@"business hrs.png"];
        [baseCell addSubview:iconImageView2];
        
        UIImageView *iconImageView3 =   [[UIImageView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:367/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:287/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT])];
        iconImageView3.image = [UIImage imageNamed:@"contact30.png"];
        [baseCell addSubview:iconImageView3];
        
        UILabel *subHeaderLabel;
        int subHeaderY = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:140/3 currentSupSize:SCREEN_HEIGHT];
        
        for (int i = 0;i < 3; i++) {
            
            subHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:428/3 currentSupSize:SCREEN_WIDTH], subHeaderY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:448/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:57/3 currentSupSize:SCREEN_HEIGHT])];
            subHeaderLabel.textAlignment = NSTextAlignmentLeft;
            subHeaderLabel.textColor =  [UIColor whiteColor];
            subHeaderLabel.backgroundColor = [UIColor clearColor];
            subHeaderLabel.font = [UIFont fontWithName:FontLight size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
            [baseCell addSubview:subHeaderLabel];
            if(i == 0) {
                subHeaderLabel.text = [NSString stringWithFormat:@"%@",[self.addressArray objectAtIndex:indexPath.row]];
            }
            else if(i == 1) {
                subHeaderLabel.text = [NSString stringWithFormat:@"%@ %@ - %@",todayAsString,[self.startTimeArray objectAtIndex:indexPath.row],[self.endTimeArray objectAtIndex:indexPath.row]];
            }
            else {
                subHeaderLabel.text = [NSString stringWithFormat:@"%@",[self.phoneNoArray objectAtIndex:indexPath.row]];
                subHeaderLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
            }
            
            subHeaderY = subHeaderY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:57/3 currentSupSize:SCREEN_HEIGHT] + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:10/3 currentSupSize:SCREEN_HEIGHT];
        }
        
        UIButton *arrowBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1185/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:162/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:27/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT])];
        arrowBtn.tag = 100;
        [arrowBtn setBackgroundImage:[UIImage imageNamed:@"nextArrow.png"] forState:UIControlStateNormal];
        arrowBtn.backgroundColor = [UIColor clearColor];
        [arrowBtn addTarget:self action:@selector(toggleMethod) forControlEvents:UIControlEventTouchUpInside];
        [baseCell addSubview:arrowBtn];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIImageView *thisView;

    if(tableView.tag == 109) {
        NSString *url1 =  [[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",[self.placeIdArray objectAtIndex:indexPath.row],SMGoogle_Browser_Search_Key]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url1]];
        request.tag = 900;
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else {
        thisView = (UIImageView *)[tableView viewWithTag:indexPath.row+9000];
        [thisView setBackgroundColor:ColorGreenButton];
        [self showActivityIndicaterr];
       
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FormMapView"];
        [self.homeScreenViewDelegate detailDispensary:[self.idArray objectAtIndex:indexPath.row]
                                       dispensaryName:[self.dispensaryNameArray objectAtIndex:indexPath.row]
                                            orderType:[self.typeArray objectAtIndex:indexPath.row]];
    }
    [self hideActivityIndicaterr];
    [thisView setBackgroundColor:ColorBlackBK];
}

#pragma mark - RemoveView

- (void)removeView {
    [self.noDispensaryBk removeFromSuperview];
    self.noDispensaryBk = nil;
    [[Transition getSharedInstance] imageView:searchView
                                 withDuration:1
                                          toX:0
                                         andY:SCREEN_HEIGHT];
    [self noOrderView];
    [self performSelector:@selector(hideKeyBoard) withObject:nil afterDelay:0.01];
    
}

#pragma mark - Refresh Control

- (void)handleRefresh:(UIRefreshControl*)refreshControl {
    [refreshControl beginRefreshing];
    
   // [homeScreenViewDelegate refreshProducts];
    [refreshControl endRefreshing];
}

#pragma mark - Filter

- (void)filterMethodCall {
    [homeScreenViewDelegate filterMethod];
}

#pragma mark - MenuButton

- (void)menuButtonMethod {
    [homeScreenViewDelegate menuButtonAction];
}

- (void)showKeyBoard {
    [searchTextField becomeFirstResponder];
}

- (void)hideKeyBoard {
    [searchTextField resignFirstResponder];
}

#pragma mark- Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.noDispensaryBk removeFromSuperview];
    self.noDispensaryBk = nil;
    searchView.hidden = NO;
    [[Transition getSharedInstance] imageView:searchView withDuration:0.8 toX:0 andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:416/3 currentSupSize:SCREEN_HEIGHT]];
    [self bringSubviewToFront:searchView];

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:searchTextField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    if (substring.length >1) {
        if ([string isEqualToString: [NSString stringWithFormat:@"\n"]])
        {
            [searchTextField resignFirstResponder];
            NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
            NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
            
            if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound)
            {
                locationString = searchTextField.text;
                whiteSpaceCount2 = 0;
                [self showActivityIndicaterr];
                [self performSelector:@selector(googleafterDelay) withObject:Nil afterDelay:0.2];
                return YES;
            }
            else
            {
                if ([string isEqualToString:@" "]) {
                    whiteSpaceCount2 = whiteSpaceCount2 + 1;
                    if (whiteSpaceCount2 >=2) {
                        return NO;
                    }
                    else {
                        locationString = searchTextField.text;
                        [self showActivityIndicaterr];
                        [self performSelector:@selector(googleafterDelay) withObject:Nil afterDelay:0.2];
                        return YES;
                    }
                }
                locationString = searchTextField.text;
                whiteSpaceCount2 = 0;
                
                [self showActivityIndicaterr];
                [self performSelector:@selector(googleafterDelay) withObject:Nil afterDelay:0.2];
            }
            return  TRUE;
        }
        
        if(textField == searchTextField)
        {
            if (textField.text.length == 0)
            {
                if ([string isEqualToString:@" "])
                    return NO;
            }
            else {
                [self googleafterDelay];
            }
        }
    }
      return YES;
}


#pragma mark - Search Cancel Button Actions

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    searchTextField.text = @"";
    
    [[Transition getSharedInstance] imageView:searchView
                                 withDuration:1
                                          toX:0
                                         andY:SCREEN_HEIGHT];
    
    [self performSelector:@selector(hideKeyBoard) withObject:nil afterDelay:0.01];
    return YES;
}

- (void)googleafterDelay {
    NSString *url1 = [[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&type=address&location=%@&radius=500&key=%@",searchTextField.text,[NSString stringWithFormat:@"%f,%f",latitude,longitude],SMGoogle_Browser_Search_Key]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url1]];
    [request setDelegate:self];
    [request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)new_request{
    
    NSData *responseData = [new_request responseData];
    NSError* error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions  error:&error];
    
    NSLog(@"json : %@",json);
    
    self.placeIdArray = nil;
    self.descArray = nil;
    
    if ([[json objectForKey:@"status"] isEqualToString:[NSString stringWithFormat:@"OVER_QUERY_LIMIT"] ] || [[json objectForKey:@"status"] isEqualToString:[NSString stringWithFormat:@"ZERO_RESULTS"]]) {
       
        dropDowntableView.hidden = TRUE;
        //noResultFoundLabel.hidden = false;
    }
    else {
        lastZoomLevel = 13.48;
        self.descArray      = [[NSMutableArray alloc] init];
        self.placeIdArray   = [[NSMutableArray alloc] init];
        
        if([[json objectForKey:@"result"] count] != 0) {
            
                [self.descArray       addObject:[[json objectForKey:@"result"] valueForKey:@"formatted_address"]];
                [self.placeIdArray    addObject:[[json objectForKey:@"result"] valueForKey:@"place_id"]];
                self.curLatString    = [[[[json objectForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lat"];
                self.curLongString   = [[[[json objectForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lng"];
            
            dropDowntableView.hidden = YES;
            searchTextField.text = @"";
            
            [[Transition getSharedInstance] imageView:searchView
                                         withDuration:1
                                                  toX:0
                                                 andY:SCREEN_HEIGHT];
            
            // Camera positioning
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.curLatString floatValue]
                                                 longitude:[self.curLongString floatValue] //currentCoordinate.longitude
                                                      zoom:12];
            mapView_.camera = camera;
            [self performSelector:@selector(hideKeyBoard) withObject:nil afterDelay:0.01];
        }
        else if([json objectForKey:@"predictions"]) {
            
            for (int i = 0; i < [[json objectForKey:@"predictions"] count]; i++) {
                [self.descArray       addObject:[[[json objectForKey:@"predictions"] valueForKey:@"description"]  objectAtIndex:i]];
                [self.placeIdArray    addObject:[[[json objectForKey:@"predictions"] valueForKey:@"place_id"]  objectAtIndex:i]];
            }
            dropDowntableView.hidden = NO;
            [dropDowntableView reloadData];
            [self bringSubviewToFront:dropDowntableView];

        }

        dropDowntableView.frame = CGRectMake(0, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT],  [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:((100/3) * self.placeIdArray.count) currentSupSize:SCREEN_HEIGHT]);
        
        if ((100/3) * self.placeIdArray.count >= SCREEN_HEIGHT){
            
            dropDowntableView.frame = CGRectMake(0, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT]);
        }
    }
    
    [request cancel];
    [request clearDelegatesAndCancel];
    [self performSelector:@selector(hideActivityIndicaterr) withObject:self afterDelay:0.7];
}

- (void)requestFailed:(ASIHTTPRequest *)new_request {
    [request cancel];
    [request clearDelegatesAndCancel];
    dropDowntableView.hidden = TRUE;
}

#pragma mark  Button Actions

- (void)featuredDealsButtonMethod {
    //[homeScreenViewDelegate featuredDealsButtonAction];
}

#pragma mark - PickUp And Delivery Action

- (void)pickUpMethod {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FormMapView"];
    [self.homeScreenViewDelegate detailDispensary:[NSString stringWithFormat:@"%d",getOrderId]
                                   dispensaryName:[self.dispensaryNameArray objectAtIndex:getIndex]
                                        orderType:[NSString stringWithFormat:@"%d",1]];
}

- (void)deliveryMethod {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FormMapView"];
    NSLog(@"%@ %d",self.dispensaryNameArray,getTypeValue);
    [self.homeScreenViewDelegate detailDispensary:[NSString stringWithFormat:@"%d",getOrderId]
                                   dispensaryName:[self.dispensaryNameArray objectAtIndex:getIndex]
                                        orderType:[NSString stringWithFormat:@"%d",0]];
}

#pragma mark : No Order View

- (void)noLocationSelectedView {
    
    UIImageView *bk =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bk.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.7];
    [self addSubview:bk];
    
   UIImageView *noDispensaryBk =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:486/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT])];
    noDispensaryBk.image = [UIImage imageNamed:@"BaseBack 2.png"];
    [bk addSubview:noDispensaryBk];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:620/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT])];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    headerLabel.textColor =  ColorFFFFFF;
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    headerLabel.text = @"Please Select A Restaurant first!";
    [noDispensaryBk addSubview:headerLabel];
    
    UIButton *crossBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [crossBtn setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
    [crossBtn setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
    crossBtn.backgroundColor = [UIColor clearColor];
    crossBtn.exclusiveTouch = YES;
    [crossBtn addTarget:self action:@selector(menuButtonMethod) forControlEvents:UIControlEventTouchUpInside];
    [bk addSubview:crossBtn];
}

- (void)clearMarker {
    if (mapView_) {
        [mapView_ clear];
    }
}

#pragma mark - Data recieved

- (void)dataRecieved:(NSDictionary *)json {
    NSLog(@"json: %@",json);
    [mapView_ clear];
    [pickUpBtn removeFromSuperview];
    pickUpBtn = nil;
    [deliveryBtn removeFromSuperview];
    deliveryBtn = nil;
    
    self.idArray                = [[json objectForKey:@"data"] valueForKey:@"id"];
    self.addressArray           = [[json objectForKey:@"data"] valueForKey:@"address"];
    self.dispensaryNameArray    = [[json objectForKey:@"data"] valueForKey:@"dispensary_name"];
    self.dispensaryImageArray   = [[json objectForKey:@"data"] valueForKey:@"dispensery_image"];
    self.phoneNoArray           = [[json objectForKey:@"data"] valueForKey:@"phone_no"];
    self.ratingArray            = [[json objectForKey:@"data"] valueForKey:@"rating"];
    
    self.typeArray              = [[json objectForKey:@"data"] valueForKey:@"type"];
    self.distanceArray          = [[json objectForKey:@"data"] valueForKey:@"distance"];
    self.latArray               = [[json objectForKey:@"data"] valueForKey:@"latitude"];
    self.longArray              = [[json objectForKey:@"data"] valueForKey:@"longitude"];


    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    [dateFormatter stringFromDate:[NSDate date]];
    todayAsString = [dateFormatter stringFromDate:[NSDate date]];
   
    self.startTimeArray = [[CommonClass getSharedInstance] convertTimeArray:[[json objectForKey:@"data"] valueForKey:@"start_time"] ];
    self.endTimeArray   = [[CommonClass getSharedInstance] convertTimeArray:[[json objectForKey:@"data"] valueForKey:@"end_time"]];
    
    for (int i=0; i<[[json objectForKey:@"data"] count]; i++) {
        
        float tempLat   = [[[[json objectForKey:@"data"] objectAtIndex:i] valueForKey:@"latitude"] floatValue];
        float tempLong  = [[[[json objectForKey:@"data"] objectAtIndex:i] valueForKey:@"longitude"] floatValue];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(tempLat ,tempLong);
        NSString *messageString = [NSString stringWithFormat:@"%@",[self.dispensaryNameArray objectAtIndex:i]];
        [self addMarker:position message:messageString type:i];
    }
    [self.noDispensaryBk removeFromSuperview];
    self.noDispensaryBk = nil;
    if([[NSUserDefaults standardUserDefaults] boolForKey:ToggleForList]) {
        [self.listTableView reloadData];
        
        if(self.idArray.count < 1)
            [self noOrderView];
    }
    [activityIndicatorView stopAnimating];
    [self hideActivityIndicaterr];
}

-(void)gpsButtonPressed:(UIButton*)sender {
    lastZoomLevel = 12.0;
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:isCurrentLocationNow];
    NSLog(@"%f",getCurrentLocation.coordinate.latitude);
    [self addingMarkerToParticularPlace:getCurrentLocation];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark -  AddMarker

-(void)addMarker:(CLLocationCoordinate2D)coordinate message:(NSString *)message type:(int)type{
    
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
    marker.title = message;
    marker.flat = YES;


    UIImage *image;
    if([[self.typeArray objectAtIndex:type] integerValue] == 0) // Delivery
        image = [UIImage imageNamed:@"delivery_pin@2x.png"];
    else if([[self.typeArray objectAtIndex:type] integerValue] == 1) // Pick up
        image = [UIImage imageNamed:@"pick_up_pin@2x.png"];
    else
        image = [UIImage imageNamed:@"both_pin@2x.png"]; // Both
    
    [marker setIcon:image]; // [self imageWithImage:image scaledToSize:CGSizeMake(image.size.width/2, image.size.height/2)]];
    marker.map  = mapView_;
}

- (void)addingMarkerToParticularPlace :(CLLocation *)location {
    
   GMSCameraPosition* camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                           longitude:location.coordinate.longitude
                                                                zoom:mapView_.camera.zoom];
   mapView_.camera=camera;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)gmsMarker {
    [mapView_ setSelectedMarker:gmsMarker];
    //NSLog(@"gmsMarker:%@",gmsMarker.title);
    //NSLog(@"TypeArray:%@",self.typeArray);

    getTypeValue = 0;
    getIndex = 0;
    for (int i = 0; i < [self.idArray count]; i++) {
        if([[self.dispensaryNameArray objectAtIndex:i] isEqualToString:gmsMarker.title]) {
            getTypeValue = [[self.typeArray objectAtIndex:i] intValue];
            getOrderId = [[self.idArray objectAtIndex:i] integerValue];
            getIndex = i;
            break;
        }
    }
    [pickUpBtn removeFromSuperview];
    pickUpBtn = nil;
    [deliveryBtn removeFromSuperview];
    deliveryBtn = nil;
    
    deliveryBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1827/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [deliveryBtn setBackgroundImage:[UIImage imageNamed:@"Delivery.png"] forState:UIControlStateNormal];
    deliveryBtn.backgroundColor = [UIColor clearColor];
    [deliveryBtn addTarget:self action:@selector(deliveryMethod) forControlEvents:UIControlEventTouchUpInside];
    
    pickUpBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2018/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [pickUpBtn setBackgroundImage:[UIImage imageNamed:@"pick up.png"] forState:UIControlStateNormal];
    pickUpBtn.backgroundColor = [UIColor clearColor];
    [pickUpBtn addTarget:self action:@selector(pickUpMethod) forControlEvents:UIControlEventTouchUpInside];
    
    if(![gmsMarker.title isEqualToString:@""]) {
        
        if(getTypeValue == 0) {
            deliveryBtn.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2018/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT]);
            [self addSubview:deliveryBtn];
        }
        else if(getTypeValue == 1) {
            pickUpBtn.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2018/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT]);
            [self addSubview:pickUpBtn];
        }
        else {
            [self addSubview:deliveryBtn];
            [self addSubview:pickUpBtn];
        }
    }
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    //[mapView_ clear];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    
    if(mapView.camera.target.latitude != 0 && mapView.camera.target.longitude != 0 && self.isMapCreated == YES) {
        [mapView_ clear];
        
        [activityIndicatorView startAnimating];
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:isCurrentLocationNow];
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%f",mapView.camera.target.latitude] forKey:searchedLatitude];
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%f",mapView.camera.target.longitude] forKey:searchedLongitude];
        [self.homeScreenViewDelegate searchLocationSelected:mapView.camera.target.latitude
                                                  longValue:mapView.camera.target.longitude
                                              startDistance:@""
                                                endDistance:@""];
        [self hideActivityIndicaterr];
    }
}

#pragma mark - loading indicator bar

- (void)showActivityIndicaterr {
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}

- (void)hideActivityIndicaterr {
    [SVProgressHUD dismiss];
}

@end
