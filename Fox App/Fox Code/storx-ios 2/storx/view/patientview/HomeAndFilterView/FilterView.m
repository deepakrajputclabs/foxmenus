//
//  FilterView.m
//  Storx
//
//  Created by clicklabs on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "FilterView.h"

@implementation FilterView
{
    UIImageView *bkImageView;
    UILabel *startLabel;
    UILabel *endLabel;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)filterView {
    
    [bkImageView removeFromSuperview];
    bkImageView = nil;
    
    bkImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.backgroundColor = [UIColor clearColor];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:40/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:183/3 currentSupSize:SCREEN_HEIGHT])];
    headerLabel.textAlignment   = NSTextAlignmentCenter;
    headerLabel.textColor       = [UIColor whiteColor];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
    headerLabel.text = @"Filter Restaurants";
    [bkImageView addSubview:headerLabel];
   
    // Distance Imageview
    UIImageView *sortByDistanceImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:243/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
    sortByDistanceImageView.backgroundColor = [UIColor colorWithRed:36/255.0f green:36/255.0f blue:36/255.0f alpha:1.0];
    sortByDistanceImageView.userInteractionEnabled = YES;
    [bkImageView addSubview:sortByDistanceImageView];
    
    NSString *stringRate2 = @"0 - 10 miles";
    UILabel *sortByDisLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:40/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT])];
    sortByDisLabel.textAlignment   = NSTextAlignmentLeft;
    sortByDisLabel.textColor       = [UIColor whiteColor];
    sortByDisLabel.backgroundColor = [UIColor clearColor];
    sortByDisLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    sortByDisLabel.text = [NSString stringWithFormat:@"Short by Distance: %@",stringRate2];
    [sortByDistanceImageView addSubview:sortByDisLabel];
    
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:sortByDisLabel.text];
    [title addAttribute:NSFontAttributeName value:[UIFont fontWithName:FontMedium  size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]] range:NSMakeRange(@"Short by Distance: ".length, stringRate2.length)];
    [title addAttribute:NSForegroundColorAttributeName value:ColorGreenButton range:NSMakeRange(@"Short by Distance: ".length, stringRate2.length)];
    sortByDisLabel.attributedText = title;
    
    [self slider2];
    
    // Filter Button
    UIButton *filterButton = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:216/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1177/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:640/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [filterButton setExclusiveTouch:TRUE];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterButton.png"] forState:UIControlStateNormal];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterPressedButton.png"] forState:UIControlStateHighlighted];
    [filterButton addTarget:self action:@selector(filterMethodClicked) forControlEvents:UIControlEventTouchUpInside];
    [bkImageView addSubview:filterButton];

}

- (void)slider2 {
    
    [startLabel removeFromSuperview];
    startLabel = nil;
    startLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:50/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:500/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:80/3 currentSupSize:SCREEN_WIDTH] , [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:140/3 currentSupSize:SCREEN_HEIGHT])];
    startLabel.textAlignment   = NSTextAlignmentCenter;
    startLabel.textColor       = [UIColor whiteColor];
    startLabel.backgroundColor = [UIColor clearColor];
    startLabel.font = [UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:50/3 currentSupSize:SCREEN_HEIGHT]];
    startLabel.text = [NSString stringWithFormat:@"0"];
    [bkImageView addSubview:startLabel];

    [rangeSlider removeFromSuperview];
    rangeSlider = nil;
     rangeSlider = [[NMRangeSlider alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:100/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:500/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:839/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:155/3 currentSupSize:SCREEN_HEIGHT])];
    rangeSlider.lowerValue      = 0.0;
    rangeSlider.upperValue      = 1.0;
    rangeSlider.minimumValue    = 0.0;
    rangeSlider.maximumValue    = 1.0;

    rangeSlider.trackImage      = [[CommonClass getSharedInstance] imageFromColor: [UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:1.0]];
    [rangeSlider addTarget:self action:@selector(labelSliderChanged:) forControlEvents:UIControlEventValueChanged];
    [bkImageView addSubview:rangeSlider];
    
    [endLabel removeFromSuperview];
    endLabel = nil;
    endLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:935/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:500/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:80/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:140/3 currentSupSize:SCREEN_HEIGHT])];
    endLabel.textAlignment   = NSTextAlignmentCenter;
    endLabel.textColor       = [UIColor whiteColor];
    endLabel.backgroundColor = [UIColor clearColor];
    endLabel.font            = [UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:50/3 currentSupSize:SCREEN_HEIGHT]];
    [bkImageView addSubview:endLabel];

    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"StartValueFilter"] integerValue] > 0) {
        rangeSlider.lowerValue  =  [self selectFloatValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"StartValueFilter"]];
        startLabel.text         = [[NSUserDefaults standardUserDefaults] valueForKey:@"StartValueFilter"];
    }
    else
        startLabel.text = @"0";
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"EndValueFilter"] integerValue] < 10) {
        rangeSlider.upperValue     = [self selectFloatValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"EndValueFilter"]];
        endLabel.text              = [[NSUserDefaults standardUserDefaults] valueForKey:@"EndValueFilter"];
    }
    else
        endLabel.text = @"10";
}

- (IBAction)labelSliderChanged:(NMRangeSlider*)sender {
    [self updateSliderLabels];
}

- (void) updateSetValuesSlider
{
    float value1 = (float)random()/RAND_MAX;
    float value2 = (float)random()/RAND_MAX;
    
    [rangeSlider setLowerValue:MIN(value1, value2) upperValue:MAX(value1, value2) animated:YES];
}

- (void) updateSliderLabels {
    startLabel.text = [self selectValue:[NSString stringWithFormat:@"%.1f", (float)rangeSlider.lowerValue]];
    endLabel.text = [self selectValue:[NSString stringWithFormat:@"%.1f", (float)rangeSlider.upperValue]];
}

#pragma mark - Filter Method call

- (void)filterMethodClicked {
    [[NSUserDefaults standardUserDefaults] setValue:startLabel.text forKey:@"StartValueFilter"];
    [[NSUserDefaults standardUserDefaults] setValue:endLabel.text forKey:@"EndValueFilter"];

    [self.filterViewDelegate searchLocationSelected:self.latString
                                           longValue:self.longString
                                      startDistance:startLabel.text
                                        endDistance:endLabel.text];
    [self.filterViewDelegate removeFilterScreen];
}

- (float) selectFloatValue:(NSString *)inputValue {
    for (int i = 0; i < 11; i ++) {
        if(i == 10) {
            return 1.0;
            break;
        }
        else if([[NSString stringWithFormat:@"%d",i] isEqualToString:inputValue]){
            return [[NSString stringWithFormat:@"0.%d",i] floatValue];
            break;
        }
    }
    return 0.0;
}

- (NSString *) selectValue:(NSString *)inputValue {
    for (int i = 0; i < 11; i ++) {
        if(i == 10) {
            return [NSString stringWithFormat:@"%d",i];
            break;
        }
        else if([[NSString stringWithFormat:@"0.%d",i] isEqualToString:inputValue]){
            return [NSString stringWithFormat:@"%d",i];
            break;
        }
    }
    return @"0";
}

@end
