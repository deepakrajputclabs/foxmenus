//
//  FilterView.h
//  Storx
//
//  Created by clicklabs on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "NMRangeSlider.h"

@protocol filterProtocol <NSObject>
- (void)searchLocationSelected:(float)latitudeValue
                     longValue:(float)longitudeValue startDistance:(NSString *)startDis endDistance:(NSString *)endDis;
- (void)removeFilterScreen;
@end

@interface FilterView : UIView
{
    NMRangeSlider* rangeSlider;
}
@property (weak) id <filterProtocol> filterViewDelegate;
@property(nonatomic) float latString;
@property(nonatomic) float longString;

- (void)filterView;
@property (weak, nonatomic) IBOutlet NMRangeSlider *standardSlider;

@end
