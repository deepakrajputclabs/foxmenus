//
//  ReminderHistoryView.h
// Storx
//
//  Created by clicklabs on 11/14/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "UIImageView+AFNetworking.h"
#import "CommonClass.h"

@protocol reminderMenuDelegate <NSObject>
- (void)menuButton;
@end

@protocol ViewOrderFromHistoryProtocol <NSObject>
- (void)viewOrder :(NSDictionary*)orderResponse
        isUpcoming:(BOOL)flag
        pickUpFlag:(NSString*)pickupOrnot;
@end

@interface ReminderHistoryView : UIView<UITableViewDataSource,UITableViewDelegate>
- (void)reminderHistoryServerCall:(NSDictionary*)reminderResponse;

@property (weak) id <reminderMenuDelegate> reminderProtocol;
@property (weak) id <ViewOrderFromHistoryProtocol> viewOrderFromHistoryDelegate;

// Ongoing
@property(strong, nonatomic) NSArray *dateArray;
@property(strong, nonatomic) NSArray *dispensaryImageArray;
@property(strong, nonatomic) NSArray *dispensaryNameArray;
@property(strong, nonatomic) NSArray *flagArray;
@property(strong, nonatomic) NSArray *itemsArray;
@property(strong, nonatomic) NSArray *messageArray;
@property(strong, nonatomic) NSArray *orderIdArray;
@property(strong, nonatomic) NSArray *priceArray;
@property(strong, nonatomic) NSArray *typeArray;

//UpComing And Past
@property(strong, nonatomic) NSArray *dateUPComingArray;
@property(strong, nonatomic) NSArray *dispensaryImageUPComingArray;
@property(strong, nonatomic) NSArray *dispensaryNameUPComingArray;
@property(strong, nonatomic) NSArray *flagUPComingArray;
@property(strong, nonatomic) NSArray *itemsUPComingArray;
@property(strong, nonatomic) NSArray *messageUPComingArray;
@property(strong, nonatomic) NSArray *orderIdUPComingIdArray;
@property(strong, nonatomic) NSArray *priceUPComingArray;
@property(strong, nonatomic) NSArray *typeUPComingArray;
@end
