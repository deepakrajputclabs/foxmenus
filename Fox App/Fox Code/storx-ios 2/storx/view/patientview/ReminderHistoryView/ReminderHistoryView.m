//
//  ReminderHistoryView.m
// Storx
//
//  Created by clicklabs on 11/14/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ReminderHistoryView.h"

@implementation ReminderHistoryView
{
    UIImageView *baseBkForBlackColor;
    NSDictionary *json;
    CGRect screenBounds;
    UITableView *onGoingTableView;
    UITableView *upComingTableView;
    UIScrollView *scroll;
    UIButton *upcomingButton;
    UIButton *pastButton;
    UIImageView *boxImage;
    //UIView *bgColorView;
    UIImageView *baseCell;
    int tableIdentifier;
    int pastTableH;
}
@synthesize viewOrderFromHistoryDelegate=_viewOrderFromHistoryDelegate;
@synthesize reminderProtocol=_reminderProtocol;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
       UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        bkImageView.image = [UIImage imageNamed:@"bg.png"];
        bkImageView.userInteractionEnabled = YES;
        [self addSubview:bkImageView];
        UIButton *menuBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
        [menuBtn setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
        [menuBtn setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
        menuBtn.backgroundColor = [UIColor clearColor];
        menuBtn.exclusiveTouch = YES;
        [menuBtn addTarget:self action:@selector(backButtonMethod) forControlEvents:UIControlEventTouchUpInside];
        [bkImageView addSubview:menuBtn];
        
        UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
        storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
        [bkImageView addSubview:storxImageView];
    }
    return  self;
}

- (void)backButtonMethod {
    [_reminderProtocol menuButton];
}

#pragma - ReturnResponse Server Call

- (void)reminderHistoryServerCall:(NSDictionary *)reminderResponse {
    
    for(UIView *removeSubview in scroll.subviews) {
        UIView *subviews=removeSubview;
        [subviews removeFromSuperview];
        subviews=nil;
    }
    [scroll removeFromSuperview];
    scroll=nil;
    
    json = reminderResponse;
    
    scroll = [[UIScrollView alloc]init];
    [scroll setFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:245/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT] - [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:245/3 currentSupSize:SCREEN_HEIGHT])];
    [scroll setBackgroundColor:[UIColor clearColor]];
    [scroll setScrollEnabled:YES];
    scroll.userInteractionEnabled = true;
    [self addSubview:scroll];
    
    upcomingButton = [[UIButton alloc]init];
    upcomingButton.exclusiveTouch = YES;
    [upcomingButton addTarget:self action:@selector(upcomingMethod:) forControlEvents:UIControlEventTouchUpInside];
    upcomingButton.userInteractionEnabled = YES;
    [upcomingButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorGreenButton] forState:UIControlStateNormal];
    [upcomingButton setTitle:[NSString stringWithFormat:@"Upcoming"] forState:UIControlStateNormal];
    upcomingButton.titleLabel.font = [UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:76/3 currentSupSize:SCREEN_HEIGHT]];
    [upcomingButton setTitleColor:ColorFFFFFF forState:UIControlStateNormal];
    [upcomingButton setTintColor:[UIColor clearColor]];
    [scroll  addSubview:upcomingButton];
    
    pastButton = [[UIButton alloc]init];
    pastButton.exclusiveTouch = YES;
    [pastButton addTarget:self action:@selector(pastMethod) forControlEvents:UIControlEventTouchUpInside];
    pastButton.userInteractionEnabled = YES;
    [pastButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorBlackBK] forState:UIControlStateNormal];
    [pastButton setTitle:[NSString stringWithFormat:@"Past"] forState:UIControlStateNormal];
    pastButton.titleLabel.font = [UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:76/3 currentSupSize:SCREEN_HEIGHT]];
    [pastButton setTitleColor:ColorFFFFFF forState:UIControlStateNormal];
    [pastButton setTintColor:[UIColor clearColor]];
    [scroll  addSubview:pastButton];

    CGRect tableframe;

    tableIdentifier = 0;
    int heightY = 0;

    if(json) {
        
        if([[json objectForKey:@"upcoming"]count] == 0 && [[json objectForKey:@"past"]count] == 0 && [[json objectForKey:@"on_going"]count] == 0) {
            [self errorView:@"No Orders History"];
            boxImage.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT]);
        }
        else if([[json objectForKey:@"on_going"]count] == 0) {
            
            upcomingButton.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:621/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT]);
            pastButton.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:621/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:623/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT]);
            
            heightY =  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT];
            
            //Creating tableView
            [self createTable:heightY];
        }
        else {
            
            self.dateArray = [[CommonClass getSharedInstance] UTCDateToLocalDateArray:[[json objectForKey:@"on_going"] valueForKey:@"date"]];

            self.dispensaryImageArray   = [[json objectForKey:@"on_going"] valueForKey:@"dispensery_image"];
            self.dispensaryNameArray    = [[json objectForKey:@"on_going"] valueForKey:@"dispensery_name"];
            self.flagArray              = [[json objectForKey:@"on_going"] valueForKey:@"flag"];
            self.itemsArray             = [[json objectForKey:@"on_going"] valueForKey:@"items"];
            self.messageArray           = [[json objectForKey:@"on_going"] valueForKey:@"message"];
            self.orderIdArray           = [[json objectForKey:@"on_going"] valueForKey:@"order_id"];
            self.priceArray             = [[json objectForKey:@"on_going"] valueForKey:@"price"];
            self.typeArray              = [[json objectForKey:@"on_going"] valueForKey:@"type"];
            
            UILabel *serviceLabel =[[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
            serviceLabel.textAlignment=NSTextAlignmentLeft;
            serviceLabel.textColor=ColorFFFFFF;
            serviceLabel.backgroundColor=ColorDarkBlackBK;
            serviceLabel.font=[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
            serviceLabel.text=@"   Service in progress";
            [scroll addSubview:serviceLabel];
            
            heightY = [[json objectForKey:@"on_going"]count] * 319/3;

            //Creating tableView OnGoing Orders
            tableframe=CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:heightY currentSupSize:SCREEN_HEIGHT]);
            onGoingTableView = [[UITableView alloc] initWithFrame:tableframe style:UITableViewStylePlain];
            onGoingTableView.delegate = self;
            onGoingTableView.dataSource = self;
            onGoingTableView.tag=500;
            [onGoingTableView setBackgroundColor:[UIColor clearColor]];
            [onGoingTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            onGoingTableView.clipsToBounds=YES;
            onGoingTableView.scrollEnabled = NO;
            [scroll addSubview:onGoingTableView];
            
            heightY = heightY + 100/3;
            upcomingButton.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:heightY currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:621/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT]);
            pastButton.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:621/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:heightY currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:623/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT]);
            
            heightY = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:heightY currentSupSize:SCREEN_HEIGHT] + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT];
            
            //Creating tableView
            [self createTable:heightY];
        }
        
        scroll.scrollEnabled = YES;
        screenBounds = [[UIScreen mainScreen] bounds];
        int scrollH  = heightY + [[json objectForKey:@"upcoming"]count] * [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:319/3 currentSupSize:SCREEN_HEIGHT];
        scroll.contentSize = CGSizeMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], scrollH);
    }
}

#pragma mark - UpComing Function

- (void)createTable:(int)tabelY {
    tableIdentifier = 0;
    pastTableH = tabelY;
    NSUInteger frameCountForPastTable;
    
    if([[json objectForKey:@"upcoming"]count] == 0 && [[json objectForKey:@"past"]count] == 0) {
        [self errorView:@"No Upcoming and Past Orders"];
        CGRect tableframe = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],tabelY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT]);
        boxImage.frame = tableframe;
    }
    else {
        
        if([[json objectForKey:@"upcoming"]count] >= 1 ) {
            frameCountForPastTable = [[json objectForKey:@"upcoming"] count] * 319/3;
            
            [upcomingButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorGreenButton] forState:UIControlStateNormal];
            [pastButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorBlackBK] forState:UIControlStateNormal];
            
            self.dateUPComingArray = [[CommonClass getSharedInstance] UTCDateToLocalDateArray:[[json objectForKey:@"upcoming"] valueForKey:@"date"]];
            self.dispensaryImageUPComingArray   = [[json objectForKey:@"upcoming"] valueForKey:@"dispensery_image"];
            self.dispensaryNameUPComingArray    = [[json objectForKey:@"upcoming"] valueForKey:@"dispensery_name"];
            self.flagUPComingArray              = [[json objectForKey:@"upcoming"] valueForKey:@"flag"];
            self.itemsUPComingArray             = [[json objectForKey:@"upcoming"] valueForKey:@"items"];
            self.messageUPComingArray           = [[json objectForKey:@"upcoming"] valueForKey:@"message"];
            self.orderIdUPComingIdArray         = [[json objectForKey:@"upcoming"] valueForKey:@"order_id"];
            self.priceUPComingArray             = [[json objectForKey:@"upcoming"] valueForKey:@"price"];
            self.typeUPComingArray              = [[json objectForKey:@"upcoming"] valueForKey:@"type"];
        }
        else {
            frameCountForPastTable = [[json objectForKey:@"past"] count] * 319/3;
            
            [upcomingButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorBlackBK] forState:UIControlStateNormal];
            [pastButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorGreenButton] forState:UIControlStateNormal];
            
            self.dateUPComingArray              = [[json objectForKey:@"past"] valueForKey:@"date"];
            self.dispensaryImageUPComingArray   = [[json objectForKey:@"past"] valueForKey:@"dispensery_image"];
            self.dispensaryNameUPComingArray    = [[json objectForKey:@"past"] valueForKey:@"dispensery_name"];
            self.flagUPComingArray              = [[json objectForKey:@"past"] valueForKey:@"flag"];
            self.itemsUPComingArray             = [[json objectForKey:@"past"] valueForKey:@"items"];
            self.messageUPComingArray           = [[json objectForKey:@"past"] valueForKey:@"message"];
            self.orderIdUPComingIdArray         = [[json objectForKey:@"past"] valueForKey:@"order_id"];
            self.priceUPComingArray             = [[json objectForKey:@"past"] valueForKey:@"price"];
            self.typeUPComingArray              = [[json objectForKey:@"past"] valueForKey:@"type"];
        }
        
        CGRect tableframe = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],tabelY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:frameCountForPastTable currentSupSize:SCREEN_HEIGHT]);
        upComingTableView = [[UITableView alloc] initWithFrame:tableframe style:UITableViewStylePlain];
        upComingTableView.delegate = self;
        upComingTableView.dataSource = self;
        upComingTableView.layer.cornerRadius = 0;
        upComingTableView.tag = 500;
        [upComingTableView setBackgroundColor:[UIColor clearColor]];
        [upComingTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        upComingTableView.scrollEnabled = NO;
        [scroll addSubview:upComingTableView];
    }
}

- (void)upcomingMethod:(int)tabelY {
    tableIdentifier = 0;
    upComingTableView.hidden = NO;

    [upcomingButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorGreenButton] forState:UIControlStateNormal];
    [pastButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorBlackBK] forState:UIControlStateNormal];

    pastTableH = tabelY;
    [self nilArrayAndErrorView];

    if([[json objectForKey:@"upcoming"]count]==0) {
        
        [self errorView:@"No Upcoming Orders"];
        
        CGRect tableframe = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],tabelY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT]);
        boxImage.frame = tableframe;
    }
    else {
        
        self.dateUPComingArray = [[CommonClass getSharedInstance] UTCDateToLocalDateArray:[[json objectForKey:@"upcoming"] valueForKey:@"date"]];
        self.dispensaryImageUPComingArray   = [[json objectForKey:@"upcoming"] valueForKey:@"dispensery_image"];
        self.dispensaryNameUPComingArray    = [[json objectForKey:@"upcoming"] valueForKey:@"dispensery_name"];
        self.flagUPComingArray              = [[json objectForKey:@"upcoming"] valueForKey:@"flag"];
        self.itemsUPComingArray             = [[json objectForKey:@"upcoming"] valueForKey:@"items"];
        self.messageUPComingArray           = [[json objectForKey:@"upcoming"] valueForKey:@"message"];
        self.orderIdUPComingIdArray         = [[json objectForKey:@"upcoming"] valueForKey:@"order_id"];
        self.priceUPComingArray             = [[json objectForKey:@"upcoming"] valueForKey:@"price"];
        self.typeUPComingArray              = [[json objectForKey:@"upcoming"] valueForKey:@"type"];
        
        [upComingTableView reloadData];
    }
}

#pragma mark - ErrorView

- (void)errorView :(NSString *)stringError {
    
    [boxImage removeFromSuperview];
    boxImage = nil;
    boxImage = [[UIImageView alloc]init];
    boxImage.backgroundColor = ColorBlackBK;
    [scroll addSubview:boxImage];
    
    UIImageView *noOrderImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:440/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:363/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:363/3 currentSupSize:SCREEN_HEIGHT])];
    noOrderImage.image = [UIImage imageNamed:@"no_order_icon.png"];
    noOrderImage.backgroundColor = [UIColor clearColor];
    [boxImage addSubview:noOrderImage];
    
    UILabel *noOrderTodayLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:513/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    noOrderTodayLabel.text = stringError;
    noOrderTodayLabel.textColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
    [noOrderTodayLabel setFont:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
    noOrderTodayLabel.textAlignment = NSTextAlignmentCenter;
    [boxImage addSubview:noOrderTodayLabel];
}

#pragma mark - UpComing Function

- (void)pastMethod {
    upComingTableView.hidden = NO;

    tableIdentifier = 1;
    [self nilArrayAndErrorView];
    [upcomingButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorBlackBK] forState:UIControlStateNormal];
    [pastButton setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorGreenButton] forState:UIControlStateNormal];
    
    if([[json objectForKey:@"past"]count]==0) {
        [self errorView:@"No Past Orders"];
        CGRect tableframe = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],pastTableH, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT]);
        boxImage.frame = tableframe;
    }
    else {
        self.dateUPComingArray              = [[json objectForKey:@"past"] valueForKey:@"date"];
        self.dispensaryImageUPComingArray   = [[json objectForKey:@"past"] valueForKey:@"dispensery_image"];
        self.dispensaryNameUPComingArray    = [[json objectForKey:@"past"] valueForKey:@"dispensery_name"];
        self.flagUPComingArray              = [[json objectForKey:@"past"] valueForKey:@"flag"];
        self.itemsUPComingArray             = [[json objectForKey:@"past"] valueForKey:@"items"];
        self.messageUPComingArray           = [[json objectForKey:@"past"] valueForKey:@"message"];
        self.orderIdUPComingIdArray         = [[json objectForKey:@"past"] valueForKey:@"order_id"];
        self.priceUPComingArray             = [[json objectForKey:@"past"] valueForKey:@"price"];
        self.typeUPComingArray              = [[json objectForKey:@"past"] valueForKey:@"type"];
        
        [upComingTableView reloadData];
    }
}

- (void)nilArrayAndErrorView {
    [boxImage removeFromSuperview];
    boxImage = nil;
    self.dateUPComingArray = nil;
    self.dispensaryImageUPComingArray = nil;
    self.dispensaryNameUPComingArray = nil;
    self.flagUPComingArray = nil;
    self.itemsUPComingArray = nil;
    self.messageUPComingArray = nil;
    self.orderIdUPComingIdArray = nil;
    self.priceUPComingArray = nil;
    self.typeUPComingArray = nil;
}

- (void)watchButtonAction {
    
}

- (void)remindeMeButtonAction {
    
}

- (void)calenderButtonAction {
    
}

#pragma mark - TableView Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:319/3 currentSupSize:SCREEN_HEIGHT];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView==onGoingTableView) {
        return [[json objectForKey:@"on_going"]count];
    }
    else if (tableView==upComingTableView) {
        
        if(tableIdentifier == 0)
            return [[json objectForKey:@"upcoming"]count];
        else
            return [[json objectForKey:@"past"]count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;
    cell=NULL;
    if(cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else {
        for(UIImageView *subview in [baseCell subviews]) {
            [subview removeFromSuperview];
        }
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    baseCell = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:319/3 currentSupSize:SCREEN_HEIGHT])];
    baseCell.userInteractionEnabled = YES;
    
    baseBkForBlackColor = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:280/3 currentSupSize:SCREEN_HEIGHT])];
    baseBkForBlackColor.userInteractionEnabled = YES;
    baseBkForBlackColor.layer.cornerRadius = 6.0;
    baseBkForBlackColor.backgroundColor = ColorBlackBK;
    [baseCell addSubview:baseBkForBlackColor];
    
    UIImageView *picImageView = [[UIImageView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(285/3 - 178/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:178/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:178/3 currentSupSize:SCREEN_HEIGHT])];
    picImageView.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:178/6 currentSupSize:SCREEN_HEIGHT];
    picImageView.layer.masksToBounds = YES;
    picImageView.layer.borderColor = ColorGreenButton.CGColor;
    picImageView.layer.borderWidth = 1.0;
    picImageView.tag=101;
    [baseBkForBlackColor addSubview:picImageView];
    
    UILabel *desLabel      = [[UILabel alloc]init];
    desLabel.textColor     = ColorFFFFFF;
    desLabel.backgroundColor = [UIColor clearColor];
    desLabel.numberOfLines = 0;
    desLabel.tag= 102;
    desLabel.textAlignment = NSTextAlignmentLeft;
    [desLabel setFont:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    [baseBkForBlackColor addSubview:desLabel];
    
    UIImageView *typeImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:150/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:160/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:68/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:68/3 currentSupSize:SCREEN_HEIGHT])];
    typeImage.tag = 104;
    [baseBkForBlackColor addSubview:typeImage];
    
    UIImageView *nextArrow = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1102/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(281/3 - 60/3)/2 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:45/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
    nextArrow.image = [UIImage imageNamed:@"nextArrow.png"];
    nextArrow.tag = 105;
    [baseBkForBlackColor addSubview:nextArrow];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];

    if(tableView == onGoingTableView) {
        [cell.contentView addSubview:baseCell];
        baseBkForBlackColor.tag = indexPath.row + 1000;

        if([[self.typeArray objectAtIndex:indexPath.row] integerValue] == 0)
            typeImage.image = [UIImage imageNamed:@"delivery_icon.png"];
        else if([[self.typeArray objectAtIndex:indexPath.row] integerValue] == 1)
            typeImage.image = [UIImage imageNamed:@"pickup_icon.png"];
        else if([[self.typeArray objectAtIndex:indexPath.row] integerValue] == 2) // Both
            typeImage.image = [UIImage imageNamed:@"location_order.png"];
        
        [picImageView setImageWithURL:[NSURL URLWithString:[self.dispensaryImageArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:DispensaryDefaultImage]];

        desLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:266/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:45/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:776/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT]);
        
        NSString *str1 = [self.dispensaryNameArray objectAtIndex:indexPath.row];
        NSString *str2;
        
        if([[self.itemsArray objectAtIndex:indexPath.row] integerValue] == 1)
            str2 = [NSString stringWithFormat:@"%@ item . INR %@",[self.itemsArray objectAtIndex:indexPath.row],[self.priceArray objectAtIndex:indexPath.row]];
        else
            str2 = [NSString stringWithFormat:@"%@ items . INR %@",[self.itemsArray objectAtIndex:indexPath.row],[self.priceArray objectAtIndex:indexPath.row]];

        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",str1,str2]];
        [style setLineSpacing:4];
        [attributedString addAttribute:NSParagraphStyleAttributeName
                                 value:style
                                 range:NSMakeRange(0, str2.length)];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:58/3 currentSupSize:SCREEN_HEIGHT]]
                                 range:NSMakeRange(str1.length+1, str2.length)];
        desLabel.attributedText = attributedString;
    }
    else {
        [cell.contentView addSubview:baseCell];

        baseBkForBlackColor.tag = indexPath.row + 5000;

        if([[self.typeUPComingArray objectAtIndex:indexPath.row] integerValue] == 0) {
            typeImage.image = [UIImage imageNamed:@"delivery_icon.png"];
        }
        else if([[self.typeUPComingArray objectAtIndex:indexPath.row] integerValue] == 1) {
            typeImage.image = [UIImage imageNamed:@"pickup_icon.png"];
        }
        else if([[self.typeUPComingArray objectAtIndex:indexPath.row] integerValue] == 2) { // Both
            typeImage.image = [UIImage imageNamed:@"location_order.png"];
        }
        
        [picImageView setImageWithURL:[NSURL URLWithString:[self.dispensaryImageUPComingArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:DispensaryDefaultImage]];

          desLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:266/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:776/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:270/3 currentSupSize:SCREEN_HEIGHT]);
        
        NSString *str1 = [NSString stringWithFormat:@"%@",[self.dispensaryNameUPComingArray objectAtIndex:indexPath.row]];
        NSString *str2 = [NSString stringWithFormat:@"%@",[self.dateUPComingArray objectAtIndex:indexPath.row]];
        NSString *str3;
        
        if([[self.itemsUPComingArray objectAtIndex:indexPath.row] integerValue] == 1)
            str3 = [NSString stringWithFormat:@"%@ item . INR %@",[self.itemsUPComingArray objectAtIndex:indexPath.row],[self.priceUPComingArray objectAtIndex:indexPath.row]];
        else
            str3 = [NSString stringWithFormat:@"%@ items . INR %@",[self.itemsUPComingArray objectAtIndex:indexPath.row],[self.priceUPComingArray objectAtIndex:indexPath.row]];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@\n%@",str1,str2,str3]];
        [style setLineSpacing:3];
        [attributedString addAttribute:NSParagraphStyleAttributeName
                                 value:style
                                 range:NSMakeRange(0, str3.length)];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                                 range:NSMakeRange(0, str1.length)];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:FontLight size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                                 range:NSMakeRange(str1.length+1, str2.length)];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                                 range:NSMakeRange(str1.length+1+str2.length+1, str3.length)];
        desLabel.attributedText = attributedString;
    }
    
  return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIView *thisView;
    if(tableView == upComingTableView)
    {
        thisView = (UIView*)[upComingTableView viewWithTag:indexPath.row+5000];
        [thisView setBackgroundColor:[UIColor colorWithRed:34/255.0f green:192/255.0f blue:100/255.0f alpha:0.5]];

        NSString *orderID = [self.orderIdUPComingIdArray objectAtIndex:indexPath.row];
        NSDictionary *jsonOrderDetails = [[ServerCallModel getSharedInstance] serverGetJSON:@"order_details_to_customer"
                                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                                              orderID] dataUsingEncoding:NSUTF8StringEncoding]
                                                                                       Type:@"POST"
                                                                           loadingindicator:YES];
        NSLog(@"order_details: %@",jsonOrderDetails);
        if(jsonOrderDetails) {
            if([[jsonOrderDetails objectForKey:@"status"] intValue] == 0) {
                [_viewOrderFromHistoryDelegate viewOrder:jsonOrderDetails
                                              isUpcoming:YES
                                              pickUpFlag:[jsonOrderDetails valueForKey:@"flag"]];
            }
        }
    }
    else {
       thisView = (UIView*)[onGoingTableView viewWithTag:indexPath.row+1000];
       [thisView setBackgroundColor:ColorGreenButton];
        NSString *orderID = [self.orderIdArray objectAtIndex:indexPath.row];
        NSDictionary *jsonOrderDetails = [[ServerCallModel getSharedInstance] serverGetJSON:@"order_details_to_customer"
                                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                                              orderID] dataUsingEncoding:NSUTF8StringEncoding]
                                                                                       Type:@"POST"
                                                                           loadingindicator:YES];
        NSLog(@"order_details: %@",jsonOrderDetails);
        if(jsonOrderDetails) {
            [_viewOrderFromHistoryDelegate viewOrder:jsonOrderDetails
                                          isUpcoming:NO
                                          pickUpFlag:[jsonOrderDetails valueForKey:@"flag"]];
        }
    }
    [thisView setBackgroundColor:ColorBlackBK];
}
@end
