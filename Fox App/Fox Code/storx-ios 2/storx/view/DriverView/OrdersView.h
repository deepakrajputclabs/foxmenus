//
//  OrdersView.h
// Storx
//
//  Created by clicklabs on 11/7/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "UIImageView+AFNetworking.h"


@protocol orderViewProtocol <NSObject>
@optional
- (void)deliveryMethod:(NSArray *)orderArray;
- (void)menuButton;
- (void)invalidAccessToken;
- (void)viewUpcomingOrders:(NSDictionary *)jsonData;
- (void)closeMethod;
@optional
- (NSDictionary *)serverListCall;
@end

@interface OrdersView : UIView <UIAlertViewDelegate>
{
    int scrollHeight;
    int heightCommingTableView;
    int detailCountAtZeroIndex;
    
    UIScrollView *scrollView;
    
    UITableView *currentTableView;
    UITableView *commingTableView;
    UIScrollView *scroll;
    UIImageView *boxImage;
    NSArray *iconArray;
    UIButton *deliveryButton;
    UIButton *callButton;
    UILabel *orderDetailLabel;
    UIButton  *upcomingButton;
}

- (void)OrderView ;
- (void)OrderScreenView :(BOOL)isOrderExist ;
- (void)jsonCall;

@property (weak) id <orderViewProtocol> orderViewDelegate;
@property (strong,nonatomic) NSArray *customerNameArray;
@property (strong,nonatomic) NSArray *customerImageArray;
@property (strong,nonatomic) NSArray *distanceArray;
@property (strong,nonatomic) NSArray *totalMedicineArray;
@property (strong,nonatomic) NSArray *detailArray;
@property (strong,nonatomic) NSArray *dropLocationAddress;
@property (strong,nonatomic) NSArray *orderId;
@property (strong,nonatomic) NSArray *orderTime;
@property (strong,nonatomic) NSArray *customerPhoneArray;
@property (strong,nonatomic) NSArray *totalDonationArray;
@property (strong,nonatomic) NSDictionary *jsonArray;
@property (strong,nonatomic) NSDictionary *orderJson;


@end
