//
//  OrdersView.m
// Storx
//
//  Created by clicklabs on 11/7/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "OrdersView.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)


@implementation OrdersView
@synthesize orderViewDelegate = _orderViewDelegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


#pragma mark - No UI View

- (void)OrderView {

    self.backgroundColor = SMBgColor;
    
    // Header Start
    self.backgroundColor = [UIColor whiteColor];
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    UIButton *menuButton;
    menuButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
    [menuButton addTarget:self action:@selector(menuButtonClicked) forControlEvents:UIControlEventTouchDown];
    [self addSubview:menuButton];

    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
    storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
    [self addSubview:storxImageView];

    iconArray = [[NSArray alloc]initWithObjects:@"contact_patient.png",@"location_order.png",@"", nil];

    [self jsonCall];
}

- (void)jsonCall {
   NSDictionary *json = [_orderViewDelegate serverListCall];
    
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
        self.orderJson              = json;
        self.customerNameArray      = [[json objectForKey:@"data"] valueForKey:@"customer_name"];
        self.customerImageArray     = [[json objectForKey:@"data"] valueForKey:@"customer_image"];
        self.totalMedicineArray     = [[json objectForKey:@"data"] valueForKey:@"total_medicines"];
        self.detailArray            = [[json objectForKey:@"data"] valueForKey:@"details"];
        self.dropLocationAddress    = [[json objectForKey:@"data"] valueForKey:@"drop_location_address"];
        self.orderTime              = [[json objectForKey:@"data"] valueForKey:@"order_time"];
        self.customerPhoneArray     = [[json objectForKey:@"data"] valueForKey:@"phone_no"];
        self.totalDonationArray     = [[json objectForKey:@"data"] valueForKey:@"price"];
            
        self.orderId                = [[json objectForKey:@"data"] valueForKey:@"order_id"];
            
            NSLog(@"%@",self.customerNameArray);
            
            if(self.customerNameArray.count >= 1) {
                self.jsonArray = [[json objectForKey:@"data"] objectAtIndex:0];
                [self OrderScreenView:YES];
            }
            else {
                [self OrderScreenView:NO];
            }
        }
        else if([[json objectForKey:@"status"] intValue] == 1){
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2){
            [_orderViewDelegate invalidAccessToken];
        }
    }
}

#pragma mark - UI View

- (void)OrderScreenView :(BOOL)isOrderExist {
    NSArray* subviewsTask;
    subviewsTask = [[NSArray alloc] initWithArray:scrollView.subviews];
    UIView *tempView;
    for (UIView* view in subviewsTask) {
        tempView = view;
        [tempView removeFromSuperview];
        tempView = nil;
    }
    [scrollView removeFromSuperview];
    scrollView = nil;
    [boxImage removeFromSuperview];
    boxImage = nil;
    
    if(!isOrderExist) {
        [boxImage removeFromSuperview];
        boxImage = nil;
        boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:488/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:810/3 currentSupSize:SCREEN_HEIGHT])];
        
        boxImage.backgroundColor = ColorBlackBK;
        [self addSubview:boxImage];
        
        UIImageView *noOrderImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:440/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:363/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:363/3 currentSupSize:SCREEN_HEIGHT])];
        noOrderImage.image = [UIImage imageNamed:@"no_order_icon.png"];
        noOrderImage.backgroundColor = [UIColor clearColor];
        [boxImage addSubview:noOrderImage];
        
        UILabel *noOrderTodayLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:513/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        noOrderTodayLabel.text = @"No Order For Today";
        noOrderTodayLabel.textColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
        [noOrderTodayLabel setFont:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
        noOrderTodayLabel.textAlignment = NSTextAlignmentCenter;
        [boxImage addSubview:noOrderTodayLabel];
    }
    else {
        //black box
        [boxImage removeFromSuperview];
        boxImage = nil;
        boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:277/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1419/3 currentSupSize:SCREEN_HEIGHT])];
        
        boxImage.backgroundColor = ColorBlackBK;
        boxImage.userInteractionEnabled = YES;
        [self addSubview:boxImage];
        
        //Customer Image
        UIImageView *customerImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:57/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:170/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
        [customerImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.customerImageArray objectAtIndex:0]]] placeholderImage:[UIImage imageNamed:UserDefaultImage]];

        customerImage.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/6 currentSupSize:SCREEN_HEIGHT];
        customerImage.layer.borderColor = ColorGreenButton.CGColor;
        customerImage.layer.masksToBounds = YES;
        customerImage.layer.borderWidth = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:6/3 currentSupSize:SCREEN_HEIGHT];
        [boxImage addSubview:customerImage];
        
        // Label Start
        UILabel *customerName = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:240/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:912/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        [customerName setFont:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];;
        customerName.textColor = ColorFFFFFF;
        customerName.numberOfLines = 1;
        customerName.backgroundColor = [UIColor clearColor];
        customerName.textAlignment = NSTextAlignmentLeft;
        customerName.text = [self.customerNameArray objectAtIndex:0];
        [boxImage addSubview:customerName];
        // Label End
        
        //Date and Time Label
        UILabel *dateTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:240/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:912/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        [dateTimeLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
        dateTimeLabel.textColor = ColorFFFFFF;
        dateTimeLabel.numberOfLines = 1;
        dateTimeLabel.backgroundColor = [UIColor clearColor];
        dateTimeLabel.textAlignment = NSTextAlignmentLeft;
        NSString *dateString = [[CommonClass getSharedInstance] convertUtcDateToLocalDateOnly:[self.orderTime objectAtIndex:0]];
        dateTimeLabel.text = [NSString stringWithFormat:@"%@",dateString];
        [boxImage addSubview:dateTimeLabel];
        
        //UnderLineImage
        UIImageView *underLineImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:284/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
        underLineImage.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
        [boxImage addSubview:underLineImage];
        
        float height = 0;
        height = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(284/3)+(48/3) currentSupSize:SCREEN_HEIGHT];
        float labelHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(284/3) + (30/3) currentSupSize:SCREEN_HEIGHT];
        
        float underlineHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(459/3) currentSupSize:SCREEN_HEIGHT];
        float verticalHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(284/3) + (30/3) currentSupSize:SCREEN_HEIGHT];
        
        for(int i =0; i<=2; i++)
        {
            UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], height, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:68/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:68/3 currentSupSize:SCREEN_HEIGHT])];
            if(i==1)
            {
                iconImage.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], height, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT]);
            }
            
            iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[iconArray objectAtIndex:i]]];
            [boxImage addSubview:iconImage];
            
            UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:175/3 currentSupSize:SCREEN_WIDTH], labelHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:948/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT])];
            infoLabel.textColor = ColorFFFFFF;
            infoLabel.numberOfLines = 0;
            infoLabel.backgroundColor = [UIColor clearColor];
            infoLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
            [boxImage addSubview:infoLabel];
            
            UIImageView *underLineImage1 = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], underlineHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            underLineImage1.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
            [boxImage addSubview:underLineImage1];
            
            UIImageView *verticalLine  = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1002/3 currentSupSize:SCREEN_WIDTH], verticalHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:3/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:115/3 currentSupSize:SCREEN_HEIGHT])];
            verticalLine.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
            verticalLine.tag = 10+i;
            verticalLine.hidden = NO;
            
            callButton = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1032/3 currentSupSize:SCREEN_WIDTH], verticalHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:120/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
            callButton.tag = i;
            callButton.userInteractionEnabled = YES;
            callButton.hidden = NO;
            [callButton addTarget:self action:@selector(callMethod:) forControlEvents:UIControlEventTouchDown];
            [callButton setBackgroundImage:[UIImage imageNamed:@"call_icon.png"] forState:UIControlStateNormal];
            [callButton setBackgroundImage:[UIImage imageNamed:@"call_icon_onclick.png"] forState:UIControlStateSelected];
            
            height = height + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(175/3)+(12/3) currentSupSize:SCREEN_HEIGHT];
            labelHeight = labelHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(175/3) currentSupSize:SCREEN_HEIGHT];
            underlineHeight = underlineHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(230/3) currentSupSize:SCREEN_HEIGHT];
            verticalHeight = verticalHeight +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(175/3)+(30/3) currentSupSize:SCREEN_HEIGHT];
            
            if(i==0)
            {
                [boxImage addSubview:callButton];
                [boxImage addSubview:verticalLine];
                infoLabel.text = [self.customerPhoneArray objectAtIndex:0];
            }
            
            if(i == 1)
            {
                callButton.hidden = YES;
                verticalLine.hidden = YES;
                infoLabel.frame =  CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:175/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:489/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:948/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT]);
                infoLabel.text = [self.dropLocationAddress objectAtIndex:0];
                infoLabel.numberOfLines = 2;
            }
            
            if(i==2)
            {
                labelHeight = (689/3)+(15/3);
                iconImage.hidden = YES;
                infoLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:68/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:labelHeight currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:948/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT]);
                verticalLine.hidden = YES;
                callButton.hidden = YES;
                underLineImage1.hidden = YES;
                NSString  *string1;
                if ([[self.totalMedicineArray objectAtIndex:0] integerValue] >1) {
                    string1 = [NSString stringWithFormat:@"Total Order Items: %@ qty", [self.totalMedicineArray objectAtIndex:0]];
                }
                else{
                    string1 = [NSString stringWithFormat:@"Total Order Items: %@ qty", [self.totalMedicineArray objectAtIndex:0]];
                }
              NSString  *string2 = [NSString stringWithFormat:@"Total Amount: INR %@", [self.totalDonationArray objectAtIndex:0]];
                
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\n%@",string1,string2]];
                [attributedString addAttribute:NSFontAttributeName
                                         value:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                                         range:NSMakeRange(19, string1.length-19)];
                [attributedString addAttribute:NSFontAttributeName
                                         value:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]]
                                         range:NSMakeRange(string1.length+1+16, string2.length-16)];
                infoLabel.attributedText = attributedString;
            }
            
        }
        
        //Delivery Button
        deliveryButton = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1799/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT])];
        [deliveryButton addTarget:self action:@selector(deliveryMethodClicked) forControlEvents:UIControlEventTouchDown];
        deliveryButton.backgroundColor = [UIColor clearColor];
        [deliveryButton setBackgroundImage:[UIImage imageNamed:@"out ot deliver.png"] forState:UIControlStateNormal];
        [deliveryButton setBackgroundImage:[UIImage imageNamed:@"out ot deliver_onclick.png"] forState:UIControlStateSelected];
        [self addSubview:deliveryButton];
        
        upcomingButton = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2034/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT])];
        [upcomingButton addTarget:self action:@selector(upcomingOrderMethod) forControlEvents:UIControlEventTouchDown];
        upcomingButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [upcomingButton setTitle:[NSString stringWithFormat:@"No of Upcoming Orders: %lu",(unsigned long)[self.customerNameArray count]-1] forState:UIControlStateNormal];
        [upcomingButton setTitleEdgeInsets:UIEdgeInsetsMake([[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH]-50, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:450/3 currentSupSize:SCREEN_WIDTH])];
        upcomingButton.titleLabel.font =  [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [upcomingButton setTitleColor:ColorGreenButton forState:UIControlStateNormal];
        [upcomingButton setImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
        [upcomingButton setImage:[UIImage imageNamed:@"arrow_onclick.png"] forState:UIControlStateHighlighted];
        [upcomingButton setImageEdgeInsets:UIEdgeInsetsMake([[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1140/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:52.5/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:55/3 currentSupSize:SCREEN_WIDTH])];
        [self addSubview:upcomingButton];
        
        orderDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:919/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
        orderDetailLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7];
        orderDetailLabel.text = @"   Order Details:";
        orderDetailLabel.textColor = ColorFFFFFF;
        orderDetailLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [boxImage addSubview:orderDetailLabel];
        
        scroll = [[UIScrollView alloc]init];
        //ScrollView
        [scroll setFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1019/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:400/3 currentSupSize:SCREEN_HEIGHT])];
        [scroll setBackgroundColor:[UIColor clearColor]];
        scroll.userInteractionEnabled=true;
        [boxImage addSubview:scroll];

        float productHeight = 0;
        productHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT];
        NSLog(@"Details%@",self.detailArray);
        
        for (int i =0; i<[[self.detailArray objectAtIndex:0] count]; i++) {
            UIImageView *productImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], productHeight +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:132/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:132/3 currentSupSize:SCREEN_HEIGHT])];
            [productImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[self.detailArray objectAtIndex:0] objectAtIndex:i]valueForKey:@"medicine_image"]]] placeholderImage:[UIImage imageNamed:@"StorX1.png"]];
            productImage.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:132/6 currentSupSize:SCREEN_HEIGHT];
            productImage.layer.borderColor = [UIColor whiteColor].CGColor;
            productImage.layer.borderWidth = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:6/3 currentSupSize:SCREEN_HEIGHT];
            // [scroll addSubview:productImage];
            
            UILabel  *productLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:630/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
            productLabel.backgroundColor = [UIColor clearColor];
            productLabel.text = [NSString stringWithFormat:@"%@",[[[self.detailArray objectAtIndex:0] objectAtIndex:i]valueForKey:@"medicine_name"]];
            productLabel.textColor = ColorFFFFFF;
            productLabel.numberOfLines = 2;
            productLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
            [scroll addSubview:productLabel];
            
            UILabel  *quantityLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:680/3 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:270/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
            quantityLabel.backgroundColor = [UIColor clearColor];
            NSString *qtyString = [[[self.detailArray objectAtIndex:0] objectAtIndex:i] valueForKey:@"medicine_quantity"];
            NSString *newString = [qtyString stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSLog(@"newString: %@",newString);

            quantityLabel.text = [NSString stringWithFormat:@"%@",newString];
            quantityLabel.textColor = ColorFFFFFF;
            quantityLabel.numberOfLines = 2;
            quantityLabel.textAlignment = NSTextAlignmentCenter;
            quantityLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
            [scroll addSubview:quantityLabel];
            
            UILabel  *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:952/3 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:230/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
            priceLabel.backgroundColor = [UIColor clearColor];
            priceLabel.text = [NSString stringWithFormat:@"INR %@",[[[self.detailArray objectAtIndex:0] objectAtIndex:i]valueForKey:@"medicine_price"]];
            priceLabel.textColor = ColorFFFFFF;
            priceLabel.textAlignment = NSTextAlignmentCenter;
            priceLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
            [scroll addSubview:priceLabel];
            
            UIImageView *borderImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(200/3) currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            borderImage.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
            [scroll addSubview:borderImage];
            
            productHeight = productHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT];
        }
        scroll.contentSize = CGSizeMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], productHeight);
    }
}

#pragma mark - Call Button
- (void)callMethod: (UIButton *)sender {
    if(sender.tag == 0) {
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",[self.customerPhoneArray objectAtIndex:0]] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSLog(@"phone number :%@",[self.customerPhoneArray objectAtIndex:0]);
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]]]){
            
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Call" message:[self.customerPhoneArray objectAtIndex:0] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
            warningAlert.tag = 1000;
            [warningAlert show];
            
        }else{
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your device doesn't support call facility!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
    }
   }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
            NSString *cleanedString = [[[NSString stringWithFormat:@"%@",[self.customerPhoneArray objectAtIndex:0]] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        
            NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
                [[UIApplication sharedApplication] openURL:telURL];
    }
}


#pragma mark - Upcoming Orders

- (void)upcomingOrderMethod{
    [_orderViewDelegate viewUpcomingOrders:self.orderJson];
}

#pragma mark - Menu Button

- (void)menuButtonClicked {
    [_orderViewDelegate menuButton];
}

#pragma mark - Out for Delivery Clicked

- (void)deliveryMethodClicked {
    [_orderViewDelegate deliveryMethod:self.orderId];
}

#pragma mark - Close Button

- (void)closeButtonClicked {
    [_orderViewDelegate closeMethod];
}

@end

