//
//  ChangePasswordView.m
//  Storx
//
//  Created by click on 12/22/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ChangePasswordView.h"

@implementation ChangePasswordView
@synthesize changePasswordDelegate;

-(void)changePasswordUI{
    self.backgroundColor = [UIColor whiteColor];
    
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
    storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
    [self addSubview:storxImageView];
    
    UIButton *backButton;
    backButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchDown];
    [self addSubview:backButton];
    
    UIButton *doneButton;
    doneButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:296/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1173/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:650/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    doneButton.userInteractionEnabled = YES;
    [doneButton addTarget:self action:@selector(doneButton) forControlEvents:UIControlEventTouchUpInside];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"done_onclick.png"] forState:UIControlStateHighlighted];
    [self addSubview:doneButton];
    
    [self createView];

}

- (void)createView{
    float labelY = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT];
    
    for(int i =0; i<=2 ; i++)
    {
        UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], labelY,[[CommonClass getSharedInstance] targetSize:414 targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
        infoLabel.backgroundColor = ColorBlackBK;
        [infoLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];
        infoLabel.textColor = ColorFFFFFF;
        infoLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:infoLabel];
        
        
        
        if(i==0){
            infoLabel.text = @"  Old Password";
        }
        if(i==1){
            infoLabel.text = @"  New Password";
        }
        if(i==2){
            infoLabel.text = @"  Confirm Password";
        }
        
        UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], labelY +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT])];
        backgroundImage.backgroundColor = ColorDarkBlackBK;
        [self addSubview:backgroundImage];
        
        oldPassword = [[UITextField alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH],labelY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT])];
        oldPassword.text = @"";
        oldPassword.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        oldPassword.userInteractionEnabled = YES;
        [oldPassword setSecureTextEntry:YES];
        oldPassword.textColor = ColorFFFFFF;
        oldPassword.tag = 100+i;
        oldPassword.backgroundColor = [UIColor clearColor];
        oldPassword.autocorrectionType = UITextAutocorrectionTypeNo;
        oldPassword.keyboardType = UIKeyboardTypeDefault;
        oldPassword.returnKeyType = UIReturnKeyDone;
        oldPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        oldPassword.delegate = self;
        oldPassword.autocapitalizationType = UITextAutocapitalizationTypeWords;
        oldPassword.textColor = [UIColor whiteColor];
        [self addSubview:oldPassword];
        
        labelY = labelY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:305/3 currentSupSize:SCREEN_HEIGHT];
    }
    
}

-(void)doneButton{
    NSMutableArray *mutArray = [[NSMutableArray alloc] init];
    BOOL isFilledData = false;
    for (int i = 0; i < 3; i++) {
        UITextField *textFld =  (UITextField*)[self viewWithTag:100+i];
        [mutArray addObject:[NSString stringWithFormat:@"%@",textFld.text]];
        if([textFld.text isEqualToString:@""]) {
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Please fill the fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            isFilledData = true;
            break;
        }
        [textFld resignFirstResponder];
    }
    
    if(!isFilledData) {
        if(![[mutArray objectAtIndex:1] isEqualToString:[mutArray objectAtIndex:2]]) {
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Passwords do not match" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else {
            NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"change_password"
                                                                               data:[[NSString stringWithFormat:@"access_token=%@&old_password=%@&new_password=%@",
                                                                                      [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],[mutArray objectAtIndex:0], [mutArray objectAtIndex:1]] dataUsingEncoding:NSUTF8StringEncoding]
                                                                               Type:@"POST"
                                                                   loadingindicator:YES];
            NSLog(@"json: %@",json);
            if(json) {
                if([[json objectForKey:@"status"] intValue] == 0) {
                    UIAlertView *alertObj = [[UIAlertView alloc] initWithTitle:@"" message:@"Password changed successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    alertObj.tag = 99882;
                    [alertObj show];
                }
                else if([[json objectForKey:@"status"] intValue] == 1){
                    [[[UIAlertView alloc] initWithTitle:@"" message:@"Please fill all the fields" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
                else if([[json objectForKey:@"status"] intValue] == 2){
                    
                }
            }
        }
    }
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 99882) {
        [changePasswordDelegate passwordChanged];
    }
}

-(void)backButtonClicked{
    [changePasswordDelegate backFromChangePassword];
}

#pragma mark - TextField

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == (UITextField *)[textField viewWithTag:100]) {
        [(UITextField *)[self viewWithTag:101] becomeFirstResponder];
        return NO;
    }
    else if (textField == (UITextField *)[self viewWithTag:101]) {
        [(UITextField *)[self viewWithTag:102] becomeFirstResponder];
        return NO;
    }
    else {
        [textField resignFirstResponder];
        [(UITextField *)[self viewWithTag:102] resignFirstResponder];
    }
    return YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
