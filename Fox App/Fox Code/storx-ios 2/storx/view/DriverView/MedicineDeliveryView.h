//
//  MedicineDeliveryView.h
//  Storx
//
//  Created by click on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "CommonClass.h"

@protocol medicineDeliveryProtocol <NSObject>
-(void)feedbackAfterDone:(NSString *)idOrder;
-(void)menuScreen;
- (void)backMethod;
@end

@interface MedicineDeliveryView : UIView
@property(strong,nonatomic) id <medicineDeliveryProtocol>medicineDeliveryDelegate;

@property(strong,nonatomic) NSArray *orderDetail;
@property(strong,nonatomic) NSString *customerName;
@property(strong,nonatomic) NSString *customerImage;
@property(strong,nonatomic) NSString *dropAddress;
@property(strong,nonatomic) NSString *totalMedicine;
@property(strong,nonatomic) NSString *totalDonation;
@property(strong,nonatomic) NSString *orderID;
@property(strong,nonatomic) NSString *flagCheck;
@property(strong,nonatomic) NSString *phoneNumber;

- (void)deliveryView : (NSMutableArray *)customerInfo
              orderId: (NSString *)idOrder
              detail : (NSArray *)orderDetail
             pushFlag: (NSString *)flagString
      pushButtonCheck: (BOOL)pushButtonCheck;
@end
