//
//  ViewOrder.m
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ViewOrderView.h"

@implementation ViewOrderView
@synthesize viewOrderDelegate = _viewOrderDelegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#pragma mark - View Order UI

- (void)ordersView : (NSDictionary *)json
{
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    UIButton *backButton;
    backButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchDown];
    [self addSubview:backButton];
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:360/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:525/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    headerLabel.text = @"Upcoming Orders";
    headerLabel.textColor = ColorFFFFFF;
    [headerLabel setFont:[UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
    [self addSubview:headerLabel];
    
    commingTableView = [[UITableView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1908/3 currentSupSize:SCREEN_HEIGHT])];
    
    commingTableView.backgroundColor = [UIColor clearColor];
    commingTableView.dataSource=self;
    commingTableView.delegate=self;
    [commingTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    commingTableView.clipsToBounds=YES;
    //commingTableView.scrollEnabled = NO;
    [self addSubview:commingTableView];
    
    tableHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:352/3 currentSupSize:SCREEN_HEIGHT];
    
    headerHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT];
    
    _customerNameArray = [[json objectForKey:@"data"] valueForKey:@"customer_name"];
    _orderImageArray = [[json objectForKey:@"data"] valueForKey:@"customer_image"];
    _orderTimeArray = [[json objectForKey:@"data"] valueForKey:@"order_time"];
    _orderIdArray = [[json objectForKey:@"data"] valueForKey:@"order_id"];
    _dropLocationAddress = [[json objectForKey:@"data"] valueForKey:@"drop_location_address"];
}

#pragma mark - Refresh Data

- (void) refreshTableView {

    [commingTableView reloadData];
}

#pragma mark - TabelView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return [_customerNameArray count]-1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableHeight;
}

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIImageView *headerView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], headerHeight)];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cell";
    UIImageView *baseCell;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    if(cell == nil){
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else {
        for(UIImageView *subview in [baseCell subviews]) {
            [subview removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    //cell.imageView.highlightedImage = [self imageWithColor:ColorGreenButton];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = ColorGreenButton;
    bgColorView.layer.cornerRadius = 6.0;
    bgColorView.tag = 99;
    [cell setSelectedBackgroundView:bgColorView];
    //    cell.selectedBackgroundView = [UIView new];
    //    cell.selectedBackgroundView.backgroundColor = ColorGreenButton;
    
    baseCell = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], tableHeight)];
    baseCell.userInteractionEnabled = YES;
    baseCell.tag = 100;
    baseCell.layer.cornerRadius = 6.0;
    [baseCell setClipsToBounds:YES];
    baseCell.backgroundColor = ColorBlackBK;
    baseCell.userInteractionEnabled = YES;
    [cell.contentView addSubview:baseCell];
    
    UIImageView *picImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:92/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:178/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:178/3 currentSupSize:SCREEN_HEIGHT])];
    [picImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[_orderImageArray objectAtIndex:indexPath.section+1]]] placeholderImage:[UIImage imageNamed:DispensaryDefaultImage]];
    picImageView.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:178/6 currentSupSize:SCREEN_HEIGHT];
    picImageView.layer.masksToBounds = YES;
    picImageView.layer.borderColor = ColorGreenButton.CGColor;
    picImageView.layer.borderWidth = 1.0;
    picImageView.tag=101;
    [baseCell addSubview:picImageView];
    
    UILabel *desLabel      = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:266/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:45/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:776/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    desLabel.textColor     = ColorFFFFFF;
    desLabel.backgroundColor = [UIColor clearColor];
    desLabel.numberOfLines = 0;
    desLabel.tag= 102;
    desLabel.textAlignment = NSTextAlignmentLeft;
    [desLabel setFont:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    //desLabel.text = @"Adam Gonzalez\n28 Nov 2104 | 09:30 AM";
    [baseCell addSubview:desLabel];
    
    NSString *str1 = [_customerNameArray objectAtIndex:indexPath.section+1];
    NSString *str2 = [[CommonClass getSharedInstance] convertUtcDateToLocalDateOnly:[_orderTimeArray objectAtIndex:indexPath.section+1]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\n%@",str1,str2]];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                             range:NSMakeRange(str1.length+1, str2.length)];
    desLabel.attributedText = attributedString;
    
    UILabel *addressLabel      = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:355/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:195/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:687/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    addressLabel.textColor     = ColorFFFFFF;
    addressLabel.backgroundColor = [UIColor clearColor];
    addressLabel.numberOfLines = 2;
    addressLabel.textAlignment = NSTextAlignmentLeft;
    addressLabel.tag =103;
    [addressLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    addressLabel.text = [_dropLocationAddress objectAtIndex:indexPath.section+1];
    [baseCell addSubview: addressLabel];
    
    UIImageView *locationImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:266/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:225/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:75/3 currentSupSize:SCREEN_HEIGHT])];
    locationImage.image = [UIImage imageNamed:@"location_order.png"];
    locationImage.tag = 104;
    [baseCell addSubview:locationImage];
    
    UIImageView *nextArrow = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1102/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:145/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:45/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
    nextArrow.image = [UIImage imageNamed:@"nextArrow.png"];
    nextArrow.tag = 105;
    [baseCell addSubview:nextArrow];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self orderDetail:[_orderIdArray objectAtIndex:indexPath.section+1]];
    
}

#pragma mark - Back Button

- (void)backButtonClicked {
    [_viewOrderDelegate backButton];
}

#pragma mark - OrderDeliver

- (void)orderDeliverClicked {
    [_viewOrderDelegate orderDeliver:orderIdString customerInfo:customerInfoMutArray];
}

-(UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)orderDetail:(NSString *)orderID
{
    [_viewOrderDelegate detailsWithUserInfo:orderID];
}

@end
