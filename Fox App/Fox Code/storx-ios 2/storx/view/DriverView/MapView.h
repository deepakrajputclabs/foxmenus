//
//  MapView.h
// Storx
//
//  Created by clicklabs on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "Header.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GoogleMapView.h"
#import "UIImageView+AFNetworking.h"
#import <MessageUI/MessageUI.h>

@protocol mapViewProtocol <NSObject>
@optional
- (void)showDetailButtonAction;
- (void)sendMessageAction;

- (void)menuButton;
- (void)infoMethod:(NSDictionary *)dict;
- (void)pickUp :(NSString *)dispensaryId
        orderId:(NSString *)orderID
    detailorder:(NSArray *)detailOrder
   customerInfo:(NSMutableArray *)customerInfo
     flagString:(NSString *)flagCheck;

- (void)backButton;
- (void)customerProfile :(NSMutableArray *)customerInfo;
- (void)invalidAccessToken;
- (void)viewOrder :(BOOL)isFromReachDestination
           orderId:(NSString *)orderId
       detailorder:(NSArray *)detailOrder
      customerInfo:(NSMutableArray *)customerInfo;

- (void)viewOrder;
- (void)toDeliveryController:(NSString *)orderID
                detailorder:(NSArray *)detailOrder
               customerInfo:(NSMutableArray *)customerInfo
                 flagString:(NSString *)flagCheck;
@end

@interface MapView : UIView <GMSMapViewDelegate, CLLocationManagerDelegate>
{
    NSString *flagCheckString;
    GMSCameraPosition *camera;
    CLLocationManager *pickupLocationManager;
    NSMutableArray *customerInfoMutArray;
    GMSCoordinateBounds *bounds;
    UILabel *timeLabel;
    UIButton *menuButton;
}

//Functions
- (void)mapUIView :(NSString *)orderId startOrder:(BOOL)isStartOrder flag:(NSString *)flagCheck isMenuButtonShaow:(BOOL)isMenuButtonShaow;

- (void)clearMarkerFromMap;
- (void)mapView ;
@property (weak) id <mapViewProtocol> mapViewDelegate;
@property(strong, nonatomic) NSDictionary *dispensaryInfo;

@property (strong,nonatomic) NSString *customerNameArray;
@property (strong,nonatomic) NSString *customerPhone;
@property (strong,nonatomic) NSArray *deliveryTimeArray;
@property (strong,nonatomic) NSArray *distanceArray;
@property (strong,nonatomic) NSString *totalMedicineArray;
@property (strong,nonatomic) NSArray *detailArray;
@property (strong,nonatomic) NSString *dropLocationAddress;
@property (strong,nonatomic) NSString *dropLat;
@property (strong,nonatomic) NSString *dropLong;
@property (strong,nonatomic) NSString *dropLatArray;
@property (strong,nonatomic) NSString *dropLongArray;
@property (strong,nonatomic) NSString *dispImageString;
@property (strong,nonatomic) NSString *phoneNumber;
@property (strong,nonatomic) NSString *orderIdArray;
@property (nonatomic,retain) CLLocation *getCurrentLocation;
@property (nonatomic,strong) NSString *timeLeft;
@property (strong,nonatomic) NSString *dispensaryId;

- (void)showMap:(float)latValue longVal:(float)longitudeValue;
@end
