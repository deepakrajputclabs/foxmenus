//
//  EditProfileView.m
//  Storx
//
//  Created by click on 12/22/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "EditProfileView.h"

@implementation EditProfileView
@synthesize editProfileDelegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)editProfileScreen {
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.backgroundColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addImage:)
                                                 name:@"ProfilePicChange"
                                               object:nil];

    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
    storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
    [self addSubview:storxImageView];
    
    UIButton *backButton;
    backButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchDown];
    [self addSubview:backButton];
    
    UIButton *doneButton;
    doneButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:296/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1881/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:650/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    doneButton.userInteractionEnabled = YES;
    [doneButton addTarget:self action:@selector(doneEditingClicked) forControlEvents:UIControlEventTouchUpInside];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"done_onclick.png"] forState:UIControlStateHighlighted];
    [self addSubview:doneButton];

    [self profileView];
}

#pragma mark - Profile View

- (void) profileView{
    
        picImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:439/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:331/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:364/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:364/3 currentSupSize:SCREEN_HEIGHT])];
        picImageView.contentMode = UIViewContentModeScaleToFill;
        picImageView.layer.borderWidth = 1.0;
        picImageView.layer.borderColor = ColorGreenButton.CGColor;
        picImageView.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:364/6 currentSupSize:SCREEN_HEIGHT];
        picImageView.layer.masksToBounds = YES;
       [picImageView setImageWithURL:[NSURL URLWithString:
                                      [NSString stringWithFormat:@"%@",
                                       [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserImage]]]
                    placeholderImage:[UIImage imageNamed:UserDefaultImage]];
        picImageView.userInteractionEnabled = NO;
        [self addSubview:picImageView];
        
        UIButton *editProfile;
        editProfile =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:719/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:356/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT])];
        [editProfile setBackgroundImage:[UIImage imageNamed:@"edit_profile.png"] forState:UIControlStateNormal];
        [editProfile setBackgroundImage:[UIImage imageNamed:@"edit_profile_onclick.png"] forState:UIControlStateHighlighted];
     [editProfile addTarget:self action:@selector(selectImage:) forControlEvents:UIControlEventTouchDown];
        [self addSubview:editProfile];
    NSArray *fields;

    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1){ //Driver
        infoArray = nil;
        iconArray = [[NSArray alloc]
                     initWithObjects:@"username_icon.png",@"email_icon.png",@"contact_icon1.png", nil];
        infoArray = [[NSArray alloc]initWithObjects:[[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserName],
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserEmail],
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPhone], nil];

        float labelX = 0;
        labelX = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:848/3 currentSupSize:SCREEN_HEIGHT];
        
        for(int i =0 ; i<=2; i++){
            
            UIImageView *icon  = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:107/3 currentSupSize:SCREEN_WIDTH],labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:75/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT])];
            icon.image = [UIImage imageNamed:[iconArray objectAtIndex:i]];
            [self addSubview:icon];
            
            if(i==3)
            {
                icon.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:107/3 currentSupSize:SCREEN_WIDTH],labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]);
            }
            
            UITextField *infoTextField = [[UITextField alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:320/3 currentSupSize:SCREEN_WIDTH], labelX,[[CommonClass getSharedInstance] targetSize:414 targetSubSize:892/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:147/3 currentSupSize:SCREEN_HEIGHT])];
            infoTextField.text = [infoArray objectAtIndex:i];
            infoTextField.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
            infoTextField.userInteractionEnabled = YES;
            infoTextField.textColor = ColorFFFFFF;
            infoTextField.backgroundColor = [UIColor clearColor];
            infoTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            infoTextField.keyboardType = UIKeyboardTypeDefault;
            infoTextField.returnKeyType = UIReturnKeyDone;
            infoTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            infoTextField.delegate = self;
            infoTextField.tag = 500 + i;
            infoTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            infoTextField.textColor = [UIColor whiteColor];
            [self addSubview:infoTextField];
            
            UIImageView *borderLine = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:320/3 currentSupSize:SCREEN_WIDTH],labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:822/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            borderLine.backgroundColor = ColorGrayBk;
            [self addSubview:borderLine];
            
            labelX = labelX + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT];
            if(i==1){
                infoTextField.userInteractionEnabled = NO;
            }
            else if (i == 2) {
                infoTextField.keyboardType = UIKeyboardTypePhonePad;
            }
        }
        
        fields = @[(UITextField *)[self viewWithTag:501],(UITextField *)[self viewWithTag:502]];
        [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
        [self.keyboardControls setDelegate:self];
    }
    else{
        infoArray = nil;
        iconArray = [[NSArray alloc]
                     initWithObjects:@"username_icon.png",@"Patient ID.png",@"Physican Name.png",@"contact_patient.png",@"email_icon_patient.png", nil];
        infoArray = [[NSArray alloc]initWithObjects:
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserName],
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPatientId],
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPhysicianName],
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPhone],
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserEmail], nil];

        float labelX = 0;
        labelX = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:848/3 currentSupSize:SCREEN_HEIGHT];
        UITextField *infoTextField;
        
        for(int i =0 ; i<=4; i++){
            
            UIImageView *icon  = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:107/3 currentSupSize:SCREEN_WIDTH],labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT])];
            icon.image = [UIImage imageNamed:[iconArray objectAtIndex:i]];
            [self addSubview:icon];
            
            infoTextField = [[UITextField alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:320/3 currentSupSize:SCREEN_WIDTH], labelX,[[CommonClass getSharedInstance] targetSize:414 targetSubSize:892/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
            infoTextField.text = [infoArray objectAtIndex:i];
            infoTextField.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
            infoTextField.userInteractionEnabled = YES;
            infoTextField.textColor = ColorFFFFFF;
            infoTextField.backgroundColor = [UIColor clearColor];
            infoTextField.autocorrectionType = UITextAutocorrectionTypeNo;
            infoTextField.keyboardType = UIKeyboardTypeDefault;
            infoTextField.returnKeyType = UIReturnKeyDone;
            infoTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            infoTextField.delegate = self;
            infoTextField.tag = 500+ i;
            infoTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            infoTextField.textColor = [UIColor whiteColor];
            [self addSubview:infoTextField];
            
            if(i==4||i==1||i==2){
                infoTextField.userInteractionEnabled = NO;
            }
            else if(i==3){
                infoTextField.keyboardType = UIKeyboardTypePhonePad;
                
                icon.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:107/3 currentSupSize:SCREEN_WIDTH],labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]);
            }
            
            UIImageView *borderLine = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:320/3 currentSupSize:SCREEN_WIDTH],labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:822/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            borderLine.backgroundColor = ColorGrayBk;
            [self addSubview:borderLine];
            
            labelX = labelX + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT];
        }
        
        fields = @[(UITextField *)[self viewWithTag:500],(UITextField *)[self viewWithTag:503]];
        [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
        [self.keyboardControls setDelegate:self];
  }
}

-(void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls {
    [self endEditing:YES];
}

//- (void)doneButton{
//    [editProfileDelegate doneEditingProfile];
//}

- (void)backButtonClicked{
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [editProfileDelegate backFromEditProfile];
}

#pragma mark - Add Image

- (void)addImage :(NSNotification *)string
{
    [[CommonClass getSharedInstance] removeImageFRom:@"ProfilePicFolder"];
    
    if ([[string name] isEqualToString:@"ProfilePicChange"])
    {
        NSDictionary *userInfo = string.userInfo;
        NSObject *myObject   = [userInfo objectForKey:@"someKey"];
        UIImage *chkPixel = (UIImage *)myObject;
        
        NSLog(@"Width: %f",chkPixel.size.width);
        NSLog(@"Height: %f",chkPixel.size.height);
        
        if(chkPixel.size.height < 100 || chkPixel.size.width < 100)
        {
            [[CommonClass getSharedInstance] removeImageFRom:@"ProfilePicFolder"];
            [[CommonClass getSharedInstance] alertMsg:@"Try uploading a different photo. Photos must be at least 100 x 100 pixels." title:@"Error"];
            
        }
        else
        {
            isImageChanged = YES;
            picImageView.image = chkPixel;
           [[CommonClass getSharedInstance] saveImage:[[CommonClass getSharedInstance] imagerotate: (UIImage *)myObject] ImageName:@"profile.jpg" FolderName:@"ProfilePicFolder"];
        }
    }
}

#pragma mark - Select Image

- (void)selectImage:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Add Image"];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"From Gallery"];
    [alert addButtonWithTitle:@"Take photo"];
    [alert addButtonWithTitle:@"Cancel"];
    alert.tag = 101;
    [alert show];
    alert = nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101) {
        if(buttonIndex == 0)
            [editProfileDelegate editButton:0];
        else if(buttonIndex == 1)
            [editProfileDelegate editButton:1];
    }
}

#pragma mark - Done Editing

- (void)doneEditingClicked {
    
    [self showActivityIndicatorWithTitle:@"Loading..."];
    UITextField *phoneTextfield;
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
        phoneTextfield=(UITextField *)[self viewWithTag:502];
    }
    else {
        phoneTextfield=(UITextField *)[self viewWithTag:503];
    }
    
    NSString *phoneNumber=[[[[[[[[phoneTextfield.text componentsSeparatedByString:@"("]componentsJoinedByString:@""]componentsSeparatedByString:@")"]componentsJoinedByString:@""]componentsSeparatedByString:@"-"]componentsJoinedByString:@""]componentsSeparatedByString:@" "]componentsJoinedByString:@""];
    
    if([phoneNumber length]!=10)
    {
        [[CommonClass getSharedInstance] HideActivityIndicator];
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Error!"        message:@"Please enter your complete phone number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert1 show];
    }
    else{
        [self endEditing:YES];
        
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reach currentReachabilityStatus];
        if (netStatus == NotReachable) {
            
            [[CommonClass getSharedInstance] HideActivityIndicator];
            [[[UIAlertView alloc] initWithTitle:@"" message:ErrorInternetConnection delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else {
            
            NSMutableArray *mutArray = [[NSMutableArray alloc] init];
            NSString *picPath;
            if(isImageChanged)
                picPath = [[CommonClass getSharedInstance] getImageFRom:@"ProfilePicFolder" imageName:@"profile.jpg"];
            else
                picPath = @"";
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,@"edit_profile"]];
            
            ASIFormDataRequest *Request = [ASIFormDataRequest requestWithURL:url];
            [Request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:AccessToken] forKey:@"access_token"];
            if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
                
                for (int i = 0; i < 3; i++) {
                    UITextField *textFld =  (UITextField*)[self viewWithTag:500+i];
                    [mutArray addObject:[NSString stringWithFormat:@"%@",textFld.text]];
                }
                [Request setPostValue:[mutArray objectAtIndex:0]                                      forKey:@"user_name"];
                [Request setPostValue:[mutArray objectAtIndex:2]                                      forKey:@"phone_no"];
            }
            else {
                for (int i = 0; i < 5; i++) {
                    UITextField *textFld =  (UITextField*)[self viewWithTag:500+i];
                    [mutArray addObject:[NSString stringWithFormat:@"%@",textFld.text]];
                }
                [Request setPostValue:[mutArray objectAtIndex:0]                                      forKey:@"user_name"];
                [Request setPostValue:[mutArray objectAtIndex:2]                                      forKey:@"physician_name"];
                [Request setPostValue:[mutArray objectAtIndex:3]                                      forKey:@"phone_no"];
            }

            if(isImageChanged)
                [Request setFile:picPath                                                         forKey:@"user_image"];

            [Request setPostValue:[NSString stringWithFormat:@"%d",isImageChanged]                forKey:@"user_pic"];
            [Request setDelegate:self];
            [Request startSynchronous];
        }
    }
}

#pragma mark : ASSIHTTPRequest

- (void)requestWentWrong:(ASIHTTPRequest *)request {
    [[CommonClass getSharedInstance] HideActivityIndicator];
}

-(void)requestStarted:(ASIHTTPRequest *)request{
    NSLog(@"ASIHTTP start request");
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    NSLog(@"ASI failed");
    [[CommonClass getSharedInstance] HideActivityIndicator];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [[CommonClass getSharedInstance] HideActivityIndicator];

    NSString *jsonString = [request responseString];
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    NSLog(@"Custom Affiliation: %@",json);
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            NSMutableDictionary *userDataToSet = [[NSUserDefaults standardUserDefaults] objectForKey:UserData];
            
            NSMutableDictionary *userData = [[NSMutableDictionary alloc] init];
            [userData setValue:[userDataToSet valueForKey:@"access_token"]              forKey:@"access_token"];
            [userData setValue:[userDataToSet valueForKey:@"current_user_status"]       forKey:@"current_user_status"];
            [userData setValue:[userDataToSet valueForKey:@"email"]                     forKey:@"email"];
            [userData setValue:[userDataToSet valueForKey:@"patient_id"]                forKey:@"patient_id"];
            [userData setValue:[userDataToSet valueForKey:@"physician_name"]            forKey:@"physician_name"];
            [userData setValue:[userDataToSet valueForKey:@"deals"]                     forKey:@"deals"];
            [userData setValue:[json valueForKey:@"phone_no"]                           forKey:@"phone_no"];
            [userData setValue:[userDataToSet valueForKey:@"rating"]                    forKey:@"rating"];
            
            if(isImageChanged)
                [userData setValue:[json valueForKey:@"user_pic"]                           forKey:@"user_image"];
            else
                [userData setValue:[userDataToSet valueForKey:@"user_image"]                           forKey:@"user_image"];
            
            [userData setValue:[json valueForKey:@"new_username"]                       forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults] setObject:userData forKey:UserData];

            [SVProgressHUD dismiss];
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"log"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            [editProfileDelegate doneEditingProfile];
        }
        else if([[json objectForKey:@"status"] intValue] == 1)
        {
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2)
        {
        }
    }
}

#pragma mark - UITextField

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self endEditing:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

#pragma mark- Check Phone Number

- (NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar
{
    
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    
    NSError *error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    
    
    // check if the number is to long
    
    if(simpleNumber.length>10) {
        
        // remove last extra chars.
        
        simpleNumber = [simpleNumber substringToIndex:10];
        
    }
    
    if(deleteLastChar) {
        
        // should we delete the last digit?
        
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
        
    }
    
    
    // 123 456 7890
    
    // format the number.. if it's less then 7 digits.. then use this regex.
    
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    return simpleNumber;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if(textField.tag == 502 || textField.tag == 503)
    {
        NSString* phoneString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        NSLog(@"%@",phoneString);
        // if it's the phone number textfield format it.
        UITextField *phoneTextfield;
        
        if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1){ //Driver
            phoneTextfield=(UITextField *)[self viewWithTag:502];
        }
        else {
            phoneTextfield=(UITextField *)[self viewWithTag:503];
        }
        
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            phoneTextfield.text = [self formatPhoneNumber:phoneString deleteLastChar:YES];
            
        } else {
            phoneTextfield.text = [self formatPhoneNumber:phoneString deleteLastChar:NO];
        }
         return NO;
    }
    else if(textField.tag!=11)
    {
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
        if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound)
        {
            whiteSpaceCount = 0;
            return YES;
        }
        else
        {
            if ([string isEqualToString:@" "]) {
                whiteSpaceCount = whiteSpaceCount + 1;
                if (whiteSpaceCount >=2 || (string.length==1 && textField.text.length == 0))
                    return NO;
                else
                    return YES;
            }
            whiteSpaceCount = 0;
            return YES;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    if (textField == (UITextField *)[textField viewWithTag:500]) {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
            [(UITextField *)[self viewWithTag:502] becomeFirstResponder];
        }
        else {
            [(UITextField *)[self viewWithTag:503] becomeFirstResponder];
        }
        return NO;
    }
    else if (textField == (UITextField *)[self viewWithTag:503]) {
        [textField resignFirstResponder];
        return YES;
    }
    else {
        [textField resignFirstResponder];
        return YES;
    }
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    if (self.frame.origin.y >= 0)
        [self setViewMovedUp:YES];
    else if (self.frame.origin.y < 0)
    {
        
    }
    //[self setViewMovedUp:NO];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    if (self.frame.origin.y >= 0)
        [self setViewMovedUp:YES];
    else if (self.frame.origin.y < 0)
        [self setViewMovedUp:NO];
}

-(void)setViewMovedUp:(BOOL)movedUp {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.frame;
    if (movedUp)
        rect.origin.y -= 80;
    else
        rect.origin.y += 80;
    self.frame = rect;
    [UIView commitAnimations];
}

- (void) viewDidUnload {
    [self removeObserver];
}

- (void)dealloc {
    [self removeObserver];
}

-(void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:@"keyboardWillShow"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"keyboardWillHide"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"ProfilePicChange"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showActivityIndicatorWithTitle:(NSString *)title{
    [SVProgressHUD showWithStatus:title maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}

- (void)hideActivityIndicator{
    [SVProgressHUD dismiss];
}


@end
