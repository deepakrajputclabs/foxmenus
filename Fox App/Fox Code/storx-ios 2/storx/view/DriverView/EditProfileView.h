//
//  EditProfileView.h
//  Storx
//
//  Created by click on 12/22/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "BSKeyboardControls.h"

@protocol editProfileProtocol <NSObject>

-(void)doneEditingProfile;
-(void)backFromEditProfile;
- (void)editButton :(int)buttonIndex;
@end

@interface EditProfileView : UIView <UITextFieldDelegate, BSKeyboardControlsDelegate>
{
    UIImageView *picImageView;
    NSArray *iconArray;
    NSArray *infoArray;
    BOOL isImageChanged;
    int whiteSpaceCount;
}
@property (nonatomic ,strong) BSKeyboardControls *keyboardControls;

@property(nonatomic,strong) id <editProfileProtocol>editProfileDelegate;
-(void)editProfileScreen;
@end
