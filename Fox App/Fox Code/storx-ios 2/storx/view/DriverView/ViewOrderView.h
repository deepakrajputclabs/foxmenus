//
//  ViewOrder.h
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "UIImageView+AFNetworking.h"

@protocol ViewOrderProtocol <NSObject>
@optional
- (void)backButton;
- (void)orderDeliver :(NSString *)orderIdConfirm
         customerInfo:(NSMutableArray *)customerInfo;
-(void)detailsWithUserInfo:(NSString *)userID;
@end

@interface ViewOrderView : UIView <UITableViewDataSource, UITableViewDelegate>
{
    int tableHeight;
    int headerHeight;
    NSString *orderIdString;
    UITableView *commingTableView;
    UIView *bgColorView;

    NSMutableArray *detailMedicineIdArray;
    NSMutableArray *detailImageArray;
    NSMutableArray *detailNameArray;
    NSMutableArray *detailQuantityArray;
    NSMutableArray *customerInfoMutArray;
    NSMutableArray *detailMedicinePriceArray;

}

- (void)ordersView:(NSDictionary *)json;
- (void)refreshTableView;

@property (weak) id <ViewOrderProtocol> viewOrderDelegate;
@property (strong,nonatomic) NSArray *customerNameArray;
@property (strong,nonatomic) NSArray *orderTimeArray;
@property (strong,nonatomic) NSArray *orderImageArray;
@property (strong,nonatomic) NSArray *orderIdArray;
@property (strong,nonatomic) NSArray *dropLocationAddress;


@end
