//
//  ProfileView.h
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "UIImageView+AFNetworking.h"
#import "ASIFormDataRequest.h"

@protocol profileViewProtocol <NSObject>
- (void)backButton;
- (void)menuButton;
- (void)invalidAccessToken;
- (void)editButton :(int)buttonIndex;
- (void)editProfileScreen;
- (void)changePassswordScreen;
@end


@interface ProfileView : UIView <UITextFieldDelegate,UITextViewDelegate>
{
    NSString *isFromMenuString;
    UIView *bgWhiteView;
    int whiteSpaceCount;
    CGFloat animatedDistance;
    UITextField *profileTextField;
    UITextView *profileTextView;
    UIImageView *picImageView;
    BOOL isImageChanged;
    UIButton *doneBtn ;
    NSString *deliveredOrders;
    NSString *upcomingOrders;
    UILabel *label1;
    UILabel *label2;
    NSString *string1;
    NSString *string2;
}
- (void)profileUIView;

@property (weak) id <profileViewProtocol> profileViewDelegate;
@property (strong,nonatomic) NSArray  *objArrayPlaceHolder;

@end
