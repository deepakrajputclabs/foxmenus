//
//  ChangePasswordView.h
//  Storx
//
//  Created by click on 12/22/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"

@protocol changePasswordProtocol <NSObject>

-(void)passwordChanged;
-(void)backFromChangePassword;

@end

@interface ChangePasswordView : UIView<UITextFieldDelegate>
{
    UITextField *oldPassword;
    UITextField *newPassword;
    UITextField *confirmPassword;
}
@property(strong,nonatomic) id<changePasswordProtocol>changePasswordDelegate;
-(void)changePasswordUI;
@end
