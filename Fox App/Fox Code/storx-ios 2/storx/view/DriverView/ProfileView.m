//
//  ProfileView.m
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ProfileView.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@implementation ProfileView
{
    UIButton *menuBtn;
    UIButton *backBtn;
    UIButton *editBtn;
}
@synthesize profileViewDelegate = _profileViewDelegate;

#define SMBorderProfileColor [UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:1.0f];
#define SMFontProfileColor [UIColor colorWithRed:93/255.0f green:93/255.0f blue:93/255.0f alpha:1.0f];

#pragma mark - Profile UIView

- (void)profileUIView {
    NSArray *infoArray;
    self.backgroundColor = [UIColor whiteColor];
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
        infoArray = [[NSArray alloc]initWithObjects:
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserEmail],
                     [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPhone], nil];
    }
    else {
         infoArray = [[NSArray alloc]initWithObjects:
                      [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPatientId],
                      [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPhysicianName],
                      [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPhone],
                      [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserEmail], nil];
    }
    
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
   
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:257/3 currentSupSize:SCREEN_HEIGHT])];
    headerView.alpha = 1;
    headerView.backgroundColor = [UIColor clearColor];
    [self addSubview:headerView];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:220/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:842/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:197/3 currentSupSize:SCREEN_HEIGHT])];
    [headerLabel setFont:[UIFont fontWithName:FontBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]]];
    headerLabel.textColor = ColorFFFFFF;
    headerLabel.numberOfLines = 0;
    headerLabel.text = [[[NSUserDefaults standardUserDefaults] valueForKey:UserData] objectForKey:UserName];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:headerLabel];
    
    UIButton *menuButton;
    menuButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [menuButton addTarget:self action:@selector(menuButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
    [headerView addSubview:menuButton];
    
    UIButton *editButton = [[UIButton alloc] init];
    editButton.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1062/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:197/3 currentSupSize:SCREEN_HEIGHT]);
    [editButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editButton setTitleColor:ColorFFFFFF forState:UIControlStateNormal];
    editButton.backgroundColor = [UIColor clearColor];
    editButton.titleLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    [editButton addTarget:self action:@selector(editButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:editButton];
    
    picImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:439/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:331/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:364/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:364/3 currentSupSize:SCREEN_HEIGHT])];
    picImageView.contentMode = UIViewContentModeScaleToFill;
    picImageView.layer.borderWidth = 1.0;
    picImageView.layer.borderColor = ColorGreenButton.CGColor;
    picImageView.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:364/6 currentSupSize:SCREEN_HEIGHT];
    [picImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:UserData] objectForKey:@"user_image"]]] placeholderImage:[UIImage imageNamed:UserDefaultImage]];
    picImageView.layer.masksToBounds = YES;
    picImageView.userInteractionEnabled = NO;
    [self addSubview:picImageView];
    
    label1 = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:117/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:730/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:423/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    label1.backgroundColor = [UIColor clearColor];
    label1.numberOfLines = 0;
    label1.textColor = ColorFFFFFF;
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];
    [self addSubview:label1];
    
    label2 = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:731/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:730/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:423/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = ColorFFFFFF;
    label2.numberOfLines = 0;
    label2.textAlignment = NSTextAlignmentCenter;
    [label2 setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];
    [self addSubview:label2];

    UIButton *changePassword = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:201/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1877/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    changePassword.layer.masksToBounds = YES;
    changePassword.exclusiveTouch = YES;
    changePassword.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/6 currentSupSize:SCREEN_HEIGHT];
    [changePassword addTarget:self action:@selector(changePasswordClicked) forControlEvents:UIControlEventTouchUpInside];
    changePassword.userInteractionEnabled = YES;
    changePassword.backgroundColor = ColorGreenButton;
    [changePassword.titleLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]]];
    [changePassword setTitle:[NSString stringWithFormat:@"Change Password"] forState:UIControlStateNormal];
    [changePassword setTitleColor:ColorFFFFFF forState:UIControlStateNormal];
    [changePassword setTintColor:[UIColor clearColor]];
    [self  addSubview:changePassword];
   
    [self orderCall];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1)  // Driver
    {
        UIImageView *boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1030/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:350/3 currentSupSize:SCREEN_HEIGHT])];
        
        boxImage.backgroundColor = ColorBlackBK;
        boxImage.userInteractionEnabled = YES;
        [self addSubview:boxImage];
        NSArray *iconArray = [[NSArray alloc]initWithObjects:@"email_icon.png",@"contact_icon1.png", nil];
        
        float labelX = 0;
        
        for(int i =0; i<=1 ; i++){
            UIImageView *underlineImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:15/3 currentSupSize:SCREEN_WIDTH], labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1152/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            underlineImage.backgroundColor = ColorGrayBk;
            [boxImage addSubview:underlineImage];
            
            UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT])];
            iconImage.image = [UIImage imageNamed:[iconArray objectAtIndex:i]];
            [boxImage addSubview:iconImage];
            
            if (i==1) {
                underlineImage.hidden = YES;
            }
            UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], labelX, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:962/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT])];
            infoLabel.text = [infoArray objectAtIndex:i];
            infoLabel.backgroundColor = [UIColor clearColor];
            [infoLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
            infoLabel.textColor = ColorFFFFFF;
            infoLabel.textAlignment = NSTextAlignmentLeft;
            [boxImage addSubview:infoLabel];
            
            labelX = labelX + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT];
        }
    }
    else
    {
        UIImageView *boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1030/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:700/3 currentSupSize:SCREEN_HEIGHT])];
        boxImage.backgroundColor = ColorBlackBK;
        boxImage.userInteractionEnabled = YES;
        [self addSubview:boxImage];
        
        NSArray *iconArray = [[NSArray alloc]initWithObjects:@"Patient ID.png",@"Physican Name.png",@"contact_patient.png",@"email_icon_patient.png", nil];
        float labelX = 0;
        
        for(int i =0; i<=3 ; i++){
            UIImageView *underlineImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:15/3 currentSupSize:SCREEN_WIDTH], labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:175/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1152/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            underlineImage.backgroundColor = ColorGrayBk;
            [boxImage addSubview:underlineImage];
            
            UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], labelX +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT])];
            iconImage.image = [UIImage imageNamed:[iconArray objectAtIndex:i]];
            [boxImage addSubview:iconImage];
            
            if (i==3) {
                underlineImage.hidden = YES;
            }
            
            UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], labelX, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:962/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:171/3 currentSupSize:SCREEN_HEIGHT])];
            infoLabel.text = [infoArray objectAtIndex:i];
            infoLabel.backgroundColor = [UIColor clearColor];
            [infoLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
            infoLabel.textColor = ColorFFFFFF;
            infoLabel.textAlignment = NSTextAlignmentLeft;
            [boxImage addSubview:infoLabel];
            
            labelX = labelX + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:171/3 currentSupSize:SCREEN_HEIGHT];
        }
    }
    
}

#pragma mark - Orders Json Call

- (void)orderCall {
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"driver_orders"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken]] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"json: %@",json);
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            deliveredOrders = [json valueForKey:@"delivered_orders"];
            upcomingOrders = [json valueForKey:@"upcoming_orders"];
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode]integerValue] == 1) {
                string1 = [NSString stringWithFormat:@"%@",deliveredOrders];
                string2 = [NSString stringWithFormat:@"%@",upcomingOrders];
                
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\nDelivered Orders",string1]];
                [attributedString addAttribute:NSFontAttributeName
                                         value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]
                                         range:NSMakeRange(0,string1.length)];
                label1.attributedText = attributedString;
                
                attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\nUpcoming Orders",string2]];
                [attributedString addAttribute:NSFontAttributeName
                                         value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]
                                         range:NSMakeRange(0, string2.length)];
                label2.attributedText = attributedString;
            }
            else {
                string1 = [NSString stringWithFormat:@"%@",[json valueForKey:@"customer_orders"]];
                string2 = [NSString stringWithFormat:@"INR %@",[json valueForKey:@"donations"]];
                
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\nTotal Orders",string1]];
                [attributedString addAttribute:NSFontAttributeName
                                         value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]
                                         range:NSMakeRange(0, string1.length)];
                label1.attributedText = attributedString;
                
                attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\nTotal Amount",string2]];
                [attributedString addAttribute:NSFontAttributeName
                                         value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]
                                         range:NSMakeRange(0, string2.length)];
                label2.attributedText = attributedString;
            }
        }
        else if([[json objectForKey:@"status"] intValue] == 1){
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2){
            [_profileViewDelegate invalidAccessToken];
        }
    }
}

#pragma mark - Back Arrow Button Method

- (void)backButtonArrowMethod {
    
    backBtn.hidden=true;
    menuBtn.hidden=false;
    
    [doneBtn removeFromSuperview];
    doneBtn=nil;
    bgWhiteView.userInteractionEnabled = NO;
    picImageView.userInteractionEnabled = NO;
    
    UITextField *textFld =  (UITextField*)[self viewWithTag:10];
    textFld.text=[[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserName];
    
    textFld =  (UITextField*)[self viewWithTag:11];
    textFld.text=[[[NSUserDefaults standardUserDefaults] objectForKey:UserData] valueForKey:UserPhone];
    
    [picImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",
                                                        [[[NSUserDefaults standardUserDefaults] objectForKey:UserData] objectForKey:UserImage]]] placeholderImage:[UIImage imageNamed:UserDefaultImage]];
}

#pragma mark - Back Button

- (void)backButtonClicked {
    [_profileViewDelegate backButton];
}

#pragma mark - Menu Button

- (void)menuButtonClicked {
    
    [_profileViewDelegate menuButton];
}

#pragma mark - Add Image

- (void)addImage :(NSNotification *)string
{
    [[CommonClass getSharedInstance] removeImageFRom:@"ProfilePicFolder"];
    
    if ([[string name] isEqualToString:@"ProfilePicChange"])
    {
        NSDictionary *userInfo = string.userInfo;
        NSObject *myObject   = [userInfo objectForKey:@"someKey"];
        UIImage *chkPixel = (UIImage *)myObject;
        
        NSLog(@"Width: %f",chkPixel.size.width);
        NSLog(@"Height: %f",chkPixel.size.height);
        
        if(chkPixel.size.height < 100 || chkPixel.size.width < 100)
        {
            [[CommonClass getSharedInstance] removeImageFRom:@"ProfilePicFolder"];
            [[CommonClass getSharedInstance] alertMsg:@"Try uploading a different photo. Photos must be at least 100 x 100 pixels." title:@"Error"];
            
        }
        else
        {
            isImageChanged = YES;
            picImageView.image = chkPixel;
            [[CommonClass getSharedInstance] saveImage:[[CommonClass getSharedInstance] imagerotate: (UIImage *)myObject] ImageName:@"profile.jpg" FolderName:@"ProfilePicFolder"];
        }
    }
}

#pragma mark - Select Image

- (void)selectImage:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Add Image"];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"From Gallery"];
    [alert addButtonWithTitle:@"Take photo"];
    [alert addButtonWithTitle:@"Cancel"];
    alert.tag = 101;
    [alert show];
    alert = nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 101) {
        if(buttonIndex == 0)
            [_profileViewDelegate editButton:0];
        else if(buttonIndex == 0)
             [_profileViewDelegate editButton:1];
    }
}

#pragma mark - Change Password

- (void)changePasswordClicked{
    [_profileViewDelegate changePassswordScreen];
}


#pragma mark - Edit button

- (void)editButtonClicked{
    [_profileViewDelegate editProfileScreen];
}

#pragma mark - Done Editing

- (void)doneEditingClicked :(UIButton *)sender {
    
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];

    UITextField *phoneTextfield=(UITextField *)[self viewWithTag:11];
    
    NSString *phoneNumber=[[[[[[[[phoneTextfield.text componentsSeparatedByString:@"("]componentsJoinedByString:@""]componentsSeparatedByString:@")"]componentsJoinedByString:@""]componentsSeparatedByString:@"-"]componentsJoinedByString:@""]componentsSeparatedByString:@" "]componentsJoinedByString:@""];
    
    if([phoneNumber length]!=10)
    {
        [[CommonClass getSharedInstance] HideActivityIndicator];
        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Error!"        message:@"Please enter your complete phone number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert1 show];
    }
    else{
        menuBtn.hidden=false;
        backBtn.hidden=true;

        [self endEditing:YES];
        
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reach currentReachabilityStatus];
        if (netStatus == NotReachable) {
            
            [[CommonClass getSharedInstance] HideActivityIndicator];
            [[[UIAlertView alloc] initWithTitle:@"" message:ErrorInternetConnection delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else {
            
            NSMutableArray *mutArray = [[NSMutableArray alloc] init];
            
            for (int i = 0; i < 2; i++) {
                UITextField *textFld =  (UITextField*)[self viewWithTag:i+10];
                [mutArray addObject:textFld.text];
            }
            
            //adding customer Address to mutarray
            NSString *customerAddress;
            if([[[NSUserDefaults standardUserDefaults]valueForKey:DriverMode]integerValue]==2)
                customerAddress=profileTextView.text;
            
            NSString *picPath;
            if(isImageChanged)
                picPath = [[CommonClass getSharedInstance] getImageFRom:@"ProfilePicFolder" imageName:@"profile.jpg"];
            else
                picPath = @"";
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,@"edit_profile"]];
            
            ASIFormDataRequest *Request = [ASIFormDataRequest requestWithURL:url];
            [Request setPostValue:[[NSUserDefaults standardUserDefaults] valueForKey:AccessToken] forKey:@"access_token"];
            [Request setPostValue:[mutArray objectAtIndex:0]                                      forKey:@"username"];
            [Request setPostValue:[mutArray objectAtIndex:1]                                      forKey:@"phone_no"];
            if(isImageChanged)
            {
                [Request setFile:picPath                                                         forKey:@"user_image"];
            }
            if([[[NSUserDefaults standardUserDefaults]valueForKey:DriverMode]integerValue]==2)
            {
                [Request setPostValue:customerAddress                                             forKey:@"address"];
            }
            [Request setPostValue:[NSString stringWithFormat:@"%d",isImageChanged]                forKey:@"user_pic"];
            [Request setDelegate:self];
            [Request startSynchronous];
        }
    }
}

#pragma mark : ASSIHTTPRequest

- (void)requestWentWrong:(ASIHTTPRequest *)request
{
    [[CommonClass getSharedInstance] HideActivityIndicator];
}

-(void)requestStarted:(ASIHTTPRequest *)request{
    NSLog(@"ASIHTTP start request");
}

-(void)requestFailed:(ASIHTTPRequest *)request{
    NSLog(@"ASI failed");
    [[CommonClass getSharedInstance] HideActivityIndicator];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSString *jsonString = [request responseString];
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
    NSLog(@"Custom Affiliation: %@",json);
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"log"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];

            bgWhiteView.userInteractionEnabled = NO;
            //self.userInteractionEnabled = NO;
            picImageView.userInteractionEnabled = NO;
            [doneBtn removeFromSuperview];
            
            NSMutableDictionary *userDataToSet = [[NSUserDefaults standardUserDefaults] objectForKey:UserData];
            
            NSMutableDictionary *userData = [[NSMutableDictionary alloc] init];
            [userData setValue:[userDataToSet valueForKey:@"access_token"]              forKey:@"access_token"];
            [userData setValue:[userDataToSet valueForKey:@"current_user_status"]       forKey:@"current_user_status"];
            [userData setValue:[userDataToSet valueForKey:@"dispensary_name"]           forKey:@"dispensary_name"];
            [userData setValue:[userDataToSet valueForKey:@"email"]                     forKey:@"email"];
            [userData setValue:[json valueForKey:@"phone_no"]                           forKey:@"phone_no"];
            [userData setValue:[userDataToSet valueForKey:@"rating"]                    forKey:@"rating"];
            
            if(isImageChanged)
                [userData setValue:[json valueForKey:@"user_pic"]                       forKey:@"user_image"];
            else
                [userData setValue:[userDataToSet valueForKey:@"user_image"]            forKey:@"user_image"];
            
            [userData setValue:[json valueForKey:@"new_username"]                       forKey:@"user_name"];
            [userData setValue:[json objectForKey:@"address"] forKey:@"address"];
            
            [[NSUserDefaults standardUserDefaults] setObject:userData forKey:UserData];
        }
        else if([[json objectForKey:@"status"] intValue] == 1)
        {
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2)
        {
            [_profileViewDelegate invalidAccessToken];
        }
    }
    
    [[CommonClass getSharedInstance] HideActivityIndicator];
}

#pragma mark - UITextField

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self endEditing:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

#pragma mark- Check Phone Number

- (NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar
{
    
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    
    NSError *error = NULL;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    
    
    // check if the number is to long
    
    if(simpleNumber.length>10) {
        
        // remove last extra chars.
        
        simpleNumber = [simpleNumber substringToIndex:10];
        
    }
    
    if(deleteLastChar) {
        
        // should we delete the last digit?
        
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
        
    }
    
    
    // 123 456 7890
    
    // format the number.. if it's less then 7 digits.. then use this regex.
    
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    return simpleNumber;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField.tag!=11)
    {
        NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
        NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
        if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound)
        {
            whiteSpaceCount = 0;
            return YES;
        }
        else
        {
            if ([string isEqualToString:@" "]) {
                whiteSpaceCount = whiteSpaceCount + 1;
                if (whiteSpaceCount >=2 || (string.length==1 && textField.text.length == 0))
                    return NO;
                else
                    return YES;
            }
            whiteSpaceCount = 0;
            return YES;
        }
    }
    else
    {
        NSString* phoneString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        // if it's the phone number textfield format it.
        
        UITextField *phoneTextfield=(UITextField *)[self viewWithTag:11];
        
        if (range.length == 1) {
            
            // Delete button was hit.. so tell the method to delete the last char.
            
            phoneTextfield.text = [self formatPhoneNumber:phoneString deleteLastChar:YES];
            
        } else {
            
            phoneTextfield.text = [self formatPhoneNumber:phoneString deleteLastChar:NO];
        }
        
        return false;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == (UITextField *)[textField viewWithTag:10]) {
        [(UITextField *)[self viewWithTag:11] becomeFirstResponder];
        return NO;
    }
    else if (textField == (UITextField *)[self viewWithTag:11]) {
        [textField resignFirstResponder];
        return YES;
    }
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    if (self.frame.origin.y >= 0)
        [self setViewMovedUp:YES];
    else if (self.frame.origin.y < 0)
    {
        
    }
    //[self setViewMovedUp:NO];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    if (self.frame.origin.y >= 0)
        [self setViewMovedUp:YES];
    else if (self.frame.origin.y < 0)
        [self setViewMovedUp:NO];
}

-(void)setViewMovedUp:(BOOL)movedUp {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.frame;
    if (movedUp)
        rect.origin.y -= 80;
    else
        rect.origin.y += 80;
    self.frame = rect;
    [UIView commitAnimations];
}

- (void) viewDidUnload {
    [self removeObserver];
}

- (void)dealloc {
    [self removeObserver];
}

-(void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:@"keyboardWillShow"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"keyboardWillHide"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"ProfilePicChange"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
