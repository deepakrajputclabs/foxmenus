//
//  MedicineDeliveryView.m
//  Storx
//
//  Created by click on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "MedicineDeliveryView.h"

@implementation MedicineDeliveryView

@synthesize medicineDeliveryDelegate;

#pragma mark - Get Data 
//In case of Driver

- (void)deliveryView : (NSMutableArray *)customerInfo
              orderId: (NSString *)idOrder
              detail : (NSArray *)orderDetail
             pushFlag: (NSString *)flagString
      pushButtonCheck: (BOOL)pushButtonCheck {
    
    
    self.customerName   = [customerInfo objectAtIndex:0];
    self.phoneNumber    = [customerInfo objectAtIndex:1];
    self.customerImage  = [customerInfo objectAtIndex:4];
    self.dropAddress    = [customerInfo objectAtIndex:3];
    self.totalMedicine  = [customerInfo objectAtIndex:7];
    self.totalDonation  = [customerInfo objectAtIndex:6];
    self.orderDetail    = orderDetail;
    self.orderID        = idOrder;
    self.flagCheck      = flagString;
    
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
    storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
    [self addSubview:storxImageView];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:DriverMode]integerValue] == 2) {
        
        UIButton *backButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
      
        [backButton addTarget:self action:@selector(backMethodClicked) forControlEvents:UIControlEventTouchUpInside];
        [bkImageView addSubview:backButton];
        
        if(pushButtonCheck) {
            backButton.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT]);
            [backButton setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
            [backButton setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
        } else {
            [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
            [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
        }
    }
    [self patientDetail];
    [self patientOrderDetail];
    [self bottomView];
}

#pragma mark - Back Method

- (void)backMethodClicked {
    [self.medicineDeliveryDelegate backMethod];
}

-(void)patientDetail{
    
  UIImageView *boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:277/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:340/3 currentSupSize:SCREEN_HEIGHT])];
    
    boxImage.backgroundColor = ColorBlackBK;
    boxImage.userInteractionEnabled = YES;
    [self addSubview:boxImage];
    
   UILabel *orderDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
    orderDetailLabel.backgroundColor = ColorDarkBlackBK;
    orderDetailLabel.text = @" Customer Details";
    orderDetailLabel.textColor = ColorFFFFFF;
    orderDetailLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    [boxImage addSubview:orderDetailLabel];
    
    UIImageView *customerImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:135/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:170/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
    [customerImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.customerImage]] placeholderImage:[UIImage imageNamed:UserDefaultImage]];
    customerImage.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/6 currentSupSize:SCREEN_HEIGHT];
    customerImage.layer.borderColor = ColorGreenButton.CGColor;
    customerImage.layer.masksToBounds = YES;
    customerImage.layer.borderWidth = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:6/3 currentSupSize:SCREEN_HEIGHT];
    [boxImage addSubview:customerImage];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode]integerValue]==2) {
        orderDetailLabel.text = @" Restaurant Details";
        
        UIButton *callButton;
        callButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1006/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT])];
        callButton.userInteractionEnabled = YES;
        [callButton addTarget:self action:@selector(callMethod) forControlEvents:UIControlEventTouchUpInside];
        [callButton setBackgroundImage:[UIImage imageNamed:@"CallIcon.png"] forState:UIControlStateNormal];
        [callButton setBackgroundImage:[UIImage imageNamed:@"CallIconPressed.png"] forState:UIControlStateHighlighted];
        [boxImage addSubview:callButton];
        
        UILabel *customerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:268/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:738/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT])];
        customerLabel.backgroundColor = [UIColor clearColor];
        customerLabel.numberOfLines = 2;
        customerLabel.textColor = ColorFFFFFF;
        customerLabel.textAlignment = NSTextAlignmentLeft;
        [customerLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];;
        
        NSString *str1 = self.customerName;
        NSString *str2 = [NSString stringWithFormat:@"    %@",self.dropAddress];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\n%@",str1,str2]];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                                 range:NSMakeRange(0, str1.length)];
        customerLabel.attributedText = attributedString;
        [boxImage addSubview:customerLabel];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [paragraphStyle setLineBreakMode:NSLineBreakByTruncatingTail];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,customerLabel.text.length)];
        customerLabel.attributedText = attributedString ;
        
        UIImageView *locationIcon = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:268/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:33/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:50/3 currentSupSize:SCREEN_HEIGHT])];
        locationIcon.image = [UIImage imageNamed:@"location_icon_small.png"];
        [boxImage addSubview:locationIcon];
    }
    
    else {
        
        UILabel *customerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:268/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:914/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT])];
        customerLabel.backgroundColor = [UIColor clearColor];
        customerLabel.numberOfLines = 2;
        customerLabel.textColor = ColorFFFFFF;
        customerLabel.textAlignment = NSTextAlignmentLeft;
        [customerLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];;
        
        NSString *str1 = self.customerName;
        NSString *str2 = [NSString stringWithFormat:@"    %@",self.dropAddress];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\n%@",str1,str2]];
        [attributedString addAttribute:NSFontAttributeName
                                 value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                                 range:NSMakeRange(0, str1.length)];
        customerLabel.attributedText = attributedString;
        [boxImage addSubview:customerLabel];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,customerLabel.text.length)];
        customerLabel.attributedText = attributedString ;
        
        UIImageView *locationIcon = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:268/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:33/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:50/3 currentSupSize:SCREEN_HEIGHT])];
        locationIcon.image = [UIImage imageNamed:@"location_icon_small.png"];
        [boxImage addSubview:locationIcon];
    }
}

#pragma mark - Call Button

- (void)callMethod {
    
    NSString *cleanedString = [[[NSString stringWithFormat:@"%@",self.phoneNumber] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];

    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]]]){
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Call" message:self.phoneNumber delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
        warningAlert.tag = 1000;
        [warningAlert show];
        
    }else{
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your device doesn't support call facility!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",self.phoneNumber] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
    }
}

-(void)patientOrderDetail{
 
    UIImageView *boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:650/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(100/3)+ (4*200)/3 currentSupSize:SCREEN_HEIGHT])];
    
    boxImage.backgroundColor = ColorBlackBK;
    boxImage.userInteractionEnabled = YES;
    [self addSubview:boxImage];
    
    UILabel *medicineDetail = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
    medicineDetail.backgroundColor = ColorDarkBlackBK;
    medicineDetail.text = @" Requested Order Details"; //MAde a Change HEre
    medicineDetail.textColor = ColorFFFFFF;
    medicineDetail.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    [boxImage addSubview:medicineDetail];
    
    UIScrollView *scroll = [[UIScrollView alloc]init];
    //ScrollView
    [scroll setFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:4*200/3 currentSupSize:SCREEN_HEIGHT])];
    [scroll setBackgroundColor:[UIColor clearColor]];
    scroll.userInteractionEnabled=true;
    [boxImage addSubview:scroll];
    
    float productHeight = 0;
    productHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT];
    
    for (int i =0; i< [self.orderDetail count]; i++) {
        UIImageView *productImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], productHeight +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:132/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:132/3 currentSupSize:SCREEN_HEIGHT])];
        [productImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[self.orderDetail objectAtIndex:i] valueForKey:@"medicine_image"]]] placeholderImage:[UIImage imageNamed:@"StorX1.png"]];
        productImage.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:132/6 currentSupSize:SCREEN_HEIGHT];
        productImage.layer.borderColor = [UIColor whiteColor].CGColor;
        productImage.layer.masksToBounds = YES;
        productImage.layer.borderWidth = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:6/3 currentSupSize:SCREEN_HEIGHT];
        [scroll addSubview:productImage];
        
        UILabel  *productLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:235/3 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1152/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        productLabel.backgroundColor = [UIColor clearColor];
        productLabel.text = [NSString stringWithFormat:@"%@",[[self.orderDetail objectAtIndex:i] valueForKey:@"medicine_name"]];
        productLabel.textColor = ColorFFFFFF;
        productLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:productLabel];
        
        UIImageView *borderImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(200/3) currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
        borderImage.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
        [scroll addSubview:borderImage];
        
        productHeight = productHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT];
    }
    scroll.contentSize = CGSizeMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:[self.orderDetail count]*200/3 currentSupSize:SCREEN_HEIGHT]);
}


-(void)bottomView
{
    UIImageView *boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1745/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:470/3 currentSupSize:SCREEN_HEIGHT])];
    
    boxImage.backgroundColor = ColorDarkBlackBK;
    boxImage.userInteractionEnabled = YES;
    [self addSubview:boxImage];
    
    UIButton *doneButton;
    doneButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:296/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:260/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:650/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    doneButton.userInteractionEnabled = YES;
    [doneButton addTarget:self action:@selector(doneOrder) forControlEvents:UIControlEventTouchUpInside];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [doneButton setBackgroundImage:[UIImage imageNamed:@"done_onclick.png"] forState:UIControlStateHighlighted];
    [boxImage addSubview:doneButton];
    
    UILabel *orderLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:326/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:650/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:260/3 currentSupSize:SCREEN_HEIGHT])];
    orderLabel.backgroundColor = [UIColor clearColor];
    orderLabel.numberOfLines = 0;
    orderLabel.textColor = ColorFFFFFF;
    orderLabel.userInteractionEnabled =YES;
    orderLabel.textAlignment = NSTextAlignmentCenter;
    [orderLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]];;
    
    NSString *str1 = [NSString stringWithFormat:@"    Total Count: %@ items",self.totalMedicine];
    NSString *str2 = [NSString stringWithFormat:@"Total Amount: INR %@",self.totalDonation];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\n%@",str1,str2]];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]
                             range:NSMakeRange(0, 16)];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                             range:NSMakeRange(16, str1.length-16)];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]]
                             range:NSMakeRange(str1.length+1, 16)];
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                             range:NSMakeRange(str1.length+16+1, str2.length-16)];

    orderLabel.attributedText = attributedString;
    [boxImage addSubview:orderLabel];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:6];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,orderLabel.text.length)];
    orderLabel.attributedText = attributedString ;

}

-(void)doneOrder{
    [medicineDeliveryDelegate feedbackAfterDone:self.orderID];
}

-(void)menuButtonClicked{
    [medicineDeliveryDelegate menuScreen];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
