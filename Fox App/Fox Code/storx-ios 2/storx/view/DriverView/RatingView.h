//
//  RatingView.h
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "RateView.h"
#import "UIImageView+AFNetworking.h"

@protocol RatingViewProtocol <NSObject>
- (void)backButton;
- (void)invalidAccessToken;
- (void)submitReview :(NSString *)rating
              comment:(NSString *)comment
              apiName:(NSString *)apiName;
@end

@interface RatingView : UIView <UITextViewDelegate, UIGestureRecognizerDelegate>
{
    NSString *orderIdString;
    NSString *ratingString;
    UITextView *commentTextView;
    int whiteSpaceCount;
    CGFloat animatedDistance;
    UIButton *commentBtn;
    UIView *bgWhiteView;
    NSMutableArray *customerArray;
    UIButton *addComment;
    UILabel *commentLabel;
    UIButton *starButton1;
    UIButton *starButton;
}

- (void)ratingUI;
- (void)ratingViewFunction :(NSMutableArray *)customerMutArray;

@property (weak) id <RatingViewProtocol> ratingViewDelegate;
@property (strong,nonatomic) RateView *rateView;

@end
