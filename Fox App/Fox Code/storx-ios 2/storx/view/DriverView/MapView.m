//
//  MapView.m
// Storx
//
//  Created by clicklabs on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "MapView.h"
#import "MDDirectionService.h"


#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@implementation MapView
{
    UIImageView *footerView;
    NSDictionary *jsonSend;
    float latitude;
    float longitude;
    GMSMarker *marker;
    GMSMarker *previousMarker;
    UIImageView *bkImageView;
    
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSPolyline *polyline;
    GMSPath *path;
    GMSMapView *mapView_;
}
@synthesize mapViewDelegate = _mapViewDelegate;
@synthesize getCurrentLocation;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)clearMarkerFromMap {
    if (mapView_) {
        [mapView_ clear];
    }
}

#pragma mark - MapView

-(void)mapView {
    self.backgroundColor = [UIColor blackColor];
    bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    menuButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    menuButton.backgroundColor = [UIColor clearColor];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
    [self addSubview:menuButton];
    
    if (mapView_) {
        [mapView_ clear];
        [mapView_ removeFromSuperview];
    }
    [self createMapView];
}

- (void)mapUIView :(NSString *)orderId
        startOrder:(BOOL)isStartOrder
              flag:(NSString *)flagCheck
 isMenuButtonShaow:(BOOL)isMenuButtonShaow {
    
    flagCheckString = flagCheck;
    // Header Start
   
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) { //Driver
        menuButton.hidden = YES;
        
        NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"particular_order_details"
                                                                           data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                                  [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                                  orderId] dataUsingEncoding:NSUTF8StringEncoding]
                                                                           Type:@"POST"
                                                               loadingindicator:YES];
        NSLog(@"order_details: %@",json);
        
        if(json) {
            
            if([[json objectForKey:@"status"] intValue] == 0) {
                self.customerNameArray      = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_name"];
                self.deliveryTimeArray      = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"delivery_time"];
                self.totalMedicineArray     = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"total_medicines"];
                self.detailArray            = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"details"];
                self.dropLocationAddress    = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_location_address"];
                self.dropLatArray           = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_latitude"];
                self.dropLongArray          = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_longitude"];
                
                self.orderIdArray           = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"order_id"];
                
                [[NSUserDefaults standardUserDefaults] setValue:self.orderIdArray forKey:@"OrderId"];
                
                customerInfoMutArray = [[NSMutableArray alloc]init];
                [customerInfoMutArray addObject: self.customerNameArray];
                [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"]];
                [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_email"]];
                [customerInfoMutArray addObject:self.dropLocationAddress];
                [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_image"]];
                [customerInfoMutArray addObject:self.orderIdArray];
                [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"] ];
                [customerInfoMutArray addObject:self.totalMedicineArray];
                
                [self viewUI:customerInfoMutArray showMenuButton:isMenuButtonShaow];
            }
            else if([[json objectForKey:@"status"] intValue] == 1) {
                [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else if([[json objectForKey:@"status"] intValue] == 2) {
                [_mapViewDelegate invalidAccessToken];
            }
        }
        else {
            [_mapViewDelegate invalidAccessToken];
        }
    }
    else {
        menuButton.hidden = NO;
        
        NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"order_details_to_customer"
                                                                           data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                                  [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                                  orderId] dataUsingEncoding:NSUTF8StringEncoding]
                                                                           Type:@"POST"
                                                               loadingindicator:YES];
        NSLog(@"Customer Map Side: %@",json);
        if(json) {
            jsonSend = [[json objectForKey:@"dispensery_details"] objectAtIndex:0];

            if([[json objectForKey:@"status"] intValue] == 0) {
                
                self.dispImageString = [[[json objectForKey:@"data"]objectAtIndex:0] valueForKey:@"dispensery_image"];
                NSString *dispensaryName = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_name"];
                [[NSUserDefaults standardUserDefaults] setValue:dispensaryName forKey:@"DispensaryName"];
                
                self.customerNameArray      = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"driver_name"];
                self.totalMedicineArray     = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"total_medicines"];
                self.detailArray            = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"details"];
                if([flagCheckString isEqualToString:@"19"]) { //Pick Up
                    self.dropLocationAddress    = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_address"];
                    self.deliveryTimeArray      = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"delivery_time"];
                }
                else  if([flagCheckString isEqualToString:@"10"]) {
                    self.dropLocationAddress    = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_address"];
                    self.deliveryTimeArray      = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"pickup_time"];
                }
                else{
                    self.dropLocationAddress    = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_location_address"];
                }
                self.dropLatArray           = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_latitude"];
                self.dropLongArray          = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_longitude"];
                self.orderIdArray           = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"order_id"];
                self.dispensaryId           = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_id"];
                [[NSUserDefaults standardUserDefaults] setValue:self.orderIdArray  forKey:@"PreviousOrderId"];
                
                customerInfoMutArray = [[NSMutableArray alloc]init];
                
                if([flagCheckString isEqualToString:@"19"] || [flagCheckString isEqualToString:@"10"]) { //Pick Up
                    
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_name"]];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"]];
                    [customerInfoMutArray addObject:@" "];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_address"]];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_image"]];
                    [customerInfoMutArray addObject:self.orderIdArray];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"]];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"total_medicines"]];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"rating"]];
                }
                else {
                    [customerInfoMutArray addObject:self.customerNameArray];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"]];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"driver_email"]];
                    [customerInfoMutArray addObject:self.dropLocationAddress ];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"driver_image"]];
                    [customerInfoMutArray addObject:self.orderIdArray];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"]];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"total_medicines"]];
                    [customerInfoMutArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"rating"]];
                }
                [self viewUI:customerInfoMutArray showMenuButton:isMenuButtonShaow];
            }
            else if([[json objectForKey:@"status"] intValue] == 1) {
                [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else if([[json objectForKey:@"status"] intValue] == 2) {
                [_mapViewDelegate invalidAccessToken];
            }
        }
        else {
            [_mapViewDelegate invalidAccessToken];
        }
    }
}

-(void)viewUI : (NSMutableArray *)customerArray showMenuButton:(BOOL)showMenuButton{
    //Header Bar View
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode]integerValue] == 1) { //Driver
        UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
        storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
        [bkImageView addSubview:storxImageView];

    }
    else {
      UILabel  *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:100/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:183/3 currentSupSize:SCREEN_HEIGHT])];
        headerLabel.textAlignment   = NSTextAlignmentCenter;
        headerLabel.textColor       = ColorFFFFFF;
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.text        = [[NSUserDefaults standardUserDefaults]valueForKey:@"DispensaryName"];
        headerLabel.font = [UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]];
        [bkImageView addSubview:headerLabel];
        
        UIButton *filterBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1050/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:183/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:183/3 currentSupSize:SCREEN_HEIGHT])];
        [filterBtn setBackgroundImage:[UIImage imageNamed:@"info_iconDetail.png"] forState:UIControlStateNormal];
        [filterBtn setBackgroundImage:[UIImage imageNamed:@"info_iconPressedDetail.png"] forState:UIControlStateHighlighted];
        filterBtn.backgroundColor = [UIColor clearColor];
        [filterBtn addTarget:self action:@selector(infoMethodClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:filterBtn];
    }
    
    UIImageView *headerView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:267/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT])];
    headerView.backgroundColor = [UIColor colorWithRed:26/255.0f green:26/255.0f blue:23/255.0f alpha:0.8];
    headerView.userInteractionEnabled = YES;
    [self addSubview:headerView];
    
    UIImageView *customerImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:25/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:170/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
    [customerImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[customerArray objectAtIndex:4]]] placeholderImage:[UIImage imageNamed:UserDefaultImage]];
    customerImage.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/6 currentSupSize:SCREEN_HEIGHT];
    customerImage.layer.borderColor = ColorGreenButton.CGColor;
    customerImage.layer.masksToBounds = YES;
    customerImage.layer.borderWidth = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:6/3 currentSupSize:SCREEN_HEIGHT];
    [headerView addSubview:customerImage];
    
    UIButton *orderDetailButton = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:816/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:210/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT])];
    orderDetailButton.userInteractionEnabled = YES;
    [orderDetailButton addTarget:self action:@selector(viewOrderClicked) forControlEvents:UIControlEventTouchDown];
    [orderDetailButton setBackgroundImage:[UIImage imageNamed:@"view_details.png"] forState:UIControlStateNormal];
    [orderDetailButton setBackgroundImage:[UIImage imageNamed:@"view_details_onclick.png"] forState:UIControlStateSelected];
    [headerView addSubview:orderDetailButton];
    
    UIButton *callButton = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1032/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:210/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:220/3 currentSupSize:SCREEN_HEIGHT])];
    callButton.userInteractionEnabled = YES;
    [callButton addTarget:self action:@selector(callMethod) forControlEvents:UIControlEventTouchDown];
    [callButton setBackgroundImage:[UIImage imageNamed:@"call_details.png"] forState:UIControlStateNormal];
    [callButton setBackgroundImage:[UIImage imageNamed:@"call_details_onclick.png"] forState:UIControlStateSelected];
    [headerView addSubview:callButton];
    
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:250/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:53/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:536/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:85/3 currentSupSize:SCREEN_HEIGHT])];
    nameLabel.text = [customerArray objectAtIndex:0];
    nameLabel.textColor = ColorFFFFFF;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    [nameLabel setFont:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    [headerView addSubview:nameLabel];
    
    footerView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2045/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
    footerView.backgroundColor = [UIColor colorWithRed:26/255.0f green:26/255.0f blue:23/255.0f alpha:0.8];
    footerView.hidden = NO;
    [self addSubview:footerView];
    
    timeLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
    timeLabel.textColor = ColorFFFFFF;                               //mins left font (70px,Bold)
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.textAlignment = NSTextAlignmentLeft;
    timeLabel.hidden = NO;
    [timeLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    [footerView addSubview:timeLabel];
    
    UIButton *deliveryBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:260/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1858/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    deliveryBtn.userInteractionEnabled = YES;
    deliveryBtn.layer.masksToBounds = YES;
    deliveryBtn.exclusiveTouch = YES;
    deliveryBtn.layer.cornerRadius = deliveryBtn.frame.size.height/2;
    [deliveryBtn setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:[UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:1.0]] forState:UIControlStateNormal];
    [deliveryBtn setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:[UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:0.8]] forState:UIControlStateHighlighted];
    [deliveryBtn.titleLabel setFont:[UIFont fontWithName:FontSemiBold size:18]];
    [deliveryBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deliveryBtn setTitle:[NSString stringWithFormat:@"ARRIVED"] forState:UIControlStateNormal];    deliveryBtn.backgroundColor = [UIColor clearColor];
    [deliveryBtn addTarget:self action:@selector(medicineDeliveredMethod) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:deliveryBtn];
    
    UIButton *currentButton = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1872/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:126/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:126/3 currentSupSize:SCREEN_HEIGHT])];
    currentButton.backgroundColor = [UIColor clearColor];
    currentButton.alpha = 1;
    [currentButton setExclusiveTouch:TRUE];
    [currentButton setBackgroundImage:[UIImage imageNamed:@"my location.png"] forState:UIControlStateNormal];
    [currentButton addTarget:self action:@selector(locButtonPressed:) forControlEvents:UIControlEventTouchDown];
    
    [self addSubview:currentButton];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:DriverMode]integerValue] == 2) {
        timeLabel.hidden = YES;
        footerView.hidden = YES;
        deliveryBtn.hidden = YES;
    }
    
    if(showMenuButton) {
        deliveryBtn.hidden = YES;
        footerView.hidden = YES;
        callButton.hidden = YES;
        orderDetailButton.hidden = YES;

        [menuButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
        [menuButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
        [menuButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchDown];
    }
    else {
        [menuButton addTarget:self action:@selector(menuButtonClicked) forControlEvents:UIControlEventTouchDown];
    }
    
    if (flagCheckString.integerValue == 19 ) {
        callButton.hidden = NO;
        orderDetailButton.hidden = NO;
        
        deliveryBtn.hidden = NO;
        [deliveryBtn setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:[UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:0.8]] forState:UIControlStateHighlighted];
        [deliveryBtn setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:[UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:1.0]] forState:UIControlStateNormal];
        [deliveryBtn.titleLabel setFont:[UIFont fontWithName:FontSemiBold size:18]];
        [deliveryBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [deliveryBtn setTitle:[NSString stringWithFormat:@"YOU ARE HERE"] forState:UIControlStateNormal];
    }
    else if (flagCheckString.integerValue == 41 ) {
        callButton.hidden = NO;
        orderDetailButton.hidden = NO;
    }
}

#pragma mark : Call

- (void)callButtonAction{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.phoneNumber]]){
        
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",self.phoneNumber] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
        
    }else{
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your device doesn't support call facility!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
    }
}

#pragma mark - Order Detail
- (void)showDetailButtonMethod{
    [_mapViewDelegate showDetailButtonAction];
}

- (void)sendMessageMethod{
    [_mapViewDelegate sendMessageAction];
}

#pragma mark - Create Map View

-(void)createMapView {
    camera = [GMSCameraPosition cameraWithLatitude:0.0
                                         longitude:0.0
                                              zoom:12];
    mapView_ = [[GoogleMapView shareCommonGoogleMap] allocateGoogleMapForCommonUseInMultipleClasses:camera withScreenSize:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:247/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1961/3 currentSupSize:SCREEN_HEIGHT])];
    mapView_.myLocationEnabled = YES;
    mapView_.settings.myLocationButton = NO;
    mapView_.delegate = self;
    mapView_.hidden = NO;
    [self addSubview: mapView_];
    [self bringSubviewToFront:mapView_];
    mapView_.camera = camera;
    
    UIButton *currentButton=[[UIButton alloc]initWithFrame:CGRectMake(self.frame.size.width-55, 157 + 10, 42, 42)];
    currentButton.backgroundColor=[UIColor clearColor];
    currentButton.alpha=1;
    [currentButton setExclusiveTouch:TRUE];
    [currentButton setBackgroundImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
    [currentButton setBackgroundImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateHighlighted];
    [currentButton addTarget:self action:@selector(locButtonPressed:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:currentButton];
    
    UIButton *openGoogleButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 55, 140, 90/2 , 90/2)];
    openGoogleButton.backgroundColor = [UIColor clearColor];
    [openGoogleButton setExclusiveTouch:TRUE];
    [openGoogleButton setBackgroundImage:[UIImage imageNamed:@"Maps_iOS.png"] forState:UIControlStateNormal];
    [openGoogleButton addTarget:self action:@selector(showGoogleApp) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:openGoogleButton];
}

#pragma mark - My current location button Method

-(void)locButtonPressed:(UIButton*)sender
{
    camera = [GMSCameraPosition cameraWithLatitude:latitude //currentCoordinate.latitude
                                         longitude:longitude //currentCoordinate.longitude
                                              zoom:12];
    mapView_.camera=camera;
}

#pragma mark - View Order

- (void)viewOrderClicked {
    [_mapViewDelegate viewOrder];
}

#pragma mark - Customer Profile

- (void)customerProfileClicked {
    [_mapViewDelegate customerProfile:customerInfoMutArray];
}

#pragma mark- Info Method 

- (void)infoMethodClicked {
    //NSLog(@"%@",jsonSend);
    [_mapViewDelegate infoMethod:jsonSend];
}

#pragma mark - Reach Destination Button

- (void)reachDestination {
    [_mapViewDelegate viewOrder:YES
                        orderId:self.orderIdArray 
                    detailorder:self.detailArray
                   customerInfo:customerInfoMutArray];
}

#pragma mark - Menu Button

- (void)menuButtonClicked {
    [_mapViewDelegate menuButton];
}

#pragma mark - Back Button

- (void)backButtonClicked {
    [_mapViewDelegate backButton];
}
#pragma mark - Medicine Delivery Button

-(void)medicineDeliveredMethod
{
    if (flagCheckString.integerValue == 41||flagCheckString.integerValue == 19) {
        if (flagCheckString.integerValue == 19) {
            [_mapViewDelegate pickUp:self.dispensaryId
                             orderId:self.orderIdArray
                         detailorder:self.detailArray
                        customerInfo:customerInfoMutArray
                          flagString:flagCheckString];
        }
        else {
            [_mapViewDelegate toDeliveryController:self.orderIdArray
                                       detailorder:self.detailArray
                                      customerInfo:customerInfoMutArray
                                        flagString:flagCheckString];
        }
    }
    else {

        [_mapViewDelegate toDeliveryController:self.orderIdArray
                                   detailorder:self.detailArray
                                  customerInfo:customerInfoMutArray
                                    flagString:flagCheckString];
    }
}

#pragma mark - Call Button
- (void)callMethod {
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",self.customerPhone] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSLog(@"phone number :%@",self.customerPhone);
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]]]){
            
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Call" message:self.customerPhone delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
            warningAlert.tag = 1000;
            [warningAlert show];
            
        }else{
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your device doesn't support call facility!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
        }
    }

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",self.customerPhone] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
    }
}

#pragma mark - Show Google Map

- (void)showGoogleApp {
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps-x-callback://?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f&directionsmode=driving&x-success=Storx://?resume=true&x-source=Storx", latitude, longitude, [self.dropLatArray floatValue], [self.dropLongArray floatValue]]]];
    }else{
        NSURL *url = [[NSURL alloc] initWithString: [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%1.6f,%1.6f&daddr=%1.6f,%1.6f&directionsmode=driving",latitude, longitude, [self.dropLatArray floatValue], [self.dropLongArray floatValue]]];
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Show Map

- (void)showMap:(float)latValue longVal:(float)longitudeValue {
    latitude = latValue;
    longitude = longitudeValue;

    waypoints_       = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]init];
    GMSCameraPosition *gCamera;
    if (latValue == 0 && longitudeValue == 0) {
        gCamera=[GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:2];
    }
    else {
        gCamera=[GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:12];
    }
    
    [mapView_ setCamera:gCamera];
    [mapView_ clear];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
    [self addFirstMarkerAtCordinate:position];
    
    position = CLLocationCoordinate2DMake([self.dropLatArray floatValue],[self.dropLongArray floatValue]);
    [self addSecondMarker:position];
}

-(void)addFirstMarkerAtCordinate:(CLLocationCoordinate2D)coordinate{
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
    
    marker = [GMSMarker markerWithPosition:position];
    
    bounds = [bounds includingCoordinate:marker.position];
    
    UIImage * image;
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // driver
        image = [UIImage imageNamed:@"both_pin@2x.png"];
    }
    else {
        image = [UIImage imageNamed:@"pick_up_pin@2x.png"];
    }
    [marker setIcon:image];
    marker.map = mapView_;
    NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                coordinate.latitude,coordinate.longitude];
    if(waypoints_.count<2){
        [waypoints_ addObject:marker];
        [waypointStrings_ addObject:positionString];
    }
    else{
        [waypoints_ replaceObjectAtIndex:1 withObject:marker];
        [waypointStrings_ replaceObjectAtIndex:1 withObject:positionString];
    }
    
    if([waypoints_ count]>1){
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                               nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                          forKeys:keys];
        MDDirectionService *mds=[[MDDirectionService alloc] init];
        SEL selector = @selector(addDirections:);
        [mds setDirectionsQuery:query
                   withSelector:selector
                   withDelegate:self];
    }
}

#pragma mark - Add Direction

- (void)addDirections:(NSDictionary *)json {
    

    //NSLog(@"%@",json);
    if(![[json valueForKey:@"status"] isEqualToString:@"ZERO_RESULTS"]) {
        
        self.timeLeft = [[[[[[json objectForKey:@"routes"] objectAtIndex:0] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"duration"] objectForKey:@"text"];
        NSDate *todayDate = [NSDate date];
        NSString *time = [NSString stringWithFormat:@"%@",todayDate];
        NSString *localTime = [self convertUtcDateToLocalDateOnly:time];
        NSString *minutesTime =  [self convertTimeIntoMinutes:localTime];
        double totalMinutes = [minutesTime integerValue] + [self.timeLeft integerValue];
        timeLabel.text = [NSString stringWithFormat:@"  Time Of Delivery: %@ (%@ left)",[self stringFromTimeInterval:totalMinutes],self.timeLeft];
        
        if([[json objectForKey:@"routes"] count] >= 1) {
            if([[[NSUserDefaults standardUserDefaults]valueForKey:DriverMode]integerValue] == 2) // Customer
                footerView.hidden = YES;
            else
                footerView.hidden = NO;

            NSDictionary *routes = [json objectForKey:@"routes"][0];
            NSDictionary *route = [routes objectForKey:@"overview_polyline"];
            NSString *overview_route = [route objectForKey:@"points"];
            
            path=nil;
            path = [GMSPath pathFromEncodedPath:overview_route];
            polyline.map =nil;
            
            polyline =nil;
            polyline = [GMSPolyline polylineWithPath:path];
            polyline.map = mapView_;
            polyline.strokeWidth = 4.0;
            polyline.strokeColor = ColorGreenButton;
        }
    }
    else {
        footerView.hidden = YES;
    }
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)addSecondMarker:(CLLocationCoordinate2D)coordinate{
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
    previousMarker.map=nil;
    previousMarker =nil;
    
    marker = [GMSMarker markerWithPosition:position];
    marker.map = mapView_;
    
   
    UIImage * image;
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) { // driver
        image = [UIImage imageNamed:@"both_pin@2x.png"];
    }
    else {
        image = [UIImage imageNamed:@"pick_up_pin@2x.png"];
    }
    
    marker.snippet = self.dropLocationAddress;

    [marker setIcon:image]; //[self imageWithImage:image scaledToSize:CGSizeMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:image.size.width/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:image.size.height/2 currentSupSize:SCREEN_HEIGHT])]];
    
    previousMarker=marker;
    NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f", coordinate.latitude,coordinate.longitude];
    if(waypoints_.count<2){
        [waypoints_ addObject:marker];
        [waypointStrings_ addObject:positionString];
    }
    else{
        [waypoints_ replaceObjectAtIndex:1 withObject:marker];
        [waypointStrings_ replaceObjectAtIndex:1 withObject:positionString];
    }
    
    if([waypoints_ count]>1){
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                               nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                          forKeys:keys];
        MDDirectionService *mds=[[MDDirectionService alloc] init];
        SEL selector = @selector(addDirections:);
        [mds setDirectionsQuery:query withSelector:selector withDelegate:self];
    }
    bounds = [bounds includingCoordinate:marker.position];
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withEdgeInsets:UIEdgeInsetsMake(0, 0,0, 0)]];
}

- (NSString *)convertUtcDateToLocalDateOnly: (NSString *)utcDateTime {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];  //Specify the format that is required.
    NSString *time = [dateFormatter stringFromDate:[NSDate date]];
    return time;
}

-(NSString *)convertTimeIntoMinutes:(NSString *)time {
    NSString *zeroString = @"00:00:00";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *zeroDate = [dateFormatter dateFromString:zeroString];
    NSDate *offsetDate = [dateFormatter dateFromString:time];
    NSTimeInterval timeDifference = [offsetDate timeIntervalSinceDate:zeroDate];
    CGFloat minutes = (timeDifference/60);
    NSLog(@"%0.2fmins",minutes);
    return [NSString stringWithFormat:@"%f",minutes];
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger minutes = ti % 60;
    NSInteger hours = (ti / 60);
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes];
}
@end
