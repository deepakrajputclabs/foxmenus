//
//  RatingView.m
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "RatingView.h"

@implementation RatingView
@synthesize ratingViewDelegate = _ratingViewDelegate;
@synthesize rateView;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.4;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

#pragma marks - Rating UI

- (void)ratingUI{
    
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    UIButton *menuButton;
    menuButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [menuButton addTarget:self action:@selector(menuButtonClicked) forControlEvents:UIControlEventTouchDown];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon.png"] forState:UIControlStateNormal];
    [menuButton setBackgroundImage:[UIImage imageNamed:@"menu_icon_onclick.png"] forState:UIControlStateHighlighted];
    //[self addSubview:menuButton];
    
    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
    storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
    [self addSubview:storxImageView];
}

#pragma mark - Driver Rating Function UI

- (void)ratingViewFunction :(NSMutableArray *)customerMutArray {
   
    [self ratingUI];
    
    UILabel *rateLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:248/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [rateLabel setFont:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    rateLabel.textColor = ColorFFFFFF;
    rateLabel.backgroundColor = [UIColor clearColor];
    rateLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:rateLabel];
    rateLabel.text = @"Please Rate Your Experience";

    //Black Square
    UIImageView *boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:610/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:700/3 currentSupSize:SCREEN_HEIGHT])];
    
    boxImage.backgroundColor = ColorBlackBK;
    boxImage.userInteractionEnabled = YES;
    [self addSubview:boxImage];
    
   //Customer Image
    UIImageView *picImageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:488/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:477/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:265/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:265/3 currentSupSize:SCREEN_HEIGHT])];
    [picImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[customerMutArray objectAtIndex:1]]] placeholderImage:[UIImage imageNamed:UserDefaultImage]];
    picImageView.layer.borderWidth = 1.0;
    picImageView.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:265/6 currentSupSize:SCREEN_HEIGHT] ;
    picImageView.layer.borderColor = ColorGreenButton.CGColor;
    picImageView.clipsToBounds = YES;
    picImageView.layer.masksToBounds = YES;
    [self addSubview:picImageView];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:105/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [nameLabel setFont:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    nameLabel.textColor = ColorFFFFFF;
    nameLabel.numberOfLines = 1;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.text = [[customerMutArray objectAtIndex:0] capitalizedString];
    [boxImage addSubview:nameLabel];
    
    UIImageView *borderImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:282/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
    borderImage.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
    [boxImage addSubview:borderImage];
    
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:150/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:325/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT])];
    iconImage.image = [UIImage imageNamed:@"Location.png"];
    [boxImage addSubview:iconImage];


    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:240/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:283/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:870/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:174/3 currentSupSize:SCREEN_HEIGHT])];
    [addressLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    addressLabel.textColor = ColorFFFFFF;
    addressLabel.numberOfLines = 0;
    addressLabel.backgroundColor = [UIColor clearColor];
    addressLabel.textAlignment = NSTextAlignmentLeft;
    addressLabel.text =  [customerMutArray objectAtIndex:2];
    [boxImage addSubview:addressLabel];
    
    // Star View Start
    UIView *bgStarView = [[UIView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:460/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:240/3 currentSupSize:SCREEN_HEIGHT])];
    bgStarView.backgroundColor = ColorGrayOp50;
    [boxImage addSubview:bgStarView];
    
    ratingString = @"0";
    
    starButton = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:287/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:155/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:140/3 currentSupSize:SCREEN_HEIGHT])];
    starButton.exclusiveTouch = YES;
    starButton.userInteractionEnabled = YES;
    starButton.backgroundColor = [UIColor clearColor];
    starButton.tag = 101;
    [starButton addTarget:self action:@selector(starMethod:) forControlEvents:UIControlEventTouchUpInside];
    [starButton setBackgroundImage:[UIImage imageNamed:@"blank_star.png"] forState:UIControlStateNormal];
    [bgStarView  addSubview:starButton];
    
    starButton1 = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:740/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:80/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:155/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:140/3 currentSupSize:SCREEN_HEIGHT])];
    starButton1.exclusiveTouch = YES;
    starButton1.userInteractionEnabled = YES;
    starButton1.backgroundColor = [UIColor clearColor];
    starButton1.tag = 102;
    [starButton1 setBackgroundImage:[UIImage imageNamed:@"blank_star.png"] forState:UIControlStateNormal];
    [starButton1 addTarget:self action:@selector(starMethod:) forControlEvents:UIControlEventTouchUpInside];
    [bgStarView  addSubview:starButton1];
    
    UILabel *bammmerLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:265/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:250/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:65/3 currentSupSize:SCREEN_HEIGHT])];
    bammmerLabel.text = @"Bammer";
    [bammmerLabel setFont:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    bammmerLabel.textColor = ColorFFFFFF;
    [bgStarView addSubview:bammmerLabel];
    
    UILabel *chronicLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:715/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:250/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:65/3 currentSupSize:SCREEN_HEIGHT])];
    chronicLabel.text = @"Chronic";
    [chronicLabel setFont:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    chronicLabel.textColor = ColorFFFFFF;
    [bgStarView addSubview:chronicLabel];
//    UIImageView *rateView = [[UIImageView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:235/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:760/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
//    
//        self.rateView.notSelectedImage = [UIImage imageNamed:@"blank_star.png"];
//        self.rateView.halfSelectedImage = [UIImage imageNamed:@"half_star.png"];
//        self.rateView.fullSelectedImage = [UIImage imageNamed:@"full_star.png"];
//
//        self.rateView.rating = 0;
//        self.rateView.editable = YES;
//        self.rateView.maxRating = 5;
//        self.rateView.delegate = self;
//        [bgStarView addSubview:self.rateView];
    
    // Rating Done
        UIButton *ratingBtn = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:201/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2003/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
        ratingBtn.layer.masksToBounds = YES;
        ratingBtn.exclusiveTouch = YES;
        ratingBtn.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/6 currentSupSize:SCREEN_HEIGHT];
        ratingBtn.userInteractionEnabled = YES;
        ratingBtn.backgroundColor = ColorGreenButton;
        [ratingBtn.titleLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]]];
        [ratingBtn setTitle:[NSString stringWithFormat:@"Submit Review"] forState:UIControlStateNormal];
        [ratingBtn setTitleColor:ColorFFFFFF forState:UIControlStateNormal];
        [ratingBtn setTintColor:[UIColor clearColor]];
        [self  addSubview:ratingBtn];
    
        addComment = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:359/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1334/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:533/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT])];
        addComment.exclusiveTouch = YES;
        [addComment addTarget:self action:@selector(addComment) forControlEvents:UIControlEventTouchUpInside];
        addComment.userInteractionEnabled = YES;
        addComment.hidden = NO;
        addComment.backgroundColor = [UIColor clearColor];
        [addComment.titleLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
        [addComment setTitle:[NSString stringWithFormat:@"Add Comment"] forState:UIControlStateNormal];
        [addComment setTitleColor:ColorGreenButton forState:UIControlStateNormal];
        [addComment setTintColor:[UIColor clearColor]];
        [addComment setImage:[UIImage imageNamed:@"add_comment.png"] forState:UIControlStateNormal];
        [addComment setImage: [UIImage imageNamed:@"add_comment_onlcick.png"] forState:UIControlStateHighlighted];
        [addComment setImageEdgeInsets:UIEdgeInsetsMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:5/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:5/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:475/3 currentSupSize:SCREEN_HEIGHT])];
        [addComment setTitleEdgeInsets:UIEdgeInsetsMake([[CommonClass getSharedInstance] targetSize:TargetSize.width    targetSubSize:5/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:-3*54/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:5/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0/3 currentSupSize:SCREEN_HEIGHT])];
        [self addSubview:addComment];
    
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1)
    {
        [ratingBtn addTarget:self action:@selector(submitReviewClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    else
    {
        [ratingBtn addTarget:self action:@selector(submitCustomerReviewClicked) forControlEvents:UIControlEventTouchUpInside];
    }

}

-(void)starMethod:(UIButton *)sender {
    if (sender.tag == 101) {
        [starButton setBackgroundImage:[UIImage imageNamed:@"full_star.png"] forState:UIControlStateNormal];
        [starButton1 setBackgroundImage:[UIImage imageNamed:@"blank_star.png"] forState:UIControlStateNormal];
        ratingString = @"1";
    }
    
    else {
        [starButton1 setBackgroundImage:[UIImage imageNamed:@"full_star.png"] forState:UIControlStateNormal];
        [starButton setBackgroundImage:[UIImage imageNamed:@"blank_star.png"] forState:UIControlStateNormal];
        ratingString = @"2";
    }
}

#pragma mark - Add Comment Clicked
- (void)addComment {
    
    addComment.hidden = YES;
    commentTextView = [[UITextView alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1434/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:400/3 currentSupSize:SCREEN_HEIGHT])];
    commentTextView.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    commentTextView.returnKeyType = UIReturnKeyDone;
    commentTextView.textAlignment = NSTextAlignmentCenter;
    commentTextView.textColor = ColorFFFFFF;
    commentTextView.tag=200;
    commentTextView.hidden = NO;
    commentTextView.delegate = self;
    commentTextView.userInteractionEnabled = YES;
    commentTextView.textAlignment = NSTextAlignmentLeft;
    commentTextView.backgroundColor = ColorBlackBK;
    [commentTextView becomeFirstResponder];
    [self addSubview:commentTextView];
    
    commentLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1334/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
    commentLabel.backgroundColor = ColorDarkBlackBK;
    commentLabel.text = @" Comments :";
    commentLabel.textColor = ColorFFFFFF;
    commentLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]];
    [self addSubview:commentLabel];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeRecognizer:)];
    recognizer.direction = UISwipeGestureRecognizerDirectionDown;
    recognizer.numberOfTouchesRequired = 1;
    recognizer.delegate = self;
    [self addGestureRecognizer:recognizer];
}

- (void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender {
    if ( sender.direction == UISwipeGestureRecognizerDirectionDown ){
        [commentTextView resignFirstResponder];
        if(commentTextView.text.length < 1)
        {
            addComment.hidden = NO;
            commentTextView.hidden = YES;
            commentLabel.hidden = YES;
        }
    }
}

#pragma mark - Submit Review

- (void)submitCustomerReviewClicked { // At customer end
    
    if([ratingString intValue] > 0) {
        if(commentTextView.text.length >= 1)
            [_ratingViewDelegate submitReview:ratingString comment:commentTextView.text apiName:@"rate_dispensary"];
        else
            [_ratingViewDelegate submitReview:ratingString comment:@"" apiName:@"rate_dispensary"];
    }
    else {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"Please rate your experience."
                                  delegate:self
                         cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

- (void)submitReviewClicked {
    
    if([ratingString intValue] > 0) {
        if(commentTextView.text.length >= 1)
            [_ratingViewDelegate submitReview:ratingString comment:commentTextView.text apiName:@"rate_customer"];
        else
            [_ratingViewDelegate submitReview:ratingString comment:@"" apiName:@"rate_customer"];
    }
    else {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"Please rate your experience."
                                  delegate:self
                         cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

#pragma mark - Back Button

- (void)menuButtonClicked{
    [_ratingViewDelegate backButton];
}

#pragma mark - TextView

#define CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,!1234567890 * -()+~&*$#@;:^%?|/_<>""||'"

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        if([text isEqualToString:@"\n"])
        {
            if(textView.text.length < 1)
            {
//                NSDictionary *attrDict = @{NSFontAttributeName : [UIFont
//                                                                  systemFontOfSize:14.0],NSForegroundColorAttributeName :SMBlueColor};
//                NSMutableAttributedString *title =[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",@"Add Comment"] attributes: attrDict];
//                [title addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, @"Add Comment".length)];
//                [commentTextView setAttributedText:title];
                addComment.hidden = NO;
                commentTextView.hidden = YES;
                commentLabel.hidden = YES;
            }
           
            [textView resignFirstResponder];
            return YES;
        }
    }
    
    NSString *resultingString = [textView.text stringByReplacingCharactersInRange: range withString: text];
    NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
    if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound)
    {
        whiteSpaceCount = 0;
        return YES;
    }
    else
    {
        if ([text isEqualToString:@" "])
        {
            whiteSpaceCount = whiteSpaceCount + 1;
            if (whiteSpaceCount >=2 || (text.length==1 && textView.text.length == 0))
            {
                return NO;
            }
            else
                return YES;
        }
        whiteSpaceCount = 0;
        return YES;
    }
    
    return YES;
}

-(void) textViewDidBeginEditing:(UITextView *)textView
{
    //btnCancel.hidden = YES;
    textView.text = @"";
    commentTextView.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1434/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:400/3 currentSupSize:SCREEN_HEIGHT]);

    CGRect textFieldRect = [self convertRect:textView.bounds fromView:textView];
    CGRect viewRect = [self convertRect:self.bounds fromView:self];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if(heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }else if(heightFraction > 1.0)
    {
        heightFraction = 1.0;
        
    }
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)+80;
    }
    CGRect viewFrame = self.frame;
    viewFrame.origin.y -= animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self setFrame:viewFrame];
    [UIView commitAnimations];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    commentTextView.backgroundColor = [UIColor clearColor];
    CGRect viewFrame = self.frame;
    viewFrame.origin.y += animatedDistance;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self setFrame:viewFrame];
    [UIView commitAnimations];
}

@end
