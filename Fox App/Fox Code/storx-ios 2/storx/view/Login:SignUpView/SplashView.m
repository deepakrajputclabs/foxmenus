//
//  SplashView.m
//  Storx
//
//  Created by clicklabs on 12/16/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "SplashView.h"

@implementation SplashView
{
    UIButton *patientButton;
    UIButton *dispensaryButton;
    UIImageView *storxIconImageView;
    UIImageView *storxImageView;
    UIImageView *imageView;
    
    UILabel *swipeLabel;
}

@synthesize splashDelegate = _splashDelegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)splashView {
    pageControlBeingUsed = NO;
    
    i = 0;
    bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg_for_splash.png"];
    bkImageView.userInteractionEnabled = YES;
    [self addSubview:bkImageView];
    
    storxIconImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 603/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:319/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:603/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:585/3 currentSupSize:SCREEN_HEIGHT])];
    storxIconImageView.image = [UIImage imageNamed:@"StorXMain.png"];
    storxIconImageView.userInteractionEnabled = YES;
    [bkImageView addSubview:storxIconImageView];
 
    patientButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:197/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1714/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [bkImageView addSubview:patientButton];
    
    dispensaryButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:197/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1935/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [bkImageView addSubview:dispensaryButton];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"TutorialScreen"]) {
        NSString *str1 = @"Swipe up to learn";
        NSString *str2 = @"HOW IT WORKS? ";
        
        storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 120/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1487/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:120/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        storxImageView.image = [UIImage imageNamed:@"swipe.png"];
        storxImageView.userInteractionEnabled = YES;
        [bkImageView addSubview:storxImageView];
        
        UIButton *backButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
        [backButton addTarget:self action:@selector(backMethodCall) forControlEvents:UIControlEventTouchUpInside];
        [bkImageView addSubview:backButton];
        
        swipeLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1560/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        swipeLabel.textAlignment = NSTextAlignmentCenter;
        swipeLabel.textColor = [UIColor whiteColor];
        swipeLabel.backgroundColor = [UIColor clearColor];
        swipeLabel.font = [UIFont fontWithName:FontRegular  size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:28/3 currentSupSize:SCREEN_HEIGHT]];
        swipeLabel.text = [NSString stringWithFormat:@"%@ %@",str1,str2];
        [bkImageView addSubview:swipeLabel];
        
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:swipeLabel.text];
        [title addAttribute:NSFontAttributeName value:[UIFont fontWithName:FontRegular  size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:46/3 currentSupSize:SCREEN_HEIGHT]] range:NSMakeRange(0, str1.length)];
        [title addAttribute:NSFontAttributeName value:[UIFont fontWithName:FontRegular  size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:48/3 currentSupSize:SCREEN_HEIGHT]] range:NSMakeRange(str1.length, str2.length)];
        [title addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:255/255.0f green:193/255.0f blue:61/255.0f alpha:1.0] range:NSMakeRange(str1.length, str2.length)];
        swipeLabel.attributedText = title;
        
        //Swipe
        UIImageView *bkSwipe =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1420/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:300/3 currentSupSize:SCREEN_HEIGHT])];
        bkSwipe.backgroundColor = [UIColor clearColor];
        bkSwipe.userInteractionEnabled = YES;
        [bkImageView addSubview:bkSwipe];
        
        UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideGestureRecognizer:)];
        swipe.direction = UISwipeGestureRecognizerDirectionUp;
        [bkSwipe addGestureRecognizer:swipe];
        // Ends
        
            [patientButton setBackgroundImage:[UIImage imageNamed:@"signin.png"] forState:UIControlStateNormal];
            [patientButton setBackgroundImage:[UIImage imageNamed:@"signin_onclick.png"] forState:UIControlStateHighlighted];
            [patientButton addTarget:self action:@selector(signInMethodCall) forControlEvents:UIControlEventTouchUpInside];
            
            dispensaryButton.hidden = NO;
            [dispensaryButton setBackgroundImage:[UIImage imageNamed:@"register.png"] forState:UIControlStateNormal];
            [dispensaryButton setBackgroundImage:[UIImage imageNamed:@"register_onclick.png"] forState:UIControlStateHighlighted];
            [dispensaryButton addTarget:self action:@selector(registerMethodCall) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [patientButton setBackgroundImage:[UIImage imageNamed:@"Patient.png"] forState:UIControlStateNormal];
        [patientButton setBackgroundImage:[UIImage imageNamed:@"Patient_onclick.png"] forState:UIControlStateHighlighted];
        [patientButton addTarget:self action:@selector(patientMethod) forControlEvents:UIControlEventTouchUpInside];
        
        [dispensaryButton setBackgroundImage:[UIImage imageNamed:@"Driver.png"] forState:UIControlStateNormal];
        [dispensaryButton setBackgroundImage:[UIImage imageNamed:@"Driver_onclick.png"] forState:UIControlStateHighlighted];
        [dispensaryButton addTarget:self action:@selector(dispensaryMethod) forControlEvents:UIControlEventTouchUpInside];
    }
}

#pragma mark - BackMethod

- (void)backMethodCall {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TutorialScreen"];
    [_splashDelegate backMethod];
}

#pragma mark - Patient Method

- (void)patientMethod {
   // [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:DriverMode];
    [_splashDelegate tutorialMethod];
}

#pragma mark - Dispensary Method

- (void)dispensaryMethod {
   // [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:DriverMode];
    [_splashDelegate dispensaryLogin];
}

#pragma mark - SignIn Method

- (void)signInMethodCall {
    [_splashDelegate signInMethod];
}

#pragma mark - Register Method

- (void)registerMethodCall {
    [_splashDelegate registerMethod];
}

#pragma mark - Swipe Gesture

-(void)slideGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer {
    animateCount = 0;
    
    [swipeLabel removeFromSuperview];
    [storxImageView removeFromSuperview];
    
    storxIconImageView.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 603/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:603/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:585/3 currentSupSize:SCREEN_HEIGHT]);
    [[CommonClass getSharedInstance] startFade:storxIconImageView];

    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:665/3  currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:973/3 currentSupSize:SCREEN_HEIGHT])];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    scrollView.bounces = NO;
    scrollView.clipsToBounds = NO;
    scrollView.layer.masksToBounds = YES;
    [bkImageView addSubview:scrollView];

    int imageViewX = [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH];
    
    for (int icount = 0; icount < 3; icount++) {
        
        imageView =    [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0  currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:973/3 currentSupSize:SCREEN_HEIGHT])];
        imageView.userInteractionEnabled = YES;
        [scrollView addSubview:imageView];
        
        if(icount == 0) {
            imageView.image = [UIImage imageNamed:@"swipe_info_1.png"];
        }
        else if(icount == 1)    {
            imageView.image = [UIImage imageNamed:@"swipe_info_2.png"];
        }
        else {
            imageView.image = [UIImage imageNamed:@"swipe_info_3.png"];
        }
        imageViewX = imageViewX + [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH];
    }

    scrollView.contentSize = CGSizeMake(([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:973/3 currentSupSize:SCREEN_WIDTH]) * 3.8, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:973/3 currentSupSize:SCREEN_HEIGHT]);
   
    bkPageController =    [[UIPageControl alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1525/3  currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:140/3 currentSupSize:SCREEN_HEIGHT])];
    bkPageController.backgroundColor = [UIColor clearColor];
    bkPageController.currentPage = 0;
    bkPageController.numberOfPages = 3;
    bkPageController.defersCurrentPageDisplay = YES;
    bkPageController.pageIndicatorTintColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:0.4];
    [bkImageView addSubview:bkPageController];
    bkPageController.currentPageIndicatorTintColor = ColorGreenButton;

    [[CommonClass getSharedInstance] startFade:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        bkPageController.currentPage = page;
        bkPageController.currentPageIndicatorTintColor = ColorGreenButton;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}
@end
