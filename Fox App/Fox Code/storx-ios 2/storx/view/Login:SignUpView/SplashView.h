//
//  SplashView.h
//  Storx
//
//  Created by clicklabs on 12/16/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonClass.h"

@protocol splashProtocol <NSObject>
@optional
- (void)signInMethod;
- (void)registerMethod;
- (void)backMethod;
- (void)dispensaryLogin;
@optional
- (void)tutorialMethod;

@end

@interface SplashView : UIView <UIScrollViewDelegate>
{
    UIImageView *bkImageView;
    int i;
    int animateCount;
    UIImageView *circleImageView;
    UIPageControl *bkPageController;
    BOOL pageControlBeingUsed;
    UIScrollView *scrollView;
}

- (void)splashView;
@property (weak) id <splashProtocol> splashDelegate;

@end
