//
//  LoginView.h
// Storx
//
//  Created by ClickLabs26 on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterView.h"
#import "Header.h"
#import "ServerCallModel.h"
#import "CommonClass.h"
#import <GoogleMaps/GoogleMaps.h>

@protocol loginViewButtonDelegate;

@interface LoginView : UIView<UITextFieldDelegate,CLLocationManagerDelegate>
@property (nonatomic, assign) id<loginViewButtonDelegate> loginViewButtonActionDelegate;
@end

@protocol loginViewButtonDelegate <NSObject>
- (void)backMethod;
- (void)homeControllerMethod;
- (void)invalidAccessToken;
-(void)forgotPasswordMethod;
@end


