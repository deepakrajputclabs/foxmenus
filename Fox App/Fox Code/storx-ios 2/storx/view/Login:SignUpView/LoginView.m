//
//  LoginView.m
// Storx
//
//  Created by ClickLabs26 on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "LoginView.h"

@implementation LoginView
{
    UIImageView *bkImageView;
    CGRect screenBounds;
    float changedYcordinate;
    int whiteSpaceCount;
}
@synthesize loginViewButtonActionDelegate = _loginViewButtonActionDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
        [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
        [[NSNotificationCenter defaultCenter] removeObserver:@"ProfilePicChange"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        bkImageView.image = [UIImage imageNamed:@"bg.png"];
        bkImageView.userInteractionEnabled = YES;
        [self addSubview:bkImageView];
        
        UIButton *backButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
        backButton.backgroundColor = [UIColor clearColor];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
        [backButton addTarget:self action:@selector(backMethodCall) forControlEvents:UIControlEventTouchUpInside];
        [bkImageView addSubview:backButton];
        
        UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
        storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
        [bkImageView addSubview:storxImageView];
        
        NSArray *signInImages    = [NSArray arrayWithObjects:@"email_icon.png",@"Lock.png",nil];
        NSArray *placeHolderName = [NSArray arrayWithObjects:@"Email Address",@"Password",nil];
        
        int textFieldY = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:378/3 currentSupSize:SCREEN_HEIGHT];
        int imageViewY  = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:410/3 currentSupSize:SCREEN_HEIGHT];

        UITextField *registerTextfield;
        UIImageView *imageView;
        
        for (int i = 0; i < 2; i++) {
            
            if(i == 0) {
                 imageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:110/3 currentSupSize:SCREEN_WIDTH], imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:55/3 currentSupSize:SCREEN_HEIGHT])];
            }
            else {
                 imageView = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:110/3 currentSupSize:SCREEN_WIDTH], imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:49/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:65/3 currentSupSize:SCREEN_HEIGHT])];
               
            }
            imageView.image = [UIImage imageNamed:[signInImages objectAtIndex:i]];
            [bkImageView addSubview:imageView];
            
            registerTextfield = [[UITextField alloc] initWithFrame: CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:270/3 currentSupSize:SCREEN_WIDTH], textFieldY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:870/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
            registerTextfield.tag = i + 10;
            registerTextfield.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
            registerTextfield.placeholder = [placeHolderName objectAtIndex:i];
            registerTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[placeHolderName objectAtIndex:i] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
            registerTextfield.backgroundColor = [UIColor clearColor];
            registerTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
            registerTextfield.keyboardType = UIKeyboardTypeDefault;
            registerTextfield.returnKeyType = UIReturnKeyDone;
            registerTextfield.clearButtonMode = UITextFieldViewModeWhileEditing;
            registerTextfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            registerTextfield.delegate = self;
            registerTextfield.autocapitalizationType = UITextAutocapitalizationTypeWords;
            registerTextfield.textColor = [UIColor whiteColor];
            [bkImageView addSubview:registerTextfield];
            
            textFieldY = textFieldY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT];
            
            imageViewY = imageViewY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT];

            if(i == 1)
                registerTextfield.secureTextEntry = YES;
            
            UIView *lineView =    [[UIView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:72/3 currentSupSize:SCREEN_WIDTH], textFieldY - 12, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1093/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            lineView.backgroundColor = ColorGrayBk;
            [self addSubview:lineView];
        }
        
        UIButton *patientButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 840/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:963/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
        [bkImageView addSubview:patientButton];
        [patientButton setBackgroundImage:[UIImage imageNamed:@"sign_in.png"] forState:UIControlStateNormal];
        [patientButton setBackgroundImage:[UIImage imageNamed:@"sign_in_onclick.png"] forState:UIControlStateHighlighted];
        [patientButton addTarget:self action:@selector(signInMethodCall) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    UILabel *privacyLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 840/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:760/3  currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    privacyLabel.backgroundColor = [UIColor clearColor];
    privacyLabel.textAlignment = NSTextAlignmentCenter;
    privacyLabel.textColor = [UIColor whiteColor];
    privacyLabel.text = @"Forgot Password?";
    privacyLabel.font = [UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    [bkImageView addSubview:privacyLabel];
    
    UIButton *forgotButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 840/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:760/3  currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
    [forgotButton addTarget:self action:@selector(forgotPassword) forControlEvents:UIControlEventTouchUpInside];
    [forgotButton setBackgroundColor:[UIColor clearColor]];
    [bkImageView addSubview:forgotButton];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Forgot Password?"];
    [attributedString addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:FontSemiBold size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]
                  range:NSMakeRange(0, @"Forgot Password?".length)];
    [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, @"Forgot Password?".length)];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, @"Forgot Password?".length)];
    [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, @"Forgot Password?".length)];
    [privacyLabel setAttributedText:attributedString];

    return self;
}

#pragma mark - Back Method

- (void)backMethodCall {
    [_loginViewButtonActionDelegate backMethod];
}

#pragma mark - Login Button Action


- (void)signInMethodCall {
    
    NSString *email     = [(UITextField *)[self viewWithTag:10] text];
    NSString *pasword   = [(UITextField *)[self viewWithTag:11] text];

    if(![CLLocationManager locationServicesEnabled]) {
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Location Service Disable"
                                                      message:@"To enable, please go to Settings and turn on Location Service for this app."
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        alert1.tag = 1092;
        [alert1 show];
    }
    else if(email.length >= 1 && pasword.length >= 1) {
        
        NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"email_login"
                                                                           data:[[NSString stringWithFormat:@"email=%@&password=%@&device_type=%@&device_token=%@&app_version=%@",
                                                                                  [[CommonClass getSharedInstance] encodeString:email],
                                                                                  pasword,
                                                                                  @"1",
                                                                                  [[NSUserDefaults standardUserDefaults]valueForKey:DeviceToken],
                                                                                  AppVersion] dataUsingEncoding:NSUTF8StringEncoding]
                                                                           Type:@"POST"
                                                               loadingindicator:YES];
        NSLog(@"login details: %@",json);

        if(json) {
            if([[json objectForKey:@"status"] intValue] == 0) {
                NSData *newdata=[[[json objectForKey:UserData] valueForKey:@"email"] dataUsingEncoding:NSUTF8StringEncoding
                                                                                      allowLossyConversion:YES];
                NSString *mystring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
                if([mystring isEqualToString:@""]  || mystring== (id)[NSNull null] || mystring.length==0)
                    [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"email"] forKey:UserEmail];
                else
                    [[NSUserDefaults standardUserDefaults] setValue:mystring forKey:UserEmail];
                
                [[NSUserDefaults standardUserDefaults] setValue:[json objectForKey:UserData] forKey:UserData];
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"access_token"] forKey:AccessToken];
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"current_user_status"] forKey:DriverMode];
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"phone_no"] forKey:UserPhone];
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"user_image"] forKey:UserImage];
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"user_name"] forKey:UserName];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) { //Driver
                    [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"dispensery_name"] forKey:UserPhysicianName];
                    [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"user_name"] forKey:UserName];
                }
                else {
                    [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"patient_id"] forKey:UserPatientId];
                    [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"physician_name"] forKey:UserPhysicianName];
                }
                [_loginViewButtonActionDelegate homeControllerMethod];
            }
            else  if([[json objectForKey:@"status"]integerValue]==2) {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!" message:[json objectForKey:@"error"]
                                          delegate:nil
                                 cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else {
                [[[UIAlertView alloc]initWithTitle:@"Error!" message:[json objectForKey:@"error"]
                                          delegate:nil
                                 cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }
    }
    else {
        
        if(email.length < 1)
        {
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Error!"        message:@"Please enter your email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert1 show];
        }
        else{
            UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Error!"        message:@"Please enter your password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert1 show];
        }
    }
}

#pragma mark - Forgot Password 

-(void)forgotPassword {
    [_loginViewButtonActionDelegate forgotPasswordMethod];
}

#pragma mark - Textfield

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
    if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound){
        whiteSpaceCount = 0;
        return YES;
    }
    else {
        if ([string isEqualToString:@" "]) {
            whiteSpaceCount = whiteSpaceCount + 1;
            if (whiteSpaceCount >=2 || (string.length==1 && textField.text.length == 0))
                return NO;
            else
                return YES;
        }
        whiteSpaceCount = 0;
        return YES;
    }

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == (UITextField *)[textField viewWithTag:10]) {
        [(UITextField *)[self viewWithTag:11] becomeFirstResponder];
        return NO;
    }
    else if (textField == (UITextField *)[self viewWithTag:11]) {
        [(UITextField *)[self viewWithTag:11] resignFirstResponder];
        return YES;
    }
    return YES;
}


#pragma mark- loading indicator bar

- (void)showActivityIndicatorWithTitle
{
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}

- (void)hideActivityIndicator
{
    [SVProgressHUD dismiss];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}


@end
