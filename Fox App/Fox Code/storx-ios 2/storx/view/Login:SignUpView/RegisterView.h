//
//  SignupView.h
// Storx
//
//  Created by ClickLabs26 on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"
#import "BSKeyboardControls.h"

@protocol registerProtocol <NSObject>
@optional
- (void)doneButtonDelegate;
- (void)backToLoginViewAction;
- (void)changePhotoFromgallery:(int)buttonIndex;
- (void)signUpServerCall :(NSString *)name
                patientId:(NSString *)patientId
            physicianName:(NSString *)physicianName
                  contact:(NSString *)contact
             emailAddress:(NSString *)emailAddress
                 password:(NSString *)password
          confirmPassword:(NSString *)confirmPassword
                   image :(NSString *)image
                   deals :(NSString *)deals;
@end

@interface RegisterView : UIView <UITextFieldDelegate, UIGestureRecognizerDelegate,BSKeyboardControlsDelegate>
{
    int whiteSpaceCount;
    UIScrollView *scroll;
    UIImageView *addImageView;
}
@property (nonatomic, assign) id <registerProtocol> registerDelegate;
@property (nonatomic ,strong) BSKeyboardControls *keyboardControls;
@end


