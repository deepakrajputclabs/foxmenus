//
//  SignupView.m
// Storx
//
//  Created by ClickLabs26 on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "RegisterView.h"

@implementation RegisterView
{
    UIImageView *select;
    UIButton *ageCheckButton;
    UIButton *dealAndPromotionButton;
    UIImageView *checkBox1;
    UIImageView *checkBox2;
    
    BOOL isImageChanged;
    BOOL isAgeChecked;
    BOOL isdealAndPromotionChecked;
}
@synthesize registerDelegate = _registerDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
        [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
        [[NSNotificationCenter defaultCenter] removeObserver:@"ProfilePicChange"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(addImage:)
                                                     name:@"ProfilePicChange"
                                                   object:nil];
        
        UIImageView *background =    [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        background.image = [UIImage imageNamed:@"bg.png"];
        [self addSubview:background];
        
        UIButton *backButton;
        UIButton *addPhotoButton;
        scroll = [[UIScrollView alloc]init];
        //ScrollView
        [scroll setFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        [scroll setBackgroundColor:[UIColor clearColor]];
        scroll.userInteractionEnabled=true;
        [self addSubview:scroll];
        
        backButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
        [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
        [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
        [scroll addSubview:backButton];
        
        UITapGestureRecognizer *scrollTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboard)];
        scrollTap.numberOfTapsRequired = 1;
        [scroll addGestureRecognizer:scrollTap];
        
        addImageView   =  [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:((TargetSize.width - 305/3)/2) currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:268/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:305/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:305/3 currentSupSize:SCREEN_HEIGHT])];
        addImageView.contentMode = UIViewContentModeScaleAspectFill;
        addImageView.layer.cornerRadius = addImageView.frame.size.height/2;
        addImageView.layer.masksToBounds = YES;
        addImageView.image = [UIImage imageNamed:@"StorX1.png"];
        [scroll addSubview:addImageView];
        
        select = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:707/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:283/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT])];
        select.image = [UIImage imageNamed:@"EditImage.png"];
        [scroll addSubview:select];

        
        addPhotoButton = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:707/3 currentSupSize:SCREEN_WIDTH],  [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:270/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:170/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
        [addPhotoButton setBackgroundColor:[UIColor clearColor]];
        [addPhotoButton addTarget:self action:@selector(addPhotoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [scroll addSubview:addPhotoButton];
        
        NSArray *registerImages = [NSArray arrayWithObjects:@"Patient Name.png",@"Patient ID.png",@"Physican Name.png",@"contact_patient.png",@"email_icon.png",@"Lock.png",@"Confirm Password.png",nil];
        
        NSArray *placeHolderName = [NSArray arrayWithObjects:@"Customer Name",@"Customer ID",@"Enter Delivery Address",@"Verification phone #",@"Your Email Address",@"Password",@"Confirm Password",nil];

        UIImageView *imageView;
        UITextField *registerTextfield;
        
        int imageViewX = [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:121/3 currentSupSize:SCREEN_WIDTH];
        int imageViewY = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:665/3 currentSupSize:SCREEN_HEIGHT];
        
        int textFieldX = [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:292/3 currentSupSize:SCREEN_WIDTH];
        int textFieldY = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:650/3 currentSupSize:SCREEN_HEIGHT];

        NSArray *fields;
        for (int i = 0; i < registerImages.count; i++) {
            
            registerTextfield = [[UITextField alloc] initWithFrame: CGRectMake(textFieldX, textFieldY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
            registerTextfield.tag = i + 10;
            registerTextfield.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
            registerTextfield.placeholder = [placeHolderName objectAtIndex:i];
            registerTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[placeHolderName objectAtIndex:i] attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
            registerTextfield.backgroundColor   = [UIColor clearColor];
            registerTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
            registerTextfield.keyboardType      = UIKeyboardTypeDefault;
            registerTextfield.returnKeyType     = UIReturnKeyDone;
            registerTextfield.clearButtonMode   = UITextFieldViewModeWhileEditing;
            registerTextfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            registerTextfield.delegate          = self;
            registerTextfield.autocapitalizationType = UITextAutocapitalizationTypeWords;
            registerTextfield.textColor = [UIColor whiteColor];
            [scroll addSubview:registerTextfield];
            
            if(i == 0) {
                imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:65/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:65/3 currentSupSize:SCREEN_HEIGHT])];
            }
            else if(i == 1) {
                imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:72/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:72/3 currentSupSize:SCREEN_HEIGHT])];
                registerTextfield.keyboardType    = UIKeyboardTypeNumberPad;
            }
            else if(i == 2)
                imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:80/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:58/3 currentSupSize:SCREEN_HEIGHT])];
            else if(i == 3) {
                imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:68/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:68/3 currentSupSize:SCREEN_HEIGHT])];
                registerTextfield.keyboardType    = UIKeyboardTypeNumberPad;
            }
            else if(i == 4) {
                imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:70/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:55/3 currentSupSize:SCREEN_HEIGHT])];
                registerTextfield.keyboardType    = UIKeyboardTypeEmailAddress;
            }
            else if(i == 5) {
                imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:49/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT])];
                registerTextfield.secureTextEntry = YES;
            }
            else if(i == 6) {
                imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageViewX, imageViewY, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:49/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:55/3 currentSupSize:SCREEN_HEIGHT])];
                registerTextfield.secureTextEntry = YES;
            }
            
            imageView.image = [UIImage imageNamed:[registerImages objectAtIndex:i]];
            [scroll addSubview:imageView];
           
            imageViewY = imageViewY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:147/3 currentSupSize:SCREEN_HEIGHT];
            textFieldY = textFieldY + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:147/3 currentSupSize:SCREEN_HEIGHT];
            
            UIView *lineView =    [[UIView alloc]initWithFrame:CGRectMake(textFieldX, imageViewY - 12, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
            lineView.backgroundColor = ColorGrayBk;
            [self addSubview:lineView];
        }
        
        fields = @[(UITextField *)[scroll viewWithTag:10],(UITextField *)[scroll viewWithTag:11],(UITextField *)[scroll viewWithTag:12],(UITextField *)[scroll viewWithTag:13],(UITextField *)[scroll viewWithTag:14],(UITextField *)[scroll viewWithTag:15],(UITextField *)[scroll viewWithTag:16]];

        [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
        [self.keyboardControls setDelegate:self];
        
        //Check Box 1
        checkBox1 = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1709/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:54/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT])];
        checkBox1.image = [UIImage imageNamed:@"Uncheck.png"];
        [scroll addSubview:checkBox1];
        
        ageCheckButton = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1699/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:154/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:154/3 currentSupSize:SCREEN_HEIGHT])];
        [ageCheckButton addTarget:self action:@selector(ageChecked) forControlEvents:UIControlEventTouchUpInside];
        [scroll addSubview:ageCheckButton];
        
        CGFloat labelX =  [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH] + [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:84/3 currentSupSize:SCREEN_WIDTH];
        
        UILabel*patientNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(labelX, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1709/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:SCREEN_WIDTH currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
        patientNameLabel.textAlignment = NSTextAlignmentLeft;
        patientNameLabel.backgroundColor = [UIColor clearColor];
        patientNameLabel.textColor = [UIColor whiteColor];
        patientNameLabel.text = @"I am 12+ and eligible to place an order";
        patientNameLabel.font = [UIFont fontWithName:FontLight size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:40/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:patientNameLabel];
        
        //CheckBox 2nd
        //Check Box 1
        checkBox2 = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1789/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:54/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT])];
        checkBox2.image = [UIImage imageNamed:@"Uncheck.png"];
        [scroll addSubview:checkBox2];
        
        dealAndPromotionButton = [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1770/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:154/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:154/3 currentSupSize:SCREEN_HEIGHT])];
        [dealAndPromotionButton addTarget:self action:@selector(dealAndPromotion) forControlEvents:UIControlEventTouchUpInside];
        [scroll addSubview:dealAndPromotionButton];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(labelX, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1789/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:SCREEN_WIDTH currentSupSize:SCREEN_WIDTH] - labelX,[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
        
        label.textAlignment = NSTextAlignmentLeft;
        label.textColor = [UIColor whiteColor];
        label.text = @"I want to receive deals and promotions.";
        label.font = [UIFont fontWithName:FontLight size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:42/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:label];
        
        UIButton *registerBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:195/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1879/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:840/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:150/3 currentSupSize:SCREEN_HEIGHT])];
        [registerBtn setTitle:@"Register" forState:UIControlStateNormal];
        registerBtn.titleLabel.font  =[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:88/3 currentSupSize:SCREEN_HEIGHT]];
        [registerBtn setBackgroundImage:[[CommonClass getSharedInstance] imageFromColor:ColorGreenButton] forState:UIControlStateNormal];
        registerBtn.exclusiveTouch = YES;
        [registerBtn addTarget:self action:@selector(registerButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        registerBtn.layer.cornerRadius = registerBtn.frame.size.height/2;
        registerBtn.layer.masksToBounds = YES;
        [scroll addSubview:registerBtn];
        
        UILabel *termAndConditionlabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2066/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:TargetSize.width currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
        
        NSString *str1 = [NSString stringWithFormat:@"%@\n",@"By creating Fox Menus Account, you are agree to our,"];

        termAndConditionlabel.backgroundColor = [UIColor clearColor];
        termAndConditionlabel.textAlignment = NSTextAlignmentCenter;
        termAndConditionlabel.textColor = [UIColor whiteColor];
        termAndConditionlabel.text = [NSString stringWithFormat:@"%@",str1];
        termAndConditionlabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:44/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:termAndConditionlabel];

        NSString *str2 = @"TERMS OF SERVICE & PRIVACY POLICY";

        UILabel *privacyLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2125/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:TargetSize.width currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
        privacyLabel.backgroundColor = [UIColor clearColor];
        privacyLabel.textAlignment = NSTextAlignmentCenter;
        privacyLabel.textColor = ColorGreenButton;
        privacyLabel.text = @"TERMS OF SERVICE & PRIVACY POLICY";
        privacyLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:34/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:privacyLabel];

        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:privacyLabel.text];
        // Add attribute NSUnderlineStyleAttributeName
        [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, str2.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:ColorGreenButton range:NSMakeRange(0, str2.length)];//
        [privacyLabel setAttributedText:attributedString];
        
        UIButton *termAndConditionBtn = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2125/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:TargetSize.width currentSupSize:SCREEN_WIDTH] - [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:20/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT])];
        [termAndConditionBtn addTarget:self action:@selector(termAndCondition) forControlEvents:UIControlEventTouchUpInside];
        termAndConditionBtn.backgroundColor = [UIColor clearColor];
        [scroll addSubview:termAndConditionBtn];
        
        scroll.contentSize = CGSizeMake(SCREEN_WIDTH, [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2076/3 currentSupSize:SCREEN_HEIGHT] + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT]);
    }
    return self;
}

#pragma mark - Register button Clicked

- (void)registerButtonClicked {
    
    NSString *name              =  [(UITextField *)[scroll viewWithTag:10] text];
    NSString *patientId         =  [(UITextField *)[scroll viewWithTag:11] text];
    NSString *physicianName     =  [(UITextField *)[scroll viewWithTag:12] text];
    NSString *contact           =  [(UITextField *)[scroll viewWithTag:13] text];
    NSString *emailAddress      =  [(UITextField *)[scroll viewWithTag:14] text];
    NSString *password          =  [(UITextField *)[scroll viewWithTag:15] text];
    NSString *confirmPassword   =  [(UITextField *)[scroll viewWithTag:16] text];
    NSString *trimmedContact;
    
    trimmedContact = [contact stringByReplacingOccurrencesOfString:@"(" withString:@""];
    trimmedContact = [trimmedContact stringByReplacingOccurrencesOfString:@")" withString:@""];
    trimmedContact = [trimmedContact stringByReplacingOccurrencesOfString:@"-" withString:@""];
    trimmedContact  = [trimmedContact stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if(isAgeChecked) {
        
        if(name.length >= 1 || patientId.length >= 1 || physicianName.length >= 1 || contact.length >= 1 || emailAddress.length >= 1 || password.length >= 1 || confirmPassword.length >= 1) {
            
            if (trimmedContact.length!=10) {
                [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter a valid Phone Number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else {
                if(isImageChanged) {
                    NSString *stringDealsChecked = @"0";
                    if(isdealAndPromotionChecked)
                        stringDealsChecked = @"1";
                    
                    if([self validateEmail:emailAddress]) {
                        
                        [_registerDelegate signUpServerCall:name
                                                  patientId:patientId
                                              physicianName:physicianName
                                                    contact:contact
                                               emailAddress:emailAddress
                                                   password:password
                                            confirmPassword:confirmPassword
                                                      image:[[CommonClass getSharedInstance] getImageFRom:@"ProfilePicFolder" imageName:@"profile.jpg"]
                                                      deals:stringDealsChecked];
                    }
                    else {
                        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter a valid email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    }
                }
                else {
                    [[[UIAlertView alloc] initWithTitle:@"" message:@"Please select the image." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
            }
        }
        else {
            [[[UIAlertView alloc] initWithTitle:@"" message:@"Please fill the all fields." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please select the Check box." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

#pragma mark - Back ButtonAction

- (void)backButton {
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:@"ProfilePicChange"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_registerDelegate backToLoginViewAction];
}

#pragma mark - Checked Action

- (void)ageChecked {
    
    if(!isAgeChecked) {
        isAgeChecked = YES;
        checkBox1.image = [UIImage imageNamed:@"check.png"];
    } else {
        isAgeChecked = NO;
        checkBox1.image = [UIImage imageNamed:@"Uncheck.png"];
    }
}

- (void)dealAndPromotion {
    
    if(!isdealAndPromotionChecked) {
        isdealAndPromotionChecked = YES;
        checkBox2.image = [UIImage imageNamed:@"check.png"];
    }
    else {
        isdealAndPromotionChecked = NO;
        checkBox2.image = [UIImage imageNamed:@"Uncheck.png"];
    }
}

#pragma mark - Term And Condition Action

- (void)termAndCondition {
    
}

#pragma mark - AddPhoto ButtonAction

- (void)addPhotoButtonAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Add Image"];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"From Gallery"];
    [alert addButtonWithTitle:@"Take photo"];
    [alert addButtonWithTitle:@"Cancel"];
    alert.tag = 101;
    [alert show];
    alert = nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag == 101) {
        if(buttonIndex == 0)
            [_registerDelegate changePhotoFromgallery:0];
        else if(buttonIndex == 1)
            [_registerDelegate changePhotoFromgallery:1];
    }
}

#pragma mark - Add Image

- (void)addImage :(NSNotification *)string
{
    [[CommonClass getSharedInstance] removeImageFRom:@"ProfilePicFolder"];

    if ([[string name] isEqualToString:@"ProfilePicChange"]) {
        NSDictionary *userInfo = string.userInfo;
        NSObject *myObject   = [userInfo objectForKey:@"someKey"];
        UIImage *chkPixel = (UIImage *)myObject;
        
        if(chkPixel.size.height < 100 || chkPixel.size.width < 100) {
            [[CommonClass getSharedInstance] removeImageFRom:@"ProfilePicFolder"];
            [[CommonClass getSharedInstance] alertMsg:@"Try uploading a different photo. Photos must be at least 100 x 100 pixels." title:@"Error"];
        }
        else {
            isImageChanged = YES;
            addImageView.image = chkPixel;
            addImageView.layer.borderWidth = 3.0;
            addImageView.layer.borderColor = [UIColor whiteColor].CGColor;
            [scroll addSubview:select];

            [[CommonClass getSharedInstance] saveImage:[[CommonClass getSharedInstance] imagerotate:(UIImage *)myObject] ImageName:@"profile.jpg" FolderName:@"ProfilePicFolder"];
        }
    }
}

#pragma mark - Check Email And Phone Number


- (BOOL)validateEmail:(NSString *)emailStr {
    
    NSArray *objEmailArray =  [[emailStr stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@","];
    NSPredicate *emailTest;
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSLog(@"%d",[emailTest evaluateWithObject:[[objEmailArray objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""]]);
    
    if([emailTest evaluateWithObject:[objEmailArray objectAtIndex:0]] == 0)
        return [emailTest evaluateWithObject:[objEmailArray objectAtIndex:0]];

    return 1;
}

#pragma mark- Check Phone Number

- (NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar
{
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    
    /*if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
     */
    return simpleNumber;
}

#pragma mark - TextField

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
     if (textField.tag == 13) {
        
        NSString* phoneString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        // if it's the phone number textfield format it.
        UITextField *phoneTextfield = (UITextField *)[self viewWithTag:13];
        
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            phoneTextfield.text = [self formatPhoneNumber:phoneString deleteLastChar:YES];
        } else {
            phoneTextfield.text = [self formatPhoneNumber:phoneString deleteLastChar:NO];
        }
         return NO;
    }
    else {
//        if(textField.tag!=11)
//        {
            NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
            NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
            if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound){
                whiteSpaceCount = 0;
                return YES;
            }
            else {
                if ([string isEqualToString:@" "]) {
                    whiteSpaceCount = whiteSpaceCount + 1;
                    if (whiteSpaceCount >=2 || (string.length==1 && textField.text.length == 0))
                        return NO;
                    else
                        return YES;
                }
                whiteSpaceCount = 0;
                return YES;
            }
  //      }
    }
     return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == (UITextField *)[textField viewWithTag:10]) {
        [(UITextField *)[self viewWithTag:11] becomeFirstResponder];
        return NO;
    }
    else if (textField == (UITextField *)[self viewWithTag:11]) {
        [(UITextField *)[self viewWithTag:12] becomeFirstResponder];
        return YES;
    }
    else if (textField == (UITextField *)[self viewWithTag:12]) {
        [(UITextField *)[self viewWithTag:13] becomeFirstResponder];
        return YES;
    }
    else if (textField == (UITextField *)[self viewWithTag:13]) {
        [(UITextField *)[self viewWithTag:14] becomeFirstResponder];
        return YES;
    }
    else if (textField == (UITextField *)[self viewWithTag:14]) {
        [(UITextField *)[self viewWithTag:15] becomeFirstResponder];
        return YES;
    }
    else if (textField == (UITextField *)[self viewWithTag:15]) {
        [(UITextField *)[self viewWithTag:16] becomeFirstResponder];
        return YES;
    }
    else if (textField == (UITextField *)[self viewWithTag:16]) {
        [textField resignFirstResponder];
        return YES;
    }
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    if (self.frame.origin.y >= 0)
        [self setViewMovedUp:YES];
    else if (self.frame.origin.y < 0)
    {
        
    }
    //[self setViewMovedUp:NO];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    if (self.frame.origin.y >= 0)
        [self setViewMovedUp:YES];
    else if (self.frame.origin.y < 0)
        [self setViewMovedUp:NO];
}

-(void)setViewMovedUp:(BOOL)movedUp {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.frame;
    if (movedUp)
        rect.origin.y -= 120;
    else
        rect.origin.y += 120;
    self.frame = rect;
    [UIView commitAnimations];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls {
    [self endEditing:YES];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

#pragma mark- slide Keyboard up

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.keyboardControls setActiveField:textField];
}

-(void)resignKeyboard {
    [self endEditing:YES];
}

@end
