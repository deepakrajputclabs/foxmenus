//
//  ForgotPasswordViewController.h
// Storx
//
//  Created by clicklabs on 11/12/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ForgotView.h"

@interface ForgotPasswordViewController : UIViewController<forgotViewDelegate>

@end
