//
//  ForgotPasswordViewController.m
// Storx
//
//  Created by clicklabs on 11/12/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "ForgotView.h"

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    //self.view.backgroundColor=[UIColor redColor];
    
    ForgotView *forgotView=[[ForgotView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:forgotView];
    forgotView.forgotScreenDelegate=self;

}

- (void)backButtonAction {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
