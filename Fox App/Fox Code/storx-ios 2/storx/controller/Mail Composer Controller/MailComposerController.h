//
//  MailComposerController.h
// Storx
//
//  Created by clicklabs on 11/27/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import <MessageUI/MessageUI.h>

@interface MailComposerController : UIViewController<MFMailComposeViewControllerDelegate>

@end
