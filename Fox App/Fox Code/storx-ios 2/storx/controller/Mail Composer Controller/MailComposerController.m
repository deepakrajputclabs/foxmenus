//
//  MailComposerController.m
// Storx
//
//  Created by clicklabs on 11/27/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "MailComposerController.h"
#import "HomeViewController.h"

@interface MailComposerController ()

@end

@implementation MailComposerController
{
    MFMailComposeViewController *mailer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(mailer==nil)
    {
        self.view.backgroundColor=[UIColor whiteColor];
        // Do any additional setup after loading the view.
        mailer = [[MFMailComposeViewController alloc] init];
        if ([MFMailComposeViewController canSendMail])
        {
            mailer.mailComposeDelegate = self;
            NSString *emailBody = @"";
            //[mailer setMessageBody:emailBody isHTML:NO];
            [mailer setSubject:@"Fox Menus Support"];
            [mailer setToRecipients:[NSArray arrayWithObjects:emailBody, nil]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"message:@"Your device doesn't support the composer sheet." delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
            [alert show];
        }
        //NSData *pngData1 = [NSData dataWithContentsOfFile:[self dataFilePath]];
        //[mailer addAttachmentData:pngData1 mimeType:@"application/pdf" fileName:@"Business Budget Cash Flow.csv"];
        [self presentViewController:mailer animated:NO completion:nil];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlert:(NSString *)text {
    
    [[[UIAlertView alloc]initWithTitle:@"" message:text
                              delegate:self
                     cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    [self menuButton];
}

#pragma mark - Mail Composer Delegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"mail cancelled");
            [self showAlert:@"Email cancelled."];
            //[self performSelector:@selector(menuButton) withObject:nil afterDelay:0.1];
            break;
            
        case MFMailComposeResultSent:
            NSLog(@"mail sent");
            [self showAlert:@"Email sent successfully."];
            break;
            
        case MFMailComposeResultSaved:
            [self showAlert:@"Email saved successfully."];

            break;
        case MFMailComposeResultFailed:
            NSLog(@"error,mail not sent");
            break;
            
        default:NSLog(@"mail not sent");
            break;
    }
   // [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)backToMenuController
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)backFromMenu {
        self.view.backgroundColor=[UIColor whiteColor];
        // Do any additional setup after loading the view.
        mailer = [[MFMailComposeViewController alloc] init];
        if ([MFMailComposeViewController canSendMail])
        {
            mailer.mailComposeDelegate = self;
            NSString *emailBody = @"support@smokeio.com";
            //[mailer setMessageBody:emailBody isHTML:NO];
            [mailer setSubject:@"Smokeio Support"];
            [mailer setToRecipients:[NSArray arrayWithObjects:emailBody, nil]];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"message:@"Your device doesn't support the composer sheet." delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
            [alert show];
        }
        //NSData *pngData1 = [NSData dataWithContentsOfFile:[self dataFilePath]];
        //[mailer addAttachmentData:pngData1 mimeType:@"application/pdf" fileName:@"Business Budget Cash Flow.csv"];
        [self presentViewController:mailer animated:YES completion:nil];
}

- (void)menuButton {
    [self.sideMenuViewController presentLeftMenuViewController];
    HomeViewController *sideMenu = [[HomeViewController alloc] init];
    [self.navigationController pushViewController:sideMenu animated:YES];

}

- (void)backButtonAction {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
