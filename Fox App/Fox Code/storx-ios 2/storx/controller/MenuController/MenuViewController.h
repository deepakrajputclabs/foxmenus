//
//  MenuViewController.h
// Storx
//
//  Created by clicklabs on 11/5/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import <MessageUI/MessageUI.h>
#import "ScheduledLocationManager.h"
#import "RESideMenu.h"


@interface MenuViewController : UIViewController <MFMailComposeViewControllerDelegate, RESideMenuDelegate>
{
    NSArray *objArray;
    UITableView *menuTableView;
    
    UIButton *menuBtn;
}
@end
