//
//  MenuViewController.m
// Storx
//
//  Created by clicklabs on 11/5/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "MenuViewController.h"
#import "OrdersViewController.h"
#import "ProfileViewController.h"
#import "HomeViewController.h"
#import "MailComposerController.h"
#import "SplashViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface MenuViewController ()

@end

@implementation MenuViewController
{
    MFMailComposeViewController *mailer;
    BOOL isSupport;
   
}

#pragma mark - ViewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addObserverToController];

    isSupport=false;
    
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"sidemenu_bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self.view addSubview:bkImageView];
    
    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:(1242/3 - 400/3)/2 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:400/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:388/3 currentSupSize:SCREEN_HEIGHT])];
    //storxImageView.contentMode = UIViewContentModeScaleToFill;
    //storxImageView.layer.borderWidth = 1.0;
    //storxImageView.layer.borderColor = ColorGreenButton.CGColor;
    //storxImageView.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:364/6 currentSupSize:SCREEN_HEIGHT];
    //storxImageView.layer.masksToBounds = YES;
    //[storxImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:UserData] objectForKey:@"user_image"]]] placeholderImage:[UIImage imageNamed:@"StorX1.png"]];
    storxImageView.image = [UIImage imageNamed:@"storxMenuLogo.png"];
    storxImageView.userInteractionEnabled = YES;
    [bkImageView addSubview:storxImageView];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) {
        objArray = [NSArray arrayWithObjects:@"Check Orders",@"Account Information",@"Support",@"Logout", nil];
    }
    else {
        objArray = [NSArray arrayWithObjects:@"HOME",@"MY BOOKINGS",@"PROFILE",@"SUPPORT",@"LOGOUT", nil];
    }
    
    int y = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:480/3 currentSupSize:SCREEN_HEIGHT];
    
    for (int i = 0; i < [objArray count]; i++) {
        
        UIImageView *baseCell = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],y, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:970/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:250/3 currentSupSize:SCREEN_HEIGHT])];
        baseCell.userInteractionEnabled = YES;
        baseCell.tag = 10200;
        baseCell.backgroundColor = [UIColor clearColor];
        [bkImageView addSubview:baseCell];
        
        UILabel *menuLabel      = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:200/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:970/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:250/3 currentSupSize:SCREEN_HEIGHT])];
        menuLabel.textColor     = [UIColor whiteColor];
        menuLabel.backgroundColor = [UIColor clearColor];
        menuLabel.numberOfLines = 0;
        menuLabel.textAlignment = NSTextAlignmentLeft;
        [menuLabel setFont:[UIFont fontWithName:FontMedium size: [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:60/3 currentSupSize:SCREEN_HEIGHT]]];
        menuLabel.text          = [NSString stringWithFormat:@"%@",[objArray objectAtIndex:i]];
        [baseCell addSubview:menuLabel];
      
        UIButton *menuButton     =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:970/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:250/3 currentSupSize:SCREEN_HEIGHT])];
        menuButton.tag = i + 10;
        [menuButton addTarget:self action:@selector(menuClicked:) forControlEvents:UIControlEventTouchUpInside];
        [baseCell addSubview:menuButton];
        y = y + 70;
    }
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    if(isSupport==false) {
        [super viewWillAppear:animated];
        [self.navigationController setNavigationBarHidden:YES animated:animated];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self removeObserver];
}

#pragma mark - MenuClicked

- (void)menuClicked :(UIButton *)sender {
    int tag = sender.tag;
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:isCurrentLocationNow];

    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) { //Driver
        
        if(tag == 10) {
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[OrdersViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
        else if(tag == 11){

            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[ProfileViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
        else if (tag==12)
        {
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[MailComposerController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
        else if(tag == 13){
            isSupport=false;

            
            [[[UIAlertView alloc]initWithTitle:@"Logout" message:@"Do you want to logout?"
                                      delegate:self
                             cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil] show];
        }
    }
    else {
       // [self removeObserver];

        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"ControllerType"];
        if(tag == 10) {
            isSupport=false;
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[HomeViewController alloc] init]]animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
        else if(tag == 11) {
            //[self addObserverToController];
            isSupport=false;
            [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"ControllerType"];

            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[ReminderHistoryViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
        else if (tag==12) {
            isSupport=false;
            //[self addObserverToController];

            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[ProfileViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
        else if (tag==13) {
            
            isSupport=false;

            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[MailComposerController alloc] init]]animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            
        }
        else if (tag==14) {
            isSupport=false;
            
            [[[UIAlertView alloc]initWithTitle:@"Logout" message:@"Do you want to logout?"
                                      delegate:self
                             cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil] show];
            
        }
    }
}

- (void)showAlert:(NSString *)text {
    
    [[[UIAlertView alloc]initWithTitle:@"" message:text
                              delegate:nil
                     cancelButtonTitle:@"OK" otherButtonTitles:nil] show];

}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"mail cancelled");
            //[self performSelector:@selector(menuButton) withObject:nil afterDelay:0.1];
            break;
            
        case MFMailComposeResultSent:
            NSLog(@"mail sent");
            break;
            
        case MFMailComposeResultSaved:
            NSLog(@"mail saved");
            break;
            
        case MFMailComposeResultFailed:
            NSLog(@"error,mail not sent");
            break;
            
        default:NSLog(@"mail not sent");
            break;
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Alert Button Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
       if (buttonIndex == 0)
        {
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
            NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:[NSString stringWithFormat:@"logout"]
                                                                               data:[[NSString stringWithFormat:@"access_token=%@",[[NSUserDefaults standardUserDefaults]valueForKey:AccessToken]] dataUsingEncoding:NSUTF8StringEncoding]
                                                                               Type:@"POST"
                                                                   loadingindicator:YES];
            NSLog(@"login details: %@",json);
            
            if([[json objectForKey:@"status"]integerValue]==0) {
                
                [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
                [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
                [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserData];

                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
                [self.sideMenuViewController hideMenuViewController];
                
                [[[UIAlertView alloc]initWithTitle:@"Logged Out Succesfully" message:@""
                                          delegate:nil
                                 cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else {
                [[[UIAlertView alloc]initWithTitle:@"Error!" message:[json objectForKey:@"error"]
                                          delegate:nil
                                 cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
        else{
            [alertView dismissWithClickedButtonIndex:1 animated:YES];
        }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Push Controller Funciton

- (void)customerOrderTrack {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"] intValue] == 40) { // Rate order
        
        MedicineDeliveryViewController *objViewOrder = [[MedicineDeliveryViewController alloc]init];
        objViewOrder.orderId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OrderId"];
        objViewOrder.flagString = [[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"];
        objViewOrder.flagType = [[NSUserDefaults standardUserDefaults] valueForKey:@"Type"];
        objViewOrder.isFromPushNotificationMenu = YES;
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:objViewOrder] animated:YES];
    }
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"] intValue] == 41 || [[[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"] intValue] == 19) { // Start order or pickup
        
        MapViewController *mapController  = [[MapViewController alloc]init];
        mapController.orderId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OrderId"];
        mapController.isStartOrder = YES;
        mapController.flagCheckString = [[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:mapController] animated:YES];
    }
    [self.sideMenuViewController hideMenuViewController];
}

- (void)dealloc {
    [self removeObserver];
}

-(void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)addObserverToController {
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(customerOrderTrack)
                                                 name:@"OrderTrackNotification"
                                               object:nil];

}
@end
