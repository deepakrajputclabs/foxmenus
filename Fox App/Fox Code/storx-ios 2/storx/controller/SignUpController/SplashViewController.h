//
//  SplashViewController.h
//  Storx
//
//  Created by clicklabs on 12/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "SplashView.h"
#import "ScheduledLocationManager.h"

@interface SplashViewController : UIViewController <RESideMenuDelegate, splashProtocol, ScheduledLocationManagerDelegate>
{
    UIImageView *bkImageView;
}
@end
