//
//  TutorialViewController.h
//  Storx
//
//  Created by clicklabs on 12/16/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashView.h"
#import "LoginViewController.h"
#import "RegisterController.h"

@interface TutorialViewController : UIViewController <splashProtocol>

@end
