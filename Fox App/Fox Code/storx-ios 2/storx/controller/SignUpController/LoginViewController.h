//
//  ViewController.h
// Storx
//
//  Created by clicklabs on 11/3/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReminderHistoryViewController.h"
#import "Header.h"
#import "MenuViewController.h"
#import "OrdersViewController.h"
#import "LoginView.h"
#import "RegisterController.h"
#import "SplashViewController.h"
#import "ForgotPasswordViewController.h"

@interface LoginViewController : UIViewController<loginViewButtonDelegate, RESideMenuDelegate>

@end

