//
//  SignupController.m
// Storx
//
//  Created by clicklabs on 11/7/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "RegisterController.h"
#import "HomeViewController.h"

@implementation RegisterController

-(void)viewDidLoad{
    
    self.view.backgroundColor=SMBgColor;
    
    RegisterView *signView=[[RegisterView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    signView.registerDelegate = self;
    [self.view addSubview:signView];
}

#pragma mark - viewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

#pragma mark - Sign Succesfully Method

- (void)signUpServerCall :(NSString *)name
                patientId:(NSString *)patientId
            physicianName:(NSString *)physicianName
                  contact:(NSString *)contact
             emailAddress:(NSString *)emailAddress
                 password:(NSString *)password
          confirmPassword:(NSString *)confirmPassword
                   image :(NSString *)image
                    deals:(NSString *)deals {
    if ([self validateName:name]&&[self validateName:physicianName]) {
        [self showActivityIndicatorWithTitle];
        
        [ServerCallModel getSharedInstance].serverReturnDelegate = self;
        [[ServerCallModel getSharedInstance] signUpCall:@"customer_signup"
                                                   name:name
                                              patientId:patientId
                                          physicianName:physicianName
                                                contact:contact
                                           emailAddress:emailAddress
                                               password:password
                                        confirmPassword:confirmPassword
                                                  image:image
                                                  deals:deals];
    }
    
    else {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid Physician name/Patient name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

#pragma mark - Validate Patient Name and Physician Name

-(BOOL)validateName:(NSString *)nameString {
    NSString *myRegex = @"[A-Za-z ]*";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex];
    NSString *string = nameString;
    BOOL valid = [myTest evaluateWithObject:string];
    return valid;
}

#pragma mark - Done Method

- (void)doneButtonDelegate {
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[HomeViewController alloc] init]];
    
    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                    leftMenuViewController:[[MenuViewController alloc] init]
                                                                   rightMenuViewController:nil];
    sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
    sideMenuViewController.delegate = self;
    sideMenuViewController.contentViewShadowEnabled = YES;
    [self.navigationController pushViewController:sideMenuViewController animated:YES];
}

#pragma mark - Back Button Action

- (void)backToLoginViewAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Change Image Action

- (void)changePhotoFromgallery:(int)buttonIndex {
    
    PickerController *obj = [[PickerController alloc]init];
    
    if(buttonIndex == 0) {
        obj.pickerType = @"PhotoLibray";
    }
    else if(buttonIndex == 1) {
        obj.pickerType = @"Camera";
    }
    [self.navigationController pushViewController:obj animated:NO];
}

#pragma mark- loading indicator bar
#pragma mark-

- (void)showActivityIndicatorWithTitle
{
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.001]];
}

@end
