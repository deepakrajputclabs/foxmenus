//
//  ViewController.m
// Storx
//
//  Created by clicklabs on 11/3/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "ForgotPasswordViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface LoginViewController ()

@end

@implementation LoginViewController
{
    LoginView *loginView;
}

#pragma mark - viewDidLoad

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [loginView removeFromSuperview];
    loginView=nil;
    loginView=[[LoginView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    loginView.loginViewButtonActionDelegate=self;
    [self.view addSubview:loginView];
}

#pragma mark - viewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

#pragma mark - Back Method

- (void)backMethod {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - homeController Method

- (void)homeControllerMethod {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) //Driver
    {
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[OrdersViewController alloc] init]];
        
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                        leftMenuViewController:[[MenuViewController alloc] init]
                                                                       rightMenuViewController:nil];
        sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
        sideMenuViewController.delegate = self;
        sideMenuViewController.contentViewShadowEnabled = YES;
        [self.navigationController pushViewController:sideMenuViewController animated:YES];
    }
    else
    {
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[HomeViewController alloc] init]];
        
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                        leftMenuViewController:[[MenuViewController alloc] init]
                                                                       rightMenuViewController:nil];
        sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
        sideMenuViewController.delegate = self;
        sideMenuViewController.contentViewShadowEnabled = YES;
        [self.navigationController pushViewController:sideMenuViewController animated:YES];
    }
}

- (void)invalidAccessToken {
    
}

-(void)forgotPasswordMethod {
    ForgotPasswordViewController *forgotMethod = [[ForgotPasswordViewController alloc]init];
    [self.navigationController pushViewController:forgotMethod animated:YES];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
