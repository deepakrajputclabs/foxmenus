//
//  SignupController.h
// Storx
//
//  Created by clicklabs on 11/7/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterView.h"

@interface RegisterController : UIViewController <registerProtocol,ServerReturnProtocol, RESideMenuDelegate>

@end
