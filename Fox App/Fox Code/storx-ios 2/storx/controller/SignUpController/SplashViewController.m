//
//  SplashViewController.m
//  Storx
//
//  Created by clicklabs on 12/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//


#import "SplashViewController.h"
#import "RegisterController.h"
#import "HomeViewController.h"
#import "TutorialViewController.h"

@interface SplashViewController ()
{
    SplashView *objView;
    BOOL isFirstStart;
}
@end

@implementation SplashViewController

#pragma mark - viewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setFloat:0.0 forKey:@"updateLat"];
    [[NSUserDefaults standardUserDefaults] setFloat:0.0 forKey:@"updateLong"];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
        [[ScheduledLocationManager getSharedInstance] setDelegate:self];
        [[ScheduledLocationManager getSharedInstance] getUserLocationWithInterval:0];
    }
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isdoMedicineCall"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isdoRatingCall"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"TutorialScreen"];
    //edited below line
    [[NSUserDefaults standardUserDefaults] setValue:@"sdfsdsdfs2342344546dddd4" forKey:DeviceToken];
    isFirstStart = NO;
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:AccessToken]) {
        
        bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        bkImageView.image = [UIImage imageNamed:@"bg_for_splash.png"];
        bkImageView.userInteractionEnabled = YES;
        [self.view addSubview:bkImageView];
        
        [self accessTokenCall];
    }
    else {
        [self splashView];
    }
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"ControllerType"];

    if (isFirstStart) {
        if([[NSUserDefaults standardUserDefaults] valueForKey:AccessToken]) {
            
            bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
            bkImageView.image = [UIImage imageNamed:@"bg_for_splash.png"];
            bkImageView.userInteractionEnabled = YES;
            [self.view addSubview:bkImageView];
            
            if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1)
                [self orderViewController];
            else
                [self homeController];
        }
        else {
            [self splashView];
        }
  }
    isFirstStart = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
}

#pragma mark : Tutorial Method

- (void)tutorialMethod {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TutorialScreen"];

    TutorialViewController *objSignUp = [[TutorialViewController alloc] init];
    [self.navigationController pushViewController:objSignUp animated:YES];
}

-(void)dispensaryLogin {
    LoginViewController *objSignIn = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:objSignIn animated:YES];
}

#pragma mark : SignIn Method

- (void)signInMethod {
    LoginViewController *objSignIn = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:objSignIn animated:YES];
}

#pragma mark : Register Method

- (void)registerMethod {
    RegisterController *objSignUp = [[RegisterController alloc] init];
    [self.navigationController pushViewController:objSignUp animated:YES];
}

- (void)splashView {
    [objView removeFromSuperview];
    objView = nil;
    
    objView = [[SplashView alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH, SCREEN_HEIGHT)];
    objView.splashDelegate = self;
    [self.view addSubview:objView];
    [objView splashView];
}

#pragma mark : homeController Method

- (void)homeController {

    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[HomeViewController alloc] init]];

    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                    leftMenuViewController:[[MenuViewController alloc] init]
                                                                   rightMenuViewController:nil];
    //sideMenuViewController.backgroundImage = [UIImage imageNamed:@"Stars"];
    sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
    sideMenuViewController.delegate = self;
    sideMenuViewController.contentViewShadowEnabled = YES;
    [self.navigationController pushViewController:sideMenuViewController animated:YES];
}

-(void)orderViewController
{
    OrdersViewController *obj = [[OrdersViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
    
    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                    leftMenuViewController:[[MenuViewController alloc] init]
                                                                   rightMenuViewController:nil];
    sideMenuViewController.menuPreferredStatusBarStyle = 1;
    sideMenuViewController.delegate = self;
    sideMenuViewController.contentViewShadowEnabled = YES;
    [self.navigationController pushViewController:sideMenuViewController animated:YES];
}

-(void)mapViewController {
    MapViewController *obj = [[MapViewController alloc] init];
    obj.orderId = [[NSUserDefaults standardUserDefaults]valueForKey:@"resumeOrder"];
    obj.isStartOrder = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
    
    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                    leftMenuViewController:[[MenuViewController alloc] init]
                                                                   rightMenuViewController:nil];
    sideMenuViewController.menuPreferredStatusBarStyle = 1;
    sideMenuViewController.delegate = self;
    sideMenuViewController.contentViewShadowEnabled = YES;
    [self.navigationController pushViewController:sideMenuViewController animated:YES];
}

- (void)medicineViewController {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isdoMedicineCall"];
    MedicineDeliveryViewController *medicineDelivery = [[MedicineDeliveryViewController alloc]init];
    medicineDelivery.orderDetail = nil;
    medicineDelivery.customerArray = nil;
    medicineDelivery.orderId = [[NSUserDefaults standardUserDefaults] valueForKey:@"resumeOrder"];
    medicineDelivery.flagString = nil;
    medicineDelivery.isFromPushNotificationMenu = NO;
    [self.navigationController pushViewController:medicineDelivery animated:YES];
}

- (void)ratingController {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isdoRatingCall"];

    RatingViewController *rating = [[RatingViewController alloc]init];
    rating.orderIdConfirm = [[NSUserDefaults standardUserDefaults] valueForKey:@"resumeOrder"];;
    rating.userName = nil;
    rating.userImage = nil;
    rating.userAddress = nil;
    [self.navigationController pushViewController:rating animated:YES];
}

#pragma mark - Access Token API Call

- (void)accessTokenCall {
    //CLLocationCoordinate2D currentCoordinate = [[CommonClass getSharedInstance] currentDealLocation]; // Current Loaction
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"access_token_login"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&device_type=%@&device_token=%@&app_version=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                              @"1",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:DeviceToken],
                                                                              AppVersion] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:NO];

    NSLog(@"AccessTokenCall json: %@",json);
    if(json) {
        
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            NSData *newdata=[[[json objectForKey:UserData] valueForKey:UserEmail] dataUsingEncoding:NSUTF8StringEncoding
                                                                                  allowLossyConversion:YES];
            
            NSString *mystring=[[NSString alloc] initWithData:newdata encoding:NSNonLossyASCIIStringEncoding];
            if([mystring isEqualToString:@""]  || mystring== (id)[NSNull null] || mystring.length==0)
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:UserEmail] forKey:UserEmail];
            else
                [[NSUserDefaults standardUserDefaults] setValue:mystring forKey:UserEmail];
            
            [[NSUserDefaults standardUserDefaults] setObject:[json objectForKey:UserData] forKey:UserData];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:AccessToken] forKey:AccessToken];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:DriverMode] forKey:DriverMode];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:UserPhone] forKey:UserPhone];
            [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:UserImage] forKey:UserImage];
            
            if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) { //Driver
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:@"dispensery_name"] forKey:UserPhysicianName];
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:UserName] forKey:UserName];

                if ([[[NSUserDefaults standardUserDefaults] valueForKey:StateFlagDriverAfterStartOrder] integerValue] == 1)
                    [self mapViewController];
                else if ([[[NSUserDefaults standardUserDefaults] valueForKey:StateFlagDriverAfterStartOrder] integerValue] == 2)
                    [self medicineViewController];
                else if ([[[NSUserDefaults standardUserDefaults] valueForKey:StateFlagDriverAfterStartOrder] integerValue] == 3)
                    [self ratingController];
                else
                    [self orderViewController];
            }
            else {
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:UserPatientId] forKey:UserPatientId];
                [[NSUserDefaults standardUserDefaults] setValue:[[json objectForKey:UserData] valueForKey:UserPhysicianName] forKey:UserPhysicianName];

                double delayInSeconds = 0.5;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self homeController];
                });
            }
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if([[json objectForKey:@"status"] intValue] == 1) {
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2) {
            [self setToNil];
            [self splashView];
        }
    }
    else {
        [self setToNil];
        [self splashView];
    }
}

- (void)setToNil {
    [[NSUserDefaults standardUserDefaults] setObject:Nil forKey:UserData];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserEmail];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserPatientId];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserPhone];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserPhysicianName];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserImage];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - Location Delegate

-(void)scheduledLocationManageDidFailWithError:(NSError *)error{
    NSLog(@"map error:%@",error.description);
}

- (void)scheduledLocationManageDidUpdateLocations:(CLLocation *)currentLocations{
    NSLog(@"map:%f, %f",currentLocations.coordinate.latitude, currentLocations.coordinate.longitude);
    [[NSUserDefaults standardUserDefaults] setFloat:currentLocations.coordinate.latitude forKey:@"updateLat"];
    [[NSUserDefaults standardUserDefaults] setFloat: currentLocations.coordinate.longitude forKey:@"updateLong"];
    [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
}

@end
