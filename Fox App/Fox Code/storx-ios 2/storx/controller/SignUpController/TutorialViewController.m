//
//  TutorialViewController.m
//  Storx
//
//  Created by clicklabs on 12/16/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SplashView *objView = [[SplashView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    objView.splashDelegate = self;
    [self.view addSubview:objView];
    [objView splashView];
}

#pragma mark : SignIn Method

- (void)signInMethod {
    LoginViewController *objSignIn = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:objSignIn animated:YES];
}

#pragma mark : Register Method

- (void)registerMethod {
    RegisterController *objSignUp = [[RegisterController alloc] init];
    [self.navigationController pushViewController:objSignUp animated:YES];
}

#pragma mark : Tutorial Method

- (void)backMethod {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
