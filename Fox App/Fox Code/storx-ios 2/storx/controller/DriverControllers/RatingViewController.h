//
//  RatingViewController.h
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "RatingView.h"
#import "OrdersViewController.h"
#import "HomeViewController.h"
@interface RatingViewController : UIViewController <RatingViewProtocol,RESideMenuDelegate>
{
    NSMutableArray *infoArray;
}
@property (strong,nonatomic) NSString *orderIdConfirm;
@property (strong,nonatomic) NSString *userName;
@property (strong,nonatomic) NSString *userImage;
@property (strong,nonatomic) NSString *userAddress;
@property (strong,nonatomic) NSMutableArray  *customerInfoMutArray;
@property (strong,nonatomic) NSString *flagCheck;
@property (strong,nonatomic) NSString *flagType;

@end
