//
//  MedicineDeliveryViewController.h
//  Storx
//
//  Created by click on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedicineDeliveryView.h"
#import "Header.h"
#import "RatingViewController.h"

@interface MedicineDeliveryViewController : UIViewController<medicineDeliveryProtocol>
{
    NSMutableArray *customerArray;
}
@property(strong,nonatomic) NSArray *orderDetail;
@property(strong,nonatomic) NSString *orderId;
@property(strong,nonatomic) NSMutableArray *customerArray;
@property(strong,nonatomic) NSString *flagString;
@property(strong,nonatomic) NSString *flagType;
@property(strong,nonatomic) NSDictionary *jsonDictionary;
@property(nonatomic) BOOL isFromPushNotificationMenu;

@end
