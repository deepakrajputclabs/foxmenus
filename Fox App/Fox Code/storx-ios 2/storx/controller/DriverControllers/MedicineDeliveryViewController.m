//
//  MedicineDeliveryViewController.m
//  Storx
//
//  Created by click on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "MedicineDeliveryViewController.h"

@interface MedicineDeliveryViewController ()

@end

@implementation MedicineDeliveryViewController
@synthesize orderDetail;
@synthesize customerArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor blackColor];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    MedicineDeliveryView *delivery = [[MedicineDeliveryView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    delivery.medicineDeliveryDelegate = self;
    [self.view addSubview:delivery];

    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) { //Driver
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:StateFlagDriverAfterStartOrder] integerValue] == 2 && [[NSUserDefaults standardUserDefaults] boolForKey:@"isdoMedicineCall"])
            [self getMedicineDetail:self.orderId];
        
    } else {
        
        if (_flagString.integerValue == 40 && _flagType.integerValue==0)
            [self orderDetailCall];
        else if ((_flagString.integerValue == 40 || _flagString.integerValue == 10) && _flagType.integerValue == 1)
            [self orderDetailCall];
    }
    [delivery deliveryView:customerArray orderId:self.orderId detail:orderDetail pushFlag:_flagString pushButtonCheck:self.isFromPushNotificationMenu];

    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

#pragma mark - GetMedicine Detail

- (void)getMedicineDetail:(NSString *)orderId {
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"particular_order_details"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                              orderId] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"order_details: %@",json);
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            
            [[NSUserDefaults standardUserDefaults] setValue:self.orderId forKey:@"OrderId"];
            customerArray = [[NSMutableArray alloc]init];
            [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_name"]];
            [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"]];
            [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_email"]];
            [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_location_address"]];
            [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_image"]];
            [customerArray addObject:self.orderId];
            [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"] ];
            [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"total_medicines"]];
            self.orderDetail =       [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"details"];

        }
        else if([[json objectForKey:@"status"] intValue] == 1) {
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2) {
             [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
}

#pragma mark - Back Button

- (void)backMethod {
    if(!self.isFromPushNotificationMenu)
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
        [self menuScreen];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) removeAllViews {
    for ( UIView *removeSubviews in self.view.subviews) {
        [removeSubviews removeFromSuperview];
    }
}

- (void)menuScreen {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [self.sideMenuViewController presentLeftMenuViewController];
}

#pragma mark - Rating Done

- (void)feedbackAfterDone:(NSString *)idOrder {
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:DriverMode]integerValue]==2) {
        
        if (_flagString.integerValue==40 && _flagType.integerValue == 0) { //Rate Driver
            RatingViewController *rating = [[RatingViewController alloc]init];
            rating.orderIdConfirm = idOrder;
            rating.userName = [customerArray objectAtIndex:9];
            rating.userImage = [customerArray objectAtIndex:10];
            rating.userAddress = [customerArray objectAtIndex:3];
            rating.flagCheck = _flagString;
            [self.navigationController pushViewController:rating animated:YES];
        }
        else if (_flagString.integerValue==40 && _flagType.integerValue == 1) {//Rate Pickup Order
            RatingViewController *rating = [[RatingViewController alloc]init];
            rating.orderIdConfirm = idOrder;
            rating.userName = [customerArray objectAtIndex:0];
            rating.userImage = [customerArray objectAtIndex:4];
            rating.userAddress = [customerArray objectAtIndex:3];
            rating.flagCheck = _flagString;
            [self.navigationController pushViewController:rating animated:YES];
        }
        else {
            if (_flagString.integerValue == 10 || _flagString.integerValue == 19) {
                [[[UIAlertView alloc] initWithTitle:@"" message:@"Please wait for your order to confirm." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else {
                RatingViewController *rating = [[RatingViewController alloc]init];
                rating.orderIdConfirm = idOrder;
                rating.userName = [customerArray objectAtIndex:0];
                rating.userImage = [customerArray objectAtIndex:4];
                rating.userAddress = [customerArray objectAtIndex:3];
                rating.flagCheck = _flagString;
                [self.navigationController pushViewController:rating animated:YES];
            }
        }
    }
    else {
        NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"order_delivered"
                                                                           data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                                  [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken] ,idOrder]dataUsingEncoding:NSUTF8StringEncoding]
                                                                           Type:@"POST"
                                                               loadingindicator:YES];
        NSLog(@"json: %@",json);
        if(json) {
            
            if([[json objectForKey:@"status"] intValue] == 0) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isdoMedicineCall"];
                [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:StateFlagDriverAfterStartOrder];
                RatingViewController *rating = [[RatingViewController alloc]init];
                rating.orderIdConfirm = idOrder;
                rating.userName = [customerArray objectAtIndex:0];
                rating.userImage = [customerArray objectAtIndex:4];
                rating.userAddress = [customerArray objectAtIndex:3];
                [self.navigationController pushViewController:rating animated:YES];
            }
            else if([[json objectForKey:@"status"] intValue] == 1){
                [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
            else if([[json objectForKey:@"status"] intValue] == 2){
                [self invalidAccessToken];
            }
        }
    }
}

#pragma mark - Order Detail

-(void)orderDetailCall {
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"order_details_to_customer"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken] ,_orderId]dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"json: %@",json);
    if(json) {
        if([[json objectForKey:@"status"] intValue] == 0) {

            self.jsonDictionary = json;
            customerArray = [[NSMutableArray alloc]init];
            
            if (_flagType.integerValue == 1) { // Driver
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_name"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"]];
                [customerArray addObject:@""];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_address"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_image"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"order_id"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"total_medicines"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"rating"]];
            }
            else {
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_name"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"driver_email"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"drop_location_address"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_image"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"order_id"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"total_medicines"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"rating"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"driver_name"]];
                [customerArray addObject:[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"driver_image"]];
            }
            self.orderDetail =       [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"details"];
            
        } else if([[json objectForKey:@"status"] intValue] == 1) {
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
        } else if([[json objectForKey:@"status"] intValue] == 2){
                [self invalidAccessToken];
        }
    }
}

- (void)invalidAccessToken {
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
    [[NSUserDefaults standardUserDefaults] setObject:Nil forKey:UserData];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
