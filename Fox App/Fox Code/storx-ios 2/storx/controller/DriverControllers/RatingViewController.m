//
//  RatingViewController.m
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "RatingViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface RatingViewController ()

@end

@implementation RatingViewController
@synthesize orderIdConfirm = _orderIdConfirm;

- (void)viewDidLoad {
    [super viewDidLoad];
    RatingView *object = [[RatingView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    object.ratingViewDelegate = self;
    [self.view addSubview:object];
    self.view.backgroundColor=[UIColor blackColor];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) { //Driver
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"isdoRatingCall"]){
            [self getData:self.orderIdConfirm urlString:@"particular_order_details"];
        }
    }
    else {
        [self getData:self.orderIdConfirm urlString:@"order_details_to_customer"];
    }
    
    infoArray = [[NSMutableArray alloc] initWithObjects:self.userName,self.userImage,self.userAddress, nil];
    [object ratingViewFunction:infoArray];
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}


-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)getData:(NSString *)orderId
      urlString:(NSString *)urlString {
    
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:urlString
                                                         data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken] ,orderId]dataUsingEncoding:NSUTF8StringEncoding]
                                                         Type:@"POST"
                                             loadingindicator:YES];
    //NSLog(@"json: %@",json);
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            self.orderIdConfirm = orderId;
            
            if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 1) { //Driver
                self.userName       = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"data"] valueForKey:@"customer_name"] objectAtIndex:0]];
                self.userImage      = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"data"] valueForKey:@"customer_image"] objectAtIndex:0]];
                self.userAddress    = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"data"] valueForKey:@"drop_location_address"]objectAtIndex:0]];
            }
            else {
                self.userName       = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_name"]];
                self.userImage      = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_image"]];
                self.userAddress    = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_address"]];
            }
        }
        else if([[json objectForKey:@"status"] intValue] == 1){
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2){
            [self invalidAccessToken];
        }
    }
}

#pragma mark - Back Button

- (void)backFromMenu{
    
}

- (void)backButton {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [self.sideMenuViewController presentLeftMenuViewController];
}

#pragma mark - invalid Access Token

- (void)invalidAccessToken {
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserData];
     [[NSUserDefaults standardUserDefaults] setBool:Nil forKey:@"isdoRatingCall"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
    [self.sideMenuViewController hideMenuViewController];

}

#pragma mark - Submit Review

- (void)submitReview :(NSString *)rating
              comment:(NSString *)comment
              apiName:(NSString *)apiName {
    
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:apiName
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@&rating=%@&comments=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                              _orderIdConfirm,
                                                                              rating,
                                                                              comment] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    //NSLog(@"rate_customer: %@",json);
    
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            
            if ([[[NSUserDefaults standardUserDefaults]valueForKey:DriverMode]integerValue]==1) {
                [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:StateFlagDriverAfterStartOrder];
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isdoMedicineCall"];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isdoRatingCall"];
                OrdersViewController *obj = [[OrdersViewController alloc] init];
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
                
                RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                                leftMenuViewController:[[MenuViewController alloc] init]
                                                                               rightMenuViewController:nil];
                sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
                sideMenuViewController.delegate = self;
                sideMenuViewController.contentViewShadowEnabled = YES;
                [self.navigationController pushViewController:sideMenuViewController animated:NO];
            }
            else {
                
                if([[[NSUserDefaults standardUserDefaults] valueForKey:@"ControllerType"] integerValue] == 2) {
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[ReminderHistoryViewController alloc] init]];
                    
                    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                                    leftMenuViewController:[[MenuViewController alloc] init]
                                                                                   rightMenuViewController:nil];
                    sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
                    sideMenuViewController.delegate = self;
                    sideMenuViewController.contentViewShadowEnabled = YES;
                    [self.navigationController pushViewController:sideMenuViewController animated:NO];
                }
                else
                {
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[HomeViewController alloc] init]];
                    
                    RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                                    leftMenuViewController:[[MenuViewController alloc] init]
                                                                                   rightMenuViewController:nil];
                    sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
                    sideMenuViewController.delegate = self;
                    sideMenuViewController.contentViewShadowEnabled = YES;
                    [self.navigationController pushViewController:sideMenuViewController animated:NO];
                }
            }
        }
        else if([[json objectForKey:@"status"] intValue] == 1){
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2){
            [self invalidAccessToken];
        }
    }
}

#pragma mark - back To List Order

- (void)backToListOrder {
    
   /* OrdersViewController *ordersViewControllerObject;
    for(int i = 0; i <allControllers.count; i++) {
        if([[allControllers objectAtIndex:i] isKindOfClass:[OrdersViewController class]]) {
            ordersViewControllerObject = [allControllers objectAtIndex:i];
            break;
        }
    }*/
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
