//
//  ViewOrderViewController.h
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewOrderView.h"
#import "OrderDetailViewController.h"
#import "RatingViewController.h"

@interface ViewOrderViewController : UIViewController <ViewOrderProtocol> {
    ViewOrderView *object;
}
@property (assign,nonatomic) BOOL isFromReachDestination;
@property (strong,nonatomic) NSString *orderId;
@property (strong,nonatomic) NSArray  *detailOrderArray;
@property (strong,nonatomic) NSMutableArray  *customerInfoArray;
@property (strong,nonatomic) NSString *flagCheck;
@property (strong,nonatomic) NSDictionary *jsonDictionary;

@end
