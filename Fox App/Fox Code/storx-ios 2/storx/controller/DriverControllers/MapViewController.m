//
//  MapViewController.m
// Storx
//
//  Created by clicklabs on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "MapViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface MapViewController ()

@end

@implementation MapViewController
@synthesize orderId       = _orderId;
@synthesize isStartOrder  = _isStartOrder;
@synthesize orderInfoFlag = _orderInfoFlag;

#pragma mark - viewDidLoad

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (object) {
        [object clearMarkerFromMap];
    }
    
    object = [[MapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    object.mapViewDelegate = self;
    [self.view addSubview:object];
    [object mapView];
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
        
        [self updateLocation:[[NSUserDefaults standardUserDefaults] floatForKey:@"updateLat"]
                     longLoc:[[NSUserDefaults standardUserDefaults] floatForKey:@"updateLong"]];
    }
}

- (void)updateLocation:(float)latLoc longLoc:(float)longLoc {
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"update_driver_location"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&latitude=%@&longitude=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                              [NSString stringWithFormat:@"%f",latLoc],
                                                                              [NSString stringWithFormat:@"%f",longLoc]] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"json: %@",json);
    if(json) {
        if([[json objectForKey:@"status"] intValue] == 0) {
            
        }
    }
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [[ScheduledLocationManager getSharedInstance] setDelegate:self];
    [[ScheduledLocationManager getSharedInstance] getUserLocationWithInterval:5];
    
    [object mapUIView:_orderId startOrder:_isStartOrder flag:self.flagCheckString isMenuButtonShaow:self.isMenuButtonShow];
    
    if(![CLLocationManager locationServicesEnabled])
    {
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Location Service Disable"
                                                      message:@"To enable, please go to Settings and turn on Location Service for this app."
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        alert1.tag = 1092;
        [alert1 show];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) removeAllViews {
    for ( UIView *removeSubviews in self.view.subviews) {
        [removeSubviews removeFromSuperview];
    }
}

#pragma mark - View Order

- (void)viewOrder :(BOOL)isFromReachDestination
           orderId:(NSString *)orderId
       detailorder:(NSArray *)detailOrder
      customerInfo:(NSMutableArray *)customerInfo {
    
    ViewOrderViewController *objViewOrder = [[ViewOrderViewController alloc]init];
    objViewOrder.isFromReachDestination = isFromReachDestination;
    objViewOrder.orderId = orderId;
    objViewOrder.detailOrderArray = detailOrder;
    objViewOrder.customerInfoArray = customerInfo;
    [self.navigationController pushViewController:objViewOrder animated:YES];
}

- (void)viewOrder {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFromMapView"];
    OrderDetailViewController *orderDetail = [[OrderDetailViewController alloc]init];
    orderDetail.orderId = _orderId;
    [self.navigationController pushViewController:orderDetail animated:YES];
}
#pragma mark - Custyomer Profile

- (void)customerProfile :(NSMutableArray *)customerInfo {
    ProfileViewController *objProfile = [[ProfileViewController alloc] init];
    objProfile.customerInfoMutArray = customerInfo;
    [self.navigationController pushViewController:objProfile animated:YES];
}

#pragma mark -Info Method 

- (void)infoMethod :(NSDictionary *)dict  {
   DispensaryView *objSlideDispensaryView = [[DispensaryView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    [self.view addSubview:objSlideDispensaryView];
    
    [self slideView:object withDuration:0.3 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:-1050/3 currentSupSize:SCREEN_WIDTH] andY:0];
    [self.view bringSubviewToFront:object];
    
    [objSlideDispensaryView dispensaryInfoAPICall:dict];
    
    [tempViewForTouch removeFromSuperview];
    tempViewForTouch = nil;
    tempViewForTouch = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, SCREEN_HEIGHT)];  // for touch begin
    tempViewForTouch.backgroundColor = [UIColor clearColor];
    tempViewForTouch.userInteractionEnabled = YES;
    [self.view addSubview:tempViewForTouch];
}

#pragma mark - Touch

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if(touch.view == tempViewForTouch) {
        [tempViewForTouch removeFromSuperview];
        tempViewForTouch = nil;
        [self slideView:object withDuration:0.3 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH] andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT]];
    }
}

#pragma mark - Pick Up Call 

- (void)pickUp :(NSString *)dispensaryId
        orderId:(NSString *)orderID
    detailorder:(NSArray *)detailOrder
   customerInfo:(NSMutableArray *)customerInfo
     flagString:(NSString *)flagCheck  {
    
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"customer_reached"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                              dispensaryId] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"Customer Map Side: %@",json);
    if(json) {
        if([[json objectForKey:@"status"] intValue] == 0) {
            MedicineDeliveryViewController *medicineDelivery = [[MedicineDeliveryViewController alloc]init];
            medicineDelivery.orderDetail = detailOrder;
            medicineDelivery.customerArray = customerInfo;
            medicineDelivery.orderId = orderID;
            medicineDelivery.flagString = flagCheck;
            medicineDelivery.isFromPushNotificationMenu = NO;
            [self.navigationController pushViewController:medicineDelivery animated:YES];
        }
        else if([[json objectForKey:@"status"] intValue] == 1) {
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2) {
           // [_mapViewDelegate invalidAccessToken];
        }
    }
}

#pragma mark - Menu Button

- (void)menuButton {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)backFromMenu {
    
}

#pragma mark - Back Button from Map

- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Close Button

- (void)closeMethod {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Medicine Delivery Controller

-(void)toDeliveryController:(NSString *)orderID
                detailorder:(NSArray *)detailOrder
               customerInfo:(NSMutableArray *)customerInfo
                flagString:(NSString *)flagCheck{
    
    [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:StateFlagDriverAfterStartOrder];

    //[self performSelector:@selector(removeAllViews) withObject:nil afterDelay:0.2];
    MedicineDeliveryViewController *medicineDelivery = [[MedicineDeliveryViewController alloc]init];
    medicineDelivery.orderDetail = detailOrder;
    medicineDelivery.customerArray = customerInfo;
    medicineDelivery.orderId = orderID;
    medicineDelivery.flagString = flagCheck;
    medicineDelivery.isFromPushNotificationMenu = NO;
    [self.navigationController pushViewController:medicineDelivery animated:YES];
}

#pragma mark - invalid Access Token

- (void)invalidAccessToken {
    if (object) {
        [object clearMarkerFromMap];
    }
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
    [[NSUserDefaults standardUserDefaults] setObject:Nil forKey:UserData];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
    [self.sideMenuViewController hideMenuViewController];

}

#pragma mark - Show Detail Button

- (void)showDetailButtonAction {
    
    /*  CustomerMenuViewControllercustomerMenuViewControllerObject = [[CustomerMenuViewController alloc]init];
     customerMenuViewControllerObject.orderStatusType = [[valueArray objectAtIndex:1] intValue];
     customerMenuViewControllerObject.jsonRecieved = [[NSMutableDictionary alloc] initWithDictionary:@{
     @"menu":[json objectForKey:@"menu"]
     }];
     customerMenuViewControllerObject.dispenseryName = [[NSString stringWithFormat:@"%@",[json objectForKey:@"dispensery_name"]] capitalizedString];
     [customerMenuViewControllerObject loadScreen];
     [self.navigationController pushViewController:customerMenuViewControllerObject animated:TRUE]; */
}

#pragma mark - Send message

- (void)sendMessageAction {
    
    if(![MFMessageComposeViewController canSendText])
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    //    NSArray *recipents = @[@"12345678", @"72345524"];
    NSString *message = [NSString stringWithFormat:@"Send message..."];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    //    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result  {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) alertView : (UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1092) {
        [self.navigationController popToRootViewControllerAnimated:TRUE];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Schedule Manager delegates

-(void)scheduledLocationManageDidFailWithError:(NSError *)error{
    NSLog(@"map error:%@",error.description);
}

- (void)scheduledLocationManageDidUpdateLocations:(CLLocation *)currentLocations{
    [[NSUserDefaults standardUserDefaults] setFloat:currentLocations.coordinate.latitude forKey:@"updateLat"];
    [[NSUserDefaults standardUserDefaults] setFloat: currentLocations.coordinate.longitude forKey:@"updateLong"];
    
    if (currentLocations) {
        [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
        
        if(!isToUpdateMapOnce) {
            isToUpdateMapOnce = YES;
            if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
                
                NSDictionary *params = @{@"access_token": [[NSUserDefaults standardUserDefaults]valueForKey:AccessToken],
                                         @"latitude":[NSString stringWithFormat:@"%f",currentLocations.coordinate.latitude],
                                         @"longitude":[NSString stringWithFormat:@"%f",currentLocations.coordinate.longitude],
                                         };
                [[ServerCallModel getSharedInstance] updateLocationDriver:nil :params];
            }
            
            if (object)
                [object showMap:currentLocations.coordinate.latitude longVal:currentLocations.coordinate.longitude];
            
                isToUpdateMapOnce = NO;
        }
    }
}

-(void) slideView:(UIView *)uiv_slide withDuration:(double)d_duration toX:(CGFloat)xValue andY:(CGFloat)yValue {
    [UIView animateWithDuration:d_duration animations:^ {
        [uiv_slide setFrame:CGRectMake(xValue, yValue, uiv_slide.frame.size.width, uiv_slide.frame.size.height)];
    }completion:^(BOOL finished) {
    }
     ];
}
@end
