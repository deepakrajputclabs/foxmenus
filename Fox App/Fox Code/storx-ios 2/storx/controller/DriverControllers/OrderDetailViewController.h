//
//  OrderDetailViewController.h
//  Storx
//
//  Created by click on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Header.h"
#import "MapViewController.h"

@interface OrderDetailViewController : UIViewController
{
    UIImageView *boxImage;
    NSArray *iconArray;
}
@property (nonatomic) NSString *orderId;
@property (nonatomic,strong) NSDictionary *jsonForCustomerOrderDetail;
@property (nonatomic,strong) NSDictionary *customerInfo;
@property (nonatomic,strong) NSString *customerName;
@property (nonatomic,strong) NSString *customerImage;
@property (nonatomic,strong) NSString *orderTime;
@property (nonatomic,strong) NSString *customerPhone;
@property (nonatomic,strong) NSString *customerAddress;
@property (nonatomic,strong) NSArray *detailOrder;
@property (nonatomic,strong) NSString *totalMedicines;
@property (nonatomic,strong) NSString *totalPrice;

@end
