//
//  MapViewController.h
// Storx
//
//  Created by clicklabs on 11/8/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapView.h"
#import "ScheduledLocationManager.h"
#import "ProfileViewController.h"
#import "ViewOrderViewController.h"
#import "OrdersView.h"
#import "MedicineDeliveryViewController.h"
#import "OrderDetailViewController.h"
#import "DispensaryView.h"

//orderViewProtocol removed
@interface MapViewController : UIViewController <mapViewProtocol, ScheduledLocationManagerDelegate, MFMessageComposeViewControllerDelegate >
{
    MapView *object;
    OrdersView *viewOrder;
    UIView *tempViewForTouch;
    BOOL isToUpdateMapOnce;
}
@property (strong,nonatomic) NSString *orderId;
@property (strong,nonatomic) NSDictionary *orderDictionary;
@property (assign,nonatomic) BOOL isStartOrder;
@property (strong,nonatomic) NSString *flagCheckString;
@property (nonatomic) NSInteger *orderInfoFlag;
@property (nonatomic) BOOL isMenuButtonShow;

@end
