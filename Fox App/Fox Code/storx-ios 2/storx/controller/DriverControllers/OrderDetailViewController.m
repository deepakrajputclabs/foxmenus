//
//  OrderDetailViewController.m
//  Storx
//
//  Created by click on 12/18/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "OrderDetailViewController.h"

@interface OrderDetailViewController ()

@end

@implementation OrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor blackColor];
    UIImageView *bkImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    bkImageView.image = [UIImage imageNamed:@"bg.png"];
    bkImageView.userInteractionEnabled = YES;
    [self.view addSubview:bkImageView];
    
    UIButton *backButton;
    backButton =  [[UIButton alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:64/3 currentSupSize:SCREEN_HEIGHT],[[CommonClass getSharedInstance] targetSize:414 targetSubSize:180/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:180/3 currentSupSize:SCREEN_HEIGHT])];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_icon_onclick.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    
    UIImageView *storxImageView =    [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:486/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:272/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:134/3 currentSupSize:SCREEN_HEIGHT])];
    storxImageView.image = [UIImage imageNamed:@"storx logo.png"];
    [self.view addSubview:storxImageView];
    
    iconArray = [[NSArray alloc]initWithObjects:@"contact_patient.png",@"location_order.png",@"", nil];
    NSLog(@"%@",self.jsonForCustomerOrderDetail);
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromMapView"]) {
            [self serverCall:_orderId];
        }
        else {
            if(self.jsonForCustomerOrderDetail) {
                
                if([[self.jsonForCustomerOrderDetail objectForKey:@"status"] intValue] == 0) {
                    
                    _customerInfo       = [[self.jsonForCustomerOrderDetail objectForKey:@"data"] objectAtIndex:0];
                    _customerName       = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_name"];
                    _customerImage      = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"] objectAtIndex:0] valueForKey:@"dispensery_image"];
                    
                    if([[self.jsonForCustomerOrderDetail valueForKey:@"flag"] integerValue] == 10)
                        _orderTime          = [[CommonClass getSharedInstance] convertUtcDateToLocalDateOnly:[[[self.jsonForCustomerOrderDetail objectForKey:@"data"]objectAtIndex:0] valueForKey:@"pickup_time"]] ;
                    else
                        _orderTime          = [[CommonClass getSharedInstance] convertUtcDateToLocalDateOnly:[[[self.jsonForCustomerOrderDetail objectForKey:@"data"]objectAtIndex:0] valueForKey:@"delivery_time"]] ;
                    
                    _customerPhone      = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"] ;
                    _customerAddress    = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"]objectAtIndex:0] valueForKey:@"dispensery_address"] ;
                    _totalPrice         = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"];
                    _totalMedicines     = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"]objectAtIndex:0] valueForKey:@"total_medicines"] ;
                    _detailOrder        = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"]objectAtIndex:0] valueForKey:@"details"] ;
                    _orderId            = [[[self.jsonForCustomerOrderDetail objectForKey:@"data"]objectAtIndex:0]valueForKey:@"order_id"];
                }
                else if([[self.jsonForCustomerOrderDetail objectForKey:@"status"] intValue] == 1){
                    [[[UIAlertView alloc] initWithTitle:@""
                                                message:[self.jsonForCustomerOrderDetail valueForKey:@"error"]
                                               delegate:self cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                }
                else if([[self.jsonForCustomerOrderDetail objectForKey:@"status"] intValue] == 2){
                    [self invalidAccessToken];
                }
            }
        }
    }
    else {
        [self serverCall:_orderId];
    }
    [self detailView];

    // Do any additional setup after loading the view.
}
-(void)detailView{
    boxImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:277/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1931/3 currentSupSize:SCREEN_HEIGHT])];
    
    boxImage.backgroundColor = ColorBlackBK;
    boxImage.userInteractionEnabled = YES;
    [self.view addSubview:boxImage];
    UIImageView *customerImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:57/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:170/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
    [customerImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_customerImage]] placeholderImage:[UIImage imageNamed:@"no_order_icon@2x.png"]];
    customerImage.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/6 currentSupSize:SCREEN_HEIGHT];
    customerImage.layer.masksToBounds = YES;
    customerImage.layer.borderColor = ColorGreenButton.CGColor;
    customerImage.layer.borderWidth = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:6/3 currentSupSize:SCREEN_HEIGHT];
    [boxImage addSubview:customerImage];
    
    // Label Start
    UILabel *customerName = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:240/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:912/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    [customerName setFont:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];;
    customerName.textColor = ColorFFFFFF;
    customerName.numberOfLines = 1;
    customerName.backgroundColor = [UIColor clearColor];
    customerName.textAlignment = NSTextAlignmentLeft;
    customerName.text = _customerName;
    [boxImage addSubview:customerName];
    // Label End
    
    //Date and Time Label
    UILabel *dateTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:240/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:912/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
    [dateTimeLabel setFont:[UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]]];
    dateTimeLabel.textColor = ColorFFFFFF;
    dateTimeLabel.numberOfLines = 1;
    dateTimeLabel.backgroundColor = [UIColor clearColor];
    dateTimeLabel.textAlignment = NSTextAlignmentLeft;
    dateTimeLabel.text = _orderTime;
    [boxImage addSubview:dateTimeLabel];
    
    //UnderLineImage
    
    float height = 0;
    height = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(284/3)+(48/3) currentSupSize:SCREEN_HEIGHT];
    float labelHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(284/3) + (30/3) currentSupSize:SCREEN_HEIGHT];
    
    float underlineHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(459/3) currentSupSize:SCREEN_HEIGHT];
    float verticalHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(284/3) + (30/3) currentSupSize:SCREEN_HEIGHT];
    //        height = (284/3) + (48/3);
    //
    // float labelHeight = (284/3) + (30/3);
    // float underlineHeight = (459/3);
    //float verticalHeight = (284/3) + (30/3);
    
    for(int i =0; i<=2; i++)
    {
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], height, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:68/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:68/3 currentSupSize:SCREEN_HEIGHT])];
        if(i==1)
        {
            iconImage.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], height, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:60/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT]);
        }
        iconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[iconArray objectAtIndex:i]]];
        [boxImage addSubview:iconImage];
        
        UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:175/3 currentSupSize:SCREEN_WIDTH], labelHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:948/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:90/3 currentSupSize:SCREEN_HEIGHT])];
        infoLabel.textColor = ColorFFFFFF;
        infoLabel.numberOfLines = 0;
        infoLabel.tag = 20+i;
        infoLabel.backgroundColor = [UIColor clearColor];
        infoLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [boxImage addSubview:infoLabel];
        
        UIImageView *underLineImage1 = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], underlineHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
        underLineImage1.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
        [boxImage addSubview:underLineImage1];
        
        UIImageView *verticalLine  = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1002/3 currentSupSize:SCREEN_WIDTH], verticalHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:3/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:115/3 currentSupSize:SCREEN_HEIGHT])];
        verticalLine.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
        verticalLine.tag = 100+i;
        
        UIButton *callButton = [[UIButton alloc] initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1032/3 currentSupSize:SCREEN_WIDTH], verticalHeight, [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:120/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT])];
        callButton.tag = i;
        callButton.userInteractionEnabled = YES;
        [callButton addTarget:self action:@selector(callMethod:) forControlEvents:UIControlEventTouchDown];
        [callButton setBackgroundImage:[UIImage imageNamed:@"call_icon.png"] forState:UIControlStateNormal];
        [callButton setBackgroundImage:[UIImage imageNamed:@"call_icon_onclick.png"] forState:UIControlStateSelected];
        
        height = height + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(175/3)+(12/3) currentSupSize:SCREEN_HEIGHT];
        labelHeight = labelHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(175/3) currentSupSize:SCREEN_HEIGHT];
        underlineHeight = underlineHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(230/3) currentSupSize:SCREEN_HEIGHT];
        verticalHeight = verticalHeight +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(175/3)+(30/3) currentSupSize:SCREEN_HEIGHT];
        
        if(i==0){
            [boxImage addSubview:callButton];
            [boxImage addSubview:verticalLine];
            infoLabel.text = _customerPhone;
        }
        
        if(i == 1){
            [callButton setBackgroundImage:[UIImage imageNamed:@"location_icon.png"] forState:UIControlStateNormal];
            [callButton setBackgroundImage:[UIImage imageNamed:@"location_icon_onclick.png"] forState:UIControlStateSelected];
            [boxImage addSubview:callButton];
            [boxImage addSubview:verticalLine];
            infoLabel.text = _customerAddress;
            infoLabel.backgroundColor = [UIColor clearColor];
            infoLabel.frame =  CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:175/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:489/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:827/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT]);
        }
        else if(i == 2){
            labelHeight = (689/3)+(15/3);
            iconImage.hidden = YES;
            infoLabel.frame = CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:68/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:labelHeight currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:948/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT]);
            verticalLine.hidden = YES;
            callButton.hidden = YES;
            underLineImage1.hidden = YES;
            NSString  *string1;
            if ([_totalMedicines integerValue] >1) {
                string1 = [NSString stringWithFormat:@"Total Order Items: %@ qty", _totalMedicines]; // Made a Change
            }
            else{
                string1 = [NSString stringWithFormat:@"Total Order Items: %@ qty", _totalMedicines];
            }
            NSString  *string2 = [NSString stringWithFormat:@"Total Amount: INR %@", _totalPrice];
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@\n%@",string1,string2]];
            [attributedString addAttribute:NSFontAttributeName
                                     value:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]]
                                     range:NSMakeRange(19, string1.length-19)];
            [attributedString addAttribute:NSFontAttributeName
                                     value:[UIFont fontWithName:FontMedium size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:70/3 currentSupSize:SCREEN_HEIGHT]]
                                     range:NSMakeRange(string1.length+1+16, string2.length-16)];
            infoLabel.attributedText = attributedString;
        }
    }
    
    UILabel  *orderDetailLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:919/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:100/3 currentSupSize:SCREEN_HEIGHT])];
    orderDetailLabel.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.7];
    orderDetailLabel.text = @"   Order Details:";
    orderDetailLabel.textColor = ColorFFFFFF;
    orderDetailLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
    [boxImage addSubview:orderDetailLabel];
    
    UIScrollView *scroll = [[UIScrollView alloc]init];
    //ScrollView
    [scroll setFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:1019/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:912/3 currentSupSize:SCREEN_HEIGHT])];
    [scroll setBackgroundColor:[UIColor clearColor]];
    scroll.userInteractionEnabled=true;
    [boxImage addSubview:scroll];
    
    float productHeight = 0;
    productHeight = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT];
    
    for (int i =0; i<[_detailOrder count]; i++) {
        UIImageView *productImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:30/3 currentSupSize:SCREEN_WIDTH], productHeight +[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:30/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:132/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:132/3 currentSupSize:SCREEN_HEIGHT])];
        productImage.layer.cornerRadius = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:132/6 currentSupSize:SCREEN_HEIGHT];
        productImage.layer.borderColor = [UIColor whiteColor].CGColor;
        productImage.layer.masksToBounds = YES;
        [productImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[_detailOrder objectAtIndex:0] valueForKey:@"medicine_image"]]] placeholderImage:[UIImage imageNamed:@"no_order_icon@2x.png"]];
        productImage.layer.borderWidth = [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:6/3 currentSupSize:SCREEN_HEIGHT];
        [scroll addSubview:productImage];
        
        UILabel  *productLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:190/3 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:585/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
        productLabel.backgroundColor = [UIColor clearColor];
        productLabel.text =  [NSString stringWithFormat:@"%@",[[_detailOrder objectAtIndex:0]valueForKey:@"medicine_name"]];
        productLabel.textColor = ColorFFFFFF;
        productLabel.numberOfLines = 2;
        productLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:productLabel];
        
        NSString *qtyString;
        qtyString = [[[_detailOrder  objectAtIndex:0] valueForKey:@"medicine_quantity"] stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
        UILabel  *quantityLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:775/3 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:210/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
        quantityLabel.backgroundColor = [UIColor clearColor];
        quantityLabel.numberOfLines = 2;
        quantityLabel.textAlignment = NSTextAlignmentCenter;
        quantityLabel.text =  [NSString stringWithFormat:@"%@",qtyString];
        quantityLabel.textColor = ColorFFFFFF;
        quantityLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:quantityLabel];
        
        UILabel  *priceLabel = [[UILabel alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:985/3 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:15/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:210/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:170/3 currentSupSize:SCREEN_HEIGHT])];
        priceLabel.backgroundColor = [UIColor clearColor];
        priceLabel.textAlignment = NSTextAlignmentCenter;
        priceLabel.text =  [NSString stringWithFormat:@"INR %@",[[_detailOrder objectAtIndex:0] valueForKey:@"medicine_price"]];//INR
        priceLabel.textColor = ColorFFFFFF;
        priceLabel.font = [UIFont fontWithName:FontRegular size:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:54/3 currentSupSize:SCREEN_HEIGHT]];
        [scroll addSubview:priceLabel];
        
        UIImageView *borderImage = [[UIImageView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],productHeight+ [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:(200/3) currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:3/3 currentSupSize:SCREEN_HEIGHT])];
        borderImage.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.3];
        [scroll addSubview:borderImage];
        
        productHeight = productHeight + [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:200/3 currentSupSize:SCREEN_HEIGHT];
    }
    scroll.contentSize = CGSizeMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1182/3 currentSupSize:SCREEN_WIDTH], productHeight);
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isFromMapView"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFromMapView"];
        [[self.view viewWithTag:1] setHidden:YES];
        [[self.view viewWithTag:11] setHidden:YES];
        [[self.view viewWithTag:101] setHidden:YES];
        [self.view viewWithTag:21].frame =  CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:175/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:489/3 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:948/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:120/3 currentSupSize:SCREEN_HEIGHT]);

    }
}

-(void)serverCall:(NSString *)orderDetail {
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"particular_order_details"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                            [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                            orderDetail]dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"json: %@",json);
    if(json) {
        if([[json objectForKey:@"status"] intValue] == 0) {
            _customerInfo       = [[json objectForKey:@"data"] objectAtIndex:0];
            _customerName       = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_name"];
            _customerImage      = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"customer_image"];
            
            if([[json valueForKey:@"flag"] integerValue] == 10)
                _orderTime          = [[CommonClass getSharedInstance] convertUtcDateToLocalDateOnly:[[[json objectForKey:@"data"]objectAtIndex:0] valueForKey:@"pickup_time"]] ;
            else
                _orderTime          = [[CommonClass getSharedInstance] convertUtcDateToLocalDateOnly:[[[json objectForKey:@"data"]objectAtIndex:0] valueForKey:@"order_time"]] ;

            _customerPhone      = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"phone_no"] ;
            _customerAddress    = [[[json objectForKey:@"data"]objectAtIndex:0]  valueForKey:@"drop_location_address"] ;
            _totalPrice         = [[[json objectForKey:@"data"] objectAtIndex:0] valueForKey:@"price"];
            _totalMedicines     = [[[json objectForKey:@"data"]objectAtIndex:0] valueForKey:@"total_medicines"] ;
            _detailOrder        = [[[json objectForKey:@"data"]objectAtIndex:0] valueForKey:@"details"] ;
            _orderId            = [[[json objectForKey:@"data"]objectAtIndex:0] valueForKey:@"order_id"];
        }
        else if([[json objectForKey:@"status"] intValue] == 1){
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2){
            [self invalidAccessToken];
        }
    }
}

- (void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)invalidAccessToken {
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
    [[NSUserDefaults standardUserDefaults] setObject:Nil forKey:UserData];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - Call Button
- (void)callMethod: (UIButton *)sender {
    if(sender.tag == 0) {
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",_customerPhone] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSLog(@"phone number :%@",_customerPhone);
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]]]){
            
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Call" message:_customerPhone delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
            warningAlert.tag = 1000;
            [warningAlert show];
            
        }else{
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your device doesn't support call facility!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
        }
    }
    else {
        [self getDirections];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 0) {
        NSString *cleanedString = [[[NSString stringWithFormat:@"%@",_customerPhone] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Map Method
-(void)getDirections {
   // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFromOrderDetail"];
    MapViewController *mapController  = [[MapViewController alloc]init];
    mapController.orderId = _orderId;
    mapController.isMenuButtonShow = YES;
    [self.navigationController pushViewController:mapController animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
