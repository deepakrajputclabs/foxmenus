//
//  OrdersViewController.h
// Storx
//
//  Created by clicklabs on 11/5/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
#import "OrdersView.h"
#import "MapView.h"

@interface OrdersViewController : UIViewController <orderViewProtocol, ScheduledLocationManagerDelegate>
{
    OrdersView *object;
}
@end
