//
//  ProfileViewController.h
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileView.h"
#import "PickerController.h"
#import "EditProfileView.h"
#import "ChangePasswordView.h"
#import "SplashViewController.h"

@interface ProfileViewController : UIViewController <profileViewProtocol,editProfileProtocol,changePasswordProtocol>
{
    ProfileView *object;
    EditProfileView *editScreen;
    ChangePasswordView *changeObject;
}
@property (strong,nonatomic) NSMutableArray *customerInfoMutArray;
@property (strong,nonatomic) NSString *isFromMenu;

@end
