//
//  ViewOrderViewController.m
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ViewOrderViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface ViewOrderViewController ()

@end

@implementation ViewOrderViewController
@synthesize isFromReachDestination;
@synthesize orderId = _orderId;
@synthesize jsonDictionary = _jsonDictionary;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor blackColor];
    object = [[ViewOrderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    object.viewOrderDelegate = self;
    [self.view addSubview:object];
    
    [object ordersView:_jsonDictionary]; //driver
}

-(void) removeAllViews {
    for ( UIView *removeSubviews in self.view.subviews) {
        [removeSubviews removeFromSuperview];
    }
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    object = [[ViewOrderView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    object.viewOrderDelegate = self;
    [self.view addSubview:object];
    [object ordersView:_jsonDictionary];
    [super viewWillAppear:animated];
    [object refreshTableView];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

#pragma mark - order Deliver

- (void)orderDeliver :(NSString *)orderIdConfirm
         customerInfo:(NSMutableArray *)customerInfo {
    RatingViewController *objRating = [[RatingViewController alloc]init];
    objRating.orderIdConfirm = orderIdConfirm;
    objRating.customerInfoMutArray = customerInfo;
    [self.navigationController pushViewController:objRating animated:YES];
}

#pragma mark - Back Button

- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)detailsWithUserInfo:(NSString *)userID{
    //[self performSelector:@selector(removeAllViews) withObject:nil afterDelay:0.2];
    OrderDetailViewController *orderDetail = [[OrderDetailViewController alloc]init];
    orderDetail.orderId = userID;
    [self.navigationController pushViewController:orderDetail animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
