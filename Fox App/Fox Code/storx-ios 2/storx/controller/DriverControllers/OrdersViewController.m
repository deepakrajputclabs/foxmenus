//
//  OrdersViewController.m
// Storx
//
//  Created by clicklabs on 11/5/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

// Tag currentTableView: 101 , comingTableView: 102

#import "OrdersViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface OrdersViewController ()

@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self removeObserver];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(listOrderRefresh)
                                                 name:@"listOrderRefreshNotification"
                                               object:nil];
    
    object = [[OrdersView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    object.orderViewDelegate = self;
    [self.view addSubview:object];
    [object OrderView];
}

-(void)listOrderRefresh {
    [object jsonCall];
}

-(void) removeAllViews {
    for ( UIView *removeSubviews in self.view.subviews) {
        [removeSubviews removeFromSuperview];
    }
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] integerValue] == 1) {
        [[ScheduledLocationManager getSharedInstance] setDelegate:self];
        [[ScheduledLocationManager getSharedInstance] getUserLocationWithInterval:5.0];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Menu Button

- (void)menuButton {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)backFromMenu {
    
}

#pragma mark - Upcoming Order Button

- (void)viewUpcomingOrders:(NSDictionary *)jsonData{
   // [self performSelector:@selector(removeAllViews) withObject:nil afterDelay:0.2];
    ViewOrderViewController *viewOrder = [[ViewOrderViewController alloc] init];
    viewOrder.jsonDictionary = jsonData;
    [self.navigationController pushViewController:viewOrder animated:YES];
}

#pragma mark - invalid Access Token

- (void)invalidAccessToken {
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
    [[NSUserDefaults standardUserDefaults] setObject:Nil forKey:UserData];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - Server Call for Order List

-(NSDictionary *) serverListCall {
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"list_orders"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken]] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"json: %@",json);
    return json;
}

#pragma mark - Deliver Button

- (void)deliveryMethod:(NSArray *)orderArray {
    [[ScheduledLocationManager getSharedInstance] getUserLocationWithInterval:5.0];

    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"start_order"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&order_id=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],[orderArray  objectAtIndex:0]] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:YES];
    NSLog(@"json: %@",json);
    if(json) {
        if([[json objectForKey:@"status"] intValue] == 0) {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:StateFlagDriverAfterStartOrder];
            MapViewController *mapController  = [[MapViewController alloc]init];
            mapController.orderId = [orderArray  objectAtIndex:0];
            [[NSUserDefaults standardUserDefaults] setValue:[orderArray objectAtIndex:0] forKey:@"resumeOrder"];
            mapController.isStartOrder = YES;
            [self.navigationController pushViewController:mapController animated:YES];
        }
        else if([[json objectForKey:@"status"] intValue] == 1){
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2){
            [self invalidAccessToken];
        }
    }
}

#pragma - Location Delegate

-(void)scheduledLocationManageDidFailWithError:(NSError *)error{
    NSLog(@"map error:%@",error.description);
}

- (void)scheduledLocationManageDidUpdateLocations:(CLLocation *)currentLocations{
    NSLog(@"Order Map:%f, %f",currentLocations.coordinate.latitude, currentLocations.coordinate.longitude);
    [[NSUserDefaults standardUserDefaults] setFloat:currentLocations.coordinate.latitude forKey:@"updateLat"];
    [[NSUserDefaults standardUserDefaults] setFloat: currentLocations.coordinate.longitude forKey:@"updateLong"];
    [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self removeObserver];
}

- (void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:@"listOrderRefreshNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
