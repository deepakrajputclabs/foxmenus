//
//  ProfileViewController.m
// Storx
//
//  Created by clicklabs on 11/10/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ProfileViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize customerInfoMutArray = _customerInfoMutArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor blackColor];
    object = [[ProfileView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    object.profileViewDelegate = self;
    [self.view addSubview:object];
}

#pragma mark - ViewWillAppear

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [object profileUIView];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Back Button

- (void)backButton {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Menu Button
- (void)menuButton {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)backFromMenu {
    
}


#pragma mark - Edit Button

- (void)editButton :(int)buttonIndex {
    PickerController *obj = [[PickerController alloc]init];

    if(buttonIndex == 0) {
        obj.pickerType = @"PhotoLibray";
    }
    else if(buttonIndex == 1) {
        obj.pickerType = @"Camera";
    }
    [self.navigationController pushViewController:obj animated:NO];
}

#pragma mark - invalid Access Token

- (void)invalidAccessToken {
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
    [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:UserData];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
    [self.sideMenuViewController hideMenuViewController];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  - Change Passsword Screen

- (void)changePassswordScreen{
    changeObject = [[ChangePasswordView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:changeObject];
    changeObject.changePasswordDelegate = self;
    [changeObject changePasswordUI];
    [self.view bringSubviewToFront:changeObject];
    
    
    [UIView animateWithDuration:0.3 delay:0.01 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         NSLog(@"animation is running.");
                         [self.view bringSubviewToFront:changeObject];
                         changeObject.frame = CGRectMake(0,changeObject.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT);
                     }
                     completion:^(BOOL finished) {
                         NSLog(@"animation finished.");
                         [self.view bringSubviewToFront:changeObject];
    }];
}

#pragma mark - Back from Change Password

- (void)backFromChangePassword{
    [UIView animateWithDuration:0.3 delay:0.01 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         NSLog(@"animation is running.");
                         changeObject.frame = CGRectMake(SCREEN_WIDTH,changeObject.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT);
                     }
                     completion:^(BOOL finished) {
                         NSLog(@"animation finished.");
                     }];
}

#pragma mark - Password Changed
- (void)passwordChanged{
    [UIView animateWithDuration:0.3 delay:0.01 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         NSLog(@"animation is running.");
                         changeObject.frame = CGRectMake(SCREEN_WIDTH,changeObject.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT);
                     }
                     completion:^(BOOL finished) {
                         NSLog(@"animation finished.");
                     }];
}


#pragma mark - Edit Profile Screen

- (void)editProfileScreen{
    editScreen = [[EditProfileView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [self.view addSubview:editScreen];
    editScreen.editProfileDelegate = self;
    [editScreen editProfileScreen];
    [self.view bringSubviewToFront:editScreen];
    
    [UIView animateWithDuration:0.3 delay:0.01 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         NSLog(@"animation is running.");
                         [self.view bringSubviewToFront:editScreen];
                         
                         editScreen.frame = CGRectMake(0,editScreen.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT);
                     }
                     completion:^(BOOL finished) {
                         NSLog(@"animation finished.");
                         [self.view bringSubviewToFront:editScreen];
                         
                     }];
   }

- (void)backFromEditProfile{
    [UIView animateWithDuration:0.3 delay:0.01 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         NSLog(@"animation is running.");
                         editScreen.frame = CGRectMake(SCREEN_WIDTH,editScreen.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT);
                     }
                     completion:^(BOOL finished) {
                         NSLog(@"animation finished.");
                     }];
}

-(void)doneEditingProfile
{
    [UIView animateWithDuration:0.3 delay:0.01 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         NSLog(@"animation is running.");
                         editScreen.frame = CGRectMake(SCREEN_WIDTH,editScreen.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT);
                     }
                     completion:^(BOOL finished) {
                         [self viewWillAppear:YES];
                         NSLog(@"animation finished.");
                     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
