//
//  RequestViewController.m
//  Storx
//
//  Created by clicklabs on 12/27/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "RequestViewController.h"
@interface RequestViewController ()
{
    DispensaryView *dispensaryObject;
    RequestDispensaryView *objRequestView;
    UIView *tempViewForTouch;
}
@end

@implementation RequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];    
    objRequestView = [[RequestDispensaryView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    objRequestView.requestDelegate = self;
    [self.view addSubview:objRequestView];
    
    [objRequestView requestInfoView:self.dispensaryId
                     dispensaryName:self.dispensaryName
                    requestMethodId:self.requestIdArray
                requestNameMutArray:self.requestNameArray
               requestPriceMutArray:self.requestPriceArray
             requestQtyTypeMutArray:self.requestQtyTypeArray
            requestQtyValueMutArray:self.requestQtyValueArray
                   requestImaeArray:self.requestImageArray
                     dispensaryInfo:self.dispensaryInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - back Button

- (void)backMethod {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Info Button

- (void)infoMethod:(NSDictionary *)dict  {
    [dispensaryObject removeFromSuperview];
    dispensaryObject = nil;
     dispensaryObject = [[DispensaryView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    [self.view addSubview:dispensaryObject];
    [self slideView:objRequestView withDuration:0.4 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:-1050/3 currentSupSize:SCREEN_WIDTH] andY:0];
    [self.view bringSubviewToFront:objRequestView];
    
    [[CommonClass getSharedInstance] startFade:dispensaryObject];

    [dispensaryObject dispensaryInfoAPICall:dict];
    
    tempViewForTouch = [[UIView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH],[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:50 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];  // for touch begin
    tempViewForTouch.backgroundColor = [UIColor clearColor];
    tempViewForTouch.userInteractionEnabled = YES;
    [self.view addSubview:tempViewForTouch];
}

#pragma mark - Touch

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if(touch.view == tempViewForTouch) {
        [tempViewForTouch removeFromSuperview];
        tempViewForTouch = nil;
        [self slideView:objRequestView withDuration:0.4 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH] andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT]];
    }
}


#pragma mark - Remove Filter Screen

- (void)removeFilterScreen {
    
    [tempViewForTouch removeFromSuperview];
    tempViewForTouch = nil;
    [self slideView:objRequestView withDuration:0.4 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH] andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT]];
}

#pragma mark - Remove Filter Screen

- (void)confirmOrderMethod:(NSString *)dispId
                    itemId:(NSString *)itemId
                       qty:(NSString *)qnty
                 orderType:(NSString *)orderType
                pickupTime:(NSString *)pickUpTime
                 itemPrice:(NSString *)itemPrice
                     price:(NSString *)price
                  discount:(NSString *)discount
             recurringType:(NSString *)recurringType
                   endDate:(NSString *)endDate
                   address:(NSString *)address
                   latCord:(NSString *)latCord
                  longCord:(NSString *)longCord {
    
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"book_order"
                                                                       data:[[NSString stringWithFormat:@"access_token=%@&disp_id=%@&item_id=%@&quantity=%@&type=%@&pickup_time=%@&item_price=%@&price=%@&discount=%@&recurring_type=%@&end_date=%@&drop_location_address=%@&drop_latitude=%@&drop_longitude=%@",
                                                                              [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                                                                              dispId,
                                                                              itemId,
                                                                              qnty,
                                                                              orderType,
                                                                              pickUpTime,
                                                                              itemPrice,
                                                                              price,
                                                                              discount,
                                                                              recurringType,
                                                                              endDate,
                                                                              address,
                                                                              latCord,
                                                                              longCord] dataUsingEncoding:NSUTF8StringEncoding]
                                                                       Type:@"POST"
                                                           loadingindicator:NO];
    NSLog(@"Dispensery Info: %@",json);

    if(json) {
        if([[json objectForKey:@"status"] intValue] == 0) {
           UIAlertView *alertObj = [[UIAlertView alloc]initWithTitle:@"" message:[json objectForKey:@"log"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alertObj.Tag = 909;
            [alertObj show];
        }
        else if([[json objectForKey:@"status"] intValue] == 1) {
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2) {
            
        }
    }
 }

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag == 909)
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) slideView:(UIView *)uiv_slide withDuration:(double)d_duration toX:(CGFloat)xValue andY:(CGFloat)yValue {
    
    [UIView animateWithDuration:d_duration animations:^ {
        [uiv_slide setFrame:CGRectMake(xValue, yValue, uiv_slide.frame.size.width, uiv_slide.frame.size.height)];
    }completion:^(BOOL finished) {
    }
     ];
}

@end
