//
//  DetailViewController.h
//  Storx
//
//  Created by clicklabs on 12/23/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailView.h"
#import "RequestViewController.h"

@interface DetailViewController : UIViewController <detailProtocol>
@property (strong,nonatomic) NSString *orderType;
@property (strong,nonatomic) NSString *dispensaryId;
@property (strong,nonatomic) NSString *dispensaryName;

@end
