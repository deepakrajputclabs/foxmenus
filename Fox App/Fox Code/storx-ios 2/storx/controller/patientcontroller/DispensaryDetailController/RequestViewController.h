//
//  RequestViewController.h
//  Storx
//
//  Created by clicklabs on 12/27/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestDispensaryView.h"
#import "Header.h"
#import "DispensaryView.h"

@interface RequestViewController : UIViewController <requestProtocol>
@property (strong,nonatomic) NSString *dispensaryId;
@property (strong,nonatomic) NSString *dispensaryName;

@property(strong, nonatomic) NSDictionary *dispensaryInfo;
@property(strong, nonatomic) NSMutableArray *requestIdArray;
@property(strong, nonatomic) NSMutableArray *requestImageArray;
@property(strong, nonatomic) NSMutableArray *requestPriceArray;
@property(strong, nonatomic) NSMutableArray *requestQtyTypeArray;
@property(strong, nonatomic) NSMutableArray *requestQtyValueArray;
@property(strong, nonatomic) NSMutableArray *requestNameArray;
@end
