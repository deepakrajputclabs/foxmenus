//
//  DetailViewController.m
//  Storx
//
//  Created by clicklabs on 12/23/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "DetailViewController.h"
#import "CommonClass.h"

@interface DetailViewController ()
{
    DetailView *objDetail;
    UIView *tempViewForTouch;
    DispensaryView *objSlideDispensaryView;
}
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objDetail = [[DetailView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    objDetail.detailDelegate = self;
    [self.view addSubview:objDetail];
    
    [objDetail dispensaryView:self.dispensaryId
               dispensaryName:self.dispensaryName
                    ordertype:self.orderType];
    [self getDispensaryDetail:self.dispensaryId];
}

#pragma mark - API Call

- (void)getDispensaryDetail:(NSString *)dispensaryId {
    
    NSDictionary *json = [[ServerCallModel getSharedInstance] serverGetJSON:@"get_dispensary_details"
                                                         data:[[NSString stringWithFormat:@"access_token=%@&id=%@",
                                                                [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],dispensaryId] dataUsingEncoding:NSUTF8StringEncoding]
                                                         Type:@"POST"
                                             loadingindicator:NO];
    NSLog(@"json: %@",json);
    if(json) {
        
        if([[json objectForKey:@"status"] intValue] == 0) {
            [objDetail dispensaryReturn:json];
        }
        else if([[json objectForKey:@"status"] intValue] == 1){
            [[[UIAlertView alloc] initWithTitle:@"" message:[json valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else if([[json objectForKey:@"status"] intValue] == 2){
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@""
                                                          message:[json valueForKey:@"error"]
                                                         delegate:Nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
            alert1.tag = 1092;
            [alert1 show];
        }
    }
}

#pragma mark - back Button

- (void)backMethod {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Request Button

- (void)requestMethodId:(NSMutableArray *)idMutArray
           nameMutArray:(NSMutableArray *)nameMutArray
          priceMutArray:(NSMutableArray *)priceMutArray
        qtyTypeMutArray:(NSMutableArray *)qtyTypeMutArray
       qtyValueMutArray:(NSMutableArray *)qtyValueMutArray
              imaeArray:(NSMutableArray *)imageArray
        dispensaryArray:(NSDictionary *)dispensaryArray {
    
    RequestViewController *objRequest = [[RequestViewController alloc] init];
    objRequest.requestIdArray         = idMutArray;
    objRequest.requestNameArray       = nameMutArray;
    objRequest.requestImageArray      = imageArray;
    objRequest.requestPriceArray      = priceMutArray;
    objRequest.requestQtyValueArray   = qtyValueMutArray;
    objRequest.requestQtyTypeArray    = qtyTypeMutArray;
    objRequest.dispensaryId           = self.dispensaryId;
    objRequest.dispensaryName         = self.dispensaryName;
    objRequest.dispensaryInfo         = dispensaryArray;
    [self.navigationController pushViewController:objRequest animated:YES];
}

#pragma mark - Info Button

- (void)infoMethod :(NSDictionary *)dict  {
    [objSlideDispensaryView removeFromSuperview];
    objSlideDispensaryView = nil;
    objSlideDispensaryView = [[DispensaryView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    [self.view addSubview:objSlideDispensaryView];
    [[CommonClass getSharedInstance] startFade:objSlideDispensaryView];

    [self slideView:objDetail withDuration:0.4 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:-1050/3 currentSupSize:SCREEN_WIDTH] andY:0];
    [self.view bringSubviewToFront:objDetail];
    
    [objSlideDispensaryView dispensaryInfoAPICall:dict];
    
    tempViewForTouch = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, SCREEN_HEIGHT)];  // for touch begin
    tempViewForTouch.backgroundColor = [UIColor clearColor];
    tempViewForTouch.userInteractionEnabled = YES;
    [self.view addSubview:tempViewForTouch];
}

#pragma mark - Touch

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if(touch.view == tempViewForTouch) {
        [tempViewForTouch removeFromSuperview];
        tempViewForTouch = nil;
        [self slideView:objDetail withDuration:0.4 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH] andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT]];
    }
}

#pragma mark - Remove Filter Screen

- (void)removeFilterScreen {
    
    [tempViewForTouch removeFromSuperview];
    tempViewForTouch = nil;
    objDetail.hidden = NO;
    [self slideView:objDetail withDuration:0.4 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH] andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT]];
    [objDetail removeFromSuperview];
    objDetail = nil;
}

-(void) slideView:(UIView *)uiv_slide withDuration:(double)d_duration toX:(CGFloat)xValue andY:(CGFloat)yValue {
    
    [UIView animateWithDuration:d_duration delay:0.0f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
        [uiv_slide setFrame:CGRectMake(xValue, yValue, uiv_slide.frame.size.width, uiv_slide.frame.size.height)];
    }completion:^(BOOL finished) {
    }
     ];
}

//[UIView animateWithDuration:0.5f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
//    [view setAlpha:1.f];
//} completion:^(BOOL finished) {
//    
//}];
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
