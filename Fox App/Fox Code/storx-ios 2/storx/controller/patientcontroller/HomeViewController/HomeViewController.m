//
//  HomeViewController.m
// Storx
//
//  Created by ClickLabs26 on 11/9/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "HomeViewController.h"
#import "DetailViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
{
    float latitude;
    float longitude;
    BOOL isUpdateMap;
    FilterView *filterObject;
    HomeScreenView *homeScreenViewObject;
    NSDictionary *json;
    float varingUsedLatitude,varingUsedLongitude;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    varingUsedLatitude=0.0;
    varingUsedLongitude=0.0;
    }

- (void)viewWillAppear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
//    [self removeObserver];
//    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(customerOrderTrack)
//                                                 name:@"OrderTrackNotification"
//                                               object:nil];
    

    if(![CLLocationManager locationServicesEnabled])
    {
        [self hideActivityIndicaterr];
        
        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Location Service Disable"
                                                      message:@"To enable, please go to Settings and turn on Location Service for this app."
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
        alert1.tag = 1092;
        [alert1 show];
    }
    
   // [[ScheduledLocationManager getSharedInstance] allocatLocation];
   
    if(!isUpdateMap) {
        [homeScreenViewObject clearMarker];
        [homeScreenViewObject removeFromSuperview];
        homeScreenViewObject.homeScreenViewDelegate = nil;
        homeScreenViewObject = nil;
        homeScreenViewObject = [[HomeScreenView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
        homeScreenViewObject.homeScreenViewDelegate = self;
        [self.view addSubview:homeScreenViewObject];
    }
    isUpdateMap = NO;
    [[ScheduledLocationManager getSharedInstance] setDelegate:self];
    [[ScheduledLocationManager getSharedInstance] getUserLocationWithInterval:5];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
    [self removeObserver];
}

#pragma mark - Push Notification Function

/*
- (void)customerOrderTrack {
    
    if (homeScreenViewObject) {
        [homeScreenViewObject clearMarker];
    }
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"] intValue] == 40) { // Rate order
        MedicineDeliveryViewController *objViewOrder = [[MedicineDeliveryViewController alloc]init];
        objViewOrder.orderId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OrderId"];
        objViewOrder.flagString = [[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"];
        objViewOrder.flagType = [[NSUserDefaults standardUserDefaults] valueForKey:@"Type"];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objViewOrder];
        
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                        leftMenuViewController:[[MenuViewController alloc] init]
                                                                       rightMenuViewController:nil];
        sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
        sideMenuViewController.delegate = self;
        sideMenuViewController.contentViewShadowEnabled = YES;
        [self.navigationController pushViewController:sideMenuViewController animated:YES];
    }
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"] intValue] == 41 || [[[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"] intValue] == 19) { // Start order or pickup

        MapViewController *mapController  = [[MapViewController alloc]init];
        mapController.orderId = [[NSUserDefaults standardUserDefaults] valueForKey:@"OrderId"];
        mapController.isStartOrder = YES;
        mapController.flagCheckString = [[NSUserDefaults standardUserDefaults] valueForKey:@"Flag"];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapController];
        
        RESideMenu *sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                                        leftMenuViewController:[[MenuViewController alloc] init]
                                                                       rightMenuViewController:nil];
        sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
        sideMenuViewController.delegate = self;
        sideMenuViewController.contentViewShadowEnabled = YES;
        [self.navigationController pushViewController:sideMenuViewController animated:YES];
    }
} */

#pragma mark - Alert View delegates

- (void) alertView : (UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1092) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        
        [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:AccessToken];
        [[NSUserDefaults standardUserDefaults] setValue:Nil forKey:DriverMode];
        [[NSUserDefaults standardUserDefaults] setObject:Nil forKey:UserData];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (homeScreenViewObject) {
            [homeScreenViewObject clearMarker];
        }
         [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[SplashViewController alloc] init]] animated:YES];
    }
}

#pragma mark - Schedule Manager delegates

-(void)scheduledLocationManageDidFailWithError:(NSError *)error{
    NSLog(@"HomeViewController ==> scheduledLocationManageDidFailWithError ==> error:%@",error.description);
}

- (void)scheduledLocationManageDidUpdateLocations:(CLLocation *)currentLocations{
    NSLog(@"HomeViewController ==> currentLocations: latitude :%f, longitude: %f",currentLocations.coordinate.latitude, currentLocations.coordinate.longitude);
    if (currentLocations && !isUpdateMap) {
        isUpdateMap = YES;
        homeScreenViewObject.getCurrentLocation=currentLocations;
        
        if([[NSUserDefaults standardUserDefaults]boolForKey:isCurrentLocationNow]) {
            latitude = currentLocations.coordinate.latitude;
            longitude = currentLocations.coordinate.longitude;
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%f,%f",latitude,longitude] forKey:@"userCurrentLocation"];
        }
        else {
            latitude  = [[[NSUserDefaults standardUserDefaults]objectForKey:searchedLatitude] floatValue];
            longitude = [[[NSUserDefaults standardUserDefaults]objectForKey:searchedLongitude] floatValue];
        }
      
        CLLocation *loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
        [homeScreenViewObject addingMarkerToParticularPlace:loc];

        if([[NSUserDefaults standardUserDefaults] boolForKey:ToggleForList]) {
            [self searchLocationSelected:latitude
                               longValue:longitude
                           startDistance:@""
                             endDistance:@""];
        }
        else {
        }
        
        if(currentLocations.coordinate.latitude != 0.000000)         //[[ScheduledLocationManager getSharedInstance] setDelegate:nil];
        [[ScheduledLocationManager getSharedInstance] removeTimerForLocationUpdates];
    }
}

- (void)dataReturn:(NSDictionary *)jsonReturn {
    [homeScreenViewObject dataRecieved:jsonReturn];
}

- (void)invalidAccessToken {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - API Call

- (void)searchLocationSelected:(float)latitudeValue
                     longValue:(float)longitudeValue
                 startDistance:(NSString *)startDis
                   endDistance:(NSString *)endDis {
    [homeScreenViewObject clearMarker];
    
    latitude  = latitudeValue;
    longitude = longitudeValue;
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%f",latitude] forKey:@"latToSendAPICall"];
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%f",longitude] forKey:@"longToSendAPICall"];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDictionary *params = @{@"access_token": [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken],
                             @"latitude":[NSString stringWithFormat:@"%f",latitudeValue],
                             @"longitude":[NSString stringWithFormat:@"%f",longitudeValue],
                             @"start_distance":startDis,
                             @"end_distance":endDis,
                             @"day":[self getDayNumber:[dateFormatter stringFromDate:[NSDate date]]]};
    
    [ServerCallModel getSharedInstance].serverReturnDelegate = self;
    [[ServerCallModel getSharedInstance] getDispensary:params];
    
    dateFormatter = nil;
}

- (NSString *)getDayNumber :(NSString *)stringDay {
    
    NSString *dayAsString;
    
    if([stringDay isEqualToString:@"Monday"]) {
        dayAsString = @"0";
    }
    else if([stringDay isEqualToString:@"Tuesday"]) {
        dayAsString = @"1";
    }
    else if([stringDay isEqualToString:@"Wednesday"]) {
        dayAsString = @"2";
    }
    else if([stringDay isEqualToString:@"Thursday"]) {
        dayAsString = @"3";
    }
    else if([stringDay isEqualToString:@"Friday"]) {
        dayAsString = @"4";
    }
    else if([stringDay isEqualToString:@"Saturday"]) {
        dayAsString = @"5";
    }
    else if([stringDay isEqualToString:@"Sunday"]) {
        dayAsString = @"6";
    }
    return dayAsString;
}

#pragma mark - Filter Method

- (void)filterMethod {
    
    [filterObject removeFromSuperview];
    filterObject = nil;
    filterObject = [[FilterView alloc]initWithFrame:CGRectMake([[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT], [[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:1242/3 currentSupSize:SCREEN_WIDTH], [[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:2208/3 currentSupSize:SCREEN_HEIGHT])];
    filterObject.userInteractionEnabled = YES;
    [filterObject filterView];
    filterObject.filterViewDelegate = self;
    filterObject.latString = latitude;
    filterObject.longString = longitude;
    [self.view addSubview:filterObject];
    
    [self slideView:homeScreenViewObject withDuration:0.3 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:-1050/3 currentSupSize:SCREEN_WIDTH] andY:0];
    [self.view bringSubviewToFront:homeScreenViewObject];
    
    [tem_View_ForTouch removeFromSuperview];
    tem_View_ForTouch = nil;
    tem_View_ForTouch = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, SCREEN_HEIGHT)];  // for touch begin
    tem_View_ForTouch.backgroundColor = [UIColor clearColor];
    tem_View_ForTouch.userInteractionEnabled = YES;
    [self.view addSubview:tem_View_ForTouch];
}

#pragma mark - Touch

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if(touch.view == tem_View_ForTouch) {
        [tem_View_ForTouch removeFromSuperview];
        tem_View_ForTouch = nil;
        [filterObject removeFromSuperview];
        filterObject = nil;
        [self slideView:homeScreenViewObject withDuration:0.3 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH] andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT]];
    }
    else {
        [homeScreenViewObject removeView];
    }
}

#pragma mark - Remove Filter Screen

- (void)removeFilterScreen {
    
    [tem_View_ForTouch removeFromSuperview];
    tem_View_ForTouch = nil;
    [self slideView:homeScreenViewObject withDuration:0.3 toX:[[CommonClass getSharedInstance] targetSize:TargetSize.width targetSubSize:0 currentSupSize:SCREEN_WIDTH] andY:[[CommonClass getSharedInstance] targetSize:TargetSize.height targetSubSize:0 currentSupSize:SCREEN_HEIGHT]];
    [filterObject removeFromSuperview];
    filterObject = nil;
}

-(void) slideView:(UIView *)uiv_slide withDuration:(double)d_duration toX:(CGFloat)xValue andY:(CGFloat)yValue {
    [UIView animateWithDuration:d_duration animations:^ {
        [uiv_slide setFrame:CGRectMake(xValue, yValue, uiv_slide.frame.size.width, uiv_slide.frame.size.height)];
    }completion:^(BOOL finished) {
    }
     ];
}


#pragma mark Menu Button

- (void)menuButtonAction {
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)backFromMenu {

}

#pragma mark - Dispensary Detail

- (void)detailDispensary :(NSString *)dispensaryId
           dispensaryName:(NSString *)dispensaryName
                orderType:(NSString *)orderType {
    DetailViewController *objDetail = [[DetailViewController alloc] init];
    objDetail.dispensaryId   = dispensaryId;
    objDetail.dispensaryName = dispensaryName;
    objDetail.orderType      = orderType;
    [self.navigationController pushViewController:objDetail animated:TRUE];
}

#pragma mark- loading indicator bar

- (void)showActivityIndicaterr
{
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}

- (void)hideActivityIndicaterr
{
    [SVProgressHUD dismiss];
}

- (void)dealloc {
    [self removeObserver];
}

-(void)removeObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
