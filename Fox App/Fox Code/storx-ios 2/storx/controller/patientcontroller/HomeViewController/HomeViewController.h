//
//  HomeViewController.h
// Storx
//
//  Created by ClickLabs26 on 11/9/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeScreenView.h"
#import "ScheduledLocationManager.h"
#import "LoginViewController.h"
#import "MapViewController.h"
#import "ViewOrderViewController.h"

@interface HomeViewController : UIViewController<homeScreenViewProtocol, ScheduledLocationManagerDelegate, GMSMapViewDelegate, ServerReturnProtocol, filterProtocol, RESideMenuDelegate>
{
    UIView *tem_View_ForTouch;
}
@end
