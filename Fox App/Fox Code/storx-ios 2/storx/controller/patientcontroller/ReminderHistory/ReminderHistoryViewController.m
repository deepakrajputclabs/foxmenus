//
//  ReminderHistoryViewController.m
// Storx
//
//  Created by clicklabs on 11/14/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "ReminderHistoryViewController.h"
#import "ViewOrderViewController.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface ReminderHistoryViewController ()

@end

@implementation ReminderHistoryViewController
{
    ReminderHistoryView *reminderView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewOrder :(NSDictionary *)orderResponse
        isUpcoming:(BOOL)isUpcoming
        pickUpFlag:(NSString *)pickupOrnotFlag
{
    NSLog(@"flag  :%@",pickupOrnotFlag);
 
    if(isUpcoming) {
        OrderDetailViewController *objViewOrder = [[OrderDetailViewController alloc]init];
        objViewOrder.orderId = [[[orderResponse objectForKey:@"data"]objectAtIndex:0]objectForKey:@"order_id"];
        objViewOrder.jsonForCustomerOrderDetail = orderResponse;
        [self.navigationController pushViewController:objViewOrder animated:YES];
        
    } else {
        
        if([pickupOrnotFlag intValue] == 40) {
            RatingViewController *rating = [[RatingViewController alloc]init];
            rating.orderIdConfirm = [[[orderResponse objectForKey:@"data"]objectAtIndex:0] objectForKey:@"order_id"];
            rating.userName = Nil;
            rating.userImage = Nil;
            rating.userAddress = Nil;
            rating.flagCheck = Nil;
            [self.navigationController pushViewController:rating animated:YES];
        } else if([pickupOrnotFlag intValue] == 10) { // rate not confirmed
            MedicineDeliveryViewController *objViewOrder = [[MedicineDeliveryViewController alloc]init];
            objViewOrder.orderId    = [[[orderResponse objectForKey:@"data"]objectAtIndex:0] objectForKey:@"order_id"];
            objViewOrder.flagString = [NSString stringWithFormat:@"%@",pickupOrnotFlag];
            objViewOrder.flagType   = [NSString stringWithFormat:@"%@",[orderResponse valueForKey:@"type"]];
            objViewOrder.isFromPushNotificationMenu = NO;
            [self.navigationController pushViewController:objViewOrder animated:YES];
        } else {
            MapViewController *object = [[MapViewController alloc] init];
            object.orderId = [[[orderResponse objectForKey:@"data"]objectAtIndex:0]objectForKey:@"order_id"];
            object.isStartOrder = YES;
            object.isMenuButtonShow = YES;
            object.flagCheckString = [NSString stringWithFormat:@"%@",pickupOrnotFlag];
            [self.navigationController pushViewController:object animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    if(reminderView==nil)
    {
        reminderView=[[ReminderHistoryView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        reminderView.reminderProtocol=self;
        reminderView.viewOrderFromHistoryDelegate=self;
        [self.view addSubview:reminderView];
    }
    
    [self performSelector:@selector(reminderSercerCall) withObject:nil afterDelay:0.2];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)reminderSercerCall
{
    NSString *userAccestoken=[[NSUserDefaults standardUserDefaults]objectForKey:AccessToken];
    NSDictionary* json = [[ServerCallModel getSharedInstance] serverGetJSON:[NSString stringWithFormat:@"reminders_and_history"]
                                                         data:[[NSString stringWithFormat:@"access_token=%@",userAccestoken] dataUsingEncoding:NSUTF8StringEncoding]
                                                         Type:@"POST"
                                             loadingindicator:YES];
    NSLog(@"featured deals details: %@",json);
    if(json) {
        [reminderView reminderHistoryServerCall:json];
    }
}

- (void)menuButton {
    if([[[NSUserDefaults standardUserDefaults] valueForKey:DriverMode] intValue] == 2) { // Customer
        [[NSNotificationCenter defaultCenter] removeObserver:@"OrderTrackNotification"];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)backButtonAction {
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
