//
//  ReminderHistoryViewController.h
// Storx
//
//  Created by clicklabs on 11/14/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReminderHistoryView.h"

@interface ReminderHistoryViewController : UIViewController<reminderMenuDelegate,ViewOrderFromHistoryProtocol>

@end
