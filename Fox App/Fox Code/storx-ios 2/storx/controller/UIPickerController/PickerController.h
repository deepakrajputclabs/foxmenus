//
//  PickerController.h
//  Kehoots
//
//  Created by clicklabs on 7/31/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIPopoverController *popover;
    UIImagePickerController *picker;
}

@property (strong,nonatomic) NSString *pickerType;

@end
