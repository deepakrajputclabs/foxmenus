//
//  PickerController.m
//  Kehoots
//
//  Created by clicklabs on 7/31/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#import "PickerController.h"
#import "Header.h"

@interface PickerController ()

@end

@implementation PickerController
@synthesize pickerType = _pickerType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    double delayInSeconds = 0.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){

        if([_pickerType isEqualToString:@"PhotoLibray"])
        {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                if([UIImagePickerController isSourceTypeAvailable:
                    UIImagePickerControllerSourceTypePhotoLibrary]){
                    picker= [[UIImagePickerController alloc]init];
                    picker.delegate = self;
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                    [popover presentPopoverFromRect:CGRectMake(400.0, -140.0, 400.0, 400.0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
            }
            else{
                if([UIImagePickerController isSourceTypeAvailable:
                    UIImagePickerControllerSourceTypePhotoLibrary]){
                    picker= [[UIImagePickerController alloc]init];
                    picker.delegate = self;
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    [self presentViewController:picker animated:NO completion:nil];
                }
            }
        }
        else
        {
            if([UIImagePickerController isSourceTypeAvailable:
                UIImagePickerControllerSourceTypeCamera]){
                picker= [[UIImagePickerController alloc]init];
                picker.delegate = self;
                picker.editing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.showsCameraControls = YES;
                [self presentViewController:picker animated:YES completion:nil];
            }
        }
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void) imagePickerController:(UIImagePickerController *)aPicker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    NSDictionary *userInfo;
    
    if ([mediaType isEqualToString:@"public.image"]){
        UIImage *aImage= (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        userInfo = [NSDictionary dictionaryWithObject:aImage forKey:@"someKey"];
        
        // NSNotifcation center
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if ([popover isPopoverVisible])
            [popover dismissPopoverAnimated:YES];
        else
            [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfilePicChange" object:nil userInfo:userInfo];

        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)apicker
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if ([popover isPopoverVisible])
            [popover dismissPopoverAnimated:YES];
        else
            [self dismissViewControllerAnimated:NO completion:nil];
    }
    else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        double delayInSeconds = 0.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self.navigationController popViewControllerAnimated:NO];
        });
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pc {
    
    [popover dismissPopoverAnimated:YES];
    popover.delegate = nil;
    popover = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
