//
//  Header.h
// Storx
//
//  Created by clicklabs on 11/3/14.
//  Copyright (c) 2014 clicklabs. All rights reserved.
//

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#ifndef Smokeio_Header_h
//#define NSLog(s,...)
#define TargetSize CGSizeMake(414, 736)
#define Smokeio_Header_h
#import "ServerCallModel.h"
#import "CommonClass.h"
#import "MenuViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "Reachability.h"
#import "AFHTTPRequestOperationManager.h"
#import "SVProgressHUD.h"
#import <UIKit/UIKit.h>
#import "RESideMenu.h"

#ifdef __IPHONE_8_0
    #define GregorianCalendar NSCalendarIdentifierGregorian
#else
    #define GregorianCalendar NSGregorianCalendar
#endif

#define ToggleForList @"istoggleForList"

//Error Message
#define ErrorInternetConnection         @"You may not have internet connection. Please check your connection and try again."
#define ErrorServerConnection           @"Could not connect to the server. Please try again later."

#define TestServer   @"http://54.67.92.77:7000/"
#define ClientServer @"http://app.storx.clicklabs.in:7001/"

#define ServerUrl                       TestServer
#define SMAppstore_Url                  @"https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa"
#define SMGoogle_APIKey                 @"AIzaSyDgAyANbGYZJqWglVK4mHNEmWvLT7Zzj1A"
#define SMGoogle_Browser_Search_Key     @"AIzaSyBd5h4DSBPFwcD46ICt7OwGRfrx7aMPJ_Y"

// Constant
#define AppVersion                      @"100"
#define OSVersion                       @"100"
#define DeviceToken                     @"deviceToken"
#define DeviceName                      @"deviceName"
#define isOrderStatusCompleted          @"orderStatus"
#define communicationPreferencesType    @"CommunicationType"
#define isOrderStart                    @"orderStatus"
#define isCurrentLocationNow            @"whetherCurrentLocationOrnot"
#define searchedLatitude                @"searchLocationLatitude"
#define searchedLongitude               @"searchLocationLongitude"
#define PickUpOrderType                 @"PickUpOrderType"
#define StateFlagDriverAfterStartOrder  @"stateFlagDriverAfterStartOrder"
#define DispensaryDefaultImage @"dispensary_placeholder.png"
#define UserDefaultImage @"user_placeholder.png"

//Font
#define FontExtraBold       @"Raleway-ExtraBold"
#define FontBold            @"Raleway-Bold"
#define FontThin            @"Raleway-Thin"
#define FontLight           @"Raleway-Light"
#define FontSemiBold        @"Raleway-SemiBold"
#define FontHeavy           @"Raleway-Heavy"
#define FontMedium          @"Raleway-Medium"
#define FontExtraLight      @"Raleway-ExtraLight"
#define FontRegular         @"Raleway"

// Color
#define ColorBlackBK        [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.3]
#define ColorDarkBlackBK    [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.5]
#define ColorGreenButton    [UIColor colorWithRed:0/255.0f green:190/255.0f blue:109/255.0f alpha:0.6]
#define ColorGrayBk         [UIColor colorWithRed:236.0/255.0f green:236.0/255.0f blue:236.0/255.0f alpha:1.0f]
#define ColorGrayOp50       [UIColor colorWithRed:236.0/255.0f green:236.0/255.0f blue:236.0/255.0f alpha:0.5f]
#define ColorFFFFFF         [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]
#define SMBgColor           [UIColor colorWithRed:244/255.0f green:244/255.0f blue:244/255.0f alpha:1.0f]

#define AccessToken         @"access_token"
#define UserName            @"user_name"
#define UserImage           @"user_image"
#define UserEmail           @"email"
#define UserRating          @"rating"
#define DriverMode          @"current_user_status"
#define UserDispensaryName  @"dispensery_name"
#define UserPhone           @"phone_no"
#define UserPatientId       @"patient_id"
#define UserPhysicianName   @"physician_name"
#define UserData            @"user_data"
#endif
